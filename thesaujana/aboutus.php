<?php
session_start();
include("includes/db.php");
include("functions/functions.php");

?>
<!DOCTYPE html>
<html lang="en">

<title>The Saujana Store</title>

<head>

    <!-- Basic page needs
    ============================================ -->
    <!-- <title>The Saujana Store</title>
    <meta charset="utf-8">
    <meta name="keywords"
        content="The Saujana Sdn Bhd" />
    <meta name="description"
        content="The Saujana Sdn Bhd" />
    <meta name="author" content="The Saujana">
    <meta name="robots" content="index, follow" /> -->
    <title>The Saujana</title>
    <!-- Mobile specific metas
    ============================================ -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Favicon
    ============================================ -->

    <link rel="shortcut icon" type="image/png" href="ico/favicon-16x16.png" />


    <!-- Libs CSS
    ============================================ -->
    <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="js/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="js/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="css/themecss/lib.css" rel="stylesheet">
    <link href="js/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <link href="js/minicolors/miniColors.css" rel="stylesheet">

   
    <!-- Theme CSS
    ============================================ -->
    <link href="css/themecss/so_searchpro.css" rel="stylesheet">
    <link href="css/themecss/so_megamenu.css" rel="stylesheet">
    <link href="css/themecss/so-categories.css" rel="stylesheet">
    <link href="css/themecss/so-listing-tabs.css" rel="stylesheet">
    <link href="css/themecss/so-newletter-popup.css" rel="stylesheet">

    <link href="css/footer/footer1.css" rel="stylesheet">
    <link href="css/header/header1.css" rel="stylesheet">
    <link id="color_scheme" href="css/theme.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">




    <!-- Google web fonts
    ============================================ -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700' rel='stylesheet'
        type='text/css'>
    <style>	
      body {	
        font-family: 'Source Sans Pro', sans-serif;	
      }	
    </style>

</head>

<body class="res layout-1">

    <div id="wrapper" class="wrapper-fluid banners-effect-5">


        <!-- Header Container  -->
          <header id="header" class=" typeheader-1">
            <!-- Header Top -->
            <div class="header-top hidden-compact">
                <div class="container">
                    <div class="row">
                        <div class="header-top-left col-lg-7 col-md-8 col-sm-6 col-xs-4">
                            
                            <ul class="top-link list-inline hidden-lg hidden-md">
                                <li class="account" id="my_account">
                                    <?php
                                    if(!isset($_SESSION['customer_email'])){
                                       ?>
                                        <ul class="dropdown-menu ">
                                                <li><a href="register.php"><i class="fa fa-user"></i> Register</a></li>
                                                <li><a href="checkout.php"><i class="fa fa-pencil-square-o"></i> Login</a></li>
                                            </ul>
                                       <?php
                                    }else{
                                       ?>
                                            <?php
                                            
                                            $customer_email = $_SESSION['customer_email'];
                                    
                                            $get_customer_details = "Select * from customers where customer_email='$customer_email'";
                                            $run_customer_details = mysqli_query($con, $get_customer_details);
                                            $fetch_customer_details = mysqli_fetch_array($run_customer_details);
                                                                                                
                                            $customer_id = $fetch_customer_details['customer_id']; //:: Customer id 
                                            $customer_name = $fetch_customer_details['customer_name']; //:: customer name

                                            ?>
                                             <?php echo" 
                                               $customer_name"?>
                                             <a href="" title="My Account " class="btn-xs dropdown-toggle"
                                            data-toggle="dropdown"> <span class="hidden-xs">My Account </span> <span
                                            class="fa fa-caret-down"></span>
                                            </a>
                                            <ul class="dropdown-menu ">
                                                <li><a href="customer_myaccount.php"><i class="fa fa-user"></i> <?php echo" 
                                                $_SESSION[customer_email]"?></a></li>
                                                <li><a href="logout.php"><i class="fa fa-pencil-square-o"></i> Logout</a></li>
                                                
                                            </ul>

                                       <?php
                                    }

                                    ?>
                                    
                                    <!-- <ul class="dropdown-menu ">
                                        <li><a href="register.html"><i class="fa fa-user"></i> Register</a></li>
                                        <li><a href="login.html"><i class="fa fa-pencil-square-o"></i> Login</a></li>
                                    </ul> -->
                                </li>
                            </ul>
                        </div>
                        <div class="header-top-right collapsed-block col-lg-5 col-md-4 col-sm-6 col-xs-8">
                            <ul class="top-link list-inline lang-curr">
                                <li class="language">
                                    <div class="btn-group languages-block ">
                                        <form action="http://demo.smartaddons.com/templates/html/emarket/index.php"
                                            method="post" enctype="multipart/form-data" id="bt-language">
                                            <a class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                                <span class="">English</span>
                                                <span class="fa fa-angle-down"></span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li><a href="index.php"> English </a></li>
                                                <li> <a href="index.php">  Bhasa Malayu </a> </li>
                                            </ul>
                                        </form>
                                    </div>

                                </li>
                            </ul>



                        </div>
                    </div>
                </div>
            </div>
            <!-- //Header Top -->

            <!-- Header center -->
            <div class="header-middle">
                <div class="container">
                    <div class="row">
                        <!-- Logo -->
                        <div class="navbar-logo col-lg-2 col-md-2 col-sm-12 col-xs-12">
                            <div class="logo"><a href="index.php"><img src="image/catalog/1550635265_5c6cd101de30c-thumb.png" title="Your Store"
                                        /></a></div>
                        </div>
                        <!-- //end Logo -->

                        <!-- Main menu -->
                        <div class="main-menu col-lg-6 col-md-7 ">
                            <div class="responsive so-megamenu megamenu-style-dev">
                                <nav class="navbar-default">
                                    <div class=" container-megamenu  horizontal open ">
                                        <div class="navbar-header">
                                            <button type="button" id="show-megamenu" data-toggle="collapse"
                                                class="navbar-toggle">
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                        </div>

                                        <div class="megamenu-wrapper">
                                            <span id="remove-megamenu" class="fa fa-times"></span>
                                            <div class="megamenu-pattern">
                                                <div class="container-mega">
                                                <ul class="megamenu" 
                                                        data-animationtime="250">
                                                        <li class="home hover">
                                                            <a href="index.php">HOME </a>
                                                        </li>
                                                        <li class="home hover">
                                                        <a href="hotels.php">STORES </a>
                                                        </li>
                                                        <li class="home hover">
                                                            <a href="displaycategory.php">CATEGORIES </a>
                                                        </li>
                                                        <li class="home hover">
                                                            <a href="https://www.saujanahotels.com/about">ABOUT US </a>
                                                        </li>
                                                        <li class="home hover">
                                                            <a href="https://www.saujanahotels.com/contact">CONTACT US </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </nav>
                            </div>
                        </div>
                        <!-- //end Main menu -->

                        <div class="middle-right col-lg-4 col-md-3 col-sm-6 col-xs-8">
                            <div class="signin-w hidden-sm hidden-xs">
                                <ul class="signin-link blank">
                                <?php 
                                if(!isset($_SESSION['customer_email'])){
                                    echo"";
                                }else{
                                    echo" <li class='log login'> 
                                    <a class='link-lg' href='customer_myaccount.php'>$_SESSION[customer_email]</a>
                                 </li>";
                                }
                                ?>
                                </ul>
                            </div>



                           

                            <div class="signin-w hidden-sm hidden-xs">
                                <ul class="signin-link blank">
                                <?php
                            if(!isset($_SESSION['customer_email'])){
                                echo" <li class='log login'></i> 
                                <a class='link-lg' href='checkout.php'>Login</a>
                             </li>";
                             echo" <li class='log login'></i> 
                             <a class='link-lg' href='checkout.php'>|</a>
                          </li>";
                             echo" <li class='log login'></i> 
                             <a class='link-lg' href='register.php'>Create an Account</a>
                          </li>";
                            }else{
                                echo"<li class='log login'></i> 
                                <a class='link-lg' href='logout.php'>Logout</a>
                             </li>";
                            }
                            ?>
                                </ul>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
            <!-- //Header center -->

            <!-- Header Bottom -->
            <div class="header-bottom hidden-compact">
                <div class="container">
                    <div class="row">

                         <!-- Category -->
                           <?php
                           include("features/allcategories.php");
                           ?>
                        <!-- //end Category -->

                        <!-- Search -->
                           <?php
                           include("features/search.php");
                           ?>
                        <!-- //end Search -->

                       

                    </div>
                </div>

            </div>
          </header>
        <!-- //Header Container  -->

        <!-- Main Container  -->

        <div class="main-container container">
            <ul class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-home"></i></a></li>
                <li><a href="https://www.saujanahotels.com/about">ABOUT US </a></li>
            </ul>

            <div class="row">
                <div id="content" class="col-sm-12">
                    <div class="about-us about-demo-4">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 about-us-content">
                                <div class="content-about">
                                    <h2 class="about-title">About Us</h2> <img src="image/demo/about/The Saujanamall.png"
                                        alt="About Us">
                                    
                                </div>
                            </div>

                            <div class='col-lg-6 col-md-6 faq-about-us'>
                                <h2 class='about-title'>Questions</h2>

                            <?php

                            $query = "Select * from about_questions";
                            $run_query = mysqli_query($con, $query);

                            while($fetch_query = mysqli_fetch_array($run_query)){

                                $answer = $fetch_query['about_answer'];
                                $question = $fetch_query['about_question'];
                                $id = $fetch_query['about_id'];

                                echo " 
                                <div class='content-faq'>
                                    <div id='accordion' role='tablist' aria-multiselectable='true'>
                                        <div class='panel'>
                                            <div class='panel-head' role='tab' id='headingOne'>
                                                <h4 class='panel-title'> <a role='button' data-toggle='collapse'
                                                        data-parent='#accordion' href='#collapse$id'
                                                        aria-expanded='false' aria-controls='collapse$id'
                                                        class='collapsed'> <span> $question </span> </a>
                                                </h4>
                                            </div>
                                            <div id='collapse$id' class='panel-collapse collapse' role='tabpanel'
                                                aria-labelledby='headingOne' aria-expanded='false' style='height: 0px;'>
                                                <div class='panel-body'> $answer</div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            ";

                            }

                            ?>
                           </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    
        
        <!-- //Main Container -->

        

       

        <!-- Footer Container -->
        <footer class="footer-container typefooter-1">

            <hr>

            <div class="footer-middle ">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-style">
                            <div class="infos-footer">
                                <h3 class="modtitle">Contact Us</h3>
                                <ul class="menu">
                                    <li class="adres">
                                        The Saujana Hotel Kuala Lumpur,Jalan Lapangan Terbang SAAS,40150 Shah Alam, Selangor Darul Ehsan, Malaysia
                                    </li>
                                    <li class="phone">
                                        +603 7843 1234 / +603 7652 1234    
                                    </li>
                                    <li class="mail">
                                        info@thesaujana.com
                                    </li>
                                    <li class="time">
                                        Open time: 7:00AM - 5:30PM
                                    </li>
                                </ul>
                            </div>


                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                            <div class="box-information box-footer">
                                <div class="module clearfix">
                                    <h3 class="modtitle">Information</h3>
                                    <div class="modcontent">
                                        <ul class="menu">
                                            <li><a href="https://www.saujanahotels.com/about">ABOUT US </a></li>
                                            <li><a href="#">FAQ</a></li>
                                            <li><a href="#">Warranty And Services</a></li>
                                            <li><a href="#">Support 24/7 page</a></li>
                                            <li><a href="#">Product Registration</a></li>
                                            <li><a href="#">Product Support</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                            <div class="box-account box-footer">
                                <div class="module clearfix">
                                    <h3 class="modtitle">Extras</h3>
                                    <div class="modcontent">
                                        <ul class="menu">
                                            <li> <a href="https://www.saujanahotels.com/about"> The Saujana </a></li>
                                            <li><a href="displaycategory.php">CATEGORIES</a></li>
                                            <li><a href="https://www.saujanahotels.com/contact">Contact Us</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                            <div class="box-service box-footer">
                                <div class="module clearfix">
                                    <h3 class="modtitle">Services</h3>
                                    <div class="modcontent">
                                        <ul class="menu">
                                            <li><a href="#">Kuala Lumpur</a></li>
                                            <li><a href="#">Selangor</a></li>
                                            <li><a href="#">KL Central</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-style">


                        </div>

                        <div class="col-lg-12 col-xs-12 text-center">
                            <img src="image/catalog/demo/payment/payment.png" alt="imgpayment">
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer Bottom Container -->
            <div class="footer-bottom ">
                <div class="container">
                    <div class="copyright">
                        The Saujana © COPYRIGHT 2020 SAUJANA HOTEL SDN BHD (123333-T)
                    </div>
                </div>
            </div>
            <!-- /Footer Bottom Container -->


            <!--Back To Top-->
            <div class="back-to-top"><i class="fa fa-angle-up"></i></div>
        </footer>
        <!-- //end Footer Container -->

    </div>


  


    <!-- Include Libs & Plugins
	============================================ -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="js/jquery-2.2.4.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/owl-carousel/owl.carousel.js"></script>
    <script type="text/javascript" src="js/themejs/libs.js"></script>
    <script type="text/javascript" src="js/unveil/jquery.unveil.js"></script>
    <script type="text/javascript" src="js/countdown/jquery.countdown.min.js"></script>
    <script type="text/javascript" src="js/dcjqaccordion/jquery.dcjqaccordion.2.8.min.js"></script>
    <script type="text/javascript" src="js/datetimepicker/moment.js"></script>
    <script type="text/javascript" src="js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>


    <!-- Theme files
	============================================ -->


    <script type="text/javascript" src="js/themejs/so_megamenu.js"></script>
    <script type="text/javascript" src="js/themejs/addtocart.js"></script>
    <script type="text/javascript" src="js/themejs/application.js"></script>
    <script type="text/javascript">
     
        // Check if Cookie exists
        if ($.cookie('display')) {
            view = $.cookie('display');
        } else {
            view = 'grid';
        }
        if (view) display(view);
        
    </script>
</body>
</html>







