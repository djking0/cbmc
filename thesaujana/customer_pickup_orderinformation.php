<?php
include ("includes/db.php");


$c = $_SESSION ['customer_email'];

$get_c = "select * from customers where customer_email = '$c'";

$run_c = mysqli_query($con , $get_c);

$row_c = mysqli_fetch_array($run_c);

$customer_id = $row_c['customer_id'];

//: Getting order information data

$order_id = $_GET['order_pickup_information'];
?>

 <!--Middle Part Start-->
 <div id="content" class="col-sm-9">
                    <h2 class="title">Order information</h2>
                    
                    <?php
                     //: Getting order date from customer_pickuporder table..
                     $get_order_data = "Select * from customer_pickuporder where porder_id='$order_id'";
                     $run_order_data = mysqli_query($con, $get_order_data);

                     while($row_order_data=mysqli_fetch_array($run_order_data)){

                         $order_no=$row_order_data['customer_order_no'];
                         $order_hotelid = $row_order_data['hotel_id'];
                         $order_date = $row_order_data['order_date'];
                         $order_invoiceno = $row_order_data['invoiceno'];
                         $order_description = $row_order_data['order_description'];
                         $order_dueamount = $row_order_data['due_amount'];
                         $order_pickupdate = $row_order_data['pickup_order_date'];
                         $order_pickuptime = $row_order_data['pickup_order_time'];
                         $order_status = $row_order_data['order_status'];
                         $order_payment = $row_order_data['payment_detail'];

                         //: Getting company name from food_company table
                         $get_company_details = "Select * from food_company where fcompany_id='$order_hotelid'";
                         $run_company_details = mysqli_query($con, $get_company_details);
                         $fetch_company_details = mysqli_fetch_array($run_company_details);

                         $companyname = $fetch_company_details['Company_name'];
                         $companylocation = $fetch_company_details['company_address'];

                         // //: Getting GST...
                         // $gst_rate = 6;
                         // $gst = ($gst_rate / 100) * $order_dueamount;

                         //  //: Grand Total without GST..
                         //  $grand_total = ($order_dueamount +$gst);


                         

                         echo "<table class='table table-bordered table-hover'>
                         <thead>
                             <tr>
                                 <td colspan='2' class='text-left'>Order # $order_no <br> Due Amount: RM$order_dueamount</td>
                             </tr>
                         </thead>
                         <tbody>
                             
                             <tr>
                                 <td style='width: 50%;' class='text-left'> 
                                     <b>Order ID:</b> #$order_invoiceno
                                     <br>
                                     <b>Date Added:</b>  $order_date
                                     <br>
                                     <b>Status:</b>  $order_status
                                     <br>
                                     <br>

                                     <h4> <strong>Pickup Details:</strong> </h4>
                                     <b>Name:</b> $companyname
                                     <br>
                                     <b>location:</b>  $companylocation
                                     <br>
                                     <b>Date & Time:</b>  $order_pickupdate $order_pickuptime
                                     <br><br>

                                     <h4> <strong>Order Description:</strong> </h4>
                                     $order_description
                                     <br>
                                     
                                     <h4> <strong> Payment Details:</strong> </h4>
                                     <b>Method:</b>  $order_payment
                                     <br><br>
                                     

                                     <h4> <strong>Total Amount with 6% GST</strong> </h4>
                                     <b>Total:</b> $order_dueamount RM 
                                     <br>
                                     
                                     


                                 
                                 
                                 </td>
                                 
                             </tr>
                         </tbody>
                     </table>";



                     } ?>

                    
                </div>
                <!--Middle Part End-->
