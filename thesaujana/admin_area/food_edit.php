<?php
include('security.php');
include('includes/header.php'); 
include('includes/navbar.php'); 
?>


<div class="container-fluid">

<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"> EDIT Store Item Data </h6>
  </div>
  <div class="card-body">
<?php

    

    if(isset($_POST['item_edit_btn'])){
        $id = $_POST['item_edit_id'];
        
        require 'dbconfig.php'; 

        $query = "SELECT * FROM food_items WHERE item_id='$id'";
        $query_run = mysqli_query($connection, $query);

        foreach($query_run as $row)
        {
            ?>

            <form action="code.php" method="POST" enctype="multipart/form-data">

            <input type="hidden" name="item_updateid" value="<?php echo $row['item_id'] ?>" >

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label> Item Title: </label>
                    <input type="text" name="item_updatetitle" value="<?php echo $row['item_title'] ?>" class="form-control" placeholder="Enter Product title" required="required">

            </div>
            <div class="form-group col-md-6">
                    <label> item Category (       
                                <?php $foodcategory_id = $row['fcat_id'];

                                $get_cat_name = "select * from food_category where fcat_id = '$foodcategory_id'";
                                $run_cat_name = mysqli_query($connection, $get_cat_name);
                                $fetch_cat_name = mysqli_fetch_array($run_cat_name);
                                $cat_name = $fetch_cat_name['food_cat'];
                                echo "$cat_name";
                                
                                ?>
                          )</label>
                    
                    <select name="item_updatecat" class="form-control" required="required" placeholder="Enter Food title" >
                                <option>
                              
                                </option>
                                <?php
                                    require 'dbconfig.php';
                                    $get_cats = "select * from food_category where cat_type = 'food'";
                                    $run_cats = mysqli_query($connection, $get_cats);
                                    while($row_cats = mysqli_fetch_array($run_cats)){
                                        $cat_id = $row_cats['fcat_id'];
                                        $cat_title = $row_cats['food_cat'];
                                        echo "<option value='$cat_id'>$cat_title</option>";
                                    } 
                                ?>
                            </select>
                </div>
            </div>

            

            <div class="form-row">
                <div class="form-group col-md-6">
                            <label>Food Item Quantity:</label>
                            <input type="number" name="item_updatequantity" class="form-control" placeholder="Enter item quantity" required="required" min="0">
                        </div>

                <div class="form-group col-md-6">
                    <label> item Company (       
                                <?php $foodcompany_id = $row['fcompany_id'];

                                $get_company_name = "select * from food_company where fcompany_id = '$foodcompany_id'";
                                $run_company_name = mysqli_query($connection, $get_company_name);
                                $fetch_company_name = mysqli_fetch_array($run_company_name);
                                $company_name = $fetch_company_name['Company_name'];
                                echo "$company_name";
                                
                                ?>
                          )</label>
                    
                    <select name="item_updatecompany" class="form-control" required="required" placeholder="Enter Food title" >
                                <option>
                              
                                </option>
                                <?php
                                    require 'dbconfig.php';
                                    $get_company = "select * from food_company where company_type = 'food'";
                                    $run_company = mysqli_query($connection, $get_company);
                                    while($row_company = mysqli_fetch_array($run_company)){
                                        $company_id = $row_company['fcompany_id'];
                                        $company_title = $row_company['Company_name'];
                                        echo "<option value='$company_id'>$company_title</option>";
                                    } 
                                ?>
                            </select>
                </div>

                
            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                    <label>Price</label>
                    <input type="text" name="item_updateprice" value="<?php echo $row['item_price'] ?>" class="form-control" required="required" />
                </div>

                <!-- <div class="form-group col-md-6"> -->
                    <!-- <label>Discount:</label> -->
                    <!-- <select name="item_discounted_percentage" class="form-control" required="required" >
                                <option>
                                </option>
                                <option>10</option>
                                <option>20</option>
                                <option>40</option>
                                <option>60</option>
                                <option>NO</option>
                            </select> -->
                
                <!-- </div> -->
            </div>

                
            <div class="form-row">
                <div class="form-group col-md-12">
                <label for="inputZip">item Image</label>
                <input type="file" class="form-control" name="item_updateimg">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                <label for="inputZip">Food Youtube Link</label>
                <input type="text"  class="form-control" value="<?php echo $row['youtube_link'] ?>" name="item_updateyoutubelink" size="50"/>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                <label for="inputZip">item-Description</label>
                <textarea name="item_updatedesc" class="form-control" cols="20" rows="5"><?php echo $row['item_desc']?></textarea>
                </div>
            </div>
                
	

            <div style=" border-top: 0 none;" class="modal-footer"> 
                <button type="button" class="btn btn-secondary" href="food_item.php" data-dismiss="modal">Close</button>
                <button type="submit"  class="btn btn-primary"  name="updateitemitem">Save</button>
            </div>
            </form> 
			

        <?php
		
        }

      
    }
?>
  </div>
  </div>
</div>

</div>

<!-- /.container-fluid -->


<?php
include('includes/scripts.php');
include('includes/footer.php');
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>













