<?php
include('security.php');
include('includes/header.php'); 
include('includes/navbar.php'); 
?>


<div class="container-fluid">

<?php

if(isset($_POST['subproductcat_btn'])){
        $id = $_POST['subproductcat_id'];
        
       


?>

<!-- DataTales Example -->
    <div class="card shadow mb-4">
    <div class="card-header py-3">

        <?php

            require 'dbconfig.php'; 

            $query = "Select * from food_category where fcat_id = '$id'";
            $run_query = mysqli_query($connection, $query);
            $fetch_query = mysqli_fetch_array($run_query);

            $category_name = $fetch_query['food_cat'];
        ?>

        <h3 class="m-0 font-weight-bold text-primary"><?php echo "$category_name" ?>
        </h3>
    </div>

    <div class="card-body">

        <?php
        if(isset($_SESSION['success']) && $_SESSION['success']!=''){
            echo '<h2> '.$_SESSION['success'].' </h2>.';
            unset($_SESSION['success']);
        } 
        if(isset($_SESSION['status']) && $_SESSION['status']!=''){
            echo '<h2> '.$_SESSION['status'].' </h2>.';
            unset($_SESSION['status']);
        } 
        
        ?>

        <div class="table-responsive">

        <?php

        
        require 'dbconfig.php';

        $query = "SELECT * FROM subcategory_stageone where sc_main_id = '$id'";
        $query_run = mysqli_query($connection, $query);

        ?>

        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th> ID </th>
                <th> Name </th>
                <th>EDIT </th>
                <th>DELETE </th>
            </tr>
            </thead>
            <tbody>

            <?php 
                if(mysqli_num_rows($query_run) > 0)        
                {
                    while($row = mysqli_fetch_assoc($query_run))
                    {

                    ?>

                    <tr>
                        <td> <?php  echo $row['sc_id']; ?></td>
                        <td> <?php  echo $row['sc_name']; ?></td>
                        
                        <td>
                            <form action="product_subcategory_edit.php" method="post">
                                <input type="hidden" name="edit_subproductcat_id" value="<?php  echo $row['sc_id']; ?>">
                                <button  type="submit" name="edit_subproductcat_btn" class="btn btn-success"> EDIT</button>
                            </form>
                    </td>
                    <td>
                        <form action="code.php" method="post">
                            <input type="hidden" name="delete_subproductcat_id" value="<?php  echo $row['sc_id']; ?>">
                            <button type="submit" name="delete_subproductcat_btn" class="btn btn-danger"> DELETE</button>
                        </form>
                    </td>
                    </tr>
            <?php
                    }
                }else {
                    echo "No Record Found";
                }
            ?>
        
            
            
            </tbody>
        </table>

        </div>
    </div>
    </div>

<?php

}

?>

</div>
<!-- /.container-fluid -->

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>