<?php
session_start();
include("includes/db.php");
include("functions/functions.php");

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>The Saujana Store</title>

    <link rel="shortcut icon" href="image/favicon.ico" type="image/x-icon" />

      <!-- Mobile specific metas
    ============================================ -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Bootstrap CDN -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
      crossorigin="anonymous"
    />

    <!--  Font Awesome CDN -->
    <script src="https://kit.fontawesome.com/23412c6a8d.js"></script>


    <!-- EXTRA -->
    <!-- <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css"> -->
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="js/owl-carousel/owl.carousel.css" rel="stylesheet">

     <!-- Theme CSS
    ============================================ -->
    <link href="css/footer/footer1.css" rel="stylesheet">
    <link id="color_scheme" href="./css/theme.css" rel="stylesheet">

    

     <!-- EXTRA -->

     

    <!-- Slick Slider -->
    <link
      rel="stylesheet"
      type="text/css"
      href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"
    />

    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="./css/style.css" />
    <!-- <link rel="stylesheet" href="./css/viewproduct.css" /> -->


   

    
  </head>
    <!-- Google Fonts -->
	    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@600&display=swap" rel="stylesheet">
	    <style>
	      body {
	        font-family: 'Source Sans Pro', sans-serif;
	      }
	    </style>
  <body>
    <!-- header -->

    <header>
      <div class="container">

        <div class="row">
          <div class="col-md-4 col-sm-12 col-12">
            <div class="btn-group">
              <button
                class="btn border dropdown-toggle my-md-4 my-2"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                ENG
              </button>
              <!-- <div class="dropdown-menu">
                <a href="#" class="dropdown-item">Bhasa - Malaysia</a>
              </div> -->
            </div>
          </div>
          <div class="col-md-4 col-12 text-center">
            <!--<h2 class="my-md-3 site-title text-white">Gogo Empire </h2>-->
		           <div class="logo"><a href=""><img src="image/catalog/The Saujanamall.png" height="60" width= "300" title="Your Store" style="margin-top:10px" 
	                                              alt="Your Store" /></div>
          </div>
          <div class="col-md-4 col-12 text-right">
            <p class="my-md-4 header-links">
              <?php
                if(!isset($_SESSION['customer_email'])){
                echo"";
                }else{
                  
                  echo"<a href='customer_myaccount.php' class='px-1'> $_SESSION[customer_email] </a>";
                  echo"<a class='px-2' style='color:black;'>|</a>";
                }
              ?>
              <?php
                if(!isset($_SESSION['customer_email'])){
                echo"<a href='checkout.php' class='px-2'>login</a>";
                echo"<a class='px-2' style='color:black;'>|</a>";
                echo"<a href='register.php' class='px-2'>Create an Account</a>";
                }else{
                  echo"<a href='logout.php' class='px-2'>logout</a>";
                }
              ?>
            </p>
          </div>
        </div>
      </div>

      <div class="container-fluid p-0 bg-primary-color">
        <nav class="navbar navbar-expand-lg navbar-light">
          <button
            class="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item active">
                <a href="index.php" class="nav-link" >HOME <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a href="displaycategory.php" class="nav-link" > CATEGORIES</a>
              </li>
              <li class="nav-item">
                <a href="hotels.php" class="nav-link" > STORES</a>
              </li>
              <li class="nav-item">
                <a href="influencers.php" class="nav-link" > INFLUENCERS</a>
              </li>
              <li class="nav-item">
                <a href="https://www.saujanahotels.com/about" class="nav-link" > ABOUT US</a>
              </li>
              <li class="nav-item">
              <a href="https://www.saujanahotels.com/contact" class="nav-link" >CONTACT US</a>
              </li>
            </ul>
          </div>
          <div class="navbar-nav">
            <form class="form-inline my-2 my-lg-0" method="get" action="search.php" enctype="multipart/form-data">
                  <input class="form-control mr-sm-2" type="search" placeholder="Search a Product" aria-label="Search" name="user_query"/>
                  <button class="btn btn-outline-info my-2 my-sm-0" type="submit" name="search_btn">Search</button>
                  </form>
          </div>
        </nav>
      </div>
    </header>

    <!-- /header -->

    <!-- Main Section   -->

    <main>
      <!--- First Slider -->
      <div class="container-fluid p-0">
        <div class="site-slider">
          <div class="slider-one">

            <?php 

            $query = "Select * from food_company where company_banner_img != ''";
            $run_query = mysqli_query($con, $query);
            

            while($row_query=mysqli_fetch_array($run_query)){

              $company_id = $row_query['fcompany_id'];
              $company_banner_img = $row_query['company_banner_img'];
              $company_name = $row_query['Company_name'];

              echo "<div class='slide'>
              <a href= 'food.php?foodcompany=$company_id' ><img src='./admin_area/homepage_images/$company_banner_img' style='width:1900px;height:500px'class='img-fluid alt='$company_name'/> </a>
            </div>";
            }

            ?>
          </div>
          <div class="slider-btn">
            <span class="prev position-top"
              ><i class="fas fa-chevron-left"></i
            ></span>
            <span class="next position-top right-0"
              ><i class="fas fa-chevron-right"></i
            ></span>
          </div>
        </div>
      </div>
      <!--- /First Slider -->

      <!-- Extra -->

      
      <div class="container text-center">
        <div class="features">
          <h1>Our Overview</h1>
          <p class="text-secondary">
          Overview categories of The Saujana Website
          </p>
        </div>
      </div>
	  
      <div class="container-fluid">
        <div class="site-slider-two px-md-4">
          <div class="row slider-two text-center">

          <?php

           $get_cats = "select * from food_category where cat_type = 'product'";
           $run_cats = mysqli_query($con, $get_cats);

           while($row_cats=mysqli_fetch_array($run_cats)){
             $cat_id = $row_cats['fcat_id'];
             $cat_title = $row_cats['food_cat'];
             $cat_img = $row_cats['cat_image'];

             echo "<div class='col-md-2 product pt-md-5 pt-4'>
             <img src='./admin_area/homepage_images/$cat_img' alt='' style='width:200px;height:200px'/>
            
            <form action='displayproductsbycat.php' method='post'>
             <input type='hidden' name='category_id' value='$cat_id'>
             <button  type='submit'  name='viewallproducts_btn' class='border site-btntwo btn-spantwo'>$cat_title</button>
             </form>
             
           </div>";

             ?>
            <!-- <div class="col-md-2 product pt-md-5 pt-4">
              <img src="./admin_area/homepage_images/<?php echo"$cat_img"?>" alt="" />
             
             <form action="displayproductsbycat.php" method="post">
              <input type="hidden" name="category_id" value="<?php  echo "$cat_id" ?>">
              <button  type="submit" name="viewallproducts_btn" class="border site-btntwo btn-spantwo"><?php echo"$cat_title"?></button>
              </form>
              
            </div> -->
            <?php
           }
          ?>
          </div>
          <div class="slider-btn">
            <span class="prev position-top"
              ><i class="fas fa-chevron-left fa-2x text-secondary"></i
            ></span>
            <span class="next position-top right-0"
              ><i class="fas fa-chevron-right fa-2x text-secondary"></i
            ></span>
          </div>
        </div>
      </div>

      <!-- /Extra -->
      
      <hr class="hr" />

      <!-- Features Section -->

      <div class="container text-center">
        <div class="features">
          <h1 style='text-decoration:none; font-size:40px;'>Our Top PRODUCTS</h1>
          <p class="text-secondary">
          Here below are top Products of Go Went Gone Website
          </p>
        </div>
      </div>

      <!-- Features third Slider -->
      <div class="container-fluid">
        <div class="site-slider-three px-md-4">
          <div class="slider-three row text-center px-4">

            <?php

            $query_top_product = "Select * from food_items where top_product = 'yes'";
            $run_top_product = mysqli_query($con, $query_top_product);

            while($row_top_product=mysqli_fetch_array($run_top_product)){

              $top_product_id = $row_top_product['item_id'];
              $top_product_img = $row_top_product['item_img'];
              $top_product_name = $row_top_product['item_title'];
              $top_product_price = $row_top_product['item_price'];

              echo "<div class='col-md-2 product pt-md-5'>
              <img src='./admin_area/fooditem_images/$top_product_img' class='img-fluid' alt='Image 2' style='width:200px;height:200px'/>
              <div class='cart-details'>
                <h3 class='pro-title p-0' font-size:25px;>$top_product_name</h3>
                <div class='pro-price py-2'>
                  <h2>
                  
                    <span>RM $top_product_price</span>
                  </h2>
                </div>
                <div class='cart mt-4'>

                  <a style='text-decoration:none;' href='singleitems.php?itemid=$top_product_id' type='button' class='border site-btn btn-span'>PURCHASE</a>
                </div>
              </div>
            </div>";
            }
            ?>
          </div>
          
          <div class="slider-btn">
            <span class="prev position-top"
              ><i class="fas fa-chevron-left fa-2x text-secondary"></i
            ></span>
            <span class="next position-top right-0"
              ><i class="fas fa-chevron-right fa-2x text-secondary"></i
            ></span>
          </div>
        </div>
      </div>
      <!-- /Features third Slider -->
      <!-- /Features Section -->

      <hr class="hr"></hr>

          <!-- GogoEmpire Mall heading Section -->
      <div class="container text-center">
        <div class="features">
          <h1>The Saujana</h1>
        </div>
      </div>

      
      <!-- Mall Section -->
        <!-- Mall Section -->
        <div class="container my-5">
            <div class="row">
                <?php
                
                $query = "Select * from food_category where cat_type ='gogo'";
                $run_query = mysqli_query($con, $query);
                while($fetch_query = mysqli_fetch_array($run_query)){

                $category_id = $fetch_query['fcat_id'];
                $category_name = $fetch_query['food_cat'];
                $category_image = $fetch_query['cat_image'];
                

                if($category_name == 'KITCHEN'){

                  $getimage = "Select * from food_category where food_cat = 'KITCHEN'";
                  $runimage = mysqli_query($con,$getimage);
                  $fetchimage = mysqli_fetch_array($runimage);

                  $imageid = $fetchimage['fcat_id'];
                  $categoryimage = $fetchimage['cat_image'];
                  $categoryname = $fetchimage['food_cat'];


                  echo " 
                  <div class='col-md-8 col-12'>
                      <a href = 'displayproductsbycat.php?category_id=$imageid' ><img src='./admin_area/homepage_images/$categoryimage' class='img-fluid' alt='$categoryname'></a>
                  </div>";
                }

                if($category_name == 'HANDMADE'){

                  $getimage = "Select * from food_category where food_cat = 'HANDMADE'";
                  $runimage = mysqli_query($con,$getimage);
                  $fetchimage = mysqli_fetch_array($runimage);

                  $imageid = $fetchimage['fcat_id'];
                  $categoryimage = $fetchimage['cat_image'];
                  $categoryname = $fetchimage['food_cat'];


                  ?>

                  <?php

                 echo " 
                    <div class='col-md-4 col-12'>
                        
                        <a href = 'displayproductsbycat.php?category_id=$imageid' ><img src='./admin_area/homepage_images/$categoryimage' class='img-fluid' alt='$categoryname'></a>
                      
                    </div>";


                }

              
                }
                ?>
              
            </div>

            <div class="row my-md-3">

                <?php

                  $query = "Select * from food_category where cat_type ='gogo'";
                  $run_query = mysqli_query($con, $query);
                  while($fetch_query = mysqli_fetch_array($run_query)){

                  $category_id = $fetch_query['fcat_id'];
                  $category_name = $fetch_query['food_cat'];
                  $category_image = $fetch_query['cat_image'];

                  if($category_name == 'WATCHES'){

                    $getimage = "Select * from food_category where food_cat = 'WATCHES'";
                    $runimage = mysqli_query($con,$getimage);
                    $fetchimage = mysqli_fetch_array($runimage);
  
                    $imageid = $fetchimage['fcat_id'];
                    $categoryimage = $fetchimage['cat_image'];
                    $categoryname = $fetchimage['food_cat'];

                    echo " 
                    <div class='col-md-4 col-12'>
                        
                        <a href = 'displayproductsbycat.php?category_id=$imageid' ><img src='./admin_area/homepage_images/$categoryimage' class='img-fluid' alt='$categoryname'></a>
                      
                    </div>";
                  }

                  if($category_name == 'LUXURY SOFA'){

                    $getimage = "Select * from food_category where food_cat = 'LUXURY SOFA'";
                    $runimage = mysqli_query($con,$getimage);
                    $fetchimage = mysqli_fetch_array($runimage);
  
                    $imageid = $fetchimage['fcat_id'];
                    $categoryimage = $fetchimage['cat_image'];
                    $categoryname = $fetchimage['food_cat'];

                    echo " 
                    <div class='col-md-8 col-12'>
                        
                        <a href = 'displayproductsbycat.php?category_id=$imageid' ><img src='./admin_area/homepage_images/$categoryimage' class='img-fluid' alt='$categoryname'></a>
                      
                    </div>";
  
                  }
                  }



                ?>
            </div>
        </div>
      <!-- /Mall Section -->
      <!-- /Mall Section -->

     
       <!-- New, dailydiscover and TopTreanding -->
       <div class="container">
          <div class="newseller">
            <div class="row">

             
                  <!-- <div class="col-lg-12 col-md-12"> -->
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 sidebar-offcanvas">
                              <!-- <div class="module col1 hidden-sm hidden-xs"></div> -->

                              <div class="module product-simple">
                                <h3 class="title-category" style='text-decoration:none; font-size:23px;'>NEW ARRIVALS</h3>
                                  <div class="modcontent">
                                      <div id="so_extra_slider_2" class="extraslider">
                                          <!-- Begin extraslider-inner -->
                                          <div class="yt-content-slider extraslider-inner" data-rtl="yes"
                                              data-pagination="yes" data-autoplay="no" data-delay="4" data-speed="0.6"
                                              data-margin="0" data-items_column0="1" data-items_column1="1"
                                              data-items_column2="1" data-items_column3="1" data-items_column4="1"
                                              data-arrows="no" data-lazyload="yes" data-loop="no" data-buttonpage="top">
                                              <div class="item">
                                                   <?php 

                                                        $trending_query = "Select * from food_items where trending = 'New Arrival'";
                                                        $trending_run_query = mysqli_query($con, $trending_query);

                                                        while($trending_fetch_query = mysqli_fetch_array($trending_run_query)){

                                                          $trending_item_id = $trending_fetch_query['item_id'];
                                                          $trending_item_cat = $trending_fetch_query['fcat_id'];
                                                          $trending_item_company = $trending_fetch_query['fcompany_id'];
                                                          $trending_item_title = $trending_fetch_query['item_title'];
                                                          $trending_item_price = $trending_fetch_query['item_price'];
                                                          $trending_item_img = $trending_fetch_query['item_img'];
                                                          $trending_item_desc = $trending_fetch_query['item_desc'];
                                                          $trending_item_discounted_price = $trending_fetch_query['item_discounted_price'];
                                                          $trending_item_availability = $trending_fetch_query['item_quantity'];
                                                          $trending_item_youtubelink = $trending_fetch_query['youtube_link'];

                                                          ?>

                                                            <div class="product-layout item-inner style1 ">
                                                                <div class="item-image">
                                                                    <div class="item-img-info">
                                                                        <a href="singleitems.php?itemid=<?php echo "$trending_item_id"?>" target="_self" title="<?php echo "$trending_item_title"?>">
                                                                            <img src="./admin_area/fooditem_images/<?php echo "$trending_item_img"?>"
                                                                                alt="<?php echo "$trending_item_title"?>">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="item-info">
                                                                    <div class="item-title">
                                                                        <a href="singleitems.php?itemid=<?php echo "$trending_item_id"?>" target="_self" title="<?php echo "$trending_item_title"?>"><?php echo "$trending_item_title"?>
                                                                             </a>
                                                                    </div>
                                                                    <div class="rating">
                                                                        <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                        <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                        <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                        <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                        <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    </div>
                                                                    <div class="content_price price">
                                                                      <h3 class="price-new product-price" style='text-decoration:none; font-size:18px;'>  RM<?php echo "$trending_item_price"?></h3>
                                                                    </div>
                                                                </div>
                                                                <!-- End item-info -->
                                                                <!-- End item-wrap-inner -->
                                                            </div>
                                                            <!-- End item-wrap -->

                                                          <?php
                                                        }

                                                      ?>

                                                       <div class="product-layout item-inner style1 ">
                                                     <br/>
                                                  </div>
                                              </div>
                                               <div class="item">
                                                   <?php 

                                                        $trending_query = "Select * from food_items where trending = 'New Arrival'";
                                                        $trending_run_query = mysqli_query($con, $trending_query);

                                                        while($trending_fetch_query = mysqli_fetch_array($trending_run_query)){

                                                          $trending_item_id = $trending_fetch_query['item_id'];
                                                          $trending_item_cat = $trending_fetch_query['fcat_id'];
                                                          $trending_item_company = $trending_fetch_query['fcompany_id'];
                                                          $trending_item_title = $trending_fetch_query['item_title'];
                                                          $trending_item_price = $trending_fetch_query['item_price'];
                                                          $trending_item_img = $trending_fetch_query['item_img'];
                                                          $trending_item_desc = $trending_fetch_query['item_desc'];
                                                          $trending_item_discounted_price = $trending_fetch_query['item_discounted_price'];
                                                          $trending_item_availability = $trending_fetch_query['item_quantity'];
                                                          $trending_item_youtubelink = $trending_fetch_query['youtube_link'];

                                                          ?>

                                                            <div class="product-layout item-inner style1 ">
                                                                <div class="item-image">
                                                                    <div class="item-img-info">
                                                                        <a href="singleitems.php?itemid=<?php echo "$trending_item_id"?>" target="_self" title="<?php echo "$trending_item_title"?>">
                                                                            <img src="./admin_area/fooditem_images/<?php echo "$trending_item_img"?>"
                                                                                alt="<?php echo "$trending_item_title"?>">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="item-info">
                                                                    <div class="item-title">
                                                                        <a href="singleitems.php?itemid=<?php echo "$trending_item_id"?>" target="_self" title="<?php echo "$trending_item_title"?>"><?php echo "$trending_item_title"?>
                                                                             </a>
                                                                    </div>
                                                                    <div class="rating">
                                                                        <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                        <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                        <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                        <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                        <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    </div>
                                                                    <div class="content_price price">
                                                                      <h3 class="price-new product-price" style='text-decoration:none; font-size:18px;'>  RM<?php echo "$trending_item_price"?></h3>
                                                                    </div>
                                                                </div>
                                                                <!-- End item-info -->
                                                                <!-- End item-wrap-inner -->
                                                            </div>
                                                            <!-- End item-wrap -->

                                                          <?php
                                                        }

                                                      ?>
                                                      
                                                       <div class="product-layout item-inner style1 ">
                                                     <br/>
                                                  </div>
                                              </div>
                                          </div>
                                          <!--End extraslider-inner -->
                                      </div>
                                  </div>
                            </div>


                            
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 sidebar-offcanvas">
                              <!-- <div class="module col1 hidden-sm hidden-xs"></div> -->

                              <div class="module product-simple">
                                <h3 class="title-category"style='text-decoration:none; font-size:23px;'>DAILY DISCOVER</h3>
                                  <div class="modcontent">
                                      <div id="so_extra_slider_2" class="extraslider">
                                          <!-- Begin extraslider-inner -->
                                          <div class="yt-content-slider extraslider-inner" data-rtl="yes"
                                              data-pagination="yes" data-autoplay="no" data-delay="4" data-speed="0.6"
                                              data-margin="0" data-items_column0="1" data-items_column1="1"
                                              data-items_column2="1" data-items_column3="1" data-items_column4="1"
                                              data-arrows="no" data-lazyload="yes" data-loop="no" data-buttonpage="top">
                                              <div class="item ">

                                                  <?php 

                                                      $trending_query = "Select * from food_items where trending = 'Daily Discover'";
                                                      $trending_run_query = mysqli_query($con, $trending_query);

                                                      while($trending_fetch_query = mysqli_fetch_array($trending_run_query)){

                                                        $trending_item_id = $trending_fetch_query['item_id'];
                                                        $trending_item_cat = $trending_fetch_query['fcat_id'];
                                                        $trending_item_company = $trending_fetch_query['fcompany_id'];
                                                        $trending_item_title = $trending_fetch_query['item_title'];
                                                        $trending_item_price = $trending_fetch_query['item_price'];
                                                        $trending_item_img = $trending_fetch_query['item_img'];
                                                        $trending_item_desc = $trending_fetch_query['item_desc'];
                                                        $trending_item_discounted_price = $trending_fetch_query['item_discounted_price'];
                                                        $trending_item_availability = $trending_fetch_query['item_quantity'];
                                                        $trending_item_youtubelink = $trending_fetch_query['youtube_link'];

                                                        ?>

                                                          <div class="product-layout item-inner style1 ">
                                                                <div class="item-image">
                                                                    <div class="item-img-info">
                                                                        <a href="singleitems.php?itemid=<?php echo "$trending_item_id"?>" target="_self" title="<?php echo "$trending_item_title"?>">
                                                                            <img src="./admin_area/fooditem_images/<?php echo "$trending_item_img"?>"
                                                                                alt="<?php echo "$trending_item_title"?>">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="item-info">
                                                                    <div class="item-title">
                                                                        <a href="singleitems.php?itemid=<?php echo "$trending_item_id"?>" target="_self" title="<?php echo "$trending_item_title"?>"><?php echo "$trending_item_title"?>
                                                                             </a>
                                                                    </div>
                                                                    <div class="rating">
                                                                        <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                        <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                        <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                        <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                        <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    </div>
                                                                    <div class="content_price price">
                                                                      <h3 class="price-new product-price" style='text-decoration:none; font-size:18px;'>  RM<?php echo "$trending_item_price"?></h3>
                                                                    </div>
                                                                </div>
                                                                <!-- End item-info -->
                                                                <!-- End item-wrap-inner -->
                                                          </div>
                                                            <!-- End item-wrap -->

                                                        <?php

                                                      }

                                                  ?> 
                                                   <div class="product-layout item-inner style1 ">
                                                     <br/>
                                                  </div>
                                                 
                                              </div>
                                              <div class="item ">

                                                  <?php 

                                                      $trending_query = "Select * from food_items where trending = 'Daily Discover'";
                                                      $trending_run_query = mysqli_query($con, $trending_query);

                                                      while($trending_fetch_query = mysqli_fetch_array($trending_run_query)){

                                                        $trending_item_id = $trending_fetch_query['item_id'];
                                                        $trending_item_cat = $trending_fetch_query['fcat_id'];
                                                        $trending_item_company = $trending_fetch_query['fcompany_id'];
                                                        $trending_item_title = $trending_fetch_query['item_title'];
                                                        $trending_item_price = $trending_fetch_query['item_price'];
                                                        $trending_item_img = $trending_fetch_query['item_img'];
                                                        $trending_item_desc = $trending_fetch_query['item_desc'];
                                                        $trending_item_discounted_price = $trending_fetch_query['item_discounted_price'];
                                                        $trending_item_availability = $trending_fetch_query['item_quantity'];
                                                        $trending_item_youtubelink = $trending_fetch_query['youtube_link'];

                                                        ?>

                                                          <div class="product-layout item-inner style1 ">
                                                                <div class="item-image">
                                                                    <div class="item-img-info">
                                                                        <a href="singleitems.php?itemid=<?php echo "$trending_item_id"?>" target="_self" title="<?php echo "$trending_item_title"?>">
                                                                            <img src="./admin_area/fooditem_images/<?php echo "$trending_item_img"?>"
                                                                                alt="<?php echo "$trending_item_title"?>">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="item-info">
                                                                    <div class="item-title">
                                                                        <a href="singleitems.php?itemid=<?php echo "$trending_item_id"?>" target="_self" title="<?php echo "$trending_item_title"?>"><?php echo "$trending_item_title"?>
                                                                             </a>
                                                                    </div>
                                                                    <div class="rating">
                                                                        <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                        <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                        <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                        <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                        <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    </div>
                                                                    <div class="content_price price">
                                                                      <h3 class="price-new product-price" style='text-decoration:none; font-size:18px;'>  RM<?php echo "$trending_item_price"?></h3>
                                                                    </div>
                                                                </div>
                                                                <!-- End item-info -->
                                                                <!-- End item-wrap-inner -->
                                                          </div>
                                                            <!-- End item-wrap -->

                                                        <?php

                                                      }

                                                  ?> 
                                                   <div class="product-layout item-inner style1 ">
                                                     <br/>
                                                  </div>
                                              </div>
                                             
                                          </div>
                                          <!--End extraslider-inner -->
                                      </div>
                                  </div>
                            </div>


                            
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 sidebar-offcanvas">
                              <!-- <div class="module col1 hidden-sm hidden-xs"></div> -->

                              <div class="module product-simple">
                              <h3 class="title-category" style='text-decoration:none; font-size:23px;'>Trending Searches</h3>
                                  <div class="modcontent">
                                      <div id="so_extra_slider_2" class="extraslider">
                                          <!-- Begin extraslider-inner -->
                                          <div class="yt-content-slider extraslider-inner" data-rtl="yes"
                                              data-pagination="yes" data-autoplay="no" data-delay="4" data-speed="0.6"
                                              data-margin="0" data-items_column0="1" data-items_column1="1"
                                              data-items_column2="1" data-items_column3="1" data-items_column4="1"
                                              data-arrows="no" data-lazyload="yes" data-loop="no" data-buttonpage="top">
                                              <div class="item ">

                                                  <?php 

                                                      $trending_query = "Select * from food_items where trending = 'Trending Search'";
                                                      $trending_run_query = mysqli_query($con, $trending_query);

                                                      while($trending_fetch_query = mysqli_fetch_array($trending_run_query)){

                                                        $trending_item_id = $trending_fetch_query['item_id'];
                                                        $trending_item_cat = $trending_fetch_query['fcat_id'];
                                                        $trending_item_company = $trending_fetch_query['fcompany_id'];
                                                        $trending_item_title = $trending_fetch_query['item_title'];
                                                        $trending_item_price = $trending_fetch_query['item_price'];
                                                        $trending_item_img = $trending_fetch_query['item_img'];
                                                        $trending_item_desc = $trending_fetch_query['item_desc'];
                                                        $trending_item_discounted_price = $trending_fetch_query['item_discounted_price'];
                                                        $trending_item_availability = $trending_fetch_query['item_quantity'];
                                                        $trending_item_youtubelink = $trending_fetch_query['youtube_link'];

                                                          ?>

                                                             <div class="product-layout item-inner style1 ">
                                                                <div class="item-image">
                                                                    <div class="item-img-info">
                                                                        <a href="singleitems.php?itemid=<?php echo "$trending_item_id"?>" target="_self" title="<?php echo "$trending_item_title"?>">
                                                                            <img src="./admin_area/fooditem_images/<?php echo "$trending_item_img"?>"
                                                                                alt="<?php echo "$trending_item_title"?>">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="item-info">
                                                                    <div class="item-title">
                                                                        <a href="singleitems.php?itemid=<?php echo "$trending_item_id"?>" target="_self" title="<?php echo "$trending_item_title"?>"><?php echo "$trending_item_title"?>
                                                                             </a>
                                                                    </div>
                                                                    <div class="rating">
                                                                        <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                        <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                        <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                        <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                        <span class="fa fa-stack"><i
                                                                                class="fa fa-star fa-stack-2x"></i></span>
                                                                    </div>
                                                                    <div class="content_price price">
                                                                      <h3 class="price-new product-price" style='text-decoration:none; font-size:18px;'>  RM<?php echo "$trending_item_price"?></h3>
                                                                    </div>
                                                                </div>
                                                                <!-- End item-info -->
                                                                <!-- End item-wrap-inner -->
                                                              </div>
                                                            <!-- End item-wrap -->



                                                          <?php

                                                      }

                                                  ?> 
                                                   <div class="product-layout item-inner style1 ">
                                                     <br/>
                                                  </div>
                                              </div>
                                              <!-- <div class="item "> -->
                                                  <!-- <div class="product-layout item-inner style1 ">
                                                      <div class="item-image">
                                                          <div class="item-img-info">
                                                              <a href="#" target="_self" title="Qeserunt shortloin ">
                                                                  <img src="image/catalog/demo/product/80/4.jpg"
                                                                      alt="Qeserunt shortloin">
                                                              </a>
                                                          </div>

                                                      </div>
                                                      <div class="item-info">
                                                          <div class="item-title">
                                                              <a href="#" target="_self" title="Qeserunt shortloin">
                                                                  Qeserunt shortloin
                                                              </a>
                                                          </div>
                                                          <div class="rating">
                                                              <span class="fa fa-stack">
                                                                  <i class="fa fa-star fa-stack-2x"></i>
                                                              </span>
                                                              <span class="fa fa-stack">
                                                                  <i class="fa fa-star fa-stack-2x"></i>
                                                              </span>
                                                              <span class="fa fa-stack">
                                                                  <i class="fa fa-star fa-stack-2x"></i>
                                                              </span>
                                                              <span class="fa fa-stack">
                                                                  <i class="fa fa-star fa-stack-2x"></i>
                                                              </span>
                                                              <span class="fa fa-stack">
                                                                  <i class="fa fa-star fa-stack-2x"></i>
                                                              </span>
                                                          </div>
                                                          <div class="content_price price">
                                                              <span class="price product-price">
                                                                  $88.00
                                                              </span>
                                                          </div>
                                                      </div>
                                                     
                                                  </div> -->
                                                  <!-- End item-wrap -->   
                                              <!-- </div> -->
                                          </div>
                                          <!--End extraslider-inner -->
                                      </div>
                                  </div>
                            </div>


                            
                    </div>
                  <!-- </div> -->
              
              
              
              
              
              
             
            </div>
          </div>
        </div>

      <!-- /New, dailydiscover and TopTreanding -->



      

      <!-- Facilities -->
        <div class="container-fluid p-0">
          <div class="site-info">
            <div class="row text-center py-3 bg-primary-color m-0">
              <div class="col-md-4 col-sm-12 my-md-0 my-4">
                <div class="row justify-content-center text-light">
                  <i class="fas fa-rocket fa-4x px-4"></i>
                  <div class="py-2 font-roboto text-left">
                    <h6 class="m-0">Free Shipping & Return</h6>
                    <small>Free Shipping on order over RM49</small>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-12 my-md-0 my-4">
                  <div class="row justify-content-center text-light">
                      <i class="fas fa-hand-holding-usd fa-4x px-4"></i>
                      <div class="py-2 font-roboto text-left">
                        <h6 class="m-0">Money Guarantee</h6>
                        <small>30 days money back guarantee</small>
                      </div>
                    </div>
              </div>
              <div class="col-md-4 col-sm-12 my-md-0 my-4">
                  <div class="row justify-content-center text-light">
                      <i class="fas fa-headphones-alt fa-4x px-4"></i>
                      <div class="py-2 font-roboto text-left">
                        <h6 class="m-0">Online Support</h6>
                        <small>We support online 24 hours a day</small>
                      </div>
                    </div>
              </div>
            </div>
          </div>
        </div>
      <!-- /Facilities -->

      

    </main>

    <!-- /Main Section   -->


     <!-- Footer Container -->
     <footer class="footer-container typefooter-1">

<hr>

<div class="footer-middle ">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-style">
                <div class="infos-footer">
                    <h3 class="modtitle">Contact Us</h3>
                    <ul class="menu">
                       <!--<li class="adres">
                                        input address here
                                    </li>-->
                        <li class="phone">
                            (+852) 2433 3841
                        </li>
                        <li class="mail">
                            <a href="">Info@The Saujana.com</a>
                        </li>
                        <li class="time">
                            Open time: 7:00AM - 5:30PM
                            
                        </li>
                    </ul>
                </div>
                <div class="box-account box-footer">
                  <br/>
                </div>
            </div>

            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                <div class="box-information box-footer">
                    <div class="module clearfix">
                        <h3 class="modtitle">Information</h3>
                        <div class="modcontent">
                            <ul class="menu">
                                <li><a href="https://www.saujanahotels.com/about">ABOUT US </a></li>
                                <li><a href="#">FAQ</a></li>
                                <li><a href="#">Warranty And Services</a></li>
                                <li><a href="#">Support 24/7 page</a></li>
                                <li><a href="#">Product Registration</a></li>
                                <li><a href="#">Product Support</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="box-account box-footer">
                  <br/>
                </div>

            </div>
            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                <div class="box-account box-footer">
                    <div class="module clearfix">
                        <h3 class="modtitle">Extras</h3>
                        <div class="modcontent">
                            <ul class="menu">
                                <li> <a href="https://www.saujanahotels.com/about"> Go Went Gone </a></li>
                                <li><a href="displaycategory.php">Categories</a></li>
                                <li><a href="https://www.saujanahotels.com/contact">Contact Us</a></li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="box-account box-footer">
                  <br/>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                <div class="box-service box-footer">
                    <div class="module clearfix">
                        <h3 class="modtitle">Services</h3>
                        <div class="modcontent">
                            <ul class="menu">
                                <li><a href="#">Kuala Lumpur</a></li>
                                <li><a href="#">Selangor</a></li>
                                <li><a href="#">KL Central</a></li>
                                <li></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Footer Bottom Container -->
<div class="container text-center">
          <p class="pt-5">
            <img src="./assets/payment.png" alt="payment image" class="img-fluid">
          </p>
          <small class="text-secondary py-4">The Saujana © 2020 All Rights Reserved.</small>
        </div>
<!-- /Footer Bottom Container -->


<!--Back To Top-->
<!-- <div class="back-to-top"><i class="fa fa-angle-up"></i></div> -->
</footer>
<!-- //end Footer Container -->




    <script
      src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
      integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
      integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
      integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jrm"
      crossorigin="anonymous"
    ></script>
    <script
      type="text/javascript"
      src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"
    ></script>
    <script src="./js/main.js"></script>


    
    <!-- Include Libs & Plugins
============================================ -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="js/jquery-2.2.4.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/owl-carousel/owl.carousel.js"></script>
    <script type="text/javascript" src="js/themejs/libs.js"></script>
     <script type="text/javascript" src="js/countdown/jquery.countdown.min.js"></script>
    <script type="text/javascript" src="js/dcjqaccordion/jquery.dcjqaccordion.2.8.min.js"></script>
    <script type="text/javascript" src="js/datetimepicker/moment.js"></script>
    <script type="text/javascript" src="js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
  
   

    <!-- Theme files
============================================ -->

    <!-- <script type="text/javascript" src="js/themejs/application.js"></script> -->

    <script type="text/javascript" src="js/themejs/homepage.js"></script>

    <!-- <script type="text/javascript" src="js/themejs/toppanel.js"></script>
    <script type="text/javascript" src="js/themejs/so_megamenu.js"></script>
    <script type="text/javascript" src="js/themejs/addtocart.js"></script> -->


    
  </body>
</html>

