<?php
session_start();
include("includes/db.php");
include("functions/functions.php");

?>



<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Gogo Empire Store</title>

    <!-- Bootstrap CDN -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
      crossorigin="anonymous"
    />

    <!--  Font Awesome CDN -->
    <script src="https://kit.fontawesome.com/23412c6a8d.js"></script>

    <!-- Slick Slider -->
    <link
      rel="stylesheet"
      type="text/css"
      href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"
    />

    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="./css/style.css" />
    <link rel="stylesheet" href="./css/viewproduct.css" />
  </head>
  <body>
    <!-- header -->

    <header>
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-12 col-12">
            <div class="btn-group">
              <!--<button
                class="btn border dropdown-toggle my-md-4 my-2 text-white"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                ENG
              </button>-->
              <div class="dropdown-menu">
                <a href="#" class="dropdown-item">Bhasa - Malaysia</a>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-12 text-center">
            <h2 class="my-md-3 site-title text-white">Gogo Empire </h2>
          </div>
          <div class="col-md-4 col-12 text-right">
            <p class="my-md-4 header-links">
              <?php
                if(!isset($_SESSION['customer_email'])){
                echo"";
                }else{
                  
                  echo"<a href='' class='px-1'> $_SESSION[customer_email] </a>";
                  echo"<a class='px-2' style='color:white;'>|</a>";
                }
              ?>
              <?php
                if(!isset($_SESSION['customer_email'])){
                echo"<a href='checkout.php' class='px-2'>login</a>";
                echo"<a class='px-2' style='color:white;'>|</a>";
                echo"<a href='register.php' class='px-2'>Create an Account</a>";
                }else{
                  echo"<a href='logout.php' class='px-2'>logout</a>";
                }
              ?>
            </p>
          </div>
        </div>
      </div>

      <div class="container-fluid p-0">
        <nav class="navbar navbar-expand-lg navbar-light bg-white">
          <button
            class="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item active">
                <a href="index.php" class="nav-link" >HOME <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a href="index.php" class="nav-link" > FEATURES </a>
              </li>
              <li class="nav-item">
                <a href="displaycategory.php" class="nav-link" > CATEGORIES</a>
              </li>
              <li class="nav-item">
                <a href="index.php" class="nav-link" > ABOUT US</a>
              </li>
              <li class="nav-item">
                <a href="hotels.php" class="nav-link" > STORES</a>
              </li>
              <li class="nav-item">
                <!-- <a href="cart.php" class="nav-link" > Your Cart <b class="badge badge-pill badge-light float-right"> <?php total_items();?></b></a> -->
                <a href="" class="nav-link" > Your Cart <b class="badge badge-pill badge-light float-right"> <?php total_items();?></b></a>
              </li>
            </ul>
          </div>
          <div class="navbar-nav">
            <form class="form-inline my-2 my-lg-0" method="get" action="results.php" enctype="multipart/form-data">
                  <input class="form-control mr-sm-2" type="search" placeholder="Search a Product" aria-label="Search" name="user_query"/>
                  <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>
                  </form>
            <!-- <li class="nav-items rounded-circle mx-2 basket-icon">
              <i class="fas fa-shopping-basket p-2"></i>
            </li> -->
          </div>
        </nav>
      </div>
    </header>

    <!-- /header -->

    <!-- Main Section   -->

    <main>
      <!--- First Slider -->
      <div class="container-fluid p-0">
        <div class="site-slider">
          <div class="slider-one">
            <div class="slide">
              <img src="./assets/item-2.jpg" class="img-fluid" alt="Banner 1"/>
            </div>
            <div>
              <img src="./assets/item-3.jpg" class="img-fluid" alt="Banner 2" />
            </div>
            <div>
              <img src="./assets/item-1.jpg" class="img-fluid" alt="Banner 3" />
            </div>
          </div>
          <div class="slider-btn">
            <span class="prev position-top"
              ><i class="fas fa-chevron-left"></i
            ></span>
            <span class="next position-top right-0"
              ><i class="fas fa-chevron-right"></i
            ></span>
          </div>
        </div>
      </div>
      <!--- /First Slider -->

      <!-- Extra -->

      
      <div class="container-fluid">
        <div class="site-slider-two px-md-4">
          <div class="row slider-two text-center">

          <?php

           $get_cats = "select * from categories";
           $run_cats = mysqli_query($con, $get_cats);

           while($row_cats=mysqli_fetch_array($run_cats)){
             $cat_id = $row_cats['cat_id'];
             $cat_title = $row_cats['cat_title'];
             $cat_img = $row_cats['cat_img'];

             ?>
            <div class="col-md-2 product pt-md-5 pt-4">
              <img src="admin_area/assets/<?php echo"$cat_img"?>" alt="Product 1" />
             <a href="viewproduct.php?cat=<?php echo"$cat_id"?>"><button type="button" class="border site-btntwo btn-spantwo"><?php echo"$cat_title"?></button></a>
              
            </div>

            <?php

          
           }
          ?>
            <!-- <div class="col-md-2 product pt-md-5 pt-4">
              <img src="./assets/id-9-cat-1.jpg" alt="Product 1" />
              <a href="index.php"><button type="button" class="border site-btntwo btn-spantwo">SOFA & CHAIRS</button></a>
            </div> -->
          </div>
          <div class="slider-btn">
            <span class="prev position-top"
              ><i class="fas fa-chevron-left fa-2x text-secondary"></i
            ></span>
            <span class="next position-top right-0"
              ><i class="fas fa-chevron-right fa-2x text-secondary"></i
            ></span>
          </div>
        </div>
      </div>

      <!-- /Extra -->
      
      <hr class="hr" />

      <!-- Features Section -->

      <div class="container text-center">
        <div class="features">
          <h1>Our Top Products</h1>
          <p class="text-secondary">
          Here below are top Products of Gogo Empire Online Store
          </p>
        </div>
      </div>

      <!-- Features third Slider -->
      <div class="container-fluid">
        <div class="site-slider-three px-md-4">
          <div class="slider-three row text-center px-4">
            <div class="col-md-2 product pt-md-5">
              <div class="product-top">
                <img src="./assets/15_3.jpg" class="img-fluid" alt="Image 1" />
                <div class="overlay-right">
                  <button type="button" class="btn btn-secondary" , title="Youtube Link">
                    <a href="https://www.youtube.com/watch?v=6_hmK3akETU"><i class="fa fa-youtube-play"></i></a>
                  </button>
                  <!-- <button type="button" class="btn btn-secondary" , title="Add to Wishlist">
                    <i class="fa fa-youtube-play"></i>
                  </button>
                  <button type="button" class="btn btn-secondary" , title="Add to Cart">
                    <i class="fa fa-shopping-cart"></i>
                  </button> -->
                </div>
              </div>
              <div class="cart-details">
                <h6 class="pro-title p-0">Vinyl Top Folding Table</h6>
                <div class="rating">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star-half-alt"></i>
                </div>
                <div class="pro-price py-2">
                  <h5>
                    <small><s class="text-secondary">RM125</s></small>
                    <span>RM120</span>
                  </h5>
                </div>
                <div class="cart mt-4">
                  <button class="border site-btn btn-span">Add to Cart</button>
                </div>
              </div>
            </div>
            <div class="col-md-2 product pt-md-5">
              <img src="./assets/9_21.jpg" class="img-fluid" alt="Image 2" />
              <div class="cart-details">
                <h6 class="pro-title p-0">Ikea RUSCH 25x4 Wall</h6>
                <div class="rating">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                </div>
                <div class="pro-price py-2">
                  <h5>
                    <small><s class="text-secondary">RM119</s></small>
                    <span>RM103</span>
                  </h5>
                </div>
                <div class="cart mt-4">
                  <button class="border site-btn btn-span">Add to Cart</button>
                </div>
              </div>
            </div>
            <div class="col-md-2 product pt-md-5">
              <img src="./assets/10_22.jpg" class="img-fluid" alt="Image 3" />
              <div class="cart-details">
                <h6 class="pro-title p-0">Himalayan Salt Lamp Touch</h6>
                <div class="rating">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                </div>
                <div class="pro-price py-2">
                  <h5>
                    <small><s class="text-secondary">RM469</s></small>
                    <span>RM449</span>
                  </h5>
                </div>
                <div class="cart mt-4">
                  <button class="border site-btn btn-span">Add to Cart</button>
                </div>
              </div>
            </div>
            <div class="col-md-2 product pt-md-5">
              <img src="./assets/12_16.jpg" class="img-fluid" alt="Image 4" />
              <div class="cart-details">
                <h6 class="pro-title p-0">Table Lamp Beside Desk</h6>
                <div class="rating">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star-half-alt"></i>
                </div>
                <div class="pro-price py-2">
                  <h5>
                    <span>RM449</span>
                  </h5>
                </div>
                <div class="cart mt-4">
                  <button class="border site-btn btn-span">Add to Cart</button>
                </div>
              </div>
            </div>
            <div class="col-md-2 product pt-md-5">
              <img src="./assets/11_18.jpg" class="img-fluid" alt="Image 4" />
              <div class="cart-details">
                <h6 class="pro-title p-0">Parsons Modern End</h6>
                <div class="rating">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star-half-alt"></i>
                </div>
                <div class="pro-price py-2">
                  <h5>
                    <small><s class="text-secondary">RM259</s></small>
                    <span>RM240</span>
                  </h5>
                </div>
                <div class="cart mt-4">
                  <button class="border site-btn btn-span">Add to Cart</button>
                </div>
              </div>
            </div>
            <div class="col-md-2 product pt-md-5">
              <img src="./assets/7_22.jpg" class="img-fluid" alt="Image 5" />
              <div class="cart-details">
                <h6 class="pro-title p-0">Outdoor Patio Teak Side Table</h6>
                <div class="rating">
                  <span>write your review</span>
                </div>
                <div class="pro-price py-2">
                  <h5>
                    <small><s class="text-secondary">RM139</s></small>
                    <span>RM125</span>
                  </h5>
                </div>
                <div class="cart mt-4">
                  <button class="border site-btn btn-span">Add to Cart</button>
                </div>
              </div>
            </div>
          </div>
          <div class="slider-btn">
            <span class="prev position-top"
              ><i class="fas fa-chevron-left fa-2x text-secondary"></i
            ></span>
            <span class="next position-top right-0"
              ><i class="fas fa-chevron-right fa-2x text-secondary"></i
            ></span>
          </div>
        </div>
      </div>
      <!-- /Features third Slider -->
      <!-- /Features Section -->

      <hr class="hr"></hr>

          <!-- GogoEmpire Mall heading Section -->
      <div class="container text-center">
        <div class="features">
          <h1>GogoEmpire Mall</h1>
        </div>
      </div>

      
      <!-- Mall Section -->
        <div class="container my-5">
            <div class="row">
                <div class="col-md-8 col-12">
                    <img src="./assets/id-9-banner-1.jpg" class="img-fluid" alt="Banner 1">
                </div>
                <div class="col-md-4 col-12">
                    <img src="./assets/id-9-banner-2.jpg" class="img-fluid" alt="Banner 2">
                </div>
            </div>

            <div class="row my-md-3">
                <div class="col-md-4 col-12"> 
                <img src="./assets/id-9-banner-3.jpg" class="img-fluid" alt="Banner 3">
                </div>
                <div class="col-md-8 col-12"> 
                <img src="./assets/id-9-banner-4.jpg" class="img-fluid" alt="Banner 4">
                </div>
            </div>
        </div>
      <!-- /Mall Section -->

      <!-- New, dailydiscover and TopTreanding -->
        <div class="container">
          <div class="newseller">
            <div class="row">
              <div class="col-md-4 col-sm-6">
                <h3 class="text-secondary">New Arrivals</h3>
                <div class="row py-3">
                  <div class="col-md-3 p-0">
                    <div class="items border">
                      <img src="./assets/15_3.jpg" alt="Image 1" class="img-fluid">
                    </div>
                  </div>
                  <div class="col-md-9 p-0 py-4 py-md-0">
                    <div class="px-4">
                      <h6>Vinyl Top Folding Table</h6>
                      <div class="rating pb-2">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star-half-alt"></i>
                      </div>
                      <h5>
                        <small><s class="text-secondary">RM125</s></small>
                        <span class="text-color">RM120</span>
                      </h5>
                    </div>
                  </div>
                </div>
                <div class="row py-3">
                  <div class="col-md-3 p-0">
                    <div class="items border">
                      <img src="./assets/11_18.jpg" alt="Image 2" class="img-fluid">
                    </div>
                  </div>
                  <div class="col-md-9 p-0 py-4 py-md-0">
                    <div class="px-4">
                      <h6>Parsons Mordern End Black</h6>
                      <div class="rating pb-2">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                      </div>
                      <h5>
                        <small><s class="text-secondary">RM159</s></small>
                        <span class="text-color">RM240</span>
                      </h5>
                    </div>
                  </div>
                </div>
                <div class="row py-3">
                  <div class="col-md-3 p-0">
                    <div class="items border">
                      <img src="./assets/9_21.jpg" alt="Image 3" class="img-fluid">
                    </div>
                  </div>
                  <div class="col-md-9 p-0 py-4 py-md-0">
                    <div class="px-4">
                      <h6>Ikea RUSCH 25x4 Wall</h6>
                      <div class="rating pb-2">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                      </div>
                      <h5>
                        <span class="text-color">RM103</span>
                      </h5>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6">
                <h3 class="text-secondary">Daily Discovers</h3>
                <div class="row py-3">
                  <div class="col-md-3 p-0">
                    <div class="items border">
                      <img src="./assets/11_18.jpg" alt="Image 1" class="img-fluid">
                    </div>
                  </div>
                  <div class="col-md-9 p-0 py-4 py-md-0">
                    <div class="px-4">
                      <h6>Parsons Modern End Black</h6>
                      <div class="rating pb-2">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                      </div>
                      <h5>
                        <small><s class="text-secondary">RM159</s> </small>
                        <span class="text-color">RM240</span>
                      </h5>
                    </div>
                  </div>
                </div>
                <div class="row py-3">
                    <div class="col-md-3 p-0">
                      <div class="items border">
                        <img src="./assets/12_16.jpg" alt="Image 2" class="img-fluid">
                      </div>
                    </div>
                    <div class="col-md-9 p-0 py-4 py-md-0">
                      <div class="px-4">
                        <h6>Table Lamp Beside Desk</h6>
                        <div class="rating pb-2">
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star-half-alt"></i>
                        </div>
                        <h5>
                          <small><s class="text-secondary">RM449</s> </small>
                          <span class="text-color">RM469</span>
                        </h5>
                      </div>
                    </div>
                  </div>
                  <div class="row py-3">
                      <div class="col-md-3 p-0">
                        <div class="items border">
                          <img src="./assets/9_21.jpg" alt="Image 3" class="img-fluid">
                        </div>
                      </div>
                      <div class="col-md-9 p-0 py-4 py-md-0">
                        <div class="px-4">
                          <h6>Ikea RUSCH 25x4 Wall</h6>
                          <div class="rating pb-2">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star-half-alt"></i>
                          </div>
                          <h5>
                            <span class="text-color">RM103</span>
                          </h5>
                        </div>
                      </div>
                    </div>
              </div>
              <div class="col-md-4 col-sm-6">
                <h3 class="text-secondary">Trending Searches</h3>
                <div class="row py-3">
                    <div class="col-md-3 p-0">
                      <div class="items border">
                        <img src="./assets/8_20.jpg" alt="Image 1" class="img-fluid">
                      </div>
                    </div>
                    <div class="col-md-9 p-0 py-4 py-md-0">
                      <div class="px-4">
                        <h6>Large Wall Clock Sticker</h6>
                        <div class="rating pb-2">
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star-half-alt"></i>
                        </div>
                        <h5>
                          <small><s class="text-secondary">RM219</s> </small>
                          <span class="text-color">RM210</span>
                        </h5>
                      </div>
                    </div>
                  </div>
                  <div class="row py-3">
                      <div class="col-md-3 p-0">
                        <div class="items border">
                          <img src="./assets/1_11.jpg" alt="Image 2" class="img-fluid">
                        </div>
                      </div>
                      <div class="col-md-9 p-0 py-4 py-md-0">
                        <div class="px-4">
                          <h6>Upholstered Fabric Sofa</h6>
                          <div class="rating pb-2">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star-half-alt"></i>
                          </div>
                          <h5>
                            <small><s class="text-secondary">RM519</s> </small>
                            <span class="text-color">RM499</span>
                          </h5>
                        </div>
                      </div>
                    </div>
                    <div class="row py-3">
                        <div class="col-md-3 p-0">
                          <div class="items border">
                            <img src="./assets/15_3.jpg" alt="Image 3" class="img-fluid">
                          </div>
                        </div>
                        <div class="col-md-9 p-0 py-4 py-md-0">
                          <div class="px-4">
                            <h6>Vinyl Top Folding Table</h6>
                            <div class="rating pb-2">
                              <i class="fas fa-star"></i>
                              <i class="fas fa-star"></i>
                              <i class="fas fa-star"></i>
                              <i class="fas fa-star"></i>
                              <i class="fas fa-star"></i>
                            </div>
                            <h5>
                              <small><s class="text-secondary">RM125</s> </small>
                              <span class="text-color">RM120</span>
                            </h5>
                          </div>
                        </div>
                      </div>
              </div>
            </div>
          </div>
        </div>

      <!-- /New, dailydiscover and TopTreanding -->

      <!-- Facilities -->
        <div class="container-fluid p-0">
          <div class="site-info">
            <div class="row text-center py-3 bg-primary-color m-0">
              <div class="col-md-4 col-sm-12 my-md-0 my-4">
                <div class="row justify-content-center text-light">
                  <i class="fas fa-rocket fa-4x px-4"></i>
                  <div class="py-2 font-roboto text-left">
                    <h6 class="m-0">Free Shipping & Return</h6>
                    <small>Free Shipping on order over RM49</small>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-12 my-md-0 my-4">
                  <div class="row justify-content-center text-light">
                      <i class="fas fa-hand-holding-usd fa-4x px-4"></i>
                      <div class="py-2 font-roboto text-left">
                        <h6 class="m-0">Money Guarantee</h6>
                        <small>30 days money back guarantee</small>
                      </div>
                    </div>
              </div>
              <div class="col-md-4 col-sm-12 my-md-0 my-4">
                  <div class="row justify-content-center text-light">
                      <i class="fas fa-headphones-alt fa-4x px-4"></i>
                      <div class="py-2 font-roboto text-left">
                        <h6 class="m-0">Online Support</h6>
                        <small>We support online 24 hours a day</small>
                      </div>
                    </div>
              </div>
            </div>
          </div>
        </div>
      <!-- /Facilities -->

    </main>

    <!-- /Main Section   -->


    <!-- Footer -->

    <footer>
        <div class="container-fluid px-5">
          <div class="row">
            <div class="col-md-4 col-sm-12">
              <h4>Contact Us</h4>
              <div class="row pl-md-1 text-secondary">
                <div class="col-12">
                  <i class="fa fa-home px-md-2"></i>
                  <small>S0-7-01, Menara 1, No 3, Jalan Bangsar, KL Eco City</small>
                </div>
              </div>
              <div class="row pl-md-1 text-secondary py-4">
                  <div class="col-12">
                      <i class="fa fa-paper-plane px-md-2"></i>
                      <small>www.gogoempire.com</small>
                    </div>
              </div>
               <div class="row pl-md-1 text-secondary">
                  <div class="col-12">
                      <i class="fa fa-phone-volume px-md-2"></i>
                      <small>(+60) 123817908</small>
                    </div>
               </div>

              <div class="row social text-secondary">
                <div class="col-12 py-3">
                  <i class="fab fa-twitter"></i>
                  <i class="fab fa-facebook-f"></i>
                  <i class="fab fa-google-plus-g"></i>
                  <i class="fab fa-skype"></i>
                  <i class="fab fa-pinterest-p"></i>
                  <i class="fab fa-youtube"></i>
                  <i class="fab fa-linkedin-in"></i>
                </div>
              </div>
            </div>
            <div class="col-md-2 col-sm-12">
              <h4>Our Services</h4>
              <div class="d-flex flex-column pl-md-3">
                <small class="pt-0">Kuala Lumpur</small>
                <small>Selangor</small>
                <small>KL Centeral</small>
              </div>
            </div>
            <div class="col-md-2 col-sm-12">
              <h4>Extras</h4>
              <div class="d-flex flex-column pl-md-3">
                  <small class="pt-0">About GogoEmpire</small>
                  <small>CATEGORIES</small>
                  <small>Contact Us</small>
                </div>
            </div>
            <div class="col-md-4 follow-us col-sm-12">
              <h4>Follow Instagram</h4>
              <div class="d-flex flex-row">
                <img src="./assets/256_n.jpg" alt="Instagram 1" class="img-fluid">
                <img src="./assets/792_n.jpg" alt="Instagram 2" class="img-fluid">
                <img src="./assets/392_n.jpg" alt="Instagram 3" class="img-fluid">
              </div>
              <div class="d-flex flex-row">
                  <img src="./assets/664_n.jpg" alt="Instagram 1" class="img-fluid">
                  <img src="./assets/088_n.jpg" alt="Instagram 2" class="img-fluid">
                  <img src="./assets/896_n.jpg" alt="Instagram 3" class="img-fluid">
                </div>
            </div>
          </div>
        </div>

        <div class="container-fluid news pt-5">
          <div class="row">
            <div class="col-md-6 col-12 pl-5">
              <h4 class="primary-color font-roboto m-0 p-0">
                Need Help? Call Our Award-Warning
              </h4>
              <p class="m-0 p-0 text-secondary">
                Support Team 24/7 At (+60) 176844365
              </p>
            </div>
            <div class="col-md-4 col-12 my-md-0 my-3 pl-md-0 pl-5">
              <input type="text" class="form-control border-0 bg-light" placeholder="Enter Your email Address">
            </div>
            <div class="col-md-2 col-12 my-md-0 my-3 pl-md-0 pl-5">
              <button class="btn bg-primary-color text-white">Subscribe</button>
            </div>
          </div>
        </div>

        <div class="container text-center">
          <p class="pt-5">
            <img src="./assets/payment.png" alt="payment image" class="img-fluid">
          </p>
          <small class="text-secondary py-4">GogoEmpire © 2020 All Rights Reserved.</small>
        </div>

    </footer>

    <!-- /Footer -->


    <script
      src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
      integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
      integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
      integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
      crossorigin="anonymous"
    ></script>
    <script
      type="text/javascript"
      src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"
    ></script>
    <script src="./js/main.js"></script>
  </body>
</html>

