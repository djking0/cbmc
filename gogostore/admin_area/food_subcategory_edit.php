<?php
include('security.php');
include('includes/header.php'); 
include('includes/navbar.php'); 
?>


<div class="container-fluid">

<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"> EDIT SUB-FOOD CATEGORY </h6>
  </div>
  <div class="card-body">
<?php

    

    if(isset($_POST['edit_subfoodcat_btn'])){
        $id = $_POST['edit_subfoodcat_id'];
        
        require 'dbconfig.php'; 

        $query = "SELECT * FROM subcategory_stageone WHERE sc_id='$id'";
        $query_run = mysqli_query($connection, $query);

        foreach($query_run as $row)
        {
            ?>

                <form action="code.php" method="POST">

                    <div class="modal-body">

                        <input type="hidden" name="subfoodcat_updateid" value="<?php echo $row['sc_id'] ?>" >

                        <div class="form-group">
                            <label>Sub-Category Name </label>
                            <input type="text" name="subfoodcat_updatename" value="<?php echo $row['sc_name'];?>" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-danger" name="cancelhotelbtn" data-dismiss="modal">CANCEL</button> -->
                        <button type="submit" name="updatefood_subcat_btn" class="btn btn-primary">UPDATE</button>
                    </div>
                </form>
        <?php
        }

      
    }
?>
  </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>