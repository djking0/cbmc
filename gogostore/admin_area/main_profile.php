<?php
include('security.php');
include('includes/header.php'); 
include('includes/navbar.php'); 
?>


<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
          <div class="col-lg-7">
            <div class="p-5">
              
              <?php

                    // : Getting FoodStore ID
                    require 'dbconfig.php';

                    $store_email = $_SESSION['username'];
                                                
                    $get_foodstore_details = "Select * from food_company where company_email='$store_email'";
                    $run_foodstore_details = mysqli_query($connection, $get_foodstore_details);
                    $fetch_foodstore_details = mysqli_fetch_array($run_foodstore_details);
                                                                                                            
                    $foodstore_id = $fetch_foodstore_details['fcompany_id']; //:: Customer id 
                    $foodstore_name = $fetch_foodstore_details['Company_name']; //:: customer name
                    $foodstore_address = $fetch_foodstore_details['company_address'];
                    $foodstore_email = $fetch_foodstore_details['company_email'];
                    $foodstore_type = $fetch_foodstore_details['company_type'];

                    ?>

                   
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4"><strong><?php echo "$foodstore_name"?></strong></h1>
                    </div>
                    <hr>
                 
                    <div class="text-center">
                        <h3 class="h5 text-gray-900 mb-4">Email:  </h3>
                        
                        <h1 class="h4 text-gray-900 mb-4"><strong><?php echo "$foodstore_email"?></strong></h1>
                    </div>
                    <hr>
                    
                    <div class="text-center">
                        <h3 class="h5 text-gray-900 mb-4">Address: </h3>
                        <h1 class="h4 text-gray-900 mb-4"><strong><?php echo "$foodstore_address"?></strong></h1>
                    </div>
                    <hr>
                    
                    <div class="text-center">
                        <h3 class="h5 text-gray-900 mb-4">Type:</h3>
                        <h1 class="h4 text-gray-900 mb-4"><strong><?php echo "$foodstore_type"?></strong></h1>
                    </div>
                    <hr>


            

            </div>
          </div>
        </div>
      </div>
    </div>



<?php
include('includes/scripts.php');
include('includes/footer.php');
?>