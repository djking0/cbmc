   
   
   <!-- Sidebar -->
   <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

<!-- Sidebar - Brand -->
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
  <div class="sidebar-brand-icon rotate-n-15">
    <i class="fas fa-laugh-wink"></i>
  </div>
  <div class="sidebar-brand-text mx-3">Gogo<sup></sup></div>
</a>

<!-- Divider -->
<hr class="sidebar-divider my-0">



<!-- Nav Item - Dashboard -->
<li class="nav-item active">
  <a class="nav-link" href="productstore_index.php">
    <i class="fas fa-fw fa-tachometer-alt"></i>
    <span>Dashboard</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->
<div class="sidebar-heading">
  Your Pages
</div>

<!-- Nav Item - Charts -->
<li class="nav-item">
  <a class="nav-link" href="productstore_productitems.php">
    <i class="fas fa-fw fa-chart-area"></i>
    <span>Your Products</span></a>
</li>


<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">


<!-- Heading -->
<div class="sidebar-heading">
  Orders
</div>



<!-- Nav Item - Charts -->
<li class="nav-item">
  <a class="nav-link" href="productstore_pickuporders.php">
    <i class="fas fa-fw fa-chart-area"></i>
    <span>Products Pickup Orders</span></a>
</li>

<!-- Nav Item - Tables -->
<li class="nav-item">
  <a class="nav-link" href="">
    <i class="fas fa-fw fa-table"></i>
    <span>Product Delivery Orders</span></a>
</li>


<!-- Nav Item - Charts -->
<li class="nav-item">
  <a class="nav-link" href="Productstore_completed_orders.php">
    <i class="fas fa-fw fa-chart-area"></i>
    <span>Completed Orders</span></a>
</li>

<!-- Nav Item - Tables -->
<li class="nav-item">
  <a class="nav-link" href="Productstore_cancelled_orders.php">
    <i class="fas fa-fw fa-table"></i>
    <span>Cancelled Orders</span></a>
</li>





<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<!-- Heading -->
<div class="sidebar-heading">
  Promoting
</div>

<!-- Nav Item - Tables -->
<li class="nav-item">
  <a class="nav-link" href="productstore_acceptedproducts.php">
    <i class="fas fa-fw fa-table"></i>
    <span>Product's Overview</span></a>
</li>


<!-- Nav Item - Charts -->
<li class="nav-item">
  <a class="nav-link" href="productstore_promoteproducts.php">
    <i class="fas fa-fw fa-chart-area"></i>
    <span>Promotes Product</span></a>
</li>

<!-- Nav Item - Tables -->
<li class="nav-item">
  <a class="nav-link" href="productstore_influencerRequest.php">
    <i class="fas fa-fw fa-table"></i>
    <span>Influencer's  Request</span></a>
</li>




<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
  <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>



</ul>
<!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
          <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form>


          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

            <!-- Nav Item - Alerts -->
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i>
                <!-- Counter - Alerts -->
              <span class="badge badge-danger badge-counter">
                <?php

                $store_email = $_SESSION['productusername'];

                require 'dbconfig.php';
                                                    
                $get_productstore_details = "Select * from food_company where company_email='$store_email'";
                $run_productstore_details = mysqli_query($connection, $get_productstore_details);
                $fetch_productstore_details = mysqli_fetch_array($run_productstore_details);
                                                                                                                
                $productstore_id = $fetch_productstore_details['fcompany_id']; //:: Customer id 
                $productstore_name = $fetch_productstore_details['Company_name']; //:: customer name

                //:: get the number of rows which relates with current user. 
                $query = "Select * from store_promote_request where spr_hotel_id = '$productstore_id' AND spr_status = 'Request Pending'";
                $run_query = mysqli_query($connection, $query);
                $count_rows = mysqli_num_rows($run_query);

                echo "$count_rows";
               
                ?>
              
              </span>
              </a>
              <!-- Dropdown - Alerts -->

              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                  INFLUENCER REQUESTS
                </h6>

                <?php 

                //: Getting current foodstore id: 

                $store_email = $_SESSION['productusername'];

                require 'dbconfig.php';
                                                    
                $get_productstore_details = "Select * from food_company where company_email='$store_email'";
                $run_productstore_details = mysqli_query($connection, $get_productstore_details);
                $fetch_productstore_details = mysqli_fetch_array($run_productstore_details);
                                                                                                                
                $productstore_id = $fetch_productstore_details['fcompany_id']; //:: Customer id 
                $productstore_name = $fetch_productstore_details['Company_name']; //:: customer name


                //: Geteting items from table store_promote_product

                $query = "Select * from store_promote_request where spr_hotel_id = '$productstore_id' AND spr_status = 'Request Pending' ORDER BY spr_date DESC";
                $run_query = mysqli_query($connection, $query);

                while($fetch_query = mysqli_fetch_array($run_query)){

                    $influencer_id = $fetch_query['spr_influencer_id'];
                    $item_id = $fetch_query['spr_item_id'];
                    $date = $fetch_query['spr_date'];

                    //: Get influencer Details
                    $getinfluencer_details = "Select * from food_company where fcompany_id = '$influencer_id'";
                    $runinfluencer_details = mysqli_query($connection, $getinfluencer_details);
                    $fetchinfluencer_details = mysqli_fetch_array($runinfluencer_details);

                    $influencer_email = $fetchinfluencer_details['company_email'];

                    //: Getting influncer image and name from influencer table. 

                    $getinfluencer_data = "Select * from influencer where i_email = '$influencer_email' ";
                    $runinfluencer_data = mysqli_query($connection, $getinfluencer_data);
                    $fetchinfluencer_data = mysqli_fetch_assoc($runinfluencer_data); 

                    $influencerdata_name = $fetchinfluencer_data['i_name'];
                    $influencerdata_image = $fetchinfluencer_data['i_image'];


                    //: Getting item name from table food_item

                    $getitem_details = "Select * from food_items where item_id = '$item_id'";
                    $runitem_details = mysqli_query($connection, $getitem_details);
                    $fetchitem_details = mysqli_fetch_array($runitem_details);

                    $itemname = $fetchitem_details['item_title'];

                   
                    echo " <a class='dropdown-item d-flex align-items-center' href='productstore_influencerRequest.php'>
                    <div class='mr-3'>
                      <div class='icon-circle bg-primary'>
                       
                        <img class='img-profile rounded-circle' src='influencer_images/$influencerdata_image' height='60' width='60'>
                       
                      </div>
                    </div>
                    <div>
                      <div class='small text-gray-500'>$date</div>
                      <span class=''> <strong>$influencerdata_name</strong> wants to Promote your item <strong>[$itemname]</strong></span>
                    </div>
                  </a>";


                   

                    

                }







                ?>

               

                <a class="dropdown-item text-center small text-gray-500" href="productstore_influencerRequest.php">Show All Request</a>
              </div>
            </li>

            <!-- Nav Item - Messages -->
            <!-- <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-envelope fa-fw"></i> -->
                <!-- Counter - Messages -->
                <!-- <span class="badge badge-danger badge-counter">7</span>
              </a> -->
              <!-- Dropdown - Messages -->
              <!-- <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                <h6 class="dropdown-header">
                  Message Center
                </h6>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/fn_BT9fwg_E/60x60" alt="">
                    <div class="status-indicator bg-success"></div>
                  </div>
                  <div class="font-weight-bold">
                    <div class="text-truncate">Hi there! I am wondering if you can help me with a problem I've been having.</div>
                    <div class="small text-gray-500">Emily Fowler · 58m</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/AU4VPcFN4LE/60x60" alt="">
                    <div class="status-indicator"></div>
                  </div>
                  <div>
                    <div class="text-truncate">I have the photos that you ordered last month, how would you like them sent to you?</div>
                    <div class="small text-gray-500">Jae Chun · 1d</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/CS2uCrpNzJY/60x60" alt="">
                    <div class="status-indicator bg-warning"></div>
                  </div>
                  <div>
                    <div class="text-truncate">Last month's report looks great, I am very happy with the progress so far, keep up the good work!</div>
                    <div class="small text-gray-500">Morgan Alvarez · 2d</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60" alt="">
                    <div class="status-indicator bg-success"></div>
                  </div>
                  <div>
                    <div class="text-truncate">Am I a good boy? The reason I ask is because someone told me that people say this to all dogs, even if they aren't good...</div>
                    <div class="small text-gray-500">Chicken the Dog · 2w</div>
                  </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
              </div> -->
            <!-- </li> -->

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                  
               <?php echo $_SESSION['productusername']; ?>
                  
                </span>
                <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="foodstore_profile.php">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Settings
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Activity Log
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->


  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  
  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>

          <form action="logout.php" method="POST"> 
          
            <button type="submit" name="productlogout_btn" class="btn btn-primary">Logout</button>

          </form>


        </div>
      </div>
    </div>
  </div>