<?php
include('security.php');
include('includes/header.php'); 
include('includes/navbar.php'); 

?>




<div class="container">

    <div class="row justify-content-center">
        <div class="col-xl-10">
            <div class= "row">
                <div class="col-xl-9">
                    <div id="general" class="mb-5">

                        <h4 class="text-gray-900"> 
                            <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-900"></i>
                            General
                        </h4>
                        <p class="text-gray-900 text-s">  View and update your general account information and settings. </p>
                        <div class = "card">
                                <div class="list-group list-group-flush">


                                    <!-- PHP CODE -->

                                    <?php

                                    // : Getting FoodStore ID
                                    require 'dbconfig.php';

                                    $influencer_email = $_SESSION['username'];

                                                                
                                    $get_owner_details = "Select * from food_company where company_email='$influencer_email'";
                                    $run_owner_details = mysqli_query($connection, $get_owner_details);
                                    $fetch_owner_details = mysqli_fetch_array($run_owner_details);
                                                                                                                            
                                    $influencer_owner_name = $fetch_owner_details['owner_name']; //:: Customer id 
                                    $influencer_owner_ic = $fetch_owner_details['owner_ic']; //:: customer name
                                    $influencer_contactnumber1 = $fetch_owner_details['contact_number1'];
                                    $influencer_contactnumber2 = $fetch_owner_details['contact_number2'];
                                    $influencer_owner_img = $fetch_owner_details['owner_img'];
                                    
                                    
                                   


                                    ?>

                                    
                                                
                                                  

                                                    <?php
                                                        if(isset($_SESSION['success']) && $_SESSION['success']!=''){
                                                            ?>

                                                                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                                <?php 
                                                                echo ''.$_SESSION['success'].'';
                                                                ?>
                                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                                </div>

                                                            <?php
                                                            unset($_SESSION['success']);
                                                        } 
                                                        if(isset($_SESSION['status']) && $_SESSION['status']!=''){
                                                            ?>

                                                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                            <?php 
                                                            echo ''.$_SESSION['success'].'';
                                                            ?>
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                            </div>

                                                        <?php
                                                        unset($_SESSION['status']);
                                                        } 
                                                        
                                                    ?>

                                    


                                    
                                    <div class="list-group-item d-flex align-items-center">        
                                        <div class="media text-muted pt-3">
                                            <!-- <img src="influencer_images/<?php echo "$influencer_image" ?>" width="100px;" height="100px;" alt="image"></img> -->
                                            <div class="flex-fill">
                                                <h4 class="text-gray-900">&nbsp; &nbsp; <b><?php echo "$influencer_owner_name" ?></b></h4>
                                                <div class="text-gray-600">
                                                    &nbsp; &nbsp;  &nbsp; <?php echo "$influencer_email" ?>
                                                </div>
                                              
                                                
                                            </div>   
                                        </div>
                                    </div>
                                    
                                    <!-- NAME -->
                                    <div class="list-group-item d-flex align-items-center">
                                        <div class="flex-fill">
                                            <div class="text-gray-800">Name</div>
                                            <div class="text-gray-600"><?php echo "$influencer_owner_name" ?> </div>
                                        </div>
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editname">Edit</button>
                                        </div>
                                    </div> 

                                    <!-- EMAIL -->
                                    <!-- <div class="list-group-item d-flex align-items-center">
                                        <div class="flex-fill">
                                        <div class="text-gray-800">Email</div>
                                            <div class="text-gray-600">Mathankumar67@outlook.com</div>
                                        </div>
                                        <div class="width-100">
                                        <button type="button" class="btn width-100 btn-primary">Edit</button>
                                        </div>
                                    </div>  -->
                                    
                                    <!-- PROFILE PICTURE -->
                                    <!-- <div class="list-group-item d-flex align-items-center">
                                        <div class="flex-fill">
                                            <div class="text-gray-800">Profile Picture</div>
                                        </div>
                                        <div class="width-100">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editprofilepicture">Edit</button>
                                        </div>
                                    </div>  --> 
                                    
                                    <!-- PERSONAL IC -->
                                    <div class="list-group-item d-flex align-items-center">
                                        <div class="flex-fill">
                                            <div class="text-gray-800">Personal IC</div>
                                            <div class="text-gray-600"><?php echo "$influencer_owner_ic" ?></div>
                                        </div>
                                        <div class="width-100">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editpersonalic">Edit</button>
                                        </div>
                                    </div>
                                    
                                    <!-- CONTACT NUMBER 1 -->
                                    <div class="list-group-item d-flex align-items-center">
                                        <div class="flex-fill">
                                            <div class="text-gray-800">Contact number 1</div>
                                            <div class="text-gray-600"><?php echo "$influencer_contactnumber1" ?></div>
                                        </div>
                                        <div class="width-100">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editcontactnum1">Edit</button>
                                        </div>
                                    </div>
                                    
                                    <!-- CONTACT NUMBER 2 -->
                                    <div class="list-group-item d-flex align-items-center">
                                        <div class="flex-fill">
                                            <div class="text-gray-800">Contact number 2</div>
                                            <div class="text-gray-600"><?php echo "$influencer_contactnumber2" ?></div>
                                        </div>
                                        <div class="width-100">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editcontactnum2">Edit</button>
                                        </div>
                                    </div>
                            
                                    <!-- OWNER IMG -->
                                    <div class="list-group-item d-flex align-items-center">
                                        <div class="flex-fill">
                                            <div class="text-gray-800">Profile Picture</div>
                                        </div>
                                        <div class="width-100">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editownerimg">Edit</button>
                                        </div>
                                    </div>   

                                </div>
                        </div>
                    
                    
                    
                    
                    <div id="business" class="mb-5">
                    <h4 class="text-gray-900 md-5"> 
                            <i class="fa fa-briefcase fa-sm da-fw mr-2 text-gray-900" aria-hidden="true"></i>
                            Business Information
                        </h4>
                        <p class="text-gray-900 text-s">  View and update your general business information and settings. </p>
                        <div class = "card">
                                <div class="list-group list-group-flush">


                                    <!-- PHP CODE -->

                                    <?php

                                    // : Getting FoodStore ID
                                    require 'dbconfig.php';

                                    $influencer_email = $_SESSION['username'];

                                                                
                                    $get_influencer_details = "Select * from food_company where company_email='$influencer_email'";
                                    $run_influencer_details = mysqli_query($connection, $get_influencer_details);
                                    $fetch_influencer_details = mysqli_fetch_array($run_influencer_details);
                                                                                                                            
                                    $influencer_id = $fetch_influencer_details['fcompany_id']; //:: Customer id 
                                    $influencer_name = $fetch_influencer_details['Company_name']; //:: customer name
                                    $influencer_address = $fetch_influencer_details['company_address'];
                                    $influencer_ssn_number = $fetch_influencer_details['ssn_number'];        
                                    $influencer_insurance = $fetch_influencer_details['insurance'];
                                    $influencer_bcontact_number = $fetch_influencer_details['bcontact_number'];
                                    //$influencer_address = $fetch_influencer_details['company_address']; business pitcure ah kaifee dont forget
                                    
                                   


                                    ?>

                                    
                                                
                                                  

                                                    <?php
                                                        if(isset($_SESSION['success']) && $_SESSION['success']!=''){
                                                            ?>

                                                                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                                <?php 
                                                                echo ''.$_SESSION['success'].'';
                                                                ?>
                                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                                </div>

                                                            <?php
                                                            unset($_SESSION['success']);
                                                        } 
                                                        if(isset($_SESSION['status']) && $_SESSION['status']!=''){
                                                            ?>

                                                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                            <?php 
                                                            echo ''.$_SESSION['success'].'';
                                                            ?>
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                            </div>

                                                        <?php
                                                        unset($_SESSION['status']);
                                                        } 
                                                        
                                                    ?>

        
                                    <!-- SHOP NAME -->
                                    <div class="list-group-item d-flex align-items-center">
                                        <div class="flex-fill">
                                            <div class="text-gray-800">Shop Name</div>
                                            <div class="text-gray-600" value="owner_name"><?php echo "$influencer_name" ?></div>
                                        </div>
                                        <div class="width-100">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editstorename">Edit</button>
                                        </div>
                                    </div> 

                                    <!-- EMAIL -->
                                    <!-- <div class="list-group-item d-flex align-items-center">
                                        <div class="flex-fill">
                                        <div class="text-gray-800">Email</div>
                                            <div class="text-gray-600">Mathankumar67@outlook.com</div>
                                        </div>
                                        <div class="width-100">
                                        <button type="button" class="btn width-100 btn-primary">Edit</button>
                                        </div>
                                    </div>  -->

                                    <!-- SHOP ADDRESS -->
                                    <div class="list-group-item d-flex align-items-center">
                                        <div class="flex-fill">
                                            <div class="text-gray-800">Shop Physical Address</div>
                                            <div class="text-gray-600"><?php echo "$influencer_address" ?></div>
                                        </div>
                                        <div class="width-100">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editaddress">Edit</button>
                                        </div>
                                    </div>
                                    
                                    <!-- PROFILE PICTURE -->
                                    <!-- <div class="list-group-item d-flex align-items-center">
                                        <div class="flex-fill">
                                            <div class="text-gray-800">Profile Picture</div>
                                        </div>
                                        <div class="width-100">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editprofilepicture">Edit</button>
                                        </div>
                                    </div>  -->

                                    <!-- SSN NUMBER -->
                                    <div class="list-group-item d-flex align-items-center">
                                        <div class="flex-fill">
                                            <div class="text-gray-800">Ssn number</div>
                                            <div class="text-gray-600"><?php echo "$influencer_ssn_number" ?></div>
                                        </div>
                                        <div class="width-100">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editstoressn">Edit</button>
                                        </div>
                                    </div> 
                                    
                                    <!-- INSURANCE -->
                                    <div class="list-group-item d-flex align-items-center">
                                        <div class="flex-fill">
                                            <div class="text-gray-800">Insurance</div>
                                            <div class="text-gray-600"><?php echo "$influencer_insurance" ?></div>
                                        </div>
                                        <div class="width-100">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editstoreinsurance">Edit</button>
                                        </div>
                                    </div>
                                    
                                    <!-- Business Contact Number -->
                                    <div class="list-group-item d-flex align-items-center">
                                        <div class="flex-fill">
                                            <div class="text-gray-800">Business contact number</div>
                                            <div class="text-gray-600"><?php echo "$influencer_bcontact_number" ?></div>
                                        </div>
                                        <div class="width-100">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editstorebusinessnum">Edit</button>
                                        </div>
                                    </div> 

                                </div>
                        </div>
                   </div>
                    
                    
                <div id="bank" class="mb-5">
                    <h4 class="text-gray-900 md-5"> 
                            <i class="fa fa-university fa-sm da-fw mr-2 text-gray-900" aria-hidden="true"></i>
                            Payment Infromation
                        </h4>
                        <p class="text-gray-900 text-s">  View and update your payment information and settings. </p>
                        <div class = "card">
                                <div class="list-group list-group-flush">


                                    <!-- PHP CODE -->

                                    <?php

                                    // : Getting FoodStore ID
                                    require 'dbconfig.php';

                                    $influencer_email = $_SESSION['username'];

                                                                
                                    $get_bank_details = "Select * from food_company where company_email='$influencer_email'";
                                    $run_bank_details = mysqli_query($connection, $get_bank_details);
                                    $fetch_bank_details = mysqli_fetch_array($run_bank_details);
                                                                                                                            
                                    $influencer_bname =  $fetch_bank_details['bank_name']; 
                                    $influencer_acc_name =  $fetch_bank_details['acc_name']; 
                                    $influencer_acc_number =  $fetch_bank_details['acc_num'];        
                                   


                                    ?>

                                    
                                                
                                                  

                                                    <?php
                                                        if(isset($_SESSION['success']) && $_SESSION['success']!=''){
                                                            ?>

                                                                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                                <?php 
                                                                echo ''.$_SESSION['success'].'';
                                                                ?>
                                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                                </div>

                                                            <?php
                                                            unset($_SESSION['success']);
                                                        } 
                                                        if(isset($_SESSION['status']) && $_SESSION['status']!=''){
                                                            ?>

                                                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                            <?php 
                                                            echo ''.$_SESSION['success'].'';
                                                            ?>
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                            </div>

                                                        <?php
                                                        unset($_SESSION['status']);
                                                        } 
                                                        
                                                    ?>

                                              
                                                
                                            </div>   
                                        </div>
                                   
                                    
                                    <!-- BANK NAME -->
                                    <div class="list-group-item d-flex align-items-center">
                                        <div class="flex-fill">
                                            <div class="text-gray-800">Bank Name</div>
                                            <div class="text-gray-600"><?php echo "$influencer_bname" ?></div>
                                        </div>
                                        <div class="width-100">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editbankname">Edit</button>
                                        </div>
                                    </div> 

                                    <!-- EMAIL -->
                                    <!-- <div class="list-group-item d-flex align-items-center">
                                        <div class="flex-fill">
                                        <div class="text-gray-800">Email</div>
                                            <div class="text-gray-600">Mathankumar67@outlook.com</div>
                                        </div>
                                        <div class="width-100">
                                        <button type="button" class="btn width-100 btn-primary">Edit</button>
                                        </div>
                                    </div>  -->

                                    <!-- BANK ACCOUNT NAME -->
                                    <div class="list-group-item d-flex align-items-center">
                                        <div class="flex-fill">
                                            <div class="text-gray-800">Bank Account Name</div>
                                            <div class="text-gray-600"><?php echo "$influencer_acc_name" ?></div>
                                        </div>
                                        <div class="width-100">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editbankaccname">Edit</button>
                                        </div>
                                    </div>
                                    
                                    <!-- PROFILE PICTURE -->
                                    <!-- <div class="list-group-item d-flex align-items-center">
                                        <div class="flex-fill">
                                            <div class="text-gray-800">Profile Picture</div>
                                        </div>
                                        <div class="width-100">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editprofilepicture">Edit</button>
                                        </div>
                                    </div>  -->

                                    <!-- BANK ACCOUNT NUMBER -->
                                    <div class="list-group-item d-flex align-items-center">
                                        <div class="flex-fill">
                                            <div class="text-gray-800">Bank Account Number</div>
                                            <div class="text-gray-600"><?php echo "$influencer_acc_number" ?></div>
                                        </div>
                                        <div class="width-100">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editbankaccnum">Edit</button>
                                        </div>
                                    </div>
                                </div>
                            
                   
                    
                    
                    
                    <div id="notification" class="mb-5">
                        <h4 class="text-gray-900"> 
                            <i class="fas fa-bell fa-sm fa-fw mr-2 text-gray-900" aria-hidden="true"></i>
                            Notifications
                        </h4>
                        <p class="text-gray-900 text-s">  Enable or  disable what notifications you want to see. </p>
                        <div class = "card">
                            <div class="list-group list-group-flush">
                                <div class="list-group-item d-flex align-items-center">
                                    <div class="flex-fill">
                                        <div class="text-gray-800">Product Reviews</div>
                                        <div class="text-gray-600">(Enabled)</div>
                                        <!-- <a href="" class="btn btn-success btn-icon-split">
                                        <span class="icon text-white-50">
                                            <i class="fas fa-check"></i>
                                        </span>
                                        <span class="text">Enabled</span>
                                        </a> -->
                                    </div>
                                    <div class="width-100">
                                    
                                    <label class="switch">
                                    <input type="checkbox" checked>
                                    <span class="slider round"></span>
                                    </label>
                                    <!-- <button type="button" class="btn width-100 btn-primary">Edit</button> -->
                                    </div>
                                </div> 
                                <div class="list-group-item d-flex align-items-center">
                                    <div class="flex-fill">
                                    <div class="text-gray-800">New Orders</div>
                                        <div class="text-gray-600">(Enabled)</div>
                                    </div>
                                    <div class="width-100">
                                    <label class="switch">
                                    <input type="checkbox" checked>
                                    <span class="slider round"></span>
                                    </label>
                                    <!-- <button type="button" class="btn width-100 btn-primary">Edit</button> -->
                                    </div>
                                </div> 
                                <div class="list-group-item d-flex align-items-center">
                                    <div class="flex-fill">
                                        <div class="text-gray-800">Sale Report</div>
                                        <div class="text-gray-600">
                                        <div class = "btn btn-danger btn-circle-xs">
                                            </div>
                                        (Disabled)</div>
                                    </div>
                                    <div class="width-100">
                                    <label class="switch">
                                    <input type="checkbox" checked>
                                    <span class="slider round"></span>
                                    </label>
                                    <!-- <button type="button" class="btn width-100 btn-primary">Edit</button> -->
                                    </div>
                                </div> 
                                <div class="list-group-item d-flex align-items-center">
                                    <div class="flex-fill">
                                        <div class="text-gray-800">Influencers Request</div>
                                        <div class="text-gray-600">
                                            <div class = "btn btn-success btn-circle-xs">
                                            </div>
                                            (Enabled)
                                        </div>
                                    </div>
                                    <div class="width-100">
                                    <label class="switch">
                                    <input type="checkbox" checked>
                                    <span class="slider round"></span>
                                    </label>
                                    <!-- <button type="button" class="btn width-100 btn-primary">Edit</button> -->
                                    </div>
                                </div> 
                                <!-- <div class="list-group-item d-flex align-items-center">
                              
                                    <img src="influencer_images/vishal.jpg" width="100px;" height="100px;" alt="image">

                                    </img>
                            
                               
                                    
                                </div>  -->

                            </div>
                        </div>
                    </div>         
                </div>

                    

                <div class="col-xl-3">
                    <div class="navbar navbar-sticky d-none d-xl-block">
                        <ul class="nav">
                            <li class="nav-item active">
                                <a href="#general" target="_self" class="nav-link active">General</a>
                            </li>
                            <li class="nav item active">
                                <a href="#business" target="_self" class="nav-link active">Business Info</a>
                            </li>
                            <li class="nav-item active">
                                <a href="#bank" target="_self" class="nav-link active">Bank Info</a>
                            </li>
                            <li class="nav item active">
                                <a href="#notification" target="_self" class="nav-link active">Notifications</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

         

<!-- MODALS -->

<!-- NAME MODAL -->
<div class="modal fade" id="editname" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Name</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="code.php" method="POST">

        <div class="modal-body">

            <div class="form-group">
                <label> Name: </label>
                <div class="row row-space-10">
                    <div class = "col-12">
                        <input type="text" name="store_updatename" class="form-control"  required="required" placeholder="Username">
                    </div>
                    <!--
                    <div class = "col-6">
                        <input type="text" name="store_updatelname" class="form-control"  required="required" placeholder="Last Name">
                    </div>
-->
                </div>
                <br>
               
                <div class="alert alert-dark" role="alert">
                    <b>Please note: </b>
                    Don't add any unusual capatalization, punctuation,  characters or random words.
                </div>
            </div>
           
        </div>
        <div class="modal-footer">
            <input type="hidden" class="form-control"  value= "<?php echo $influencer_email ?>" name="influencer_email">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="modifystore_1" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>

<!-- PERSONAL IC -->
<div class="modal fade" id="editpersonalic" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Personal IC</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="code.php" method="POST">

        <div class="modal-body">

            <div class="form-group">
                <label> Personal IC: </label>
                <div class="row row-space-10">
                    <div class = "col-12">
                        <input type="text" name="store_updateic" class="form-control"  required="required" placeholder="Personal IC">
                    </div>
                    <!--
                    <div class = "col-6">
                        <input type="text" name="store_updatelname" class="form-control"  required="required" placeholder="Last Name">
                    </div>
-->
                </div>
                <br>
               
                <div class="alert alert-dark" role="alert">
                    <b>Please note: </b>
                    Don't add any unusual capatalization, punctuation,  characters or random words.
                </div>
            </div>
           
        </div>
        <div class="modal-footer">
            <input type="hidden" class="form-control"  value= "<?php echo $influencer_email ?>" name="influencer_email">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="modifystore_2" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>

<!-- CONTACT NUMBER 1 -->
<div class="modal fade" id="editcontactnum1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Contact Number 1</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="code.php" method="POST">

        <div class="modal-body">

            <div class="form-group">
                <label> Contact Number: </label>
                <div class="row row-space-10">
                    <div class = "col-12">
                        <input type="text" name="store_updatecontact1" class="form-control"  required="required" placeholder="Contact Number">
                    </div>
                    <!--
                    <div class = "col-6">
                        <input type="text" name="store_updatelname" class="form-control"  required="required" placeholder="Last Name">
                    </div>
-->
                </div>
                <br>
               
                <div class="alert alert-dark" role="alert">
                    <b>Please note: </b>
                    Don't add any unusual capatalization, punctuation,  characters or random words.
                </div>
            </div>
           
        </div>
        <div class="modal-footer">
            <input type="hidden" class="form-control"  value= "<?php echo $influencer_email ?>" name="influencer_email">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="modifystore_3" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>


<!-- CONTACT NUMBER 2 -->
<div class="modal fade" id="editcontactnum2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Contact Number 2</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="code.php" method="POST">

        <div class="modal-body">

            <div class="form-group">
                <label> Contact Number: </label>
                <div class="row row-space-10">
                    <div class = "col-12">
                        <input type="text" name="store_updatecontact2" class="form-control"  required="required" placeholder="Contact Number">
                    </div>
                    <!--
                    <div class = "col-6">
                        <input type="text" name="store_updatelname" class="form-control"  required="required" placeholder="Last Name">
                    </div>
-->
                </div>
                <br>
               
                <div class="alert alert-dark" role="alert">
                    <b>Please note: </b>
                    Don't add any unusual capatalization, punctuation,  characters or random words.
                </div>
            </div>
           
        </div>
        <div class="modal-footer">
            <input type="hidden" class="form-control"  value= "<?php echo $influencer_email ?>" name="influencer_email">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="modifystore_4" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>

<!-- OWNER PICTURE -->
<div class="modal fade" id="editownerimg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Profile Picture</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="code.php" method="POST" enctype="multipart/form-data">

        <div class="modal-body">

            <div class="form-group">
                <label> New Image: </label>
                <div class="row row-space-10">
                    <div class = "col-12">
                        <input type="file" name="store_updateownerimg" class="form-control"  required="required">
                    </div>
                </div>
                <br>
                <!-- <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="inputZip">Food Image</label>
                        <input type="file" class="form-control" name="food_updateimg">
                    </div>
                </div>
                -->
                <div class="alert alert-dark" role="alert">
                    <b>Your Current Picture: </b>
                    <br>
                    <img src="owner_images/<?php echo "$influencer_owner_img" ?>" width="100px;" height="100px;" alt="image">
                </div>
            </div>
           
        </div>
        <div class="modal-footer">
            <input type="hidden" class="form-control"  value= "<?php echo $influencer_email ?>" name="influencer_email">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="modifystore_5" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>


<!-- SHOP NAME -->
<div class="modal fade" id="editstorename" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Shop Name</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="code.php" method="POST">

        <div class="modal-body">

            <div class="form-group">
                <label>Shop Name: </label>
                <div class="row row-space-10">
                    <div class = "col-12">
                        <input type="text" name="store_updatestorename" class="form-control"  required="required" placeholder="Store name">
                    </div>
                    <!--
                    <div class = "col-6">
                        <input type="text" name="store_updatelname" class="form-control"  required="required" placeholder="Last Name">
                    </div>
-->
                </div>
                <br>
               
                <div class="alert alert-dark" role="alert">
                    <b>Please note: </b>
                    Don't add any unusual capatalization, punctuation,  characters or random words.
                </div>
            </div>
           
        </div>
        <div class="modal-footer">
            <input type="hidden" class="form-control"  value= "<?php echo $influencer_email ?>" name="influencer_email">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="modifystore_6" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>



<!-- ADDRESS MODAL -->
<div class="modal fade" id="editaddress" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Address</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="code.php" method="POST">

        <div class="modal-body">

            <div class="form-group">
                <label> Full Address: </label>
                <div class="row row-space-10">
                    <div class = "col-12">
                        <input type="text" name="store_updateaddress" class="form-control"  required="required" placeholder="First Name">
                    </div>
                </div>
                <br>
               
                <div class="alert alert-dark" role="alert">
                    <b>Example: </b>
                   E-10-02, One South, 43300 Seri Kembangan, Selangor, Malaysia.
                </div>
            </div>
           
        </div>
        <div class="modal-footer">
            <input type="hidden" class="form-control"  value= "<?php echo $influencer_email ?>" name="influencer_email">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="modifystore_7" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>

<!-- SSN -->
<div class="modal fade" id="editstoressn" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit SSN</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="code.php" method="POST">

        <div class="modal-body">

            <div class="form-group">
                <label>SSN: </label>
                <div class="row row-space-10">
                    <div class = "col-12">
                        <input type="text" name="store_updatessn" class="form-control"  required="required" placeholder="SSN">
                    </div>
                    <!--
                    <div class = "col-6">
                        <input type="text" name="store_updatelname" class="form-control"  required="required" placeholder="Last Name">
                    </div>
-->
                </div>
                <br>
               
                <div class="alert alert-dark" role="alert">
                    <b>Please note: </b>
                    Don't add any unusual capatalization, punctuation,  characters or random words.
                </div>
            </div>
           
        </div>
        <div class="modal-footer">
            <input type="hidden" class="form-control"  value= "<?php echo $influencer_email ?>" name="influencer_email">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="modifystore_8" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>

<!-- INSURANCE -->
<div class="modal fade" id="editstoreinsurance" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Insurance</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="code.php" method="POST">

        <div class="modal-body">

            <div class="form-group">
                <label>Insurance: </label>
                <div class="row row-space-10">
                    <div class = "col-12">
                        <input type="text" name="store_updateinsurance" class="form-control"  required="required" placeholder="Insurance">
                    </div>
                    <!--
                    <div class = "col-6">
                        <input type="text" name="store_updatelname" class="form-control"  required="required" placeholder="Last Name">
                    </div>
-->
                </div>
                <br>
               
                <div class="alert alert-dark" role="alert">
                    <b>Please note: </b>
                    Don't add any unusual capatalization, punctuation,  characters or random words.
                </div>
            </div>
           
        </div>
        <div class="modal-footer">
            <input type="hidden" class="form-control"  value= "<?php echo $influencer_email ?>" name="influencer_email">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="modifystore_9" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>

<!-- BUSINESS CONTACT NUM -->
<div class="modal fade" id="editstorebusinessnum" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Business Contact Number</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="code.php" method="POST">

        <div class="modal-body">

            <div class="form-group">
                <label>Contact Number: </label>
                <div class="row row-space-10">
                    <div class = "col-12">
                        <input type="text" name="store_updatestorenum" class="form-control"  required="required" placeholder="Contact Number">
                    </div>
                    <!--
                    <div class = "col-6">
                        <input type="text" name="store_updatelname" class="form-control"  required="required" placeholder="Last Name">
                    </div>
-->
                </div>
                <br>
               
                <div class="alert alert-dark" role="alert">
                    <b>Please note: </b>
                    Don't add any unusual capatalization, punctuation,  characters or random words.
                </div>
            </div>
           
        </div>
        <div class="modal-footer">
            <input type="hidden" class="form-control"  value= "<?php echo $influencer_email ?>" name="influencer_email">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="modifystore_10" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>

<!-- PROFILE PICTURE -->
<!--
<div class="modal fade" id="editprofilepicture" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Profile Picture</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="influencerstore_code.php" method="POST" enctype="multipart/form-data">

        <div class="modal-body">

            <div class="form-group">
                <label> New Image: </label>
                <div class="row row-space-10">
                    <div class = "col-12">
                        <input type="file" name="influencer_updateimage" class="form-control"  required="required">
                    </div>
                </div>
                <br>
                <!-- <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="inputZip">Food Image</label>
                        <input type="file" class="form-control" name="food_updateimg">
                    </div>
                </div>
                
                <div class="alert alert-dark" role="alert">
                    <b>Your Current Picture: </b>
                    <br>
                    <img src="influencer_images/<?php echo "$influencer_image" ?>" width="100px;" height="100px;" alt="image">
                </div>
            </div>
           
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="mofifyinfluencer_image" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>
-->
    
<!-- BANK NAME -->
<div class="modal fade" id="editbankname" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Bank Name</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="code.php" method="POST">

        <div class="modal-body">

            <div class="form-group">
                <label>Bank Name: </label>
                <div class="row row-space-10">
                    <div class = "col-12">
                        <input type="text" name="store_updatebankname" class="form-control"  required="required" placeholder="Bank name">
                    </div>
                    <!--
                    <div class = "col-6">
                        <input type="text" name="store_updatelname" class="form-control"  required="required" placeholder="Last Name">
                    </div>
-->
                </div>
                <br>
               
                <div class="alert alert-dark" role="alert">
                    <b>Please note: </b>
                    Don't add any unusual capatalization, punctuation,  characters or random words.
                </div>
            </div>
           
        </div>
        <div class="modal-footer">
            <input type="hidden" class="form-control"  value= "<?php echo $influencer_email ?>" name="influencer_email">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="modifystore_11" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>

    
<!-- BANK ACC NAME -->
<div class="modal fade" id="editbankaccname" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Bank Account Name</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="code.php" method="POST">

        <div class="modal-body">

            <div class="form-group">
                <label>Account Name: </label>
                <div class="row row-space-10">
                    <div class = "col-12">
                        <input type="text" name="store_updateaccname" class="form-control"  required="required" placeholder="Account name">
                    </div>
                    <!--
                    <div class = "col-6">
                        <input type="text" name="store_updatelname" class="form-control"  required="required" placeholder="Last Name">
                    </div>
-->
                </div>
                <br>
               
                <div class="alert alert-dark" role="alert">
                    <b>Please note: </b>
                    Don't add any unusual capatalization, punctuation,  characters or random words.
                </div>
            </div>
           
        </div>
        <div class="modal-footer">
            <input type="hidden" class="form-control"  value= "<?php echo $influencer_email ?>" name="influencer_email">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="modifystore_12" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>

    
<!-- BANK ACC NUM -->
<div class="modal fade" id="editbankaccnum" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Bank Account Number</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="code.php" method="POST">

        <div class="modal-body">

            <div class="form-group">
                <label>Account Number: </label>
                <div class="row row-space-10">
                    <div class = "col-12">
                        <input type="text" name="store_updateaccnum" class="form-control"  required="required" placeholder="Account Number">
                    </div>
                    <!--
                    <div class = "col-6">
                        <input type="text" name="store_updatelname" class="form-control"  required="required" placeholder="Last Name">
                    </div>
-->
                </div>
                <br>
               
                <div class="alert alert-dark" role="alert">
                    <b>Please note: </b>
                    Don't add any unusual capatalization, punctuation,  characters or random words.
                </div>
            </div>
           
        </div>
        <div class="modal-footer">
            <input type="hidden" class="form-control"  value= "<?php echo $influencer_email ?>" name="influencer_email">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="modifystore_13" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>




<!-- EDIT PASSWORD -->
<div class="modal fade" id="editpassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="code.php" method="POST" enctype="multipart/form-data">

        <div class="modal-body">

            <div class="form-group">
                <label> Current Password: </label>
                <div class="row row-space-10">
                    <div class = "col-12">
                        <input type="text" name="current_pass" class="form-control"  required="required" placeholder="Last Name">
                    </div>
                </div>
                <br>
                <label> New Password: </label>
                <div class="row row-space-10">
                    <div class = "col-6">
                        <input type="text" name="new_pass" class="form-control"  required="required" placeholder="New Password">
                    </div>
                    <div class = "col-6">
                        <input type="text" name="confirm_pass" class="form-control"  required="required" placeholder="Confirm Password">
                    </div>
                </div>
                <br>
            </div>
           
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="modifystore_password" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>




<?php
include('includes/scripts.php');
include('includes/footer.php');
?>