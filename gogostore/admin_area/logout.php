<?php 

session_start();
//:: Admin Logout..

if(isset($_POST['adminlogout_btn'])){
    session_destroy();
    unset($_SESSION['username']);
    header('Location: login.php');
}else if (isset($_POST['foodlogout_btn'])){
    session_destroy();
    unset($_SESSION['foodusername']);
    header('Location: login.php');
}else if (isset($_POST['productlogout_btn'])){
    session_destroy();
    unset($_SESSION['productusername']);
    header('Location: login.php');
}else if (isset($_POST['influencerlogout_btn'])){
    session_destroy();
    unset($_SESSION['influencerusername']);
    header('Location: login.php');
}else {
    
}
?>
