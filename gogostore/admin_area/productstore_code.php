<?php

include('productstore_security.php');
include('dbconfig.php');


//: Getting current foodstore id: 

$store_email = $_SESSION['productusername'];

$get_productstore_details = "Select * from food_company where company_email='$store_email'";
$run_productstore_details = mysqli_query($connection, $get_productstore_details);
$fetch_productstore_details = mysqli_fetch_array($run_productstore_details);

$productstore_id = $fetch_productstore_details['fcompany_id']; //:: Customer id 
$productstore_name = $fetch_productstore_details['Company_name']; //:: customer name

               


//: Add New Food Item..

if(isset($_POST['addproductstoreitem'])){
   
    $item_title = $_POST['product_title'];
    $item_cat = $_POST['product_cat'];
    $item_price = $_POST['product_price'];
    $item_desc = $_POST['product_desc'];
    $status = "on";
    $item_youtubelink = $_POST['product_youtubelink'];
    // $item_discounted_percentage = $_POST['item_discounted_percentage'];
    $item_quantity = $_POST['item_quantity'];
    $item_company = "$productstore_id";
    $item_discounted_percentage = '1';
    $item_type = 'product';
    $item_img = $_FILES['product_img']['name'];
    

     $Add_Food_Item = false; 
    
    if($item_img == '') //If the image is empty, insert default image for it
    {
        $item_img = 'Default.png';
        $_SESSION['status'] = "Image Is Uploaded With Default. $store";
        
    }
$Add_Food_Img = false;
	if($Add_Food_Img){  //file_exists("fooditem_images/" .$_FILES["item_img"]["name"])
        $image_already_exist = $_FILES["product_img"]["name"];
        $_SESSION['status'] = "Image Already exists. $store";
        header('Location: food_item.php'); 
    
    }else{

        $insert_fooditem = "insert into food_items (fcat_id,fcat_sub_id,fcompany_id,date,item_title,item_img,item_price,item_desc,youtube_link,status,item_discounted_price,item_quantity,item_type,top_product,trending) values 
        ('$item_cat','0','$item_company',NOW(),'$item_title','$item_img',
        '$item_price','$item_desc','$item_youtubelink','$status','$item_discounted_percentage','$item_quantity','$item_type','','')";

        if(mysqli_query($connection, $insert_fooditem)){
            move_uploaded_file($_FILES["product_img"]["tmp_name"], "fooditem_images/".$_FILES["product_img"]["name"] );
            $_SESSION['success'] = "Product Item is Added";
           header('Location: productstore_productitems.php'); 
        }else{ 

            $_SESSION['status'] = "Food Item is not Added";       
           header('Location: productstore_productitems.php'); 
        }

    //      if(mysqli_query($connection, $insert_fooditem)){
    //         move_uploaded_file($_FILES["food_img"]["tmp_name"], "fooditem_images/".$_FILES["food_img"]["name"] );
    //             $_SESSION['success'] = "Food Item is Added";
    //             header('Location: foodstore_fooditems.php'); 
    // }else{

    //     echo "Error insertin data: " . mysqli_error($connection);
    // }
}
    
}


//: Update Product item

if(isset($_POST['updateproductstoreitem'])){

    $updateitem_id = $_POST['product_updateid'];
    $updateitem_title = $_POST['product_updatetitle'];
    $updateitem_cat = $_POST['product_updatecat'];
    $updateitem_price = $_POST['product_updateprice'];
    $updateitem_desc = $_POST['product_updatedesc'];
    $status = "on";
    $updateitem_youtubelink = $_POST['product_updateyoutubelink'];
    // $item_discounted_percentage = $_POST['item_discounted_percentage'];
    $updateitem_quantity = $_POST['product_updatequantity'];
    $item_company = "$productstore_id";
    $item_discounted_percentage = 1;
    $updateitem_img = $_FILES['product_updateimg']['name'];
    

    //:: UPDATING THE IMAGE::
    $update_img = "SELECT * FROM food_items where item_id= '$updateitem_id'";
    $run_update_img = mysqli_query($connection, $update_img);

    foreach($run_update_img as $fa_row){
        if($updateitem_img == NULL){
            $image_data = $fa_row['item_img'];
        }else{
            //: upload with new image and delete with old image
            if($img_path = "fooditem_images/".$fa_row['item_img']){
                unlink($img_path);
                $image_data = $updateitem_img;
            }
            
        }
    }

    $query = "UPDATE food_items SET fcat_id='$updateitem_cat', fcat_sub_id='0', fcompany_id='$item_company', item_title='$updateitem_title', item_img='$image_data', item_price='$updateitem_price', 
    item_desc='$updateitem_desc', youtube_link='$updateitem_youtubelink', status='$status', item_discounted_price='$item_discounted_percentage', item_quantity='$updateitem_quantity'   
    WHERE item_id='$updateitem_id' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        if($updateitem_img == NULL){
            $_SESSION['success'] = "Item is Updated with existing image";
            header('Location: productstore_productitems.php'); 
        }else{
            //: upload with new image and delete with old image
            move_uploaded_file($_FILES["product_updateimg"]["tmp_name"], "fooditem_images/".$_FILES["product_updateimg"]["name"] );
            $_SESSION['success'] = "Item is Updated";
            header('Location: productstore_productitems.php'); 
        }
      
    }
    else
    {
        $_SESSION['status'] = "item is NOT Updated";
        // $_SESSION['status_code'] = "error";
        header('Location: productstore_productitems.php'); 
    }

}


//: Delete Food item
if(isset($_POST['deleteproductstore_btn']))
{
    $productitemdeleteid = $_POST['deleteproductstore_id'];

    $query = "DELETE FROM food_items WHERE item_id='$productitemdeleteid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "item is Deleted";
        // $_SESSION['status_code'] = "success";
        header('Location: productstore_productitems.php'); 
    }
    else
    {
        $_SESSION['status'] = "Item is NOT DELETED";       
        // $_SESSION['status_code'] = "error";
        header('Location: productstore_productitems.php'); 
    }    
}


//: Completed Pickup order button
if(isset($_POST['confirm_pending_order_btn']))
{
    $confirmpickuporderid = $_POST['confirm_pending_order_id'];

    $query = "Select * from customer_pickuporder where porder_id = '$confirmpickuporderid' ";
    $run_query = mysqli_query($connection, $query);
    $fetch_query = mysqli_fetch_array($run_query);

    $getcustomer_id = $fetch_query['customer_id'];
    $getcustomer_order_no = $fetch_query['customer_order_no'];
    $gethotel_id = $fetch_query['hotel_id'];
    $getorder_date = $fetch_query['order_date'];
    $getinvoiceno = $fetch_query['invoiceno'];
    $getorder_desc = $fetch_query['order_description'];
    $getdue_amount = $fetch_query['due_amount'];
    $getpickup_order_date = $fetch_query['pickup_order_date'];
    $getpickup_order_time = $fetch_query['pickup_order_time'];
    $getorder_status = 'Completed with Payment';
    $ordertype = 'PRODUCT-PICKUP';


    $insert_completed_order = "Insert into completed_orders(customer_id,customer_order_no,hotel_id,order_date,invoice_no,order_description,due_amount,
    pickup_order_date,pickup_order_time,order_status,order_type)values('$getcustomer_id','$getcustomer_order_no','$gethotel_id',
    '$getorder_date','$getinvoiceno','$getorder_desc','$getdue_amount','$getpickup_order_date','$getpickup_order_time','$getorder_status','$ordertype')";

    $run_completed_order = mysqli_query($connection, $insert_completed_order);

    $delete_pickup_order = "DELETE FROM customer_pickuporder where porder_id='$confirmpickuporderid'";
    $run_delete_query = mysqli_query($connection, $delete_pickup_order);

    if($run_delete_query){
        $_SESSION['success'] = "DONE";
        // $_SESSION['status_code'] = "success";
        header('Location: productstore_pickuporders.php'); 
        
    }else{ 

        $_SESSION['status'] = "NOT DONE";       
        // $_SESSION['status_code'] = "error";
        header('Location: productstore_pickuporders.php'); 
    
    }
}

//: Cancelled Pickup order button
if(isset($_POST['cancel_pending_order_btn']))
{
    $cancelledickuporderid = $_POST['cancel_pending_order_id'];

    $query = "Select * from customer_pickuporder where porder_id = '$cancelledickuporderid' ";
    $run_query = mysqli_query($connection, $query);
    $fetch_query = mysqli_fetch_array($run_query);

    $getcustomer_id = $fetch_query['customer_id'];
    $getcustomer_order_no = $fetch_query['customer_order_no'];
    $gethotel_id = $fetch_query['hotel_id'];
    $getorder_date = $fetch_query['order_date'];
    $getinvoiceno = $fetch_query['invoiceno'];
    $getorder_desc = $fetch_query['order_description'];
    $getdue_amount = $fetch_query['due_amount'];
    $getpickup_order_date = $fetch_query['pickup_order_date'];
    $getpickup_order_time = $fetch_query['pickup_order_time'];
    $getorder_status = 'Cancelled';
    $ordertype = 'PRODUCT-PICKUP';


    $insert_cancelled_order = "Insert into cancelled_orders(customer_id,customer_order_no,hotel_id,order_date,invoice_no,order_description,due_amount,
    pickup_order_date,pickup_order_time,order_status,order_type)values('$getcustomer_id','$getcustomer_order_no','$gethotel_id',
    '$getorder_date','$getinvoiceno','$getorder_desc','$getdue_amount','$getpickup_order_date','$getpickup_order_time','$getorder_status','$ordertype')";

    $run_completed_order = mysqli_query($connection, $insert_cancelled_order);

    $delete_pickup_order = "DELETE FROM customer_pickuporder where porder_id='$cancelledickuporderid'";
    $run_delete_query = mysqli_query($connection, $delete_pickup_order);

    if($run_delete_query){
        $_SESSION['success'] = "DONE";
        // $_SESSION['status_code'] = "success";
        header('Location: productstore_pickuporders.php'); 
        
    }else{ 

        $_SESSION['status'] = "NOT DONE";       
        // $_SESSION['status_code'] = "error";
        header('Location: productstore_pickuporders.php'); 
    
    }
}

//: Preparing Pickup Order..
if(isset($_POST['confirm_preparing_pending_order_btn']))
{
    $id = $_POST['confirm_preparing_pending_order_id'];

    $order_status = 'Preparing Order';

    $query = "UPDATE customer_pickuporder SET order_status = '$order_status' where porder_id = '$id' ";
    $run_query = mysqli_query($connection, $query);
  
    if($run_query){

        $_SESSION['success'] = "DONE";
        // $_SESSION['status_code'] = "success";
        header('Location: productstore_pickuporders.php');

    }else {

        $_SESSION['status'] = "NOT DONE";       
        // $_SESSION['status_code'] = "error";
        header('Location: productstore_pickuporders.php');

    }
}

//: Ready for pickup... food pickup
if(isset($_POST['confirm_readyforpickup_pending_order_btn']))
{
    $id = $_POST['confirm_readyforpickup_pending_order_id'];

    $order_status = 'Ready for Pickup';

    $query = "UPDATE customer_pickuporder SET order_status = '$order_status' where porder_id = '$id' ";
    $run_query = mysqli_query($connection, $query);
  
    if($run_query){

        $_SESSION['success'] = "DONE";
        // $_SESSION['status_code'] = "success";
        header('Location: productstore_pickuporders.php'); 

    }else {

        $_SESSION['status'] = "NOT DONE";       
        // $_SESSION['status_code'] = "error";
        header('Location: productstore_pickuporders.php');
    }
}

// : Preparing for Delivery (Pickup-Order)
if(isset($_POST['confirm_preparingdelivery_pending_order_btn']))
{
    $id = $_POST['confirm_preparingdelivery_pending_order_id'];

    $order_status = 'Preparing For Delivery';

    $query = "UPDATE customer_pickuporder SET order_status = '$order_status' where porder_id = '$id' ";
    $run_query = mysqli_query($connection, $query);
  
    if($run_query){

        $_SESSION['success'] = "DONE";
        // $_SESSION['status_code'] = "success";
        header('Location: productstore_pickuporders.php');
    }else {

        $_SESSION['status'] = "NOT DONE";       
        // $_SESSION['status_code'] = "error";
        header('Location: productstore_pickuporders.php');
    }
}


//: Add product commision for influencer in store_promote_product
if(isset($_POST['addproductcommission']))
{
    $promoteproduct_itemid = $_POST['promoteproduct_itemid'];
    $promoteproduct_productcommision = $_POST['promoteproduct_commission'];
    $promoteproduct_status = 'yes';


    // echo "$promoteproduct_itemid";
    // echo "<br>";
    // echo "$promoteproduct_productcommision";
    // echo "<br>";

    //: Get category and hotelid 
    $get_itemdata = "Select * from food_items where item_id = '$promoteproduct_itemid'";
    $run_itemdata = mysqli_query($connection, $get_itemdata);
    $fetch_itemdata = mysqli_fetch_assoc($run_itemdata);

    $promoteproduct_hotelid = $fetch_itemdata['fcompany_id'];

    
    
    // $check_pro = "select * from customer_foodcart where customer_id='$customer_id' AND fooditem_id='$getcart_id'";
    // $run_check = mysqli_query($con, $check_pro);
    // $count_rows = mysqli_num_rows($run_check);

    
    
    // if($count_rows>0){
    //    echo"<script>alert ('this item is already added in your cart')</script>";
    //    echo "<script>window.open('food.php?foodcompany=$getfood_id','_self')</script>";
    // }

    $check_product = "Select * from store_promote_product where ssp_hotelid = '$promoteproduct_hotelid' AND ssp_itemid = '$promoteproduct_itemid'";
    $run_check = mysqli_query($connection, $check_product);
    $count_rows = mysqli_num_rows($run_check);

    if($count_rows>0){

            $_SESSION['success'] = "this  item is already added for Promote";
            // $_SESSION['status_code'] = "success";
            header('Location: productstore_promoteproducts.php');

    }else {

        $insert_promote_product = "Insert into store_promote_product(ssp_itemid, ssp_hotelid, ssp_date, ssp_influencer_commission, ssp_status) 
        values ('$promoteproduct_itemid','$promoteproduct_hotelid',NOW(),'$promoteproduct_productcommision','$promoteproduct_status')";
        $run_promote_product = mysqli_query($connection, $insert_promote_product);

        if($run_promote_product){
            $_SESSION['success'] = "DONE";
            // $_SESSION['status_code'] = "success";
            header('Location: productstore_promoteproducts.php');

        }else {
            $_SESSION['success'] = "ERROR";
            // $_SESSION['status_code'] = "success";
            header('Location: productstore_promoteproducts.php');
            // echo "Error insertin data: " . mysqli_error($connection);

        }

    }
}


//: Promote Product no button

if(isset($_POST['promote_product_no_btn'])){

    $promoteproduct_no_id = $_POST['promote_product_no_id'];

    // echo "$promoteproduct_no_id";


    $update_promoteproduct = "UPDATE store_promote_product SET ssp_status = 'no' WHERE spp_id='$promoteproduct_no_id' ";
    $run_promoteproduct = mysqli_query($connection,$update_promoteproduct);

    if($run_promoteproduct){
        // $_SESSION['success'] = "Product is Added in Top Product";
        // $_SESSION['status_code'] = "success";
        header('Location: productstore_promoteproducts.php');
    }
    else
    {
        $_SESSION['status'] = "Error";       
        // $_SESSION['status_code'] = "error";
        header('Location: productstore_promoteproducts.php');
        //  echo "Error insertin data: " . mysqli_error($connection);
    }    
}


//: Promote Product yes button

if(isset($_POST['promote_product_yes_btn'])){

    $promoteproduct_yes_id = $_POST['promote_product_yes_id'];

    // echo "$promoteproduct_no_id";


    $update_promoteproduct = "UPDATE store_promote_product SET ssp_status = 'yes' WHERE spp_id='$promoteproduct_yes_id' ";
    $run_promoteproduct = mysqli_query($connection,$update_promoteproduct);

    if($run_promoteproduct){
        // $_SESSION['success'] = "Product is Added in Top Product";
        // $_SESSION['status_code'] = "success";
        header('Location: productstore_promoteproducts.php');
    }
    else
    {
        $_SESSION['status'] = "Error";       
        // $_SESSION['status_code'] = "error";
        header('Location: productstore_promoteproducts.php');
        //  echo "Error insertin data: " . mysqli_error($connection);
    }    
}

//: Update Promo Product item commission

if(isset($_POST['updateproductprmote_btn'])){

    $promoteproduct_update_id = $_POST['promoteproduct_updateid'];
    $promoteproduct_update_commission = $_POST['promoteproduct_updateCommission'];

    // echo "$promoteproduct_update_id";
    // echo "$promoteproduct_update_commission";


    $update_promoteproduct = "UPDATE store_promote_product SET ssp_influencer_commission = '$promoteproduct_update_commission' WHERE spp_id='$promoteproduct_update_id' ";
    $run_promoteproduct = mysqli_query($connection,$update_promoteproduct);

    if($run_promoteproduct){
        $_SESSION['success'] = "Product is Updated";
        // $_SESSION['status_code'] = "success";
        header('Location: productstore_promoteproducts.php');
    }
    else
    {
        $_SESSION['status'] = "Error";       
        // $_SESSION['status_code'] = "error";
        header('Location: productstore_promoteproducts.php');
        //  echo "Error insertin data: " . mysqli_error($connection);
    }    
}

//: Delete Promote Product item
if(isset($_POST['deletepromoteproduct_btn']))
{
    $promoteproductDeleteid = $_POST['deletepromoteproduct_id'];

    $query = "DELETE FROM store_promote_product WHERE spp_id='$promoteproductDeleteid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "Your product is Deleted";
        // $_SESSION['status_code'] = "success";
        header('Location: productstore_promoteproducts.php');
    }
    else
    {
        $_SESSION['status'] = "Your product is NOT DELETED";       
        // $_SESSION['status_code'] = "error";
        header('Location: productstore_promoteproducts.php');
    }    
}


//:: Accept Request of influencer
if(isset($_POST['promote_product_requestaccept_btn']))
{
    $promoteproductAcceptid = $_POST['promote_product_requestaccept_id'];

    //:: Fetch details from store_promote_request
    $getproductdetails = "Select * from store_promote_request where spr_id = ' $promoteproductAcceptid'";
    $runproductdetails = mysqli_query($connection, $getproductdetails);
    $fetchproductdetails = mysqli_fetch_array($runproductdetails);

    $promoteproductAccept_hotelid = $fetchproductdetails['spr_hotel_id'];
    $promoteproductAccept_itemid = $fetchproductdetails['spr_item_id'];
    $promoteproductAccept_influencerid = $fetchproductdetails['spr_influencer_id'];
    $promoteproductAccept_itemcommission = $fetchproductdetails['spr_commission'];

   //:: Insert into influencer_product 
   $insert_influencerProduct = "Insert into influencer_product (ip_influencer_id, ip_item_id, ip_hotel_id, ip_commission, ip_clicklink_count, ip_sold,ip_date, ip_status)
   values ('$promoteproductAccept_influencerid', '$promoteproductAccept_itemid', '$promoteproductAccept_hotelid', '$promoteproductAccept_itemcommission', '', '', NOW(),'Accepted Request')";

   $run_influencerProduct = mysqli_query($connection, $insert_influencerProduct);

   if($run_influencerProduct){
       
        //: Delete item from store_promote_request
        $delete_influencerproduct = "Delete from store_promote_request where spr_id = '$promoteproductAcceptid'";
        $run_deleteInfluencerProduct = mysqli_query($connection, $delete_influencerproduct);

        if($run_deleteInfluencerProduct){
            $_SESSION['success'] = "Done";
            // $_SESSION['status_code'] = "success";
            header('Location: productstore_influencerRequest.php');
           
        }else{
            $_SESSION['success'] = "Error";
            // $_SESSION['status_code'] = "success";
            header('Location: productstore_influencerRequest.php');
        }

   }else {
            $_SESSION['success'] = "Error";
            // $_SESSION['status_code'] = "success";
            header('Location: productstore_influencerRequest.php');
   }

}

//:: Accept Request of influencer
if(isset($_POST['promote_product_requestdelete_btn']))
{
    $promoteproductdeleteid = $_POST['promote_product_requestdelete_id'];

    $delete_promoteproduct = "UPDATE store_promote_request SET spr_status = 'Request Deleted' WHERE spr_id='$promoteproductdeleteid' ";
    $run_promoteproduct = mysqli_query($connection,$delete_promoteproduct);

    if($run_promoteproduct){
        $_SESSION['success'] = "Request is deleted";
        header('Location: productstore_influencerRequest.php');
    }
    else
    {
        $_SESSION['status'] = "Request is not Deleted";       
        // $_SESSION['status_code'] = "error";
        header('Location: productstore_influencerRequest.php');
        //  echo "Error insertin data: " . mysqli_error($connection);
    }    

   





   

}












?>


