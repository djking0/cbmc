<?php
session_start();
include("includes/db.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>GoWentGone Storee</title>

    <link rel="shortcut icon" href="image/favicon.ico" type="image/x-icon" />

    <!-- Bootstrap CDN -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
      crossorigin="anonymous"
    />

    <!--  Font Awesome CDN -->
    <script src="https://kit.fontawesome.com/23412c6a8d.js"></script>

    <!-- Slick Slider -->
    <link
      rel="stylesheet"
      type="text/css"
      href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"
    />

    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="./css/style.css" />
    <link rel="stylesheet" href="./css/viewproduct.css" />
  </head>
    <!-- Google Fonts -->
	    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@600&display=swap" rel="stylesheet">
	    <style>
	      body {
	        font-family: 'Source Sans Pro', sans-serif;
	      }
	    </style>
  <body>

  <!-- header -->

  <header>
      <div class="container">

        <div class="row">
          <div class="col-md-4 col-sm-12 col-12">
            <div class="btn-group">
              <button
                class="btn border dropdown-toggle my-md-4 my-2"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                ENG
              </button>
              <!-- <div class="dropdown-menu">
                <a href="#" class="dropdown-item">Bhasa - Malaysia</a>
              </div> -->
            </div>
          </div>
          <div class="col-md-4 col-12 text-center">
            <!-- <h2 class="my-md-3 site-title text-white">(Your Company Name) </h2> -->
            <div class="logo"><a href=""><img src="image/catalog/gowentgonemall.png" height="60" width= "300" title="Your Store" style="margin-top:10px" 
                                              alt="Your Store" /></a></div>
          </div>
          <div class="col-md-4 col-12 text-right">
            <p class="my-md-4 header-links">
              <?php
                if(!isset($_SESSION['customer_email'])){
                echo"";
                }else{
                  
                  echo"<a href='customer_myaccount.php' class='px-1'> $_SESSION[customer_email] </a>";
                  echo"<a class='px-2' style='color:white;'>|</a>";
                }
              ?>
              <?php
                if(!isset($_SESSION['customer_email'])){
                echo"<a href='checkout.php' class='px-2'>login</a>";
                echo"<a class='px-2' style='color:white;'>|</a>";
                echo"<a href='register.php' class='px-2'>Create an Account</a>";
                }else{
                  echo"<a href='logout.php' class='px-2'>logout</a>";
                }
              ?>
            </p>
          </div>
        </div>
      </div>

      <div class="container-fluid p-0 bg-primary-color">
        <nav class="navbar navbar-expand-lg navbar-light">
          <button
            class="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item active">
                <a href="index.php" class="nav-link" >HOME <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a href="displaycategory.php" class="nav-link" > COLLECTION</a>
              </li>
              <li class="nav-item">
                <a href="hotels.php" class="nav-link" > STORES</a>
              </li>
              <li class="nav-item">
                <a href="influencers.php" class="nav-link" > INFLUENCERS</a>
              </li>
              <li class="nav-item">
                <a href="aboutus.php" class="nav-link" > ABOUT US</a>
              </li>
              <li class="nav-item">
              <a href="contact.php" class="nav-link" >CONTACT US</a>
              </li>
            </ul>
          </div>
            <!--
          <div class="navbar-nav">
            <form class="form-inline my-2 my-lg-0" method="get" action="search.php" enctype="multipart/form-data">
                  <input class="form-control mr-sm-2" type="search" placeholder="Search a Product" aria-label="Search" name="user_query"/>
                  <button class="btn btn-outline-info my-2 my-sm-0" type="submit" name="search_btn">Search</button>
                  </form>
          </div>
-->
        </nav>
      </div>
    </header>

    <!-- /header -->
          <div class="container border rounded pt-2 pb-5">
        <div class="container mt-4 mb-4">
            
                
            
<?php
    
    if (isset($_POST['submit1'])) {
        
        
        echo"<table class='table table-bordered'>
                <thead class='thead-dark text-center'>
                      <tr>
                        <th scope='col'>Item</th>
                        <th scope='col'>Item Name</th>
                        <th scope='col'>Item Price</th>
                        <th scope='col'>Item Quantity</th>
                        <th scope='col'>Total</th>
                      </tr>
                </thead>
                 <tbody>";

    $orderdate= $_POST['date'];
    $picktime=$_POST['picktime'];
    $hotelid=$_POST['hotelid'];
    $status = 'Confirm';
    $invoice_no = mt_rand();
    
    //:: Customer order no..
    $customer_order_no = '1';
   
    //: Getting Customer ID..
    $customer_email = $_SESSION['customer_email'];
                            
    $get_customer_details = "Select * from customers where customer_email='$customer_email'";
    $run_customer_details = mysqli_query($con, $get_customer_details);
    $fetch_customer_details = mysqli_fetch_array($run_customer_details);
    
    $customer_id = $fetch_customer_details['customer_id']; //:: Customer id 
    $customer_name = $fetch_customer_details['customer_name']; //:: customer name

    $number = 0;
    //: Getting item info

    $query = "select * from customer_foodcart where customer_id='$customer_id' ";
    $res = mysqli_query($con, $query);
    while($data = mysqli_fetch_array($res)){
    
    $item_name = $data['item_name'];
    $item_quantity = $data['quantity'];   
    $item_unitprice = $data['unit_price'];   
    $total = $data['total_price'];
    $number++;


     //: Getting GST...
     //$gst_rate = 6;
     //$gst = ($gst_rate / 100) * $total;

    //: Grand Total w/ GST..
    //$grand_total = ($total + $gst); 
        
        //: Grand Total w/o GST..
    $grand_total = $total; 



    //: Getting Order description

    $get_order_description = "Select * from customer_foodcart where customer_id='$customer_id'";
    $run_order_description = mysqli_query($con, $get_order_description);
    
    $single_variable = '';

    while($data=mysqli_fetch_array($run_order_description)){                                                                 
         $single_variable .='('.$data['quantity'].')'.' '.$data['item_name'].' '.'*'.' '.$data['unit_price'].' '.'='.' '.'HKD'.$data['total_price'].'<br>'.'';
    }
    $order_description = trim($single_variable);


    
    
    $query = "Select * from customer_pickuporder where customer_id='$customer_id'";
    $run_query = mysqli_query($con, $query);
    $count =  mysqli_num_rows($run_query);



         //TABLE FOR THE TOTAL 
        echo"<tr>
                <th scope='row'>$number</th>
                <td>$item_name</td>
                <td class='text-center'>HKD $item_unitprice</td>
                <td class='text-center'>$item_quantity</td>
                <td class='text-center'>HKD $grand_total</td>
              </tr>
          ";
          
    }
        
                   echo"</tbody>
            </table>";
?>
            
<?php
        
            $p_query = "Select * from food_company where fcompany_id='$hotelid'";
            $run_pquery = mysqli_query($con, $p_query);

            $fetch_pquery = mysqli_fetch_array($run_pquery);

            //$bank_id = $fetch_pquery['bank_id'];
            $acc_num = $fetch_pquery['acc_num'];
            $acc_name = $fetch_pquery['acc_name'];
            $bank_name = $fetch_pquery['bank_name'];          
                       

            echo"<h1 class='pb-3 pt-3'>Choose Your Payment Method</h1>
                    <div id='accordion'>

                         <div class='card'>
                            <div class='card-header' id='headingOne'>
                                <h5 class='mb-0'>
                                    <button class='btn' style='box-shadow: none !important;' data-toggle='collapse' data-target='#collapseOne' aria-expanded='true' aria-controls='collapseOne'>
                                    Credit Card/Debit Card <i class='fa fa-credit-card' aria-hidden='true'></i>
                                    </button>
                                </h5>
                            </div>

                            <div id='collapseOne' class='collapse show' aria-labelledby='headingOne' data-parent='#accordion'>
                                <div class='card-body'>
                                    <div class='container-fluid'>
                                        <div class='row pb-3'>
                                            <div class='col-12 col-sm-8 col-md-6 col-lg-5'>
                                                <img src='./assets/payment.png' width='100%'>
                                            </div>
                                        </div>
                                        <div class='row'>
                                            <div class='col-lg-6 col-md-12 mb-3'>
                                                <div class='input-group'>
                                                    <div class='input-group-prepend'>
                                                        <span class='input-group-text' id='user_name'><i class='fa fa-user' aria-hidden='true'></i></span>
                                                    </div>
                                                        <input type='text' class='form-control' placeholder='Name On Card' aria-label='Username' aria-describedby='user_name'>
                                                </div>
                                            </div>

                                            <div class='col-lg-6 col-md-12 mb-3'>
                                                <div class='input-group'>
                                                    <div class='input-group-prepend'>
                                                        <span class='input-group-text' id='card_number'><i class='fa fa-credit-card' aria-hidden='true'></i></span>
                                                    </div>
                                                        <input type='text' class='form-control' placeholder='Card Number' aria-label='Username' aria-describedby='card_number'>
                                                </div>
                                            </div>
                                        </div>

                                        <div class='row'>
                                            <div class='col-lg-5 col-md-4 col-sm-12 mb-3'>
                                                <div class='input-group'>
                                                    <div class='input-group-prepend'>
                                                        <span class='input-group-text' id='card_code'><i class='fa fa-lock' aria-hidden='true'></i></span>
                                                    </div>
                                                        <input type='text' class='form-control' placeholder='Security Code' aria-label='Username' aria-describedby='card_code'>
                                                </div>
                                            </div>

                                            <div class='col-lg-6 col-md-5 col-sm-12 mb-3'>
                                                <div class='input-group'>
                                                    <div class='input-group-prepend'>
                                                        <span class='input-group-text' id='exp_date'><i class='fa fa-calendar' aria-hidden='true'></i></span>
                                                    </div>
                                                        <input type='text' class='form-control' placeholder='Exp Date' aria-label='Username' aria-describedby='exp_date'>
                                                </div>
                                            </div>
                                        </div>

                                        <div class='row'>
                                            <div class='col-12 pt-5'>
                                            <small>You may see extra 1$ charged for the payment transcript</small>
                                            </div>
                                        </div>

                                        <div class='row'>
                                            <div class='col-12 pt-5 text-right'>
                                                <form action='addcustomerorder.php' method='post' id='payment-form'>
                                             <input type='hidden' name='date' value='$orderdate'>
                                                <input type='hidden' name='picktime' value='$picktime'>
                                                <input type='hidden' name='hotelid' value='$hotelid'>
                                                <button class='btn btn-secondary' type='submit' name='pickup_creditbtn'>Continue</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                
        
                        </div> 
    
                                     

                               <div class='card'>
                            <div class='card-header' id='headingTwo'>
                                <h5 class='mb-0'>
                                    <button class='btn' style='box-shadow: none !important;' data-toggle='collapse' data-target='#collapseTwo' aria-expanded='true' aria-controls='collapseOne'>
                                    Online Banking <i class='fa fa-university' aria-hidden='true'></i>
                                    </button>
                                </h5>
                            </div>

                            <div id='collapseTwo' class='collapse' aria-labelledby='headingTwo' data-parent='#accordion'>
                                <div class='card-body'>
                                    $bank_name<br>
                                    $acc_name
                                    $acc_num
                                
                                    <div class='row'>
                                    
                                    <div class='col-8 pt-5 text-left'>
                                     <a href='https://www.ambank.com.my/eng/'><img src='./bank/ambank.jpg' width='100px' height='50px' style='margin-right: 5px;'></a>
                                     <a href='https://www.maybank2u.com.my/home/m2u/common/login.do'><img src='./bank/maybank.png' height='50px' width='100px' style='margin-right: 5px;'></a>
                                     <a href='https://www.citibank.com.my/index.htm'><img src='./bank/citibank.jpg' height='50px' width='100px' style='margin-right: 5px;'></a>
                                     <a href='https://s.hongleongconnect.my/rib/app/fo/login?mc=D'><img src='./bank/hongleongbank.jpg' height='50px' width='100px' style='margin-right: 5px;'></a>
                                     <a href='https://www.pbebank.com/'><img src='./bank/publicbank.png' height='50px' width='100px' style='margin-right: 5px;'></a>
                                    </div>
                                    
                                            <div class='col-4 pt-5 text-right'>
                                            <form action='addcustomerorder.php' method='post' id='payment-form'>
                                             <input type='hidden' name='date' value='$orderdate'>
                                                <input type='hidden' name='picktime' value='$picktime'>
                                                <input type='hidden' name='hotelid' value='$hotelid'>
                                                <button class='btn btn-secondary' type='submit' name='pickup_bankbtn'>Continue</button>
                                                </form>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class='card'>
                            <div class='card-header' id='headingThree'>
                                <h5 class='mb-0'>
                                    <button class='btn' style='box-shadow: none !important;' data-toggle='collapse' data-target='#collapseThree' aria-expanded='true' aria-controls='collapseOne'>
                                    Cash <i class='fa fa-money' aria-hidden='true'></i>
                                    </button>
                                </h5>
                            </div>

                            <div id='collapseThree' class='collapse' aria-labelledby='headingThree' data-parent='#accordion'>
                                <div class='card-body'>
                                    We trust you, so we allowed this action

                                    <div class='row'>
                                            <div class='col-12 pt-5 text-right'>
                                                <form action='addcustomerorder.php' method='post' id='payment-form'>
                                             <input type='hidden' name='date' value='$orderdate'>
                                                <input type='hidden' name='picktime' value='$picktime'>
                                                <input type='hidden' name='hotelid' value='$hotelid'>
                                                <button class='btn btn-secondary' type='submit' name='pickup_cashbtn'>Continue</button>
                                                </form>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                        

                    ";
        
        
        
    }elseif (isset($_POST['dying_in'])) {
        
     echo"<table class='table table-bordered'>
                <thead class='thead-dark text-center'>
                      <tr>
                        <th scope='col'>Item</th>
                        <th scope='col'>Item Name</th>
                        <th scope='col'>Item Price</th>
                        <th scope='col'>Item Quantity</th>
                        <th scope='col'>Total</th>
                      </tr>
                </thead>
                 <tbody>";
        
    $table_no=$_POST['table_no'];
    $hotelid=$_POST['hotelid'];
    $invoice_no = mt_rand();
    
    //:: Customer order no..
    $customer_order_no = '1';
   
    //: Getting Customer ID..
    $customer_email = $_SESSION['customer_email'];
                            
    $get_customer_details = "Select * from customers where customer_email='$customer_email'";
    $run_customer_details = mysqli_query($con, $get_customer_details);
    $fetch_customer_details = mysqli_fetch_array($run_customer_details);
    
    $customer_id = $fetch_customer_details['customer_id']; //:: Customer id 
    $customer_name = $fetch_customer_details['customer_name']; //:: customer name


    $number = 0;
    //: Getting item info

    $query = "select * from customer_foodcart where customer_id='$customer_id' ";
    $res = mysqli_query($con, $query);
    while($data = mysqli_fetch_array($res)){
    
    $item_name = $data['item_name'];
    $item_quantity = $data['quantity'];   
    $item_unitprice = $data['unit_price'];   
    $total = $data['total_price'];
    $number++;

     //: Getting GST...
     //$gst_rate = 6;
     //$gst = ($gst_rate / 100) * $total;

    //: Grand Total w/ GST..
    //$grand_total = ($total + $gst); 
        
        //: Grand Total w/o GST..
    $grand_total = $total; 



    //: Getting Order description

    $get_order_description = "Select * from customer_foodcart where customer_id='$customer_id'";
    $run_order_description = mysqli_query($con, $get_order_description);
    
    $single_variable = '';

    while($data=mysqli_fetch_array($run_order_description)){                                                                 
         $single_variable .='('.$data['quantity'].')'.' '.$data['item_name'].' '.'*'.' '.$data['unit_price'].' '.'='.' '.'HKD'.$data['total_price'].'<br>'.'';
    }
    $order_description = trim($single_variable);


    
    
    $query = "Select * from customer_pickuporder where customer_id='$customer_id'";
    $run_query = mysqli_query($con, $query);
    $count =  mysqli_num_rows($run_query);
        
        
        
        
        //TABLE FOR THE TOTAL 
    echo"<tr>
                <th scope='row'>$number</th>
                <td>$item_name</td>
                <td class='text-center'>HKD $item_unitprice</td>
                <td class='text-center'>$item_quantity</td>
                <td class='text-center'>HKD $grand_total</td>
              </tr>
          ";
        
    }

              echo"</tbody>
            </table>";
?>
            
<?php
        
            $p_query = "Select * from food_company where fcompany_id='$hotelid'";
            $run_pquery = mysqli_query($con, $p_query);

            $fetch_pquery = mysqli_fetch_array($run_pquery);

            //$bank_id = $fetch_pquery['bank_id'];
            $acc_num = $fetch_pquery['acc_num'];
            $acc_name = $fetch_pquery['acc_name'];
            $bank_name = $fetch_pquery['bank_name'];
        

            echo"<h1 class='pb-3 pt-3'>Choose Your Payment Method</h1>
                    <div id='accordion'>

                        <div class='card'>
                            <div class='card-header' id='headingOne'>
                                <h5 class='mb-0'>
                                    <button class='btn' style='box-shadow: none !important;' data-toggle='collapse' data-target='#collapseOne' aria-expanded='true' aria-controls='collapseOne'>
                                    Credit Card/Debit Card <i class='fa fa-credit-card' aria-hidden='true'></i>
                                    </button>
                                </h5>
                            </div>

                            <div id='collapseOne' class='collapse show' aria-labelledby='headingOne' data-parent='#accordion'>
                                <div class='card-body'>
                                    <div class='container-fluid'>
                                        <div class='row pb-3'>
                                            <div class='col-12 col-sm-8 col-md-6 col-lg-5'>
                                                <img src='./assets/payment.png' width='100%'>
                                            </div>
                                        </div>
                                        <div class='row'>
                                            <div class='col-lg-6 col-md-12 mb-3'>
                                                <div class='input-group'>
                                                    <div class='input-group-prepend'>
                                                        <span class='input-group-text' id='user_name'><i class='fa fa-user' aria-hidden='true'></i></span>
                                                    </div>
                                                        <input type='text' class='form-control' placeholder='Name On Card' aria-label='Username' aria-describedby='user_name'>
                                                </div>
                                            </div>

                                            <div class='col-lg-6 col-md-12 mb-3'>
                                                <div class='input-group'>
                                                    <div class='input-group-prepend'>
                                                        <span class='input-group-text' id='card_number'><i class='fa fa-credit-card' aria-hidden='true'></i></span>
                                                    </div>
                                                        <input type='text' class='form-control' placeholder='Card Number' aria-label='Username' aria-describedby='card_number'>
                                                </div>
                                            </div>
                                        </div>

                                        <div class='row'>
                                            <div class='col-lg-5 col-md-4 col-sm-12 mb-3'>
                                                <div class='input-group'>
                                                    <div class='input-group-prepend'>
                                                        <span class='input-group-text' id='card_code'><i class='fa fa-lock' aria-hidden='true'></i></span>
                                                    </div>
                                                        <input type='text' class='form-control' placeholder='Security Code' aria-label='Username' aria-describedby='card_code'>
                                                </div>
                                            </div>

                                            <div class='col-lg-6 col-md-5 col-sm-12 mb-3'>
                                                <div class='input-group'>
                                                    <div class='input-group-prepend'>
                                                        <span class='input-group-text' id='exp_date'><i class='fa fa-calendar' aria-hidden='true'></i></span>
                                                    </div>
                                                        <input type='text' class='form-control' placeholder='Exp Date' aria-label='Username' aria-describedby='exp_date'>
                                                </div>
                                            </div>
                                        </div>

                                        <div class='row'>
                                            <div class='col-12 pt-5'>
                                            <small>You may see extra 1$ charged for the payment transcript</small>
                                            </div>
                                        </div>

                                        <div class='row'>
                                            <div class='col-12 pt-5 text-right'>
                                                <form action='addcustomerorder.php' method='post' id='payment-form'>
                                             <input type='hidden' name='hotelid' value='$hotelid'>
                                            <input type='hidden' name='table_no' value='$table_no'>
                                                <button class='btn btn-secondary' type='submit' name='dinein_creditbtn'>Continue</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                
        
                        </div> 
                        

                        <div class='card'>
                            <div class='card-header' id='headingTwo'>
                                <h5 class='mb-0'>
                                    <button class='btn' style='box-shadow: none !important;' data-toggle='collapse' data-target='#collapseTwo' aria-expanded='true' aria-controls='collapseOne'>
                                    Online Banking <i class='fa fa-university' aria-hidden='true'></i>
                                    </button>
                                </h5>
                            </div>

                            <div id='collapseTwo' class='collapse' aria-labelledby='headingTwo' data-parent='#accordion'>
                                <div class='card-body'>
                                    $bank_name<br>
                                    $acc_name
                                    $acc_num
                                    
                                    <div class='row'>
                                    
                                    <div class='col-8 pt-5 text-left'>
                                     <a href='https://www.ambank.com.my/eng/'><img src='./bank/ambank.jpg' width='100px' height='50px' style='margin-right: 5px;'></a>
                                     <a href='https://www.maybank2u.com.my/home/m2u/common/login.do'><img src='./bank/maybank.png' height='50px' width='100px' style='margin-right: 5px;'></a>
                                     <a href='https://www.citibank.com.my/index.htm'><img src='./bank/citibank.jpg' height='50px' width='100px' style='margin-right: 5px;'></a>
                                     <a href='https://s.hongleongconnect.my/rib/app/fo/login?mc=D'><img src='./bank/hongleongbank.jpg' height='50px' width='100px' style='margin-right: 5px;'></a>
                                     <a href='https://www.pbebank.com/'><img src='./bank/publicbank.png' height='50px' width='100px' style='margin-right: 5px;'></a>
                                    </div>
                                
                                    
                                            <div class='col-4 pt-5 text-right'>
                                            <form action='addcustomerorder.php' method='post' id='payment-form'>
                                             <input type='hidden' name='hotelid' value='$hotelid'>
                                            <input type='hidden' name='table_no' value='$table_no'>
                                                <button class='btn btn-secondary' type='submit' name='dinein_bankbtn'>Continue</button>
                                                </form>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class='card'>
                            <div class='card-header' id='headingThree'>
                                <h5 class='mb-0'>
                                    <button class='btn' style='box-shadow: none !important;' data-toggle='collapse' data-target='#collapseThree' aria-expanded='true' aria-controls='collapseOne'>
                                    Cash <i class='fa fa-money' aria-hidden='true'></i>
                                    </button>
                                </h5>
                            </div>

                            <div id='collapseThree' class='collapse' aria-labelledby='headingThree' data-parent='#accordion'>
                                <div class='card-body'>
                                    We trust you, so we allowed this action

                                    <div class='row'>
                                            <div class='col-12 pt-5 text-right'>
                                                <form action='addcustomerorder.php' method='post' id='payment-form'>
                                             <input type='hidden' name='hotelid' value='$hotelid'>
                                            <input type='hidden' name='table_no' value='$table_no'>
                                                <button class='btn btn-secondary' type='submit' name='dinein_cashbtn'>Continue</button>
                                                </form>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        

                    ";
        
    }
                     ?>
   
        </div>
    </div>

    <!--/ Main Content -->








        
    <!-- Footer -->

    <footer>
        <div class="container-fluid px-5">
          <div class="row">
            <div class="col-md-4 col-sm-12">
              <h4>Contact Us</h4>
              <div class="row pl-md-1 text-secondary">
                <div class="col-12">
                  <i class="fa fa-home px-md-2"></i>
                  <!--<small>S0-7-01, Menara 1, No 3, Jalan Bangsar, KL Eco City</small>-->
                </div>
              </div>
              <div class="row pl-md-1 text-secondary py-4">
                  <div class="col-12">
                      <i class="fa fa-paper-plane px-md-2"></i>
                      <small>Info@Gowentgone.com</small>
                    </div>
              </div>
               <div class="row pl-md-1 text-secondary">
                  <div class="col-12">
                      <i class="fa fa-phone-volume px-md-2"></i>
                      <small>(+852) 2433 3841</small>
                    </div>
               </div>

              <div class="row social text-secondary">
                <div class="col-12 py-3">
                  <i class="fab fa-twitter"></i>
                  <i class="fab fa-facebook-f"></i>
                  <i class="fab fa-google-plus-g"></i>
                  <i class="fab fa-skype"></i>
                  <i class="fab fa-pinterest-p"></i>
                  <i class="fab fa-youtube"></i>
                  <i class="fab fa-linkedin-in"></i>
                </div>
              </div>
            </div>
            <div class="col-md-2 col-sm-12">
              <h4>Our Services</h4>
              <div class="d-flex flex-column pl-md-3">
                <small class="pt-0">Kuala Lumpur</small>
                <small>Selangor</small>
                <small>KL Centeral</small>
              </div>
            </div>
            <div class="col-md-2 col-sm-12">
              <h4>Extras</h4>
              <div class="d-flex flex-column pl-md-3">
              <a href="aboutus.php" style="text-decoration:none;"><small>About Us</small></a>
                  <small>Collection</small>
                  <a href="contact.php" style="text-decoration:none;"><small>Contact Us</small></a>
                </div>
            </div>
            <div class="col-md-4 follow-us col-sm-12">
              <h4>Follow Instagram</h4>
              <div class="d-flex flex-row">
                <img src="./assets/256_n.jpg" alt="Instagram 1" class="img-fluid">
                <img src="./assets/792_n.jpg" alt="Instagram 2" class="img-fluid">
                <img src="./assets/392_n.jpg" alt="Instagram 3" class="img-fluid">
              </div>
              <div class="d-flex flex-row">
                  <img src="./assets/664_n.jpg" alt="Instagram 1" class="img-fluid">
                  <img src="./assets/088_n.jpg" alt="Instagram 2" class="img-fluid">
                  <img src="./assets/896_n.jpg" alt="Instagram 3" class="img-fluid">
                </div>
            </div>
          </div>
        </div>

        <div class="container-fluid news pt-5">
          <div class="row">
            <div class="col-md-6 col-12 pl-5">
              <h4 class="primary-color font-roboto m-0 p-0">
                Need Help? Call Our Award-Warning
              </h4>
              <p class="m-0 p-0 text-secondary">
                Support Team 24/7 At (+852) 2433 3841
              </p>
            </div>
            <!-- <div class="col-md-4 col-12 my-md-0 my-3 pl-md-0 pl-5">
              <input type="text" class="form-control border-0 bg-light" placeholder="Enter Your email Address">
            </div>
            <div class="col-md-2 col-12 my-md-0 my-3 pl-md-0 pl-5">
              <button class="btn bg-primary-color text-white">Subscribe</button>
            </div> -->
          </div>
        </div>

        <div class="container text-center">
          <p class="pt-5">
            <img src="./assets/payment.png" alt="payment image" class="img-fluid">
          </p>
          <small class="text-secondary py-4">GoWentGone © 2020 All Rights Reserved.</small>
        </div>

    </footer>

    <!-- /Footer -->    


    <script
      src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
      integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
      integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
      integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
      crossorigin="anonymous"
    ></script>
    <script
      type="text/javascript"
      src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"
    ></script>
    <script src="./js/main.js"></script>
  </body>
</html>


      
