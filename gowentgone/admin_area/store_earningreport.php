<?php
include('security.php');
include('includes/header.php'); 
include('includes/navbar.php'); 
?>

<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Store's Earning Report
    </h6>
  </div>

  <div class="card-body">

    <div class="table-responsive">

      <?php

      
      require 'dbconfig.php';

      $query = "SELECT * FROM food_company where company_type = 'product'";
      $query_run = mysqli_query($connection, $query);

      ?>

      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th> ID </th>
            <th> Name </th>
            <th> No of Completed Pickup Orders</th>
            <th> No of Completed Delivery Orders</th>
            <th> No of Canceled Orders</th>
            <th> Earning So for.. </th>
          </tr>
        </thead>
        <tbody>

          <?php 
            if(mysqli_num_rows($query_run) > 0)        
            {
                while($row = mysqli_fetch_assoc($query_run))
                {

                  ?>

                <tr>
                    <td> <?php  echo $row['fcompany_id']; ?></td>
                    <td> <?php echo $row['Company_name']; ?></td>
                    <td> <?php 

                        $id = $row['fcompany_id'];

                        $get_completed_orders = "Select * from completed_orders where hotel_id = '$id'";
                        $run_completed_orders = mysqli_query($connection, $get_completed_orders);
                        $total_completed_orders = mysqli_num_rows($run_completed_orders);

                        echo "$total_completed_orders";



                        ?> </td>
                        <td> coming soon </td>
                        <td> 

                        <?php 

                        $id = $row['fcompany_id'];

                        $get_cancelled_orders = "Select * from cancelled_orders where hotel_id = '$id'";
                        $run_cancelled_orders = mysqli_query($connection, $get_cancelled_orders);
                        $total_cancelled_orders = mysqli_num_rows($run_cancelled_orders);

                        echo "$total_cancelled_orders";

                        ?> 
                        </td>
                        <td> 

                        <?php

                        $id = $row['fcompany_id'];

                        $get_hotel_earning = "select SUM(due_amount) as 'hotel_totalamount' from completed_orders where hotel_id=' $id'";
                        $res_hotel_earning = mysqli_query($connection, $get_hotel_earning);
                        $data_hotel_earning = mysqli_fetch_array($res_hotel_earning);
                        $total_hotel_earning = $data_hotel_earning['hotel_totalamount'];
                        echo "RM $total_hotel_earning";


                        ?>

                        </td>
                                </tr>
                          <?php
                                }
                              }else {
                                echo "No Record Found";
                              }
                          ?>
                    
                          
                        
        </tbody>
      </table>

    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>