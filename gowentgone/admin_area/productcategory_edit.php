<?php
include('security.php');
include('includes/header.php'); 
include('includes/navbar.php'); 
?>


<div class="container-fluid">

<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"> EDIT PRODUCT CATEGORY </h6>
  </div>
  <div class="card-body">
<?php

    

    if(isset($_POST['editproductcat_btn'])){
        $id = $_POST['editproductcat_id'];
        
        require 'dbconfig.php'; 

        $query = "SELECT * FROM food_category WHERE fcat_id='$id'";
        $query_run = mysqli_query($connection, $query);

        foreach($query_run as $row)
        {
            ?>

                <form action="code.php" method="POST" enctype="multipart/form-data">

                    <div class="modal-body">

                        <input type="hidden" name="productcat_updateid" value="<?php echo $row['fcat_id'] ?>" >

                        <div class="form-group">
                            <label>Food Category Name </label>
                            <input type="text" name="productcat_updatename" value="<?php echo $row['food_cat'];?>" class="form-control">
                        </div>

                       
                        <div class="form-group">
                            <label >Product Image</label>
                            <input type="file" class="form-control" name="productcat_updateimg">
                        </div>
                    </div>
                  
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-danger" name="cancelhotelbtn" data-dismiss="modal">CANCEL</button> -->
                        <button type="submit" name="updateproductcatbtn" class="btn btn-primary">UPDATE</button>
                    </div>
                </form>
        <?php
        }

      
    }
?>
  </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>