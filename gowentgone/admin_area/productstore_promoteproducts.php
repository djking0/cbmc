<?php
include('productstore_security.php');
include('includes/header.php'); 
include('includes/navbar_product.php'); 
?>

<div class="modal fade" id="addadminprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 style="text-align: center;  color: black; font-weight: bold; font-size: 16px;"class="modal-title" id="exampleModalScrollableTitle"> Select Your Product for influencer to Promote:</h6>
      </div>
        <div class="modal-body">
                <form action="productstore_code.php" method="POST" enctype="multipart/form-data">
                    
                <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Choose Product:</label>
                            <select name="promoteproduct_itemid" class="form-control" required="required" >
                                        <option>
                                        <!-- Select a Category -->
                                        </option>
                                        <?php
                                            require 'dbconfig.php';

                                            $store_email = $_SESSION['productusername'];

                                            //:: Get store id from food_company table
                                            $get_storeid = "Select * from food_company where company_email = '$store_email'";
                                            $run_storeid = mysqli_query($connection,$get_storeid);
                                            $fetch_storeid = mysqli_fetch_array($run_storeid);

                                            $storeid = $fetch_storeid['fcompany_id'];

                                            $get_item = "select * from food_items where fcompany_id = '$storeid'";
                                            $run_item = mysqli_query($connection, $get_item);
                                            while($row_item = mysqli_fetch_array($run_item)){
                                                $item_id = $row_item['item_id'];
                                                $item_title = $row_item['item_title'];
                                                echo "<option value='$item_id'>$item_title</option>";
                                            } 
                                        ?>
                                    </select>
                        </div>
                </div>

                <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Influener's Commission (Example: 20)</label>
                            <input type="number" name="promoteproduct_commission" class="form-control" placeholder="Enter Product Commission" required="required">
                        </div>
                </div>
                  


                    <div style=" border-top: 0 none;" class="modal-footer"> 
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit"  class="btn btn-primary"  name="addproductcommission">Save</button>
                    </div>
                </form> 
        </div>
    </div>
  </div>
</div>



<!-- MODAL -->


<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">ProductItem's Data
           
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addadminprofile">
             Select Your Product for Promote
            </button>
           
    </h6>
  </div>

  <div class="card-body">

    <?php
      if(isset($_SESSION['success']) && $_SESSION['success']!=''){
        echo '<h2> '.$_SESSION['success'].' </h2>.';
        unset($_SESSION['success']);
      } 
      if(isset($_SESSION['status']) && $_SESSION['status']!=''){
        echo '<h2> '.$_SESSION['status'].' </h2>.';
        unset($_SESSION['status']);
      } 
      
    ?>

    <div class="table-responsive">

      <?php

      // : Getting FoodStore ID
      require 'dbconfig.php';

      $store_email = $_SESSION['productusername'];
                                    
      $get_foodstore_details = "Select * from food_company where company_email='$store_email'";
      $run_foodstore_details = mysqli_query($connection, $get_foodstore_details);
      $fetch_foodstore_details = mysqli_fetch_array($run_foodstore_details);
                                                                                                
      $foodstore_id = $fetch_foodstore_details['fcompany_id']; //:: Customer id 
      $foodstore_name = $fetch_foodstore_details['Company_name']; //:: customer name

      //:: Getting the food_items..
      $query = "SELECT * FROM store_promote_product where ssp_hotelid = '$foodstore_id' ";
      $query_run = mysqli_query($connection, $query);

      ?>

      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th> Image </th>
            <th> ID </th>
            <th>Date Added</th>
            <!-- <th> Category </th> -->
            <!-- <th>Title</th> -->
            <th>Item Title</th>
            <th>Commission </th>
            <th>Price </th>
            <th>Available for influencers (Temporary)</th>
            <th>EDIT </th>
            <th>DELETE </th>
          </tr>
        </thead>
        <tbody>

<?php 
  if(mysqli_num_rows($query_run) > 0)        
  {
      while($row = mysqli_fetch_assoc($query_run))
      {

        ?>

        




      <tr>
            <!-- IMAGE -->
            <td> 
              <?php 

                    $itemid = $row['ssp_itemid'];

                    $get_itemdata = "Select * from food_items where item_id = '$itemid'";
                    $run_itemdata = mysqli_query($connection, $get_itemdata);
                    $fetch_itemdata = mysqli_fetch_array($run_itemdata);

                    $item_img = $fetch_itemdata['item_img'];
                    echo '<img src="fooditem_images/'.$item_img.'" width="100px;" height="100px;" alt="image" >' 
                ?> 
            </td> 
           
            <!-- ID -->
           <td> <?php echo $row['spp_id']; ?></td>

          <!-- Date Added -->
          
          <td> <?php echo $row['ssp_date']; ?></td>
            

          <!-- Title -->
          <td> 
              <?php  

                $itemid = $row['ssp_itemid'];

                $get_itemdata = "Select * from food_items where item_id = '$itemid'";
                $run_itemdata = mysqli_query($connection, $get_itemdata);
                $fetch_itemdata = mysqli_fetch_array($run_itemdata);

                $item_title = $fetch_itemdata['item_title'];

                echo "$item_title";
          
                ?>
          </td>

          <!-- Commission -->
          <td> <?php  echo $row['ssp_influencer_commission']; ?>%</td>

          <!-- Price -->
          <td> 
              <?php  

                $itemid = $row['ssp_itemid'];
                $commission = $row['ssp_influencer_commission'];

                $get_itemdata = "Select * from food_items where item_id = '$itemid'";
                $run_itemdata = mysqli_query($connection, $get_itemdata);
                $fetch_itemdata = mysqli_fetch_array($run_itemdata);

                $item_title = $fetch_itemdata['item_title'];
                $item_price = $fetch_itemdata['item_price'];

                //: Getting price after deducting the commision: 
                $priceAfterCommission = ($commission / 100) * $item_price;

                $finalpriceAftercommission = $priceAfterCommission - $item_price;

                echo "<strong>Real Price: </strong> RM$item_price" ; 
                echo "<br>";
                echo "<strong>After deducting a commission:</strong> RM$finalpriceAftercommission" ;
          
            ?>
          </td>

        

         <!--Available for influencers(Temporary) -->
            <td>
                        <div class="btn-group" role="group">
                            <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?php 

                                $temporary_available = $row['ssp_status'];

                                if( $temporary_available == "no"){
                                    echo "NO";
                                }else {
                                    echo "YES";
                                }
                            ?>
                            </button>

                            <?php

                            $temporary_available_id = $row['ssp_status'];
                            // echo "$top_product_id";
                            
                            if($temporary_available_id == "no"){

                                ?>

                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">

                                <form action="productstore_code.php" method="post">
                                <input type="hidden" name="promote_product_yes_id" value="<?php  echo $row['spp_id']; ?>">
                                <button  type="submit" name="promote_product_yes_btn"  class="dropdown-item">YES</button>
                                </form>
                            
                               
                                </div>

                                <?php

                              
                            }else {

                                ?>

                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">


                                <form action="productstore_code.php" method="post">
                                    <input type="hidden" name="promote_product_no_id" value="<?php  echo $row['spp_id']; ?>">
                                    <button  type="submit" name="promote_product_no_btn" class="dropdown-item"> NO</button>
                                </form>

                                </div>

                                <?php

                            }
                
                            
                            ?>
                            
                        </div>

            </td>

        
        <!--Edit Button -->
        <td>
                        <form action="productstore_promoteProductEdit.php" method="post">
                            <input type="hidden" name="editpromoteproduct_id" value="<?php  echo $row['spp_id']; ?>">
                            <button  type="submit" name="editpromoteproduct_btn" class="btn btn-success"> EDIT</button>
                        </form>
        </td>


         
        <!--Delete Button -->
        <td>
                      <form action="productstore_code.php" method="post">
                        <input type="hidden" name="deletepromoteproduct_id" value="<?php  echo $row['spp_id']; ?>">
                        <button type="submit" name="deletepromoteproduct_btn" class="btn btn-danger"> DELETE</button>
                      </form>
        </td>

         
<?php
      }
    }else {
      echo "No Record Found";
    }
?>



</tbody>
      </table>

    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>