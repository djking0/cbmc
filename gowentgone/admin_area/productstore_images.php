<?php
include('productstore_security.php');
include('includes/header.php'); 
include('includes/navbar_product.php'); 
?>

<div class="modal fade" id="addadminprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 style="text-align: center;  color: black; font-weight: bold; font-size: 16px;"class="modal-title" id="exampleModalScrollableTitle"> ADD NEW FOOD ITEM</h6>
      </div>
        <div class="modal-body">
                <form action="productstore_code.php" method="POST" enctype="multipart/form-data"> 
                    
                <?php

                    if(isset($_POST['productstore_addimages_btn'])){
                        $itemid = $_POST['productstore_item_id'];
                        $companyid = $_POST['productstore_company_id'];

                    }

        
                ?>

                    <input type="hidden" name="item_id" class="form-control" value="<?php echo "$itemid" ?>" required="required">
                    <input type="hidden" name="item_company_id" class="form-control" value="<?php echo "$companyid" ?>" required="required">
               
                    <div class="form-row">
                        <div class="form-group col-md-12">
                        <label for="inputZip">Food Image</label>
                        <input type="file" class="form-control" name="item_moreimg" required="required">
                        </div>
                    </div>

                    <div style=" border-top: 0 none;" class="modal-footer"> 
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit"  class="btn btn-primary"  name="addmoreimagesforproductstore">Save</button>
                    </div>
                </form> 
        </div>
    </div>
  </div>
</div>


<div class="container-fluid">

<?php

if(isset($_POST['productstore_addimages_btn'])){
        $itemid = $_POST['productstore_item_id'];
        $companyid = $_POST['productstore_company_id'];

        
    
?>

<!-- DataTales Example -->
    <div class="card shadow mb-4">
    <div class="card-header py-3">

        <?php

            require 'dbconfig.php'; 

            //: Getting fooditem name
            $getitemname = "Select * from food_items where item_id = '$itemid'";
            $runitemname = mysqli_query($connection, $getitemname);
            $fetchitemname = mysqli_fetch_array($runitemname);

            $item_title = $fetchitemname['item_title'];
        ?>

        <h3 class="m-0 font-weight-bold text-primary"><?php echo "$item_title" ?>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addadminprofile">
              Add New Image
            </button>
        </h3>
    </div>

    <div class="card-body">

        <?php
        if(isset($_SESSION['success']) && $_SESSION['success']!=''){
            echo '<h2> '.$_SESSION['success'].' </h2>.';
            unset($_SESSION['success']);
        } 
        if(isset($_SESSION['status']) && $_SESSION['status']!=''){  
            echo '<h2> '.$_SESSION['status'].' </h2>.';
            unset($_SESSION['status']);
        } 
        
        ?>

        <div class="table-responsive">

        
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>

            <?php

        
                require 'dbconfig.php';

                $query = "Select * from food_items where item_id = '$itemid'";
                $query_run = mysqli_query($connection, $query);

                ?>


            <!-- <tr>
                <th> ID </th>
                <th> Name </th>
                <th> Image </th>
            </tr> -->
            </thead>
            <tbody>

            <?php 
                ?>
                <strong>Main Image Added: </strong>
                <p></p>
                

            <?php 
                if(mysqli_num_rows($query_run) > 0)        
                {
                    while($row = mysqli_fetch_assoc($query_run))
                    {

                    ?>

                    <tr>
                    
                        <td> <?php echo '<img src="fooditem_images/'.$row['item_img'].'" width="100px;" height="100px;" alt="image" >' ?> </td>

                    </tr>
            <?php
                    }
                }else {
                    echo "No Record Found";
                }
            ?>
        
            
            
            </tbody>
           
        </table>

        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>

            <?php

        
                require 'dbconfig.php';

                $query = "Select * from item_images where it_item_id = '$itemid' AND it_company_id = '$companyid'";
                $query_run = mysqli_query($connection, $query);

                ?>

            <tr>
                <th> ID </th>
                <th> Image </th>
                <!-- <th>EDIT </th> -->
                <th>DELETE </th>
            </tr>
            </thead>
            <tbody>
            <?php 
                ?>
                <strong>Other Images: </strong>
                <p></p>

            <?php 
                if(mysqli_num_rows($query_run) > 0)        
                {
                    while($row = mysqli_fetch_assoc($query_run))
                    {

                    ?>

                    <tr>
                        <td> <?php  echo $row['it_id']; ?></td>
                        <!-- <td> <?php  echo $row['it_item_id']; ?></td> -->
                        <td> <?php echo '<img src="fooditem_images/'.$row['it_item_img'].'" width="100px;" height="100px;" alt="image" >' ?></td>
                        
                        <!-- <td>
                            <form action="fooditem_images_edit.php" method="post">
                                <input type="hidden" name="edit_fooditem_images_id" value="<?php  echo $row['it_id']; ?>">
                                <button  type="submit" name="edit_fooditem_images_btn" class="btn btn-success"> EDIT</button>
                            </form>
                        </td> -->
                        <td>
                            <form action="productstore_code.php" method="post">
                                <input type="hidden" name="delete_productstore_images_id" value="<?php  echo $row['it_id']; ?>">
                                <button type="submit" name="delete_productstore_images_btn" class="btn btn-danger"> DELETE</button>
                            </form>
                        </td>
                    </tr>
            <?php
                    }
                }else {
                    echo "No Record Found";
                }
            ?>
        
            
            
            </tbody>
        </table>

        </div>
    </div>
    </div>

<?php

}

?>

</div>
<!-- /.container-fluid -->

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>