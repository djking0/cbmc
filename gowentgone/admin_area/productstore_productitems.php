<?php
include('productstore_security.php');
include('includes/header.php'); 
include('includes/navbar_product.php'); 
?>

<div class="modal fade" id="addadminprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 style="text-align: center;  color: black; font-weight: bold; font-size: 16px;"class="modal-title" id="exampleModalScrollableTitle"> ADD NEW FOOD ITEM</h6>
      </div>
        <div class="modal-body">
                <form action="productstore_code.php" method="POST" enctype="multipart/form-data">
                                      
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label> Product Title: </label>
                            <input type="text" name="product_title" class="form-control" placeholder="Enter Product title" required="required">

                        </div>
                        <div class="form-group col-md-6">
                            <label> Product Category:</label>
                            <select name="product_cat" class="form-control" required="required" placeholder="Enter Product title" >
                                        <option>
                                        <!-- Select a Category -->
                                        </option>
                                        <?php
                                            require 'dbconfig.php';
                                            $get_cats = "select * from food_category where cat_type = 'product'";
                                            $run_cats = mysqli_query($connection, $get_cats);
                                            while($row_cats = mysqli_fetch_array($run_cats)){
                                                $cat_id = $row_cats['fcat_id'];
                                                $cat_title = $row_cats['food_cat'];
                                                echo "<option value='$cat_id'>$cat_title</option>";
                                            } 
                                        ?>
                                    </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Product Item Quantity:</label>
                            <input type="number" name="item_quantity" class="form-control" placeholder="Enter item quantity" required="required" min="0">
                        </div>
                    </div>
                    
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Price</label>
                            <input type="text" name="product_price" class="form-control" required="required" />
                        </div>
                        <!-- <div class="form-group col-md-6"> -->
                            <!-- <label>Discount:</label> -->
                            <!-- <select name="item_discounted_percentage" class="form-control" required="required" >
                                        <option>
                                        </option>
                                        <option>10</option>
                                        <option>20</option>
                                        <option>40</option>
                                        <option>60</option>
                                        <option>NO</option>
                                    </select> -->
                        
                        <!-- </div> -->
                    </div>
                
                        
                    <div class="form-row">
                        <div class="form-group col-md-12">
                        <label for="inputZip">Product Image</label>
                        <input type="file" class="form-control" name="product_img">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                        <label for="inputZip">Product Youtube Link</label>
                        <input type="text"  class="form-control" name="product_youtubelink" size="50"/>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                        <label for="inputZip">Product-Description</label>
                        <textarea name="product_desc" class="form-control" cols="20" rows="5"></textarea>
                        </div>
                    </div>
                        
                    <div style=" border-top: 0 none;" class="modal-footer"> 
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit"  class="btn btn-primary"  name="addproductstoreitem">Save</button>
                    </div>
                </form> 
        </div>
    </div>
  </div>
</div>



<!-- MODAL -->


<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">ProductItem's Data
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addadminprofile">
              Add New Product
            </button>
    </h6>
  </div>

  <div class="card-body">

    <?php
      if(isset($_SESSION['success']) && $_SESSION['success']!=''){
        echo '<h2> '.$_SESSION['success'].' </h2>.';
        unset($_SESSION['success']);
      } 
      if(isset($_SESSION['status']) && $_SESSION['status']!=''){
        echo '<h2> '.$_SESSION['status'].' </h2>.';
        unset($_SESSION['status']);
      } 
      
    ?>

    <div class="table-responsive">

      <?php

      // : Getting FoodStore ID
      require 'dbconfig.php';

      $store_email = $_SESSION['productusername'];
                                    
      $get_foodstore_details = "Select * from food_company where company_email='$store_email'";
      $run_foodstore_details = mysqli_query($connection, $get_foodstore_details);
      $fetch_foodstore_details = mysqli_fetch_array($run_foodstore_details);
                                                                                                
      $foodstore_id = $fetch_foodstore_details['fcompany_id']; //:: Customer id 
      $foodstore_name = $fetch_foodstore_details['Company_name']; //:: customer name

      //:: Getting the food_items..
      $query = "SELECT * FROM food_items where fcompany_id = '$foodstore_id' ";
      $query_run = mysqli_query($connection, $query);

      ?>

      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th> Image </th>
            <th> ID </th>
            <th> Category </th>
            <th> Company </th>
            <th>Date Added</th>
            <th>Title</th>
            <th>Price </th>
            <th>Description </th>
            <th>Youtube link </th>
            <!-- <th>Discount </th> -->
            <th>Quantity </th>
            <th>EDIT </th>
            <th>DELETE </th>
          </tr>
        </thead>
        <tbody>

<?php 
  if(mysqli_num_rows($query_run) > 0)        
  {
      while($row = mysqli_fetch_assoc($query_run))
      {

        ?>

        




      <tr>
          <td> <?php echo '<img src="fooditem_images/'.$row['item_img'].'" width="100px;" height="100px;" alt="image" >' ?>

              <form action="productstore_images.php" method="post">

                  <input type="hidden" name="productstore_item_id" value="<?php  echo $row['item_id']; ?>">
                  <input type="hidden" name="productstore_company_id" value="<?php  echo $row['fcompany_id']; ?>">
                  <button type="submit" name="productstore_addimages_btn" class="btn btn-link"> More Images</button>
              </form>


          </td>
          

          
          
          <!-- Fetch Category -->
          <?php 

          ?>
           <td> <?php  echo $row['item_id']; ?></td>
          <td> <?php  echo $row['fcat_id']; ?></td>
          <td> <?php  echo $row['fcompany_id']; ?></td>
          <td> <?php  echo $row['date']; ?> </td>
          <td> <?php  echo $row['item_title']; ?></td>
          <td> <?php  echo $row['item_price'];  ?>RM </td>
          <!-- <td> <?php  echo $row['item_discounted_price']; ?>% </td> -->
          <td> <?php  echo $row['item_desc']; ?></td>
          <td> <?php  echo $row['youtube_link']; ?></td>

          <td> <?php  echo $row['item_quantity']; ?></td>
          <td>
                        <form action="productstore_productedit.php" method="post">
                            <input type="hidden" name="editproductstore_id" value="<?php  echo $row['item_id']; ?>">
                            <button  type="submit" name="editproductstore_btn" class="btn btn-success"> EDIT</button>
                        </form>
                  </td>
                  <td>
                      <form action="productstore_code.php" method="post">
                        <input type="hidden" name="deleteproductstore_id" value="<?php  echo $row['item_id']; ?>">
                        <button type="submit" name="deleteproductstore_btn" class="btn btn-danger"> DELETE</button>
                      </form>
                  </td>
      </tr>
<?php
      }
    }else {
      echo "No Record Found";
    }
?>



</tbody>
      </table>

    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>