<?php
include('security.php');
include('includes/header.php'); 
include('includes/navbar.php'); 
?>



<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Influencer's Data
    </h6>
  </div>

  <div class="card-body">

    <?php
      if(isset($_SESSION['success']) && $_SESSION['success']!=''){
        echo '<h2> '.$_SESSION['success'].' </h2>.';
        unset($_SESSION['success']);
      } 
      if(isset($_SESSION['status']) && $_SESSION['status']!=''){
        echo '<h2> '.$_SESSION['status'].' </h2>.';
        unset($_SESSION['status']);
      } 
      
    ?>

    <div class="table-responsive">

      <?php

      
      require 'dbconfig.php';

      $query = "SELECT * FROM influencer where i_accountstatus = 'verified'";
      $query_run = mysqli_query($connection, $query);

      ?>

      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th> ID </th>
            <th> Name </th>
            <th>Email </th>
            <th> Influencer-Page</th>
            <th>VIEW </th>
            <th>ACCEPT </th>
            <th>REJECT </th>
          </tr>
        </thead>
        <tbody>

          <?php 
            if(mysqli_num_rows($query_run) > 0)        
            {
                while($row = mysqli_fetch_assoc($query_run))
                {
                  $link = $row['i_pagelink'];

                  ?>

                <tr>
                    <td> <?php  echo $row['i_id']; ?></td>
                    <td> <?php  echo $row['i_name']; ?></td>
                    <td> <?php  echo $row['i_email']; ?></td>
                    <td> <?php  echo "- "; echo $row['i_pagename']; echo "  (";  echo $row['i_pagetype']; echo ")"; 
                                echo "<br>"; echo "- "; echo "Pagelink: ";   echo "<a href='$link'> $link</a>"; 
                                 echo "<br>"; echo "- "; echo" Followers: "; echo $row['i_followers'];                         
                    ?></td>

                  <td>
                        <form action="request_influencers_view.php" method="post">
                            <input type="hidden" name="viewinfluencer_id" value="<?php  echo $row['i_id']; ?>">
                            <button  type="submit" name="viewinfluencer_btn" class="btn btn-info"> VIEW</button>
                        </form>
                  </td>
                    
                    <td>
                        <form action="code.php" method="post">
                            <input type="hidden" name="accept_influencer_request_id" value="<?php  echo $row['i_id']; ?>">
                            <input type="hidden" name="accept_influencer_request_email" value="<?php  echo $row['i_emeil']; ?>">
                            <button  type="submit" name="accept_influencer_request_btn" class="btn btn-success"> ACCEPT</button>
                        </form>
                  </td>
                  <td>
                      <form action="code.php" method="post">
                        <input type="hidden" name="reject_influencer_request_id" value="<?php  echo $row['i_id']; ?>">
                        <input type="hidden" name="reject_influencer_request_email" value="<?php  echo $row['i_email']; ?>">

                        <button type="submit" name="reject_influencer_request_btn" class="btn btn-danger"> REJECT</button>
                      </form>
                  </td>
                </tr>
          <?php
                }
              }else {
                echo "No Record Found";
              }
          ?>
     
          
        
        </tbody>
      </table>

    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>