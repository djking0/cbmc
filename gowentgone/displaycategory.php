<?php
session_start();
include("includes/db.php");
include("functions/functions.php");

?>
<!DOCTYPE html>
<html lang="en">
<title>GoWentGone Store</title>

<head>

    <!-- Basic page needs
    ============================================ -->
    <title>Go Went Gone STORE</title>
    <meta charset="utf-8">
    <meta name="keywords"
        content="Go Went Gone Sdn Bhd" />
    <meta name="description"
        content="Go Went Gone Sdn Bhd" />
    <meta name="author" content="Go Went Gone">
    <meta name="robots" content="index, follow" />

    <!-- Mobile specific metas
    ============================================ -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Favicon
    ============================================ -->

    <link rel="shortcut icon" type="image/png" href="ico/favicon-16x16.png" />

     <!--  jQuery -->
     <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

<!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">




    <!-- Libs CSS
    ============================================ -->
    <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="js/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="js/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="css/themecss/lib.css" rel="stylesheet">
    <link href="js/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <link href="js/minicolors/miniColors.css" rel="stylesheet">

    <!-- Theme CSS
    ============================================ -->
    <link href="css/themecss/so_searchpro.css" rel="stylesheet">
    <link href="css/themecss/so_megamenu.css" rel="stylesheet">
    <link href="css/themecss/so-categories.css" rel="stylesheet">
    <link href="css/themecss/so-listing-tabs.css" rel="stylesheet">
    <link href="css/themecss/so-newletter-popup.css" rel="stylesheet">

    <link href="css/footer/footer1.css" rel="stylesheet">
    <link href="css/header/header1.css" rel="stylesheet">
    <link id="color_scheme" href="css/theme.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!-- Google web fonts
    ============================================ -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700' rel='stylesheet'
        type='text/css'>
    <style>
      body {
        font-family: 'Source Sans Pro', sans-serif;
      }
    </style>

</head>

<body class="res layout-1">

    <div id="wrapper" class="wrapper-fluid banners-effect-5">


         <!-- Header Container  -->
         <header id="header" class=" typeheader-1">
            <!-- Header Top -->
            <div class="header-top hidden-compact">
                <div class="container">
                    <div class="row">
                        <div class="header-top-left col-lg-7 col-md-8 col-sm-6 col-xs-4">
                            
                            <ul class="top-link list-inline hidden-lg hidden-md">
                                <li class="account" id="my_account">
                                    <?php
                                    if(!isset($_SESSION['customer_email'])){
                                       ?>
                                        <ul class="dropdown-menu ">
                                                <li><a href="register.php"><i class="fa fa-user"></i> Register</a></li>
                                                <li><a href="checkout.php"><i class="fa fa-pencil-square-o"></i> Login</a></li>
                                            </ul>
                                       <?php
                                    }else{
                                       ?>
                                            <?php
                                            
                                            $customer_email = $_SESSION['customer_email'];
                                    
                                            $get_customer_details = "Select * from customers where customer_email='$customer_email'";
                                            $run_customer_details = mysqli_query($con, $get_customer_details);
                                            $fetch_customer_details = mysqli_fetch_array($run_customer_details);
                                                                                                
                                            $customer_id = $fetch_customer_details['customer_id']; //:: Customer id 
                                            $customer_name = $fetch_customer_details['customer_name']; //:: customer name

                                            ?>
                                             <?php echo" 
                                            $customer_name"?>
                                            <a title="My Account " class="btn-xs dropdown-toggle"
                                           data-toggle="dropdown"> <span class="hidden-xs">My Account </span> <span
                                           class="fa fa-caret-down"></span>
                                           </a>
                                           <ul class="dropdown-menu">
                                               <li><a href="customer_myaccount.php"><i class="fa fa-user"></i> <?php echo" 
                                               $_SESSION[customer_email]"?></a></li>
                                               <li><a href="logout.php"><i class="fa fa-pencil-square-o"></i> Logout</a></li>
                                           </ul>
                                       <?php
                                    }

                                    ?>
                                    
                                </li>
                            </ul>
                        </div>
                        <div class="header-top-right collapsed-block col-lg-5 col-md-4 col-sm-6 col-xs-8">
                            <ul class="top-link list-inline lang-curr">
                                <li class="language">
                                    <div class="btn-group languages-block ">
                                        <form action="http://demo.smartaddons.com/templates/html/emarket/index.php"
                                            method="post" enctype="multipart/form-data" id="bt-language">
                                            <a class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                                <span class="">English</span>
                                                <span class="fa fa-angle-down"></span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li><a href="index.php"> English </a></li>
                                                <li> <a href="index.php">  Bhasa Malayu </a> </li>
                                            </ul>
                                        </form>
                                    </div>

                                </li>
                            </ul>



                        </div>
                    </div>
                </div>
            </div>
            <!-- //Header Top -->

            <!-- Header center -->
            <div class="header-middle">
                <div class="container">
                    <div class="row">
                        <!-- Logo -->
                        <div class="navbar-logo col-lg-2 col-md-2 col-sm-12 col-xs-12">
                            <div class="logo"><a href="index.php"><img src="image/catalog/gowentgonemall.png" title="Your Store"
                                        alt="Your Store" /></a></div>
                        </div>
                        <!-- //end Logo -->

                        <!-- Main menu -->
                        <div class="main-menu col-lg-6 col-md-7 ">
                            <div class="responsive so-megamenu megamenu-style-dev">
                                <nav class="navbar-default">
                                    <div class=" container-megamenu  horizontal open ">
                                        <div class="navbar-header">
                                            <button type="button" id="show-megamenu" data-toggle="collapse"
                                                class="navbar-toggle">
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                        </div>

                                        <div class="megamenu-wrapper">
                                            <span id="remove-megamenu" class="fa fa-times"></span>
                                            <div class="megamenu-pattern">
                                                <div class="container-mega">
                                                <ul class="megamenu" 
                                                        data-animationtime="250">
                                                        <li class="home hover">
                                                            <a href="index.php">HOME </a>
                                                        </li>
                                                        <li class="home hover">
                                                        <a href="hotels.php">STORES </a>
                                                        </li>
                                                        <li class="home hover">
                                                            <a href="displaycategory.php">CATEGORIES </a>
                                                        </li>
                                                        <li class="home hover">
                                                            <a href="aboutus.php">ABOUT US </a>
                                                        </li>
                                                        <li class="home hover">
                                                            <a href="contact.php">CONTACT US </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </nav>
                            </div>
                        </div>
                        <!-- //end Main menu -->

                        <div class="middle-right col-lg-4 col-md-3 col-sm-6 col-xs-8">
                            <div class="signin-w hidden-sm hidden-xs">
                                <ul class="signin-link blank">
                                <?php 
                                if(!isset($_SESSION['customer_email'])){
                                    echo"";
                                }else{
                                    echo" <li class='log login'> 
                                    <a class='link-lg' href='customer_myaccount.php'>$_SESSION[customer_email]</a>
                                 </li>";
                                }
                                ?>
                                </ul>
                            </div>

                            <div class="signin-w hidden-sm hidden-xs">
                                <ul class="signin-link blank">
                                <?php
                            if(!isset($_SESSION['customer_email'])){
                                echo" <li class='log login'></i> 
                                <a class='link-lg' href='checkout.php'>Login</a>
                             </li>";
                             echo" <li class='log login'></i> 
                             <a class='link-lg' href='checkout.php'>|</a>
                          </li>";
                             echo" <li class='log login'></i> 
                             <a class='link-lg' href='register.php'>Create an Account</a>
                          </li>";
                            }else{
                                echo"<li class='log login'></i> 
                                <a class='link-lg' href='logout.php'>Logout</a>
                             </li>";
                            }
                            ?>
                                </ul>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
            <!-- //Header center -->

            <!-- Header Bottom -->
            <div class="header-bottom hidden-compact">
                <div class="container">
                    <div class="row">

                        <div class="bottom1 menu-vertical col-lg-2 col-md-3 col-sm-3">
                            <div class="responsive so-megamenu megamenu-style-dev ">
                                <div class="so-vertical-menu ">
                                    <nav class="navbar-default">

                                        <div class="container-megamenu vertical">
                                            <div id="menuHeading">
                                                <div class="megamenuToogle-wrapper">
                                                    <div class="megamenuToogle-pattern">
                                                        <div class="container">
                                                            <div>
                                                                <span></span>
                                                                <span></span>
                                                                <span></span>
                                                            </div>
                                                            All Categories
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="navbar-header">
                                                <button type="button" id="show-verticalmenu" data-toggle="collapse"
                                                    class="navbar-toggle">
                                                    <i class="fa fa-bars"></i>
                                                    <span> All Categories </span>
                                                </button>
                                            </div>
                                            <div class="vertical-wrapper">
                                                <span id="remove-verticalmenu" class="fa fa-times"></span>
                                                <div class="megamenu-pattern">
                                                    <div class="container-mega">
                                                        <ul class="megamenu">
                                                            <br>

                                                            <?php


                                                            //: Category..
                                                            $getcategoryname = "Select * from food_category limit 0,9";
                                                            $runcategoryname = mysqli_query($con,$getcategoryname);

                                                            while($fetchcategoryname = mysqli_fetch_array($runcategoryname)){

                                                                $fetchcategoryname_id = $fetchcategoryname['fcat_id'];
                                                                $fetchcategoryname_name = $fetchcategoryname['food_cat'];
                                                               
                                                                $getsubcategory = "Select * from subcategory_stageone where sc_main_id = '$fetchcategoryname_id'";
                                                                $runsubcategory = mysqli_query($con, $getsubcategory);
                                                                $countsubcategory = mysqli_num_rows($runsubcategory);

                                                                if($countsubcategory == 0){

                                                                    ?>

                                                                        <li class='item-vertical'>
                                                                        <p class='close-menu'></p>
                                                                        <a href='displayproductsbycat.php?category_id=<?php echo"$fetchcategoryname_id"?>' class='clearfix'>
                                                                            <img src='image/catalog/menu/icons/ico3.png'
                                                                            alt='icon'>
                                                                            <span><?php echo "$fetchcategoryname_name" ?></span>
                                                                        </a>
                                                                    </li>

                                                                    <?php
                                                                   
                                                                }else {

                                                                    ?>

                                                                    <li class='item-vertical css-menu with-sub-menu hover'>
                                                                <p class='close-menu'></p>
                                                                <a href='displayproductsbycat.php?category_id=<?php echo"$fetchcategoryname_id"?>' class='clearfix'>
                                                                <img src='image/catalog/menu/icons/ico3.png'
                                                                        alt='icon'>
                                                                    <span><?php echo "$fetchcategoryname_name" ?></span>
                                                                    <b class='caret'></b>
                                                                </a>
                                                                <div class='sub-menu' data-subwidth='20'>
                                                                    <div class='content'>
                                                                        <div class='row'>
                                                                            <div class='col-sm-12'>
                                                                                <div class='row'>
                                                                                    <div class='col-sm-12 hover-menu'>
                                                                                        <div class='menu'>
                                                                                            <ul>

                                                                                            <?php

                                                                                            while($fetchsubcategory = mysqli_fetch_array($runsubcategory)){
                                                                                                
                                                                                                $subcategory_name = $fetchsubcategory['sc_name'];
                                                                                                $subcategory_id = $fetchsubcategory['sc_id'];
                                                                                                ?>

                                                                                                 <li>
                                                                                                    <a href='displayproductsbycat.php?subcategory_id=<?php echo"$subcategory_id"?>'
                                                                                                        class='main-menu'><?php echo"$subcategory_name" ?></a>
                                                                                                </li>
                                                                                                <?php
                                                

                                                                                            }

                                                                                            ?>
                                                                                               
                                                                                    
                                                                                            
                                                                                               
                                                                                                
                                                                                        
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>

                                                            <?php
                                                                  


                                                                   
                                                                }


                                                            }



                                                            ?>



                                                            <?php 
                                                                // $getcategoryname = "Select * from food_category limit 0,9";
                                                                // $runcategoryname = mysqli_query($con,$getcategoryname);
                                                                // $fetchcategoryname = mysqli_fetch_array($runcategoryname);

                                                                // while($fetchcategoryname = mysqli_fetch_array($runcategoryname)){
                                                                    
                                                                //     $fetchcategoryname_id = $fetchcategoryname['fcat_id'];
                                                                //     $fetchcategoryname_name = $fetchcategoryname['food_cat'];

                                                                //     echo "<li class='item-vertical'>
                                                                //     <p class='close-menu'></p>
                                                                //     <a href='' class='clearfix'>
                                                                //         <img src='image/catalog/menu/icons/ico3.png'
                                                                //         alt='icon'>
                                                                //         <span>$fetchcategoryname_name</span>
                                                                //     </a>
                                                                // </li>";
                                                                // }
                                                            ?>
                                                             
                                                            
                                                           
                                                            <li class="loadmore">
                                                                <a href="displaycategory.php" class='clearfix'>
                                                                <span class="more-view">VIEW ALL</span>
                                                                </a>
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </nav>
                                </div>
                            </div>

                        </div>

                       <!-- Search -->
                       <?php
                       include("features/search.php");
                       ?>
                        <!-- //end Search -->

                        <!-- Secondary menu -->
                        <div class="bottom3 col-lg-3 col-md-3 col-sm-3">


                          <!--cart-->
                          <!--<div class="shopping_cart">
                                <div id="cart" class="btn-shopping-cart">

                                    <a data-loading-text="Loading... " class="btn-group top_cart dropdown-toggle"
                                        data-toggle="dropdown" aria-expanded="true">
                                        <div class="shopcart">
                                            <span class="icon-c">
                                                <i class="fa fa-shopping-bag"></i>
                                            </span>
                                            <div class="shopcart-inner">
                                                <p class="text-shopping-cart">
                                                    My cart
                                                </p>

                                                <?php

                                                if(!isset($_SESSION['customer_email'])){

                                                    ?>

                                                        <span class="total-shopping-cart cart-total-full">
                                                    <span class="items_cart">0</span><span class="items_cart2">
                                                        item(s)</span><span class="items_carts"> + HKD -0 </span>
                                                </span>

                                                <?php
                                                  

                                                }else{

                                                    //: Getting total number of cart_items
                                                    $customer_email = $_SESSION['customer_email'];
                                    
                                                    $get_customer_details = "Select * from customers where customer_email='$customer_email'";
                                                    $run_customer_details = mysqli_query($con, $get_customer_details);
                                                    $fetch_customer_details = mysqli_fetch_array($run_customer_details);
                                                                                                        
                                                    $customer_id = $fetch_customer_details['customer_id']; //:: Customer id 
                                                    $customer_name = $fetch_customer_details['customer_name']; //:: customer name

                                                    $get_items = "select * from customer_foodcart where customer_id='$customer_id'";
                                                    $run_items = mysqli_query($con, $get_items);
                                                    $count_items = mysqli_num_rows($run_items);

                                                    //: Getting the total of customer cart items

                                                    //: Getting the total of prize..

                                                    $query = "select SUM(total_price) as 'sumtotalprice' from customer_foodcart where customer_id='$customer_id' ";
                                                    $res = mysqli_query($con, $query);
                                                    $data = mysqli_fetch_array($res);

                                                    $total = $data['sumtotalprice'];

                                                        //: Getting GST...
                                                        $gst_rate = 6;
                                                        $gst = ($gst_rate / 100) * $total;

                                                        //: Delivery Charges...
                                                        $delivery = 5;

                                                        //: Grand Total..
                                                        $grand_total = ($total + $gst);

                                                        ?>

                                                        <span class="total-shopping-cart cart-total-full">
                                                    <span class="items_cart"><?php echo"$count_items"?></span><span class="items_cart2">
                                                        item(s)</span><span class="items_carts"> + HKD <?php echo "$grand_total"?> </span>
                                                </span>

                                                <?php
                                                    
                                                }

                                                        

                                                ?>

                                                
                                            </div>
                                        </div>
                                    </a>

                                    <?php

                                        if(!isset($_SESSION['customer_email'])){

                                            ?>
                                            <ul class="dropdown-menu pull-right shoppingcart-box">
                                            <li>
                                                <p class="text-center empty">Your shopping cart is empty!</p>
                                            </li>
                                        </ul>
                                            <?php

                                        }else{

                                            ?>

                                        <ul class="dropdown-menu pull-right shoppingcart-box" role="menu">
                                            <li>
                                                <table class="table table-striped">
                                                    <tbody>
                                                        <?php

                                                            //: Getting Customer ID..
                                                            $customer_email = $_SESSION['customer_email'];
                                
                                                            $get_customer_details = "Select * from customers where customer_email='$customer_email'";
                                                            $run_customer_details = mysqli_query($con, $get_customer_details);
                                                            $fetch_customer_details = mysqli_fetch_array($run_customer_details);
                                                            
                                                            $customer_id = $fetch_customer_details['customer_id']; //:: Customer id 
                                                            $customer_name = $fetch_customer_details['customer_name']; //:: customer name

                                                            //: Cart Script..
                                                            $get_cart_data = "Select * from customer_foodcart where customer_id='$customer_id'";
                                                            $run_cart_data = mysqli_query($con, $get_cart_data);
                                                            $count_brands = mysqli_num_rows($run_cart_data);
                                                            if($count_brands==0){
                                                            echo "<li>
                                                            <p class='text-center empty'>Your shopping cart is empty!</p>
                                                        </li>";
                                                            }


                                                            //:: delete item from cart..
                                                            if(isset($_GET['removeitem'])){

                                                                $removeitem_id = $_GET['removeitem'];
                                                                $foodcompanya_id = $_GET['foodcompany'];

                                                                $delete_item = "delete from customer_foodcart where fooditem_id='$removeitem_id' AND customer_id='$customer_id'";

                                                                $run_delete = mysqli_query($con, $delete_item);

                                                                if($run_delete){
                                                                    echo "<script>window.open('food.php?foodcompany=$foodcompanya_id','_self')</script>";
                                                                    
                                                                }
                                                            
                                                            }
                                                            
                                                            while($row_cart_data=mysqli_fetch_array($run_cart_data)){
                                                                
                                                                $fcompanya_id = $_GET['foodcompany'];
                                                                $cart_id=$row_cart_data['foodcart_id'];
                                                                $cart_quantity=$row_cart_data['quantity'];
                                                                $cart_itemid=$row_cart_data['fooditem_id'];

                                                              

                                                                // Getting Product data from food_items table.
                                                                
                                                                $get_cartitem_data = "Select * from food_items where item_id='$cart_itemid'";
                                                                $run_cartitem_data = mysqli_query($con, $get_cartitem_data);
                                                                $fetch_cartitem_data = mysqli_fetch_array($run_cartitem_data);

                                                                $cartitem_id = $fetch_cartitem_data['item_id'];
                                                                $cartitem_image = $fetch_cartitem_data['item_img'];
                                                                $cartitem_price = $fetch_cartitem_data['item_price'];
                                                                $cartitem_name = $fetch_cartitem_data['item_title'];

                                                                //:: 
                                                                $totalunitprice = ($cartitem_price * $cart_quantity);
                                                                
                                                                echo " <tr>
                                                                <td class='text-center' style='width:70px'>
                                                                    <a href='product.html'>
                                                                        <img src='./admin_area/fooditem_images/$cartitem_image'
                                                                            style='width:70px' alt='$cartitem_name'
                                                                            title='$cartitem_name' class='preview'>
                                                                    </a>
                                                                </td>
                                                                <td class='text-left'> <a class='cart_product_name'
                                                                        href='product.html'>$cartitem_name</a>
                                                                </td>
                                                                <td class='text-center'>$cart_quantity</td>
                                                                <td class='text-center'>HKD$totalunitprice</td>
                                                                <td class='text-right'>
                                                                    <a href='product.html' class='fa fa-edit'></a>
                                                                </td>
                                                                <td class='text-right'>
                                                                    
                                                                    <a href='food.php?removeitem=$cartitem_id&foodcompany=$fcompanya_id'class='fa fa-times fa-delete'></a>
                                                                   
                                                                    </td>
                                                            </tr>";

                                                            }


                                                        ?>
                                                    </tbody>
                                                </table>
                                            </li>
                                            <li>
                                                <div>
                                                    <table class="table table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <td class="text-left"><strong>Sub-Total</strong>
                                                                </td>
                                                                <td class="text-right">HKD <?Php echo "$total"?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left"><strong>GST Tax (-6.00)%</strong>
                                                                </td>
                                                                <td class="text-right">HKD<?php echo"$gst"?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left"><strong>Total</strong>
                                                                </td>
                                                                <td class="text-right">HKD <?php echo "$grand_total"?></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <p class="text-right"> 
                                                    <a class="btn view-cart" href="foodcart.php"><i class="fa fa-shopping-cart"></i>Update Cart</a>&nbsp;&nbsp;&nbsp; 

                                                            <a type="submit" value="submit" name="submit" href="#my_modal" class="btn btn-light" data-toggle="modal" ><i class="fa fa-share"></i>Checkout</a>    
                                                    </p>
                                                </div>
                                            </li>
                                        </ul>
                                            
                                            <?php

                                        }
                                    ?>

                                </div>

                            </div>-->
                            <!--//cart-->

                    
                        </div>

                    </div>
                </div>

            </div>
          </header>
        <!-- //Header Container  -->

        <!-- Main Container  -->
        <div class="main-container container">
            <ul class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i></a></li>
                <li><a href="displaycategory.php">CATEGORIES</a></li>
            </ul>

            <div class="row">
                


                <?php

                 //: get store cat_type
                 $getstorecategorytype = "Select * from food_category where cat_type = 'product'";
                 $runstorecategorytype = mysqli_query($con,$getstorecategorytype);
                 $countstorecategorytype = mysqli_num_rows($runstorecategorytype);

                 if($countstorecategorytype == 0){
                    
                 }else {


                 


                
                ?>
                

                    <aside class="col-sm-6 col-md-6 content-aside" id="column-left">
                        <div class="module category-style">
                            
                            <h3 class="modtitle">STORE Categories</h3>
                                <div class="modcontent">
                                    <div class="box-category">
                                        <ul id="cat_accordion" class="list-group">

                                            <?php

                                                //: Category..
                                                $getcategoryname = "Select * from food_category where cat_type = 'product' order by food_cat";
                                                $runcategoryname = mysqli_query($con,$getcategoryname);

                                                while($fetchcategoryname = mysqli_fetch_array($runcategoryname)){

                                                    $fetchcategoryname_id = $fetchcategoryname['fcat_id'];
                                                    $fetchcategoryname_name = $fetchcategoryname['food_cat'];
                                                
                                                    $getsubcategory = "Select * from subcategory_stageone where sc_main_id = '$fetchcategoryname_id'";
                                                    $runsubcategory = mysqli_query($con, $getsubcategory);
                                                    $countsubcategory = mysqli_num_rows($runsubcategory);

                                                    if($countsubcategory == 0){

                                                        ?>

                                                        <li class=""><a href="displayproductsbycat.php?category_id=<?php echo"$fetchcategoryname_id"?>" class="cutom-parent" ><?php echo "$fetchcategoryname_name" ?></a>
                                                            <span class="dcjq-icon"></span></li>

                                                        <?php
                                                    
                                                    }else{

                                                        ?>

                                                        
                                                        <li class="hadchild"><a href="displayproductsbycat.php?category_id=<?php echo"$fetchcategoryname_id"?>" class="cutom-parent"> <?php echo "$fetchcategoryname_name" ?>
                                                            </a> <span class="button-view  fa fa-plus-square-o"></span>
                                                            <ul style="display: block;">

                                                            <?php

                                                                while($fetchsubcategory = mysqli_fetch_array($runsubcategory)){
                                                                    
                                                                    $subcategory_name = $fetchsubcategory['sc_name'];
                                                                    $subcategory_id = $fetchsubcategory['sc_id'];

                                                                    ?>
                                                                        <li><a href="displayproductsbycat.php?subcategory_id=<?php echo"$subcategory_id"?>"><?php echo"$subcategory_name" ?></a></li>
                                                                    <?php
                                                        

                                                                }

                                                            ?>
                                                                
                                                            
                                                            </ul>
                                                        </li>

                                                <?php
                                                    


                                                    
                                                    }

                                                }

                                            ?>
                                            
                                        

                                        
                                        </ul>
                                    </div>


                                </div>
                        </div>
                        
                    
                    </aside>

                    <?php

                        }

                    ?>

                
                <?php

                 //: get food cat_type
                 $gethotelcategorytype = "Select * from food_category where cat_type = 'food'";
                 $runhotelcategorytype = mysqli_query($con,$gethotelcategorytype);
                 $counthotelcategorytype = mysqli_num_rows($runhotelcategorytype);

                 if($counthotelcategorytype == 0){
                    
                }else {


                ?>
                 
                
                
                    <aside class="col-sm-6 col-md-6 content-aside" id="column-lefta">
                        <div class="module category-style">
                            
                            <h3 class="modtitle">HOTEL Categories</h3>
                            <div class="modcontent">
                                <div class="box-category">
                                    <ul id="cat_accordiona" class="list-group">

                                        <?php

                                            //: Category..
                                            $getcategoryname = "Select * from food_category where cat_type = 'food' order by food_cat";
                                            $runcategoryname = mysqli_query($con,$getcategoryname);

                                            while($fetchcategoryname = mysqli_fetch_array($runcategoryname)){

                                                $fetchcategoryname_id = $fetchcategoryname['fcat_id'];
                                                $fetchcategoryname_name = $fetchcategoryname['food_cat'];
                                            
                                                $getsubcategory = "Select * from subcategory_stageone where sc_main_id = '$fetchcategoryname_id'";
                                                $runsubcategory = mysqli_query($con, $getsubcategory);
                                                $countsubcategory = mysqli_num_rows($runsubcategory);

                                                if($countsubcategory == 0){

                                                    ?>

                                                    <li class=""><a href="displayproductsbycat.php?category_id=<?php echo"$fetchcategoryname_id"?>" class="cutom-parent" ><?php echo "$fetchcategoryname_name" ?></a>
                                                        <span class="dcjq-icon"></span></li>

                                                    <?php
                                                
                                                }else{

                                                    ?>

                                                    
                                                    <li class="hadchild"><a href="displayproductsbycat.php?category_id=<?php echo"$fetchcategoryname_id"?>" class="cutom-parent"> <?php echo "$fetchcategoryname_name" ?>
                                                        </a> <span class="button-view  fa fa-plus-square-o"></span>
                                                        <ul style="display: block;">

                                                        <?php

                                                            while($fetchsubcategory = mysqli_fetch_array($runsubcategory)){
                                                                
                                                                $subcategory_name = $fetchsubcategory['sc_name'];
                                                                $subcategory_id = $fetchsubcategory['sc_id'];

                                                                ?>
                                                                    <li><a href="displayproductsbycat.php?subcategory_id=<?php echo"$subcategory_id"?>"><?php echo"$subcategory_name" ?></a></li>
                                                                <?php
                                                    

                                                            }

                                                        ?>
                                                            
                                                        
                                                        </ul>
                                                    </li>

                                            <?php
                                                


                                                
                                                }

                                            }

                                        ?>
                                        
                                    

                                    
                                    </ul>
                                </div>


                            </div>
                        </div>
                        
                    
                    </aside>

                     <?php

                        }

                    ?>
                
            </div>
        </div>
        <!-- //Main Container -->


        <!-- Footer Container -->
        <footer class="footer-container typefooter-1">

            <hr>

            <div class="footer-middle ">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-style">
                            <div class="infos-footer">
                                <h3 class="modtitle">Contact Us</h3>
                                <ul class="menu">
                                    <!--<li class="adres">
                                        input address here
                                    </li>-->
                                    <li class="phone">
                                        (+852) 2433 3841
                                    </li>
                                    <li class="mail">
                                        <a href="">Info@Gowentgone.com</a>
                                    </li>
                                    <li class="time">
                                        Open time: 7:00AM - 5:30PM
                                    </li>
                                </ul>
                            </div>


                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                            <div class="box-information box-footer">
                                <div class="module clearfix">
                                    <h3 class="modtitle">Information</h3>
                                    <div class="modcontent">
                                        <ul class="menu">
                                            <li><a href="aboutus.php">ABOUT US </a></li>
                                            <li><a href="#">FAQ</a></li>
                                            <li><a href="#">Warranty And Services</a></li>
                                            <li><a href="#">Support 24/7 page</a></li>
                                            <li><a href="#">Product Registration</a></li>
                                            <li><a href="#">Product Support</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                            <div class="box-account box-footer">
                                <div class="module clearfix">
                                    <h3 class="modtitle">Extras</h3>
                                    <div class="modcontent">
                                        <ul class="menu">
                                            <li> <a href="aboutus.php"> Go Went Gone </a></li>
                                            <li><a href="displaycategory.php">CATEGORIES</a></li>
                                            <li><a href="contact.php">Contact Us</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                            <div class="box-service box-footer">
                                <div class="module clearfix">
                                    <h3 class="modtitle">Services</h3>
                                    <div class="modcontent">
                                        <ul class="menu">
                                            <li><a href="#">Kuala Lumpur</a></li>
                                            <li><a href="#">Selangor</a></li>
                                            <li><a href="#">KL Central</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-style">


                        </div>

                        <div class="col-lg-12 col-xs-12 text-center">
                            <img src="image/catalog/demo/payment/payment.png" alt="imgpayment">
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer Bottom Container -->
            <div class="footer-bottom ">
                <div class="container">
                    <div class="copyright">
                        gowentgone © 2020. All Rights Reserved. 
                    </div>
                </div>
            </div>
            <!-- /Footer Bottom Container -->


            <!--Back To Top-->
            <div class="back-to-top"><i class="fa fa-angle-up"></i></div>
        </footer>
        <!-- //end Footer Container -->

    </div>

    <!-- Checkout Modal -->
<div class="modal fade" id="my_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                    <div class="modal-header">
                                                    <h6 style="text-align: center;  color: black; font-weight: bold; font-size: 16px;"
                                                            class="modal-title" id="exampleModalLongTitle">SELECT YOUR DINING PREFERENCE </h6>
                                                    
                                                        
                                                    </div>
                                                    <div class="modal-body">

                                                    <?php 

                                                    

                                                    // //: Getting data from food_company table

                                                    // $getfoodcompany_data = "Select * from food_company where fcompany_id = '$foodcompany'";
                                                    // $runfoodcompany_data = mysqli_query($con, $getfoodcompany_data);
                                                    // $fetchfoodcompany_data = mysqli_fetch_array($runfoodcompany_data);

                                                    // $company_type = $fetchfoodcompany_data['company_type'];

                                                    $customer_email = $_SESSION['customer_email'];
                                
                                                    $get_customer_details = "Select * from customers where customer_email='$customer_email'";
                                                    $run_customer_details = mysqli_query($con, $get_customer_details);
                                                    $fetch_customer_details = mysqli_fetch_array($run_customer_details);
                                                                                                        
                                                    $customer_id = $fetch_customer_details['customer_id']; //:: Customer id 
                                                    $customer_name = $fetch_customer_details['customer_name']; //:: customer name


                                                    //: data from customer_foodcart

                                                    $getfoodcart_data = "Select * from customer_foodcart where customer_id = '$customer_id'";
                                                    $runfoodcart_data = mysqli_query($con, $getfoodcart_data);
                                                    $fetchfoodcart_data = mysqli_fetch_array($runfoodcart_data);
                                                    
                                                    $fetchhotel_id = $fetchfoodcart_data['foodhotel_id'];

                                                    
                                                    //: Getting data from food_company table

                                                    $getfoodcompany_data = "Select * from food_company where fcompany_id = '$fetchhotel_id'";
                                                    $runfoodcompany_data = mysqli_query($con, $getfoodcompany_data);
                                                    $fetchfoodcompany_data = mysqli_fetch_array($runfoodcompany_data);

                                                    $company_type = $fetchfoodcompany_data['company_type'];

                                                    if($company_type == 'food'){

                                                        ?>

                                                        <div class="form-group col-md-3 ">
                                                            <div class="card" style="width: 18rem;">
                                                                <img class="card-img-top" src="./assets/dinein.jpeg" alt="Card image cap">
                                                                <div class="card-body">
                                                                    <h2 class="card-title">DINE IN</h5>
                                                                    <p class="card-text">Choose your table and enjoy dinner</p>
                                                                    <a id="button-comment" href="#dyinginModal" data-toggle="modal"
                                                                    class="btn buttonGray" ><span>SELECT</span></a>
                                                                </div>
                                                             </div>
                                                        </div>
                                                      
                                                        

                                                        <div class="form-group col-md-1 ">
                                                        </div>

                                                        <div class="form-group col-md-3 ">
                                                            <div class="card" style="width: 18rem;">
                                                                <img class="card-img-top" src="./assets/pickup.jpeg" alt="Card image cap">
                                                                <div class="card-body">
                                                                    <h2 class="card-title" style="align: center;">Pickup</h5>
                                                                    <p class="card-text">Self Collect and beat the queue</p>

                                                                    <a id="button-comment" href="#datetimeModal" data-toggle="modal"
                                                                    class="btn buttonGray" ><span>SELECT</span></a>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        

                                                        <div class="form-group col-md-1 ">
                                                        </div>



                                                        <div class="form-group col-md-3 ">
                                                            <div class="card" style="width: 18rem;">
                                                                <img class="card-img-top" src="./assets/delivery.jpeg" alt="Card image cap">
                                                                <div class="card-body">
                                                                    <h2 class="card-title">Delivery</h5>
                                                                    <p class="card-text">We will deliver your food at your doorstep</p>
                                                                    <a id="button-comment" href="fcheckout.php?delivery"
                                                                    class="btn buttonGray"><span>SELECT</span></a>
                                                                </div>
                                                             </div>
                                                        </div>

                                                        <div class="form-group col-md-1">
                                                        </div>
                                                        

                                                        <?php
                                                       
                                                    } else if($company_type == 'product') {

                                                        ?>

                                                        <div class="form-group col-md-6 ">
                                                            <div class="card" style="width: 18rem;">
                                                                <img class="card-img-top" src="./assets/pickup.jpeg" alt="Card image cap">
                                                                <div class="card-body">
                                                                    <h2 class="card-title" style="align: center;">Pickup</h5>
                                                                    <p class="card-text">Self Collect and beat the queue</p>

                                                                    <a id="button-comment" href="#datetimeModal" data-toggle="modal"
                                                                    class="btn buttonGray" ><span>SELECT</span></a>
                                                                </div>
                                                            </div>
                                                        </div>



                                                        <div class="form-group col-md-6">
                                                            <div class="card" style="width: 18rem;">
                                                                <img class="card-img-top" src="./assets/delivery.jpeg" alt="Card image cap">
                                                                <div class="card-body">
                                                                    <h2 class="card-title">Delivery</h5>
                                                                    <p class="card-text">We will deliver your food at your doorstep</p>
                                                                    <a id="button-comment" href="fcheckout.php?delivery"
                                                                    class="btn buttonGray"><span>SELECT</span></a>
                                                                </div>
                                                             </div>
                                                        </div>
                                                        <?php
                                                           
                                                    }else{

                                                        echo "Your Shopping cart is empty";

                                                    }
                                                    ?>

                                                    

                                                    


                                                        <div style=" border-top: 0 none;" class="modal-footer"> 
                                                        
                                                        </div>
                                                    
                                                        <!-- </form> -->
                                                    
                                                    </div>
                                                    
                                                    </div>
                            </div>
</div>
<!-- /Checkout Modal -->




<?php 

   

        //  if(!isset($_SESSION['customer_email'])){

        // }else {


        $get_carttable_rows = "Select * from customer_foodcart";
        $run_query = mysqli_query($con, $get_carttable_rows);
        $count =  mysqli_num_rows($run_query);

        if($count == 0){

        }else {



        $gethotelid = "Select * from customer_foodcart where customer_id='$customer_id'";
        $runhotelid = mysqli_query($con, $gethotelid);
        $fetch_hotelid = mysqli_fetch_array($runhotelid);
        $hotelid = $fetch_hotelid['foodhotel_id'];

        $gethotelAddress = "Select * from food_company where fcompany_id='$hotelid'";
        $runhotelAddress = mysqli_query($con, $gethotelAddress);
        $fetch_hotelAddress = mysqli_fetch_array($runhotelAddress);
        $hotelAddress = $fetch_hotelAddress['company_address'];


        }

    // }


?>


 <!-- (PICK UP) DATE & TIME MODAL -->
    
 <div class="modal fade" id="datetimeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h1 style="text-align: center;  color: black; font-weight: bold; font-size: 16px;"
                class="modal-title" id="exampleModalLongTitle">PICK UP</h1>                              
            </div>
            <div class="modal-body">
                <form action="addcustomerorder.php" method="POST" enctype="multipart/form-data">
                    
                <div class="form-group col-md-12 ">
                        <label for="inputAddress"><h3><strong>Pickup Store: </strong> </h3> </label>
                        <br>
                        <?php echo "$hotelAddress" ?>
                        
                    </div>
                    
                    <div class="form-group col-md-12 ">
                        <label for="inputAddress"><h3><strong>Select Pickup Date: </strong> </h3> </label>
                        <input class="form-control" id="date" placeholder="DD-MM-YYYY" name="date" required="required">
                    </div>

                                    
                    <div class="form-group col-md-12 ">
                        <label for="inputAddress"><h3><strong>Select Pickup Time: </strong> </h3> </label>
                        <select name="picktime" class="form-control" id="sel1">
                            <option>11:00AM to 11:30AM</option>
                            <option>11:30AM to 12:00PM</option>
                            <option>12:00PM to 12:30PM</option>
                            <option>12:30PM to 01:00PM</option>
                            <option>01:00PM to 01:30PM</option>
                            <option>01:30PM to 02:00PM</option>
                            <option>02:00PM to 02:30PM</option>
                            <option>02:30PM to 03:00PM</option>
                            <option>03:00PM to 03:30PM</option>
                            <option>03:30PM to 04:00PM</option>
                            <option>04:00PM to 04:30PM</option>
                            <option>04:30PM to 05:00PM</option>
                        </select>
                    </div>

                    <div class="form-group col-md-12 ">
                        <input type="hidden" class="form-control"  value= "<?php echo $hotelid ?>" name="hotelid">
                    </div>


                    <div style=" border-top: 0 none;" class="modal-footer"> 
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" type="submit" id="registeruser"  name="submit1">Proceed to Order</button>
                    </div>
                                                        
                </form>
                                                            
            </div>
                                                            
        </div>
    </div>

</div>


<!-- /DATE & TIME MODAL -->

 <!-- (DYINGIN MODAL) -->
    
 <div class="modal fade" id="dyinginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h1 style="text-align: center;  color: black; font-weight: bold; font-size: 16px;"
                class="modal-title" id="exampleModalLongTitle">DINE IN </h1>                              
            </div>
            <div class="modal-body">
                <form action="addcustomerorder.php" method="POST" enctype="multipart/form-data">
                    
                <div class="form-group col-md-12 ">
                        <label for="inputAddress"><h3><strong>Store: </strong> </h3> </label>
                        <br>
                        <?php echo "$hotelAddress" ?>
                        
                    </div>
                    
                    <div class="form-group col-md-12 ">
                        <label for="inputAddress"><h3><strong>TABLE / ROOM NO: </strong> </h3> </label>
                        <input type="number" class="form-control" name="table_no" required="required" >
                    </div>

                    <div class="form-group col-md-12 ">
                        <input type="hidden" class="form-control"  value= "<?php echo $hotelid ?>" name="hotelid">
                        
                    </div>


                    <div style=" border-top: 0 none;" class="modal-footer"> 
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" type="submit" id="dying_in"  name="dying_in">Proceed to Order</button>
                    </div>                                    
                </form>
                                                            
            </div>
                                                            
        </div>
    </div>

</div>


<!-- (DYINGIN MODAL) -->


   

    <!-- Include Libs & Plugins
	============================================ -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="js/jquery-2.2.4.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/owl-carousel/owl.carousel.js"></script>
    <script type="text/javascript" src="js/themejs/libs.js"></script>
    <script type="text/javascript" src="js/unveil/jquery.unveil.js"></script>
    <script type="text/javascript" src="js/countdown/jquery.countdown.min.js"></script>
    <script type="text/javascript" src="js/dcjqaccordion/jquery.dcjqaccordion.2.8.min.js"></script>
    <script type="text/javascript" src="js/datetimepicker/moment.js"></script>
    <script type="text/javascript" src="js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>


    <!-- Theme files
	============================================ -->


    <script type="text/javascript" src="js/themejs/so_megamenu.js"></script>
    <script type="text/javascript" src="js/themejs/addtocart.js"></script>
    <script type="text/javascript" src="js/themejs/application.js"></script>
    <script type="text/javascript">
        <!--
        // Check if Cookie exists
        if ($.cookie('display')) {
            view = $.cookie('display');
        } else {
            view = 'grid';
        }
        if (view) display(view);
        //-->
    </script>
</body>

<!--  demo.smartaddons.com/templates/html/emarket/category.html by AkrAm, Sat, 20 Apr 2019 20:00:15 GMT -->

</html>