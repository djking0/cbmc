<?php
session_start();
include("includes/db.php");
include("functions/functions.php");

?>
<!DOCTYPE html>
<html lang="en">


<title>GoWentGone Store</title>
<head>
    <!-- Libs CSS
    ============================================ -->
    <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="js/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="js/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="css/themecss/lib.css" rel="stylesheet">
    <link href="js/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <link href="js/minicolors/miniColors.css" rel="stylesheet">

   
    <!-- Theme CSS
    ============================================ -->
    <link href="css/themecss/so_searchpro.css" rel="stylesheet">
    <link href="css/themecss/so_megamenu.css" rel="stylesheet">
    <link href="css/themecss/so-categories.css" rel="stylesheet">
    <link href="css/themecss/so-listing-tabs.css" rel="stylesheet">
    <link href="css/themecss/so-newletter-popup.css" rel="stylesheet">

    <link href="css/footer/footer1.css" rel="stylesheet">
    <link href="css/header/header1.css" rel="stylesheet">
    <link id="color_scheme" href="css/theme.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">




    <!-- Google web fonts
    ============================================ -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700' rel='stylesheet'
        type='text/css'>
    <style type="text/css">
        body {
            font-family: 'Roboto', sans-serif
        }
    </style>

</head>

<body class="res layout-1">

    <div id="wrapper" class="wrapper-fluid banners-effect-5">


      

        <!-- Main Container  -->

    
        <div class="main-container container">
          
              <div class="row">
                    <div class="info-contact clearfix">
                        
                        <div class="col-lg-8 col-sm-8 col-xs-12 contact-form">
                            <form action="contact.php" method="post" enctype="multipart/form-data" class="form-horizontal">
                                <fieldset>
                                
                                    <h1> Welcome to Putraja Hotel </h1>
                                    <p> Enter your name, Email and Press "I AM IN" button.</p>
                                    <br>
                                    <br>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-name">Your Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="contact_name"  id="input-name"
                                                class="form-control" required="required">
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-email">E-Mail Address</label>
                                        <div class="col-sm-10">
                                            <input type="email" name="contact_email"  id="input-email"
                                                class="form-control" required="required">
                                        </div>
                                    </div>

                                </fieldset>
                                <div class="buttons">
                                    <div class="pull-right">
                                        <button class="btn btn-default buttonGray" name="addcontactquery" type="submit">
                                            <span>I AM IN</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

           
        </div>
        <!-- //Main Container -->

        

    </div>


  


    <!-- Include Libs & Plugins
	============================================ -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="js/jquery-2.2.4.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/owl-carousel/owl.carousel.js"></script>
    <script type="text/javascript" src="js/themejs/libs.js"></script>
    <script type="text/javascript" src="js/unveil/jquery.unveil.js"></script>
    <script type="text/javascript" src="js/countdown/jquery.countdown.min.js"></script>
    <script type="text/javascript" src="js/dcjqaccordion/jquery.dcjqaccordion.2.8.min.js"></script>
    <script type="text/javascript" src="js/datetimepicker/moment.js"></script>
    <script type="text/javascript" src="js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>


    <!-- Theme files
	============================================ -->


    <script type="text/javascript" src="js/themejs/so_megamenu.js"></script>
    <script type="text/javascript" src="js/themejs/addtocart.js"></script>
    <script type="text/javascript" src="js/themejs/application.js"></script>
    <script type="text/javascript">
     
        // Check if Cookie exists
        if ($.cookie('display')) {
            view = $.cookie('display');
        } else {
            view = 'grid';
        }
        if (view) display(view);
        
    </script>
</body>
</html>



<?php

if(isset($_POST['addcontactquery'])){

    $contact_name = $_POST['contact_name'];
    $contact_email = $_POST['contact_email'];
    $contact_enquiry = $_POST['contact_enquiry'];


    $query = "INSERT INTO contact_query(contact_customer_name,contact_customer_email, contact_customer_enquiry) values ('$contact_name', '$contact_email', '$contact_enquiry')";
    $run_query = mysqli_query($con, $query);

    if($run_query){
        echo"<script>alert ('Your Query is Submitted, We will contact you at your given Email Address')</script>";
        echo "<script>window.open('contact.php','_self')</script>";
    }else {
        echo"<script>alert ('Error')</script>";
        echo "<script>window.open('contact.php','_self')</script>";
    }
}

?>



