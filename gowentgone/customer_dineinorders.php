<?php
include ("includes/db.php");


$c = $_SESSION ['customer_email'];

$get_c = "select * from customers where customer_email = '$c'";

$run_c = mysqli_query($con , $get_c);

$row_c = mysqli_fetch_array($run_c);

$customer_id = $row_c['customer_id'];
?>

 <!--Custom Style-->
<link href="css/order.css" rel="stylesheet">

 <!--Middle Part Start-->
 <div id="content" class="col-sm-9">
                    <h2 class="title">Dine-In Orders</h2>
                    <div class="table-responsive">
                    
                    <?php 

                    $query = "SELECT * FROM customer_dyingin_order where dcustomer_id = '$customer_id' ORDER BY dorder_date desc";
                    $query_run = mysqli_query($con, $query);


                    ?>
                        <table class="table table-bordered table-hover">
                            <thead>

                                <tr>
                                    <td class="text-center">Order ID</td>
                                    <td class="text-center">Order Description</td>
                                    <td class="text-center">Hotel/Store</td>
                                    <td class="text-center">Date Added</td>
                                    <td class="text-center">Invoice No</td>
                                    <td class="text-center">Table No</td>
                                    <td class="text-center">Total Due Amount with 6% GST</td>
                                    <td class="text-center">Payment Method</td>
                                    <td class="text-center">Status</td>
                                    <td class="text-center">Receipt</td>
                                </tr>
                            </thead>
                            <tbody>

                            <?php 

                                if(mysqli_num_rows($query_run) > 0)        
                                {
                                    $i = 1;
                                    while($row = mysqli_fetch_assoc($query_run))
                                    {

                            ?>
                                <tr>
                                    <td> <?php  echo $row['dorder_id']; ?></td>
                                    <td> <?php  echo $row['dorder_description']; ?></td>
                                    <td> 
                                        <?php  
                                            $hotel_id  = $row['dhotel_id']; 

                                            $get_hotel_data = "Select * from food_company where fcompany_id = '$hotel_id'";
                                            $run_hotel_data = mysqli_query($con,$get_hotel_data);
                                            $fetch_hotel_data = mysqli_fetch_array($run_hotel_data);

                                            $hotel_name = $fetch_hotel_data['Company_name'];

                                            echo "$hotel_name";
            
                                        ?>
                                    </td>
                                    <td> <?php  echo $row['dorder_date']; ?></td>
                                    <td> <?php  echo $row['dinvoiceno']; ?></td>
                                    <td> <?php  echo $row['dorder_tableno']; ?></td>
                                    <td> <?php  echo $row['ddue_amount']; ?></td>
                                    <td> <?php  echo $row['dorder_status']; ?></td>
                                    <td class="text-center"><a class="btn btn-info" title="" data-toggle="tooltip"
                                            href="customer_myaccount.php?order_dinein_information=<?php echo $row['dorder_id']?>" data-original-title="View"><i
                                                class="fa fa-eye"></i></a>
                                    </td>
                                    <td>
                                        <a data-toggle="modal" href="#editreceipt-<?php echo "$i" ?>"> <button class="btn btn-info"><i class="fa fa-plus"></i></button>
                                     </a>                                      
                                    </td>
                                    
                                </tr>
                                    
                            
                <!--Middle Part End-->

<!-- OWNER PICTURE -->
<div class="modal fade" id="editreceipt-<?php echo "$i" ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="container-fluid">
                <div class="modal-header d-flex">
                            <h5 class="modal-title flex-larger title-text" id="exampleModalLabel">Update Receipt</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                </div>
                <form action="main_code.php" method="POST" enctype="multipart/form-data">

                    <div class="modal-body">

                        <div class="form-group">
                            <label class="body-text"> New Receipt: </label>
                            <input type="file" name="store_updatereceiptimg" class="form-control"  required="required">    
                            <br>
                            <br>
                            <!-- <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="inputZip">Food Image</label>
                                    <input type="file" class="form-control" name="food_updateimg">
                                </div>
                            </div>
                            -->
                            <div class="alert bg-light border-radius" role="alert">
                                <b>Your Current Receipt Picture: </b>
                                <br>
                                <br>
                                <img src="receipt_img/<?php  echo $row['receipt_img']; ?>" width="100px;" height="100px;" alt="image">
                            </div>
                        </div>
                    
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" class="form-control"  value= "<?php  echo $row['dorder_id']; ?>" name="ddorder_id">
                        <input type="hidden" class="form-control"  value= " <?php  echo "$customer_id" ?>" name="corder_id">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" name="save_receipt2" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
                                
                                <?php
                                $i++;
                                }
                                }else {
                                echo "No Record Found";
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>

                </div>