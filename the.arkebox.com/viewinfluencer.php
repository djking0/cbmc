<?php
session_start();
include("includes/db.php");
include("functions/functions.php");

?>
<!DOCTYPE html>
<html lang="en">



<head>
	<title>CBMC Store</title>
    <!-- Libs CSS
    ============================================ -->
    <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="js/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="js/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="css/themecss/lib.css" rel="stylesheet">
    <link href="js/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <link href="js/minicolors/miniColors.css" rel="stylesheet">

    <!-- Theme CSS
    ============================================ -->
    <link href="css/themecss/so_searchpro.css" rel="stylesheet">
    <link href="css/themecss/so_megamenu.css" rel="stylesheet">
    <link href="css/themecss/so-categories.css" rel="stylesheet">
    <link href="css/themecss/so-listing-tabs.css" rel="stylesheet">
    <link href="css/themecss/so-newletter-popup.css" rel="stylesheet">

    <link href="css/footer/footer1.css" rel="stylesheet">
    <link href="css/header/header1.css" rel="stylesheet">
    <link id="color_scheme" href="css/theme.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    
    <!-- Google web fonts
    ============================================ -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700' rel='stylesheet'
        type='text/css'>
    <style type="text/css">
        body {
            font-family: 'Roboto', sans-serif
        }
    </style>

</head>

<body class="res layout-1">

    <div id="wrapper" class="wrapper-fluid banners-effect-5">


      
         <!-- Header Container  -->
         <header id="header" class=" typeheader-1">
            <!-- Header Top -->
            <div class="header-top hidden-compact">
                <div class="container">
                    <div class="row">
                        <div class="header-top-left col-lg-7 col-md-8 col-sm-6 col-xs-4">
                            
                                
                            <ul class="top-link list-inline hidden-lg hidden-md">
                                <li class="account" id="my_account">
                                    <?php
                                    if(!isset($_SESSION['customer_email'])){
                                       ?>
                                        <ul class="dropdown-menu ">
                                                <li><a href="register.php"><i class="fa fa-user"></i> Register</a></li>
                                                <li><a href="checkout.php"><i class="fa fa-pencil-square-o"></i> Login</a></li>
                                            </ul>
                                       <?php
                                    }else{
                                       ?>
                                            <?php
                                            
                                            $customer_email = $_SESSION['customer_email'];
                                    
                                            $get_customer_details = "Select * from customers where customer_email='$customer_email'";
                                            $run_customer_details = mysqli_query($con, $get_customer_details);
                                            $fetch_customer_details = mysqli_fetch_array($run_customer_details);
                                                                                                
                                            $customer_id = $fetch_customer_details['customer_id']; //:: Customer id 
                                            $customer_name = $fetch_customer_details['customer_name']; //:: customer name

                                            ?>
                                             <?php echo" 
                                               $customer_name"?>
                                             <a href="#" title="My Account " class="btn-xs dropdown-toggle"
                                            data-toggle="dropdown"> <span class="hidden-xs">My Account </span> <span
                                            class="fa fa-caret-down"></span>
                                            </a>
                                            <ul class="dropdown-menu ">
                                                <li><a href="food.php"><i class="fa fa-user"></i> <?php echo" 
                                                $_SESSION[customer_email]"?></a></li>
                                                <li><a href="logout.php"><i class="fa fa-pencil-square-o"></i> Logout</a></li>
                                                
                                            </ul>

                                       <?php
                                    }

                                    ?>
                                    
                                    <!-- <ul class="dropdown-menu ">
                                        <li><a href="register.html"><i class="fa fa-user"></i> Register</a></li>
                                        <li><a href="login.html"><i class="fa fa-pencil-square-o"></i> Login</a></li>
                                    </ul> -->
                                </li>
                            </ul>
                        </div>
                        <div class="header-top-right collapsed-block col-lg-5 col-md-4 col-sm-6 col-xs-8">
                            <ul class="top-link list-inline lang-curr">
                                <li class="language">
                                    <div class="btn-group languages-block ">
                                        <form action="http://demo.smartaddons.com/templates/html/emarket/index.html"
                                            method="post" enctype="multipart/form-data" id="bt-language">
                                            <a class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                                <span class="">English</span>
                                                <span class="fa fa-angle-down"></span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li><a href="index.html"> English </a></li>
                                                <li> <a href="index.html">  Bhasa Malayu </a> </li>
                                            </ul>
                                        </form>
                                    </div>

                                </li>
                            </ul>



                        </div>
                    </div>
                </div>
            </div>
            <!-- //Header Top -->

            <!-- Header center -->
            <div class="header-middle">
                <div class="container">
                    <div class="row">
                        <!-- Logo -->
                        <div class="navbar-logo col-lg-2 col-md-2 col-sm-12 col-xs-12">
                            <div class="logo"><a href="index.html"><img src="image/catalog/ark_logo.png" height="90" width="90" title="Your Store"></a></div>
                        </div>
                        <!-- //end Logo -->

                        <!-- Main menu -->
                        <div class="main-menu col-lg-6 col-md-7 ">
                            <div class="responsive so-megamenu megamenu-style-dev">
                                <nav class="navbar-default">
                                    <div class=" container-megamenu  horizontal open ">
                                        <div class="navbar-header">
                                            <button type="button" id="show-megamenu" data-toggle="collapse"
                                                class="navbar-toggle">
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                        </div>

                                        <div class="megamenu-wrapper">
                                            <span id="remove-megamenu" class="fa fa-times"></span>
                                            <div class="megamenu-pattern">
                                                <div class="container-mega">
                                                <ul class="megamenu" 
                                                        data-animationtime="250">
                                                        <li class="home hover">
                                                            <a href="index.php">HOME </a>
                                                        </li>
                                                        <li class="home hover">
                                                        <a href="hotels.php">STORES </a>
                                                        </li>
                                                        <li class="home hover">
                                                            <a href="displaycategory.php">CATEGORIES </a>
                                                        </li>
                                                        <li class="home hover">
                                                            <a href="aboutus.php">ABOUT US </a>
                                                        </li>
                                                        <li class="home hover">
                                                            <a href="contact.php">CONTACT US </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </nav>
                            </div>
                        </div>
                        <!-- //end Main menu -->

                        <div class="middle-right col-lg-4 col-md-3 col-sm-6 col-xs-8">
                            <div class="signin-w hidden-sm hidden-xs">
                                <ul class="signin-link blank">
                                <?php 
                                if(!isset($_SESSION['customer_email'])){
                                    echo"";
                                }else{
                                    echo" <li class='log login'> 
                                    <a class='link-lg'>$_SESSION[customer_email]</a>
                                 </li>";
                                }
                                ?>
                                </ul>
                            </div>



                           

                            <div class="signin-w hidden-sm hidden-xs">
                                <ul class="signin-link blank">
                                <?php
                            if(!isset($_SESSION['customer_email'])){
                                echo" <li class='log login'></i> 
                                <a class='link-lg' href='checkout.php'>Login</a>
                             </li>";
                             echo" <li class='log login'></i> 
                             <a class='link-lg' href='checkout.php'>|</a>
                          </li>";
                             echo" <li class='log login'></i> 
                             <a class='link-lg' href='register.php'>Create an Account</a>
                          </li>";
                            }else{
                                echo"<li class='log login'></i> 
                                <a class='link-lg' href='logout.php'>Logout</a>
                             </li>";
                            }
                            ?>
                                </ul>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
            <!-- //Header center -->

            <!-- Header Bottom -->
            <div class="header-bottom hidden-compact">
                <div class="container">
                    <div class="row">

                        <div class="bottom1 menu-vertical col-lg-2 col-md-3 col-sm-3">
                            <div class="responsive so-megamenu megamenu-style-dev ">
                                <div class="so-vertical-menu ">
                                    <nav class="navbar-default">

                                        <div class="container-megamenu vertical">
                                            <div id="menuHeading">
                                                <div class="megamenuToogle-wrapper">
                                                    <div class="megamenuToogle-pattern">
                                                        <div class="container">
                                                            <div>
                                                                <span></span>
                                                                <span></span>
                                                                <span></span>
                                                            </div>
                                                            All Categories
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="navbar-header">
                                                <button type="button" id="show-verticalmenu" data-toggle="collapse"
                                                    class="navbar-toggle">
                                                    <i class="fa fa-bars"></i>
                                                    <span> All Categories </span>
                                                </button>
                                            </div>
                                            <div class="vertical-wrapper">
                                                <span id="remove-verticalmenu" class="fa fa-times"></span>
                                                <div class="megamenu-pattern">
                                                    <div class="container-mega">
                                                        <ul class="megamenu">
                                                            
                                                            
                                                             <li class="item-vertical">
                                                                <p class="close-menu"></p>
                                                                <a href="" class="clearfix">
                                                                    <span>FOOD</span>

                                                                </a>
                                                            </li>
                                                            <!-- <li class="item-vertical  style1 with-sub-menu hover">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <span class="label"></span>
                                                                    <img src="image/catalog/menu/icons/ico9.png"
                                                                        alt="icon">
                                                                    <span>Electronic</span>

                                                                    <b class="caret"></b>
                                                                </a>
                                                                <div class="sub-menu" data-subwidth="40">
                                                                    <div class="content">
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 static-menu">
                                                                                        <div class="menu">
                                                                                            <ul>
                                                                                                <li><a href="#"
                                                                                                        class="main-menu">Smartphone</a>
                                                                                                    <ul>
                                                                                                        <li><a
                                                                                                                href="#">Esdipiscing</a>
                                                                                                        </li>
                                                                                                        <li><a
                                                                                                                href="#">Scanners</a>
                                                                                                        </li>
                                                                                                        <li><a
                                                                                                                href="#">Apple</a>
                                                                                                        </li>
                                                                                                        <li><a
                                                                                                                href="#">Dell</a>
                                                                                                        </li>
                                                                                                        <li><a
                                                                                                                href="#">Scanners</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                                <li><a href="#"
                                                                                                        class="main-menu">Electronics</a>
                                                                                                    <ul>
                                                                                                        <li><a
                                                                                                                href="#">Asdipiscing</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Diam
                                                                                                                sit</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Labore
                                                                                                                et</a>
                                                                                                        </li>
                                                                                                        <li><a
                                                                                                                href="#">Monitors</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="row banner">
                                                                                    <a href="#">
                                                                                        <img src="image/catalog/menu/megabanner/vbanner1.jpg"
                                                                                            alt="banner1">
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="item-vertical with-sub-menu hover">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico7.png"
                                                                        alt="icon">
                                                                    <span>Health &amp; Beauty</span>
                                                                    <b class="caret"></b>
                                                                </a>
                                                                <div class="sub-menu" data-subwidth="60">
                                                                    <div class="content">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-4 static-menu">
                                                                                        <div class="menu">
                                                                                            <ul>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Car
                                                                                                        Alarms and
                                                                                                        Security</a>
                                                                                                    <ul>
                                                                                                        <li><a href="#">Car
                                                                                                                Audio
                                                                                                                &amp;
                                                                                                                Speakers</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Gadgets
                                                                                                                &amp;
                                                                                                                Auto
                                                                                                                Parts</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Gadgets
                                                                                                                &amp;
                                                                                                                Auto
                                                                                                                Parts</a>
                                                                                                        </li>
                                                                                                        <li><a href="#">Headphones,
                                                                                                                Headsets</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="24.html"
                                                                                                        onclick="window.location = '24.html';"
                                                                                                        class="main-menu">Health
                                                                                                        &amp; Beauty</a>
                                                                                                    <ul>
                                                                                                        <li>
                                                                                                            <a href="#">Home
                                                                                                                Audio</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Helicopters
                                                                                                                &amp;
                                                                                                                Parts</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Outdoor
                                                                                                                &amp;
                                                                                                                Traveling</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Toys
                                                                                                                &amp;
                                                                                                                Hobbies</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 static-menu">
                                                                                        <div class="menu">
                                                                                            <ul>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Electronics</a>
                                                                                                    <ul>
                                                                                                        <li>
                                                                                                            <a
                                                                                                                href="#">Earings</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Salon
                                                                                                                &amp;
                                                                                                                Spa
                                                                                                                Equipment</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Shaving
                                                                                                                &amp;
                                                                                                                Hair
                                                                                                                Removal</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Smartphone
                                                                                                                &amp;
                                                                                                                Tablets</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Sports
                                                                                                        &amp;
                                                                                                        Outdoors</a>
                                                                                                    <ul>
                                                                                                        <li>
                                                                                                            <a href="#">Flashlights
                                                                                                                &amp;
                                                                                                                Lamps</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a
                                                                                                                href="#">Fragrances</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a
                                                                                                                href="#">Fishing</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">FPV
                                                                                                                System
                                                                                                                &amp;
                                                                                                                Parts</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 static-menu">
                                                                                        <div class="menu">
                                                                                            <ul>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">More
                                                                                                        Car
                                                                                                        Accessories</a>
                                                                                                    <ul>
                                                                                                        <li>
                                                                                                            <a href="#">Lighter
                                                                                                                &amp;
                                                                                                                Cigar
                                                                                                                Supplies</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Mp3
                                                                                                                Players
                                                                                                                &amp;
                                                                                                                Accessories</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Men
                                                                                                                Watches</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Mobile
                                                                                                                Accessories</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Gadgets
                                                                                                        &amp; Auto
                                                                                                        Parts</a>
                                                                                                    <ul>
                                                                                                        <li>
                                                                                                            <a href="#">Gift
                                                                                                                &amp;
                                                                                                                Lifestyle
                                                                                                                Gadgets</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Gift
                                                                                                                for
                                                                                                                Man</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Gift
                                                                                                                for
                                                                                                                Woman</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Gift
                                                                                                                for
                                                                                                                Woman</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="item-vertical css-menu with-sub-menu hover">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">

                                                                    <img src="image/catalog/menu/icons/ico6.png"
                                                                        alt="icon">
                                                                    <span>Smartphone &amp; Tablets</span>
                                                                    <b class="caret"></b>
                                                                </a>
                                                                <div class="sub-menu" data-subwidth="20">
                                                                    <div class="content">
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <div class="row">
                                                                                    <div class="col-sm-12 hover-menu">
                                                                                        <div class="menu">
                                                                                            <ul>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Headphones,
                                                                                                        Headsets</a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Home
                                                                                                        Audio</a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Health
                                                                                                        &amp; Beauty</a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Helicopters
                                                                                                        &amp; Parts</a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#"
                                                                                                        class="main-menu">Helicopters
                                                                                                        &amp; Parts</a>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="item-vertical">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico5.png"
                                                                        alt="icon">
                                                                    <span>Health & Beauty</span>

                                                                </a>
                                                            </li>
                                                            <li class="item-vertical">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico4.png"
                                                                        alt="icon">
                                                                    <span>Bathroom</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico3.png"
                                                                        alt="icon">
                                                                    <span>Metallurgy</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico2.png"
                                                                        alt="icon">
                                                                    <span>Bedroom</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical">
                                                                <p class="close-menu"></p>

                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico1.png"
                                                                        alt="icon">
                                                                    <span>Health &amp; Beauty</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical" style="display: none;">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico11.png"
                                                                        alt="icon">
                                                                    <span>Toys &amp; Hobbies </span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical" style="display: none;">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico12.png"
                                                                        alt="icon">
                                                                    <span>Jewelry &amp; Watches</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical" style="display: none;">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico9.png"
                                                                        alt="icon">
                                                                    <span>Home &amp; Lights</span>
                                                                </a>
                                                            </li>
                                                            <li class="item-vertical" style="display: none;">
                                                                <p class="close-menu"></p>
                                                                <a href="#" class="clearfix">
                                                                    <img src="image/catalog/menu/icons/ico6.png"
                                                                        alt="icon">
                                                                    <span>Metallurgy</span>
                                                                </a>
                                                            </li>

                                                            <li class="loadmore">
                                                                <i class="fa fa-plus-square-o"></i>
                                                                <span class="more-view">More Categories</span>
                                                            </li> -->

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </nav>
                                </div>
                            </div>

                        </div>

                         <!-- Search -->
                           <?php
                           include("features/search.php");
                           ?>
                        <!-- //end Search -->

                        <!-- Secondary menu -->
                        <div class="bottom3 col-lg-3 col-md-3 col-sm-3">


                          <!--cart-->
                          <div class="shopping_cart">
                                <div id="cart" class="btn-shopping-cart">

                                    <a data-loading-text="Loading... " class="btn-group top_cart dropdown-toggle"
                                        data-toggle="dropdown" aria-expanded="true">
                                        <div class="shopcart">
                                            <span class="icon-c">
                                                <i class="fa fa-shopping-bag"></i>
                                            </span>
                                            <div class="shopcart-inner">
                                                <p class="text-shopping-cart">
                                                    My cart
                                                </p>

                                                <?php

                                                if(!isset($_SESSION['customer_email'])){

                                                    ?>

                                                        <span class="total-shopping-cart cart-total-full">
                                                    <span class="items_cart">0</span><span class="items_cart2">
                                                        item(s)</span><span class="items_carts"> + RM -0 </span>
                                                </span>

                                                <?php
                                                  

                                                }else{

                                                    //: Getting total number of cart_items
                                                    $customer_email = $_SESSION['customer_email'];
                                    
                                                    $get_customer_details = "Select * from customers where customer_email='$customer_email'";
                                                    $run_customer_details = mysqli_query($con, $get_customer_details);
                                                    $fetch_customer_details = mysqli_fetch_array($run_customer_details);
                                                                                                        
                                                    $customer_id = $fetch_customer_details['customer_id']; //:: Customer id 
                                                    $customer_name = $fetch_customer_details['customer_name']; //:: customer name

                                                    $get_items = "select * from customer_foodcart where customer_id='$customer_id'";
                                                    $run_items = mysqli_query($con, $get_items);
                                                    $count_items = mysqli_num_rows($run_items);

                                                    //: Getting the total of customer cart items

                                                    //: Getting the total of prize..

                                                    $query = "select SUM(total_price) as 'sumtotalprice' from customer_foodcart where customer_id='$customer_id' ";
                                                    $res = mysqli_query($con, $query);
                                                    $data = mysqli_fetch_array($res);

                                                    $total = $data['sumtotalprice'];

                                                        //: Getting GST...
                                                        $gst_rate = 6;
                                                        $gst = ($gst_rate / 100) * $total;

                                                        //: Delivery Charges...
                                                        $delivery = 5;

                                                        //: Grand Total..
                                                        $grand_total = ($total + $gst);

                                                        ?>

                                                        <span class="total-shopping-cart cart-total-full">
                                                    <span class="items_cart"><?php echo"$count_items"?></span><span class="items_cart2">
                                                        item(s)</span><span class="items_carts"> + RM <?php echo "$grand_total"?> </span>
                                                </span>

                                                <?php
                                                    
                                                }

                                                        

                                                ?>

                                                
                                            </div>
                                        </div>
                                    </a>

                                    <?php

                                        if(!isset($_SESSION['customer_email'])){

                                            ?>
                                            <ul class="dropdown-menu pull-right shoppingcart-box">
                                            <li>
                                                <p class="text-center empty">Your shopping cart is empty!</p>
                                            </li>
                                        </ul>
                                            <?php

                                        }else{

                                            ?>

                                        <ul class="dropdown-menu pull-right shoppingcart-box" role="menu">
                                            <li>
                                                <table class="table table-striped">
                                                    <tbody>
                                                        <?php

                                                            //: Getting Customer ID..
                                                            $customer_email = $_SESSION['customer_email'];
                                
                                                            $get_customer_details = "Select * from customers where customer_email='$customer_email'";
                                                            $run_customer_details = mysqli_query($con, $get_customer_details);
                                                            $fetch_customer_details = mysqli_fetch_array($run_customer_details);
                                                            
                                                            $customer_id = $fetch_customer_details['customer_id']; //:: Customer id 
                                                            $customer_name = $fetch_customer_details['customer_name']; //:: customer name

                                                            //: Cart Script..
                                                            $get_cart_data = "Select * from customer_foodcart where customer_id='$customer_id'";
                                                            $run_cart_data = mysqli_query($con, $get_cart_data);
                                                            $count_brands = mysqli_num_rows($run_cart_data);
                                                            if($count_brands==0){
                                                            echo "<li>
                                                            <p class='text-center empty'>Your shopping cart is empty!</p>
                                                        </li>";
                                                            }


                                                            //:: delete item from cart..
                                                            if(isset($_GET['removeitem'])){

                                                                $removeitem_id = $_GET['removeitem'];
                                                                $influencer_id = $_GET['influencer'];

                                                                $delete_item = "delete from customer_foodcart where fooditem_id='$removeitem_id' AND customer_id='$customer_id'";

                                                                $run_delete = mysqli_query($con, $delete_item);

                                                                if($run_delete){
                                                                    echo "<script>window.open('viewinfluencer.php?influencer=$influencer_id','_self')</script>";
                                                                    
                                                                }
                                                            
                                                            }
                                                            
                                                            while($row_cart_data=mysqli_fetch_array($run_cart_data)){
                                                                
                                                                $fcompanya_id = $_GET['influencer'];
                                                                $cart_id=$row_cart_data['foodcart_id'];
                                                                $cart_quantity=$row_cart_data['quantity'];
                                                                $cart_itemid=$row_cart_data['fooditem_id'];

                                                              

                                                                // Getting Product data from food_items table.
                                                                
                                                                $get_cartitem_data = "Select * from food_items where item_id='$cart_itemid'";
                                                                $run_cartitem_data = mysqli_query($con, $get_cartitem_data);
                                                                $fetch_cartitem_data = mysqli_fetch_array($run_cartitem_data);

                                                                $cartitem_id = $fetch_cartitem_data['item_id'];
                                                                $cartitem_image = $fetch_cartitem_data['item_img'];
                                                                $cartitem_price = $fetch_cartitem_data['item_price'];
                                                                $cartitem_name = $fetch_cartitem_data['item_title'];

                                                                //:: 
                                                                $totalunitprice = ($cartitem_price * $cart_quantity);
                                                                
                                                                echo " <tr>
                                                                <td class='text-center' style='width:70px'>
                                                                    <a href='product.html'>
                                                                        <img src='./admin_area/fooditem_images/$cartitem_image'
                                                                            style='width:70px' alt='$cartitem_name'
                                                                            title='$cartitem_name' class='preview'>
                                                                    </a>
                                                                </td>
                                                                <td class='text-left'> <a class='cart_product_name'
                                                                        href='product.html'>$cartitem_name</a>
                                                                </td>
                                                                <td class='text-center'>$cart_quantity</td>
                                                                <td class='text-center'>RM$totalunitprice</td>
                                                                <td class='text-right'>
                                                                    <a href='product.html' class='fa fa-edit'></a>
                                                                </td>
                                                                <td class='text-right'>
                                                                    
                                    
                                                                     <a href='viewinfluencer.php?removeitem=$cartitem_id&influencer=$fcompanya_id'class='fa fa-times fa-delete'></a>
                                                                   
                                                                    </td>
                                                            </tr>";

                                                            }


                                                        ?>
                                                    </tbody>
                                                </table>
                                            </li>
                                            <li>
                                                <div>
                                                    <table class="table table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <td class="text-left"><strong>Sub-Total</strong>
                                                                </td>
                                                                <td class="text-right">RM <?Php echo "$total"?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left"><strong>GST Tax (-6.00)%</strong>
                                                                </td>
                                                                <td class="text-right">RM<?php echo"$gst"?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left"><strong>Total</strong>
                                                                </td>
                                                                <td class="text-right">RM <?php echo "$grand_total"?></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <p class="text-right"> 
                                                    <a class="btn view-cart" href="foodcart.php"><i class="fa fa-shopping-cart"></i>Update Cart</a>&nbsp;&nbsp;&nbsp; 

                                                            <a type="submit" value="submit" name="submit" href="#my_modal" class="btn btn-light" data-toggle="modal" ><i class="fa fa-share"></i>Checkout</a>    
                                                    </p>
                                                </div>
                                            </li>
                                        </ul>
                                            
                                            <?php

                                        }
                                    ?>

                                </div>

                            </div>
                            <!--//cart-->

                    
                        </div>

                    </div>
                </div>

            </div>
          </header>
        <!-- //Header Container  -->

        <!-- Main Container  -->
        <div class="main-container container">
            <ul class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i></a></li>
                <li><a href="#">INFLUENCER</a></li>
            </ul>

            <?php

            if(isset($_GET['influencer'])){

                $influencer_id = $_GET['influencer'];


                $get_influencercompany_details = "select * from food_company where fcompany_id='$influencer_id'";
                $run_influencercompany_details = mysqli_query($con, $get_influencercompany_details); 
                $fetch_influencercompany_details = mysqli_fetch_array($run_influencercompany_details);

                $influencer_companyemail = $fetch_influencercompany_details['company_email'];

                //: getting details fromm influencer table.                 
                $get_influencer_details = "select * from influencer where i_email='$influencer_companyemail'";
                $run_influencer_details = mysqli_query($con, $get_influencer_details); 
                $fetch_influencer_details = mysqli_fetch_array($run_influencer_details);

                $influencer_getname =  $fetch_influencer_details['i_name'];
                $influencer_getpagelink =  $fetch_influencer_details['i_pagelink'];
                $influencer_getimage =  $fetch_influencer_details['i_image'];
                $influencer_getname =  $fetch_influencer_details['i_name'];
                $influencer_getpagetype =  $fetch_influencer_details['i_pagetype'];


                ?>

<div class="col-md-12 our-member">
               
               <div class="overflow-owl-slider1">
                   <div class="wrapper-owl-slider1">
                       <div class="row slider-ourmember">
                           <div class="item-about">
                               <div class="item respl-item">
                                   <div class="item-inner">
                                       <div class="w-image-box">
                                           <div class="item-image">
                                               <a title="<?php echo "$influencer_getname" ?>" href="#">
                                                   <img src="admin_area/influencer_images/<?php echo "$influencer_getimage" ?>"
                                                   style="width:160px; height: 150px;"
                                                       alt="Image Client">
                                               </a>
                                           </div>
                                       </div>
                                       <div class="info-member">
                                           <h2 class="cl-name"><a title="<?php echo "$influencer_getname" ?>"
                                                   href="#"><?php echo "$influencer_getname" ?></a></h2>
                                           <p class="cl-job">1k Products Sold by <?php echo "$influencer_getname" ?></p>
                                           <p class="cl-des">Donec dignissim, enim ac semper tempus, ligula
                                               neque pulvinar mi, sed facilisis arcu placerat consequat <?php echo "$influencer_getpagetype" ?></p>

                                           <ul>
                                                <li>
                                                   <a class="fa fa-youtube-play" title="Twitter" href="<?php echo "$influencer_getpagelink" ?>" style = "color:#c4302b"></a>
                                               </li>
                                               <li>
                                                   <a class="fa fa-facebook" title="Facebook" href="#" style = "color:#3b5998"></a>
                                               </li>
                                               <li>
                                                   <a class="fa fa-twitter" title="Twitter" href="#" style = "color:#00acee"></a>
                                               </li>
                                              
                                               <li>
                                                   <a class="fa fa-skype" title="skype" href="#" style = "color:#00aff0"></a>
                                               </li>
                                               <li>
                                                   <a class="fa fa-instagram" title="instagram" href="#" style = "color:#3f729b"></a>
                                               </li>
                                           </ul>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
           </div>

           <?php

               
                

            }
            ?>

            

            

            <div class="row">

                  
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-right">

                   
               
                <!-- Deals -->
                    <div class="module deals-layout1">
                            <h3 class="modtitle"><span>Promotes Products: </span></h3>
                            <div class="modcontent">
                                <div id="so_deal_1" class="so-deal style2">
                                    <div class="extraslider-inner products-list yt-content-slider" data-rtl="yes"
                                        data-autoplay="no" data-autoheight="no" data-delay="4" data-speed="0.6"
                                        data-margin="30" data-items_column0="2" data-items_column1="1"
                                        data-items_column2="1" data-items_column3="1" data-items_column4="1"
                                        data-arrows="yes" data-pagination="yes" data-lazyload="yes" data-loop="no"
                                        data-hoverpause="yes">

                                        <?php

                                        // $influenceritem_id = $_GET['influencer'];

                                        // $query = "Select * from influencer_product where ip_influencer_id = '$influenceritem_id'";
                                        // $run_query = mysqli_query($con, $query);
                                        // $fetch_query = mysqli_fetch_array($run_query);

                                        // $item_id = $fetch_query['ip_item_id'];

                                        // //:: Get data from food_item

                                        // $get_fooditem_data = "Select * from food_items where item_id = '$item_id'";
                                        // $run_fooditem_data = mysqli_query($con, $get_fooditem_data);

                                       
                                        // while($fetch_fooditem_data = mysqli_fetch_array($run_fooditem_data)){

                                        //     $fooditem_itemid = $fetch_fooditem_data['item_id'];
                                        //     $fooditem_itemtitle = $fetch_fooditem_data['item_title'];

                                        //     // echo "$fooditem_itemtitle";
                                        //     echo "bunny";
                                        // }

                                        $influenceritem_id = $_GET['influencer'];

                                        $query = "Select * from influencer_product where ip_influencer_id = '$influenceritem_id'";
                                        $run_query = mysqli_query($con, $query);

                                        while($fetch_query = mysqli_fetch_array($run_query)){

                                            $ip_id = $fetch_query['ip_id'];
                                            $item_id = $fetch_query['ip_item_id'];
                                            $store_id = $fetch_query['ip_hotel_id'];

                                            //: get store name 
                                            $get_company_name = "Select * from food_company where fcompany_id = '$store_id'";
                                            $run_company_name = mysqli_query($con, $get_company_name);
                                            $fetch_company_name = mysqli_fetch_array($run_company_name);

                                            $store_name = $fetch_company_name['Company_name'];


                                            

                                            // echo "$ip_id";
                                            

                                            //:: Get data from food_item
                                            $get_fooditem_data = "Select * from food_items where item_id = '$item_id'";
                                            $run_fooditem_data = mysqli_query($con, $get_fooditem_data);
                                            $fetch_fooditem_data = mysqli_fetch_array($run_fooditem_data);
                                            
                                            $fooditem_itemid = $fetch_fooditem_data['item_id'];
                                            $fooditem_itemtitle = $fetch_fooditem_data['item_title'];
                                            $fooditem_itemimage = $fetch_fooditem_data['item_img'];
                                            $fooditem_price = $fetch_fooditem_data['item_price'];
                                            $fooditem_store = $fetch_fooditem_data['fcompany_id'];
                                            $fooditem_desc = $fetch_fooditem_data['item_desc'];

                                            echo "<div class='item'>
                                            <div class='product-thumb'>
                                                <div class='row'>
                                                    <div class='inner'>
                                                        <div class='item-left col-lg-6 col-md-5 col-sm-5 col-xs-12'>
                                                            <div class='image'>
                                                                <span class='label-product label-product-sale'>
                                                                    -22%
                                                                </span>
                                                                <a href='influenceritem.php?influencer_itemid=$ip_id' target='_self' title='product'>
                                                                    <img src='admin_area/fooditem_images/$fooditem_itemimage'
                                                                        alt='$fooditem_itemtitle' class='img-responsive'>
                                                                </a>
                                                                <div class='button-group so-quickview'>
                                                                    <button class='btn-button addToCart'
                                                                        title='Add to Cart' type='button'
                                                                        onclick='cart.add('69');'><i
                                                                            class='fa fa-shopping-basket'></i> <span>Add
                                                                            to Cart</span>
                                                                    </button>
                                                                    <!--quickview-->
                                                                    <a class='iframe-link btn-button quickview quickview_handler visible-lg'
                                                                        href='quickview.html' title='Quick view'
                                                                        data-fancybox-type='iframe'><i
                                                                            class='fa fa-eye'></i><span>Quick
                                                                            view</span></a>
                                                                    <!--end quickview-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class='item-right col-lg-6 col-md-7 col-sm-7 col-xs-12'>
                                                            <div class='caption'>
                                                                <h4><a href='influenceritem.php?influencer_itemid=$ip_id' target='_self'
                                                                        title='$fooditem_itemtitle'>$fooditem_itemtitle</a>
                                                                </h4>
                                                                <p class='price'> <span class='price-new'>$fooditem_price.00RM</span>
                                                                    <span class='price-old'>RM77.00</span>
                                                                </p>
                                                                <p class='desc'>Lorem ipsum dolor sit amet, consectetur
                                                                    adipisicing elit, sed do eiusmod tempor incididunt
                                                                    ut labore..</p>
                                                                <div class='item-available'>
                                                                    <div class='row'>
                                                                        <p class='col-xs-6 a1'><b>$store_name</b>
                                                                        </p>
                                                                        
                                                                    </div>
                                                                    <div class='row'>
                                                                        <p class='col-xs-6 a1'>Available: <b>98</b>
                                                                        </p>
                                                                        <p class='col-xs-6 a2'>Sold: <b>32</b>
                                                                        </p>
                                                                    </div>
                                                                    <div class='available'> <span class='color_width'
                                                                            data-title='75%'' data-toggle='tooltip'
                                                                            title='75%' style='width: 75%'></span>
                                                                    </div>
                                                                </div>
                                                                <!--countdown box-->
                                                                <div class='item-time-w'>
                                                                    <div class='time-title'><span>Hurry Up!</span> Offer
                                                                        ends in:</div>
                                                                    <div class='item-time'>
                                                                        <div class='item-timer'>
                                                                            <div class='defaultCountdown-30'></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end countdown box-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>";

                                        }



                                        ?>

                                    </div>
                                    <div class="item-available">
                                        <br>
                                                                    <div class="row">
                                                                        <a href=""><h4 class="text-center">VIEW MORE</h4></a>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Deals -->
                
                    
                    <br>
                    <br>
                    <br>
                     <!-- Listing tabs -->
                     <div class="products-category">
                        <h3 class="title-category ">Influencer Store</h3>
                        <div class="category-derc">
                        </div>
                        <!-- Filters -->
                        <div class="product-filter product-filter-top filters-panel">
                            <div class="row">
                                <div class="col-md-5 col-sm-3 col-xs-12 view-mode">

                                    <!--<div class="list-view">-->
                                    <!--    <button class="btn btn-default grid active" data-view="grid"-->
                                    <!--        data-toggle="tooltip" data-original-title="Grid"><i-->
                                    <!--            class="fa fa-th"></i></button>-->
                                    <!--    <button class="btn btn-default list" data-view="list" data-toggle="tooltip"-->
                                    <!--        data-original-title="List"><i class="fa fa-th-list"></i></button>-->
                                    <!--</div>-->

                                </div>
                                <div class="short-by-show form-inline text-right col-md-7 col-sm-9 col-xs-12">
                                    <div class="form-group short-by">
                                        <label class="control-label" for="input-sort">Sort By:</label>
                                        <select id="input-sort" class="form-control" onchange="location = this.value;">
                                            <option value="" selected="selected">Default</option>
                                            <option value="">Name (A - Z)</option>
                                            <option value="">Name (Z - A)</option>
                                            <option value="">Price (Low &gt; High)</option>
                                            <option value="">Price (High &gt; Low)</option>
                                            <option value="">Rating (Highest)</option>
                                            <option value="">Rating (Lowest)</option>
                                            <option value="">Model (A - Z)</option>
                                            <option value="">Model (Z - A)</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="input-limit">Show:</label>
                                        <select id="input-limit" class="form-control" onchange="location = this.value;">
                                            <option value="" selected="selected">15</option>
                                            <option value="">25</option>
                                            <option value="">50</option>
                                            <option value="">75</option>
                                            <option value="">100</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- <div class="box-pagination col-md-3 col-sm-4 col-xs-12 text-right">
                                <ul class="pagination">
                                    <li class="active"><span>1</span></li>
                                    <li><a href="">2</a></li><li><a href="">&gt;</a></li>
                                    <li><a href="">&gt;|</a></li>
                                </ul>
                            </div> -->
                            </div>
                        </div>
                        <!-- //end Filters -->
                        <!--changed listings-->

                        <div class="products-list row nopadding-xs so-filter-gird">

                            <div class="product-layout col-lg-15 col-md-4 col-sm-6 col-xs-12">
                                <div class="product-item-container">
                                    <div class="left-block">
                                        <div class="product-image-container second_img">
                                            <a href="" target="_self" title="Chicken swinesha">
                                                <img src="admin_area/fooditem_images/brunell-bed-45-768x768.jpg"
                                                    class="img-1 img-responsive" alt="image">
                                                <img src="admin_area/fooditem_images/brunell-bed-45-768x768.jpg"
                                                    class="img-2 img-responsive" alt="image">
                                            </a>
                                        </div>
                                        <div class="box-label"> <span class="label-product label-sale"> -16% </span>
                                        </div>
                                        <div class="button-group so-quickview cartinfo--left">
                                            <button type="button" class="addToCart btn-button" title="Add to cart"
                                                onclick="cart.add('60 ');"> <i class="fa fa-shopping-basket"></i>
                                                <span>Add to cart </span>
                                            </button>
                                            <!--quickview-->
                                            <a class="iframe-link btn-button quickview quickview_handler visible-lg"
                                                href="quickview.html" title="Quick view" data-fancybox-type="iframe"><i
                                                    class="fa fa-eye"></i><span>Quick view</span></a>
                                            <!--end quickview-->
                                        </div>
                                    </div>
                                    <div class="right-block">
                                        <div class="caption">
                                            <h4><a href="product.html" title="Chicken swinesha" target="_self">Chicken
                                                    swinesha</a></h4>
                                            <div class="price"> <span class="price-new">$46.00</span>
                                                <span class="price-old">$55.00</span>
                                            </div>
                                            <div class="description item-desc">
                                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                                    nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                                    erat, sed diam voluptua. At vero eos et accusam et justo duo dolores
                                                    et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est
                                                    . </p>
                                            </div>
                                            <div class="list-block">
                                                <button class="addToCart btn-button" type="button" title="Add to Cart"
                                                    onclick="cart.add('101', '1');"><i
                                                        class="fa fa-shopping-basket"></i>
                                                </button>
                                              
                                                
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                           

                           

                        </div>
                        <!--// End Changed listings-->
                        <!-- Filters -->
                        <!-- <div class="product-filter product-filter-bottom filters-panel">
                            <div class="row">
                                <div class="col-sm-6 text-left"></div>
                                <div class="col-sm-6 text-right">Showing 1 to 15 of 15 (1 Pages)</div>
                            </div>
                        </div> -->
                        <!-- //end Filters -->

                    </div>
                    <!-- end Listing tabs -->


                </div>
              
            </div>

                

        </div>
        <!-- //Main Container -->


        <!-- Footer Container -->
        <!-- Footer Container -->
        <footer class="footer-container typefooter-1">

            <hr>

            <div class="footer-middle ">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-style">
                            <div class="infos-footer">
                                <h3 class="modtitle">Contact Us</h3>
                                <ul class="menu">
                                    <li class="adres">
                                         Unit 25-2-15, Level 2, Plaza Prima, Bt 4 ½ , Jalan Klang Lama, 58200 Kuala Lumpur 
                                    </li>
                                    <li class="phone">
                                        03- 8210 8677 / 013-2199 373
                                    </li>
                                    <li class="mail">
                                        <a href="">cbmc.malaysia60@gmail.com</a>
                                    </li>
                                    <li class="time">
                                        Open time: 7:00AM - 5:30PM
                                    </li>
                                </ul>
                            </div>


                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                            <div class="box-information box-footer">
                                <div class="module clearfix">
                                    <h3 class="modtitle">Information</h3>
                                    <div class="modcontent">
                                        <ul class="menu">
                                            <li><a href="aboutus.php">ABOUT US </a></li>
                                            <li><a href="#">FAQ</a></li>
                                            <li><a href="#">Warranty And Services</a></li>
                                            <li><a href="#">Support 24/7 page</a></li>
                                            <li><a href="#">Product Registration</a></li>
                                            <li><a href="#">Product Support</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                            <div class="box-account box-footer">
                                <div class="module clearfix">
                                    <h3 class="modtitle">Extras</h3>
                                    <div class="modcontent">
                                        <ul class="menu">
                                            <li> <a href="aboutus.php"> Gogo Empire </a></li>
                                            <li><a href="displaycategory.php">CATEGORIES</a></li>
                                            <li><a href="contact.php">Contact Us</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                            <div class="box-service box-footer">
                                <div class="module clearfix">
                                    <h3 class="modtitle">Services</h3>
                                    <div class="modcontent">
                                        <ul class="menu">
                                            <li><a href="#">Kuala Lumpur</a></li>
                                            <li><a href="#">Selangor</a></li>
                                            <li><a href="#">KL Central</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-style">


                        </div>

                        <div class="col-lg-12 col-xs-12 text-center">
                            <img src="image/catalog/demo/payment/payment.png" alt="imgpayment">
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer Bottom Container -->
            <div class="footer-bottom ">
                <div class="container">
                    <div class="copyright">
                        CBMC © 2020. All Rights Reserved. 
                    </div>
                </div>
            </div>
            <!-- /Footer Bottom Container -->


            <!--Back To Top-->
            <div class="back-to-top"><i class="fa fa-angle-up"></i></div>
        </footer>
        <!-- //end Footer Container -->
        <!-- //end Footer Container -->

    </div>



    <!-- Include Libs & Plugins
	============================================ -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="js/jquery-2.2.4.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/owl-carousel/owl.carousel.js"></script>
    <script type="text/javascript" src="js/themejs/libs.js"></script>
    <script type="text/javascript" src="js/unveil/jquery.unveil.js"></script>
    <script type="text/javascript" src="js/countdown/jquery.countdown.min.js"></script>
    <script type="text/javascript" src="js/dcjqaccordion/jquery.dcjqaccordion.2.8.min.js"></script>
    <script type="text/javascript" src="js/datetimepicker/moment.js"></script>
    <script type="text/javascript" src="js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/modernizr/modernizr-2.6.2.min.js"></script>
    <script type="text/javascript" src="js/minicolors/jquery.miniColors.min.js"></script>


    <!-- Theme files
	============================================ -->

    <script type="text/javascript" src="js/themejs/application.js"></script>

    <script type="text/javascript" src="js/themejs/homepage.js"></script>

    <script type="text/javascript" src="js/themejs/toppanel.js"></script>
    <script type="text/javascript" src="js/themejs/so_megamenu.js"></script>
    <script type="text/javascript" src="js/themejs/addtocart.js"></script>

    <script type="text/javascript">
     
        // Check if Cookie exists
        if ($.cookie('display')) {
            view = $.cookie('display');
        } else {
            view = 'grid';
        }
        if (view) display(view);
        
    </script>

</body>


</html>