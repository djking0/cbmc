<?php
include('influencerstore_security.php');
include('includes/header.php'); 
include('includes/navbar_influencer.php'); 
?>


<div class="modal fade" id="addadminprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Page</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="influencerstore_code.php" method="POST">

        <div class="modal-body">

            <div class="form-group">
                <label> Page Name </label>
                <input type="text" name="influencer_pagename" class="form-control" placeholder="Enter Page Name" required="required">
            </div>
            <div class="form-group">
                <label>Page type</label>
                <div>
                    <select name="influencer_pagetype" class="form-control" required="">
                        <option selected="">Youtube</option>
                        <option>Tiktok</option>
                        <option>Facebook</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label>Page link</label>
                <input type="text" name="influencer_pagelink" class="form-control" placeholder="Enter Page link" required="required">
            </div>
            <div class="form-group">
                <label>Followers</label>
                <input type="text" name="influencer_pagefollowers" class="form-control" placeholder="No of followers" required="required">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="addnewpage" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>


<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Your Pages:
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addadminprofile">
              Add New Page
            </button>
    </h6>
  </div>

  <div class="card-body">

    <?php
      if(isset($_SESSION['success']) && $_SESSION['success']!=''){
        echo '<h2> '.$_SESSION['success'].' </h2>.';
        unset($_SESSION['success']);
      } 
      if(isset($_SESSION['status']) && $_SESSION['status']!=''){
        echo '<h2> '.$_SESSION['status'].' </h2>.';
        unset($_SESSION['status']);
      } 
      
    ?>

    <div class="table-responsive">

     

      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>

            <?php

      
            require 'dbconfig.php';

            $email = $_SESSION['influencerusername'];

            $query = "SELECT * FROM influencer where i_email = '$email'";
            $query_run = mysqli_query($connection, $query);

            ?>
            <tr>
                <th>ID </th>
                <th>Page Name </th>
                <th>Page type </th>
                <th>Page link</th>
                <th>Page Followers</th>
                <!-- <th>EDIT </th> -->
            </tr>
            </thead>
            <tbody>

            <?php 
                ?>
                <strong>Main Page: </strong>
                <p></p>
                <?php
                if(mysqli_num_rows($query_run) > 0)        
                {
                    while($row = mysqli_fetch_assoc($query_run))
                    {
                        $link = $row['i_pagelink'];

                    ?>
    
                    <tr>
                        <td> <?php  echo $row['i_id']; ?></td>
                        <td> <?php  echo $row['i_pagename']; ?></td>
                        <td> <?php  echo $row['i_pagetype']; ?></td>
                        <td> <?php  echo "<a href='$link'>$link</a>" ?> </td>
                        <td> <?php  echo $row['i_followers']; ?> </td>
                        <!-- <td>
                            <form action="influencerstore_mainpage_edit.php" method="post">
                                <input type="hidden" name="editinfluencermainpage_id" value="<?php  echo $row['i_id']; ?>">
                                <button  type="submit" name="editinfluencermainpage_btn" class="btn btn-success"> EDIT</button>
                            </form>
                    </td> -->
                    </tr>
            <?php
                    }
                }else {
                    echo "No Record Found";
                }
            ?>
        
            
            
            </tbody>
      </table>

      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <?php

            require 'dbconfig.php';

            $email = $_SESSION['influencerusername'];

            $query = "SELECT * FROM influencer_digital_page where ip_email = '$email'";
            $query_run = mysqli_query($connection, $query);

            ?>
            <tr>
                <th>ID </th>
                <th>Page Name </th>
                <th>Page type </th>
                <th>Page link</th>
                <th>Page Followers</th>
                <th>EDIT </th>
                <th>DELETE </th>
            </tr>
            </thead>
            <tbody>

            <?php 
                ?>
                <p></p>
                <strong>Other Pages: </strong><br>
                <p> <p>
                <?php
                if(mysqli_num_rows($query_run) > 0)        
                {
                    while($row = mysqli_fetch_assoc($query_run))
                    {
                        $link = $row['ip_pagelink'];

                    ?>
                    <tr>
                        <td> <?php  echo $row['id']; ?></td>
                        <td> <?php  echo $row['ip_pagename']; ?></td>
                        <td> <?php  echo $row['ip_pagetype']; ?></td>
                        <td> <?php  echo "<a href='$link'>$link</a>" ?> </td>
                        <td> <?php  echo $row['ip_followers']; ?> </td>
                        <td>
                        <form action="influencerstore_mainpage_edit.php" method="post">
                                <input type="hidden" name="editinfluencermainpage_id" value="<?php  echo $row['id']; ?>">
                                <button  type="submit" name="editinfluencermainpage_btn" class="btn btn-success"> EDIT</button>
                            </form>
                    </td>
                    <td>
                        <form action="influencerstore_code.php" method="post">
                            <input type="hidden" name="deleteinfluencermainpage_id" value="<?php  echo $row['id']; ?>">
                            <button type="submit" name="deleteinfluencermainpage_btn" class="btn btn-danger"> DELETE</button>
                        </form>
                    </td>
                    </tr>
            <?php
                    }
                }else {
                    echo "No Record Found";
                }
            ?>
        
            
            
            </tbody>
      </table>

    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>