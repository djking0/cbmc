<?php 
include('security.php');
include('dbconfig.php');
require("PHPMailer-master/src/PHPMailer.php");
// $connection = mysqli_connect("localhost","root","","gogoshop");
// $connection = mysqli_connect("localhost","gogoempi_gotravelweb","OfKTL6PbD6IP","gogoempi_gotravelweb");

//: Add New Food Store
if(isset($_POST['addhotelbtn']))
{
    $hotelname = $_POST['hotel_name'];
    $hotelemail = $_POST['hotel_email'];
    $hoteladd = $_POST['hotel_add'];
    $hotelpassword = $_POST['hotel_password'];
    $hotelcpassword = $_POST['hotel_confirmpassword'];
    $hoteltype = 'food';
    $hotelimg = $_FILES["hotel_img"]['name'];

    $email_query = "SELECT * FROM food_company WHERE company_email='$hotelemail'";
    $email_query_run = mysqli_query($connection, $email_query);
    
    
    if($hotelimg ==''){
        $hotelimg = 'Default.jpg';
    }

    
    
    
    if(mysqli_num_rows($email_query_run) > 0)
    {
        $_SESSION['status'] = "Email Already Taken. Please Try Another one.";
        $_SESSION['status_code'] = "error";
        header('Location: hotel.php');  
    }
    else
    {
        if($hotelpassword === $hotelcpassword)
        {
            $query_run = "INSERT INTO food_company (owner_name,owner_ic,contact_number1,contact_number2,owner_img, Company_name,company_address,ssn_number,insurance,bcontact_number,company_openinghours,company_email,company_pass,bank_name,acc_name,acc_num,company_type,company_banner_img,company_img) VALUES ('','','','','', '$hotelname','$hoteladd','','','', '','$hotelemail','$hotelpassword','','','', '$hoteltype','','$hotelimg')";
            
            
            if(mysqli_query($connection, $query_run)){
            move_uploaded_file($_FILES["hotel_img"]["tmp_name"], "company_img/".$_FILES["hotel_img"]["name"] );
            $_SESSION['success'] = "Store picture is Added";
                
    
            }else{ 

            $_SESSION['status'] = "Store picture is not Added";       
            }


            if($query_run)
            {
                // echo "Saved";
                $_SESSION['success'] = "New Store Added";
                // $_SESSION['status_code'] = "success";
                header('Location: hotel.php');
            }
            else 
            {   
               
                $_SESSION['status'] = "Store Not Added";
                // $_SESSION['status_code'] = "error";
                header('Location: hotel.php');  
            }
        }
        else 
        {
            $_SESSION['status'] = "Password and Confirm Password Does Not Match";
            // $_SESSION['status_code'] = "warning";
            header('Location: hotel.php');  
        }
    }

}


 

//:: Update the Food Store data:

 if(isset($_POST['updatehotelbtn'])){
    $hotelupdateid = $_POST['hotel_updateid'];
    $hotelupdatename = $_POST['hotel_updatename'];
    $hotelupdateemail = $_POST['hotel_updateemail'];
    $hotelupdateadd = $_POST['hotel_updateadd'];
    $hotelupdatepassword = $_POST['hotel_updatepassword'];
    $updatehotelimg = $_FILES["hotel_img"]['name'];
    
      //:: UPDATING THE IMAGE::


    $query = "UPDATE food_company SET Company_name='$hotelupdatename', company_email='$hotelupdateemail', company_address='$hotelupdateadd', company_pass='$hotelupdatepassword', company_img= '$updatehotelimg' WHERE fcompany_id='$hotelupdateid'";
    $query_run = mysqli_query($connection, $query);


            if($query_run)
            {
               move_uploaded_file($_FILES["hotel_img"]["tmp_name"], "company_img/".$_FILES["hotel_img"]["name"] );
                $_SESSION['success'] = "Store is updated";
                header('Location: hotel.php');
            }
            else 
            {

               $_SESSION['status'] = "Your Data is NOT updated";
        header('Location: hotel.php'); 
            }
 
        }



//:: Delete the Food Store Data..

if(isset($_POST['deletehotel_btn']))
{
    $hoteldeleteid = $_POST['deletehotel_id'];

    $query = "DELETE FROM food_company WHERE fcompany_id='$hoteldeleteid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "Your Data is Deleted";
        // $_SESSION['status_code'] = "success";
        header('Location: hotel.php'); 
    }
    else
    {
        $_SESSION['status'] = "Your Data is NOT DELETED";       
        // $_SESSION['status_code'] = "error";
        header('Location: hotel.php'); 
    }    
}




//:: Admin login

if(isset($_POST['adminlogin_btn'])){

    $adminloginemail = $_POST['adminlogin_email'];
    $adminloginpassword = $_POST['adminlogin_password'];

    // $query = "SELECT * FROM admin where admin_email='$adminloginemail' AND admin_pass='$adminloginpassword' ";
    // $query_run = mysqli_query($connection, $query);

    // if(mysqli_fetch_array($query_run)){

    //     $_SESSION['username'] = $adminloginemail;
    //     header('Location: index.php');
    // }else{
    //     $_SESSION['status'] = 'Email id / Password is invalid';
    //     // $_SESSION['status'] = $adminloginemail;
    //     header('Location: login.php');
    // }

    $query = "SELECT * FROM food_company where company_email='$adminloginemail' AND company_pass='$adminloginpassword' ";
    $query_run = mysqli_query($connection, $query);
    $storetype = mysqli_fetch_array($query_run);


    if($storetype['company_type'] == "food"){

        $_SESSION['foodusername'] = $adminloginemail;
        header('Location: foodstore_index.php');

    }else if($storetype['company_type'] == "product"){

        $_SESSION['productusername'] = $adminloginemail;
        header('Location: productstore_index.php');

    }else if($storetype['company_type'] == "influencer"){

        $_SESSION['influencerusername'] = $adminloginemail;
        header('Location: influencerstore_index.php');

    }else if($storetype['company_type'] == "admin"){
        
        $_SESSION['username'] = $adminloginemail;
        header('Location: index.php');

    }else{
        $_SESSION['status'] = 'Email id / Password is invalid';
        // $_SESSION['status'] = $adminloginemail;
        header('Location: login.php');
    }

}


// Add new food item

if(isset($_POST['addfooditem'])){
   
    $item_title = $_POST['item_title'];
    $item_cat = $_POST['item_cat'];
    $item_company = $_POST['item_company'];
    $item_price = $_POST['item_price'];
	$item_desc = mysqli_real_escape_string($connection,$_POST['item_desc']);
    $status = "on";
    $item_youtubelink = $_POST['item_youtubelink'];
    $item_discounted_percentage = $_POST['item_discounted_percentage'];
    $item_quantity = $_POST['item_quantity'];
    $item_type = 'food';
    // //: Image Name
    $item_img = $_FILES['item_img']['name'];

     $Add_Food_Item = false; 
	
	    if($item_img == '') //If the image is empty, insert default image for it
	    {
	        $item_img = 'Default.jpg';
	        $_SESSION['status'] = "Image Is Uploaded With Default. $store";
	        $Add_Food_Img = false;
	    }
	
		if($Add_Food_Img){  //file_exists("fooditem_images/" .$_FILES["item_img"]["name"])

        $image_already_exist = $_FILES["item_img"]["name"];
        $_SESSION['status'] = "Image Already exists. $store";
        header('Location: food_item.php'); 

    }else{

        $insert_product ="insert into food_items (fcat_id,fcat_sub_id,fcompany_id,date,item_title,item_img,item_price,item_desc,youtube_link,status,item_discounted_price,item_quantity,item_type,top_product,trending) values 
         ('$item_cat','0','$item_company',NOW(),'$item_title','$item_img',
         '$item_price','$item_desc','$item_youtubelink','$status','$item_discounted_percentage','$item_quantity', '$item_type','','')";

            if(mysqli_query($connection, $insert_product)){
            move_uploaded_file($_FILES["item_img"]["tmp_name"], "fooditem_images/".$_FILES["item_img"]["name"] );
            $_SESSION['success'] = "Item is Added";
            header('Location: food_item.php'); 
        }else{ 

            $_SESSION['status'] = "Item is not Added";       
            header('Location: food_item.php'); 
        }

    }
    
  
}

//: Update Food Item
	
	if(isset($_POST['updateitemitem'])){
	
	    $updateitem_id = $_POST['item_updateid'];
	    $updateitem_title = $_POST['item_updatetitle'];
	    $updateitem_cat = $_POST['item_updatecat'];
	    $updateitem_price = $_POST['item_updateprice'];
	    $updateitem_desc = mysqli_real_escape_string($connection,$_POST['item_updatedesc']);
	    $status = "on";
	    $updateitem_youtubelink = $_POST['item_updateyoutubelink'];
	    // $item_discounted_percentage = $_POST['item_discounted_percentage'];
	    $updateitem_quantity = $_POST['item_updatequantity'];
	    $item_company = $_POST['item_updatecompany'];
	    $item_discounted_percentage = 1;
	    $updateitem_img = $_FILES['item_updateimg']['name'];
	    
	
	    //:: UPDATING THE IMAGE::
	    $update_img = "SELECT * FROM food_items where item_id= '$updateitem_id'";
	    $run_update_img = mysqli_query($connection, $update_img);
	
	    foreach($run_update_img as $fa_row){
	        if($updateitem_img == NULL){
	            $image_data = $fa_row['item_img'];
	        }else{
	            //: upload with new image and delete with old image
	            if($img_path = "fooditem_images/".$fa_row['item_img']){
	                unlink($img_path);
	                $image_data = $updateitem_img;
	            }
	            
	        }
	    }
	
	    $query = "UPDATE food_items SET fcat_id='$updateitem_cat', fcompany_id='$item_company', item_title='$updateitem_title', item_img='$image_data', item_price='$updateitem_price', 
	    item_desc='$updateitem_desc', youtube_link='$updateitem_youtubelink', status='$status', item_discounted_price='$item_discounted_percentage', item_quantity='$updateitem_quantity'   
	    WHERE item_id='$updateitem_id' ";
	    $query_run = mysqli_query($connection, $query);
	
	    if($query_run)
	    {
	        if($updateitem_img == NULL){
	            $_SESSION['success'] = "Item is Updated with existing image";
	            header('Location: food_item.php'); 
	        }else{
	            //: upload with new image and delete with old image
	            move_uploaded_file($_FILES["item_updateimg"]["tmp_name"], "fooditem_images/".$_FILES["item_updateimg"]["name"] );
	            $_SESSION['success'] = "Item is Updated";
	            header('Location: food_item.php'); 
	        }
	      
	    }
	    else
	    {
	        $_SESSION['status'] = "Item is NOT Updated";
	        // $_SESSION['status_code'] = "error";
	        header('Location: food_item.php'); 
	    }
	
	}
	
	
	//: Delete Food Item
	if(isset($_POST['deleteitem_btn']))
	{
	    $fooditemdeleteid = $_POST['deleteitem_id'];
	
	    $query = "DELETE FROM food_items WHERE item_id='$fooditemdeleteid' ";
	    $query_run = mysqli_query($connection, $query);
	
	    if($query_run)
	    {
	        $_SESSION['success'] = "Item is Deleted";
	        // $_SESSION['status_code'] = "success";
	        header('Location: food_item.php'); 
	    }
	    else
	    {
	        $_SESSION['status'] = "Item is NOT DELETED";       
	        // $_SESSION['status_code'] = "error";
	        header('Location: food_item.php'); 
	    }    
	
	    
	}





//: Add Food Category..

if(isset($_POST['addfoodcategorybtn'])){

    $fcat_title = $_POST['foodcategory_name'];
    $fitem_img = $_FILES['foodcat_img']['name'];
    $fcat_type = 'food';

    if(file_exists("homepage_images/" .$_FILES["foodcat_img"]["name"])){

        $image_already_exist = $_FILES["foodcat_img"]["name"];
        $_SESSION['status'] = "Image Already exists. $store";
        header('Location: food_category.php'); 

    }else{
        
    $insert_foodcategory = "insert into food_category (food_cat,cat_image,cat_type) values ('$fcat_title','$fitem_img','$fcat_type')";

    if(mysqli_query($connection, $insert_foodcategory)){
        
        move_uploaded_file($_FILES["foodcat_img"]["tmp_name"], "homepage_images/".$_FILES["foodcat_img"]["name"] );
        $_SESSION['success'] = "Item is Added";
        header('Location: food_category.php'); 
    }else{ 

        $_SESSION['status'] = "Item is not Added";       
            header('Location: food_category.php'); 
            // echo "Error insertin data: " . mysqli_error($connection);
    }
}
}

//:: Update Food Category
if(isset($_POST['updatefoodcatbtn'])){

    $fcatupdateid = $_POST['foodcat_updateid'];
    $fcatupdatename = $_POST['foodcat_updatename'];
    $updatefood_img = $_FILES['foodcat_updateimg']['name'];

     //:: UPDATING THE IMAGE::
     $update_img = "SELECT * FROM food_category where fcat_id= '$fcatupdateid'";
     $run_update_img = mysqli_query($connection, $update_img);
 
     foreach($run_update_img as $fa_row){
         if($updatefood_img == NULL){
             $image_data = $fa_row['cat_image'];
         }else{
             //: upload with new image and delete with old image
             if($img_path = "homepage_images/".$fa_row['cat_image']){
                 unlink($img_path);
                 $image_data = $updatefood_img;
             }
             
         }
     }

     $query = "UPDATE food_category SET food_cat='$fcatupdatename',cat_image='$image_data' WHERE fcat_id='$fcatupdateid' ";
     $query_run = mysqli_query($connection, $query);
 
     if($query_run)
     {
         if($updatefood_img == NULL){
             $_SESSION['success'] = "Item is Updated with existing image";
             header('Location: food_category.php'); 
         }else{
             //: upload with new image and delete with old image
             move_uploaded_file($_FILES["foodcat_updateimg"]["tmp_name"], "homepage_images/".$_FILES["foodcat_updateimg"]["name"] );
             $_SESSION['success'] = "Item is Updated";
             header('Location: food_category.php'); 
         }
       
     }
     else
     {
         $_SESSION['status'] = "item is NOT Updated";
         // $_SESSION['status_code'] = "error";
         header('Location: food_category.php'); 
     }

}

//:: Delete the Food Category Data..

if(isset($_POST['deletefoodcat_btn']))
{
    $fcatdeleteid = $_POST['deletefoodcat_id'];

    $query = "DELETE FROM food_category WHERE fcat_id='$fcatdeleteid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "Food Category is Deleted";
        // $_SESSION['status_code'] = "success";
        header('Location: food_category.php'); 
    }
    else
    {
        $_SESSION['status'] = "Food Category Data is NOT DELETED";       
        // $_SESSION['status_code'] = "error";
        header('Location: food_category.php'); 
    }    
}


//: Add Product Category..

if(isset($_POST['addcategorybtn'])){

    $cat_title = $_POST['category_title'];  
    $category_img = $_FILES['category_img']['name'];

    if(file_exists("assets/" .$_FILES["category_img"]["name"])){

        $image_already_exist = $_FILES["category_img"]["name"];
        $_SESSION['status'] = "Image Already exists. $store";
        header('Location: product_category.php'); 

    }else{

        $insert_category = "insert into categories (cat_title,cat_image,cat_type) values ('$cat_title', '$category_img', 'food')";

        if(mysqli_query($connection, $insert_category)){
            move_uploaded_file($_FILES["category_img"]["tmp_name"], "assets/".$_FILES["category_img"]["name"] );
            $_SESSION['success'] = "Food Category is Added";
            header('Location: product_category.php'); 
        }else{ 

            $_SESSION['status'] = "Food Category is not Added";       
            header('Location: product_category.php'); 
        }

    }
}


// :: Update Product Category
if(isset($_POST['productcategory_update_btn'])){
    
    $catupdateid = $_POST['category_updateid'];
    $catupdatename = $_POST['updatecategory_title'];
    $category_img = $_FILES['updatecategory_img']['name'];

    //:: UPDATING THE IMAGE::
    $update_img = "SELECT * FROM categories where cat_id= '$catupdateid'";
    $run_update_img = mysqli_query($connection, $update_img);

    foreach($run_update_img as $fa_row){
        if($category_img == NULL){
            $image_data = $fa_row['cat_img'];
        }else{
            //: upload with new image and delete with old image
            if($img_path = "assets/".$fa_row['cat_img']){
                unlink($img_path);
                $image_data = $category_img;
            }
            
        }
    }

    $query = "UPDATE categories SET cat_title='$catupdatename', cat_img='$image_data' WHERE cat_id='$catupdateid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        if($category_img == NULL){
            $_SESSION['success'] = "Food Category is Updated with existing image";
            header('Location: product_category.php'); 
        }else{
            //: upload with new image and delete with old image
            move_uploaded_file($_FILES["updatecategory_img"]["tmp_name"], "assets/".$_FILES["updatecategory_img"]["name"] );
            $_SESSION['success'] = "Category is Updated";
            header('Location: product_category.php'); 
        }
      
    }
    else
    {
        $_SESSION['status'] = "Category is NOT Updated";
        // $_SESSION['status_code'] = "error";
        header('Location: product_category.php'); 
    }
}

//: Delete Product Category:
if(isset($_POST['deletecat_btn']))
{
    $catdeleteid = $_POST['deletecat_id'];

    $query = "DELETE FROM categories WHERE cat_id='$catdeleteid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "Category is Deleted";
        // $_SESSION['status_code'] = "success";
        header('Location: product_category.php'); 
    }
    else
    {
        $_SESSION['status'] = "Category Data is NOT DELETED";       
        // $_SESSION['status_code'] = "error";
        header('Location: product_category.php'); 
    }    
}



//: Add New store
if(isset($_POST['addstore_btn']))
{
    $storename = $_POST['store_name'];
    $storeemail = $_POST['store_email'];
    $storeadd = $_POST['store_add'];
    $storepassword = $_POST['store_password'];
    $storecpassword = $_POST['store_confirmpassword'];
    $storetype = 'product';
    $companyimg = $_FILES["company_img"]['name'];
    
    $email_query = "SELECT * FROM food_company WHERE company_email='$storeemail'";
    $email_query_run = mysqli_query($connection, $email_query);
    
    
    if($companyimg ==''){
        $companyimg = 'Default.jpg';
    }

    
    
    
    if(mysqli_num_rows($email_query_run) > 0)
    {
        $_SESSION['status'] = "Email Already Taken. Please Try Another one.";
        $_SESSION['status_code'] = "error";
        header('Location: store.php');  
    }
    else
    {
        if($storepassword === $storecpassword)
        {
            $query_run = "INSERT INTO food_company (owner_name,owner_ic,contact_number1,contact_number2,owner_img, Company_name,company_address,ssn_number,insurance,bcontact_number,company_openinghours,company_email,company_pass,bank_name,acc_name,acc_num,company_type,company_banner_img,company_img) VALUES ('','','','','', '$storename','$storeadd','','','', '','$storeemail','$storepassword','','','', '$storetype','','$companyimg')";
            
            
            if(mysqli_query($connection, $query_run)){
            move_uploaded_file($_FILES["company_img"]["tmp_name"], "store_img/".$_FILES["company_img"]["name"] );
            $_SESSION['success'] = "Hotel picture is Added";
                
    
            }else{ 

            $_SESSION['status'] = "Hotel picture is not Added";       
            }


            if($query_run)
            {
                move_uploaded_file($_FILES["company_img"]["tmp_name"], "store_img/".$_FILES["company_img"]["name"] );
                $_SESSION['success'] = "Store is Added";
                header('Location: store.php');
            }
            else 
            {   
               
                $_SESSION['status'] = "Store Not Added";
                // $_SESSION['status_code'] = "error";
                header('Location: store.php');  
            }
        }
        else 
        {
            $_SESSION['status'] = "Password and Confirm Password Does Not Match";
            // $_SESSION['status_code'] = "warning";
            header('Location: store.php');  
        }
    }
}

 //:: Update the store data:

 if(isset($_POST['updatestorebtn']))
{
    $storeupdateid = $_POST['store_updateid'];
    $storeupdatename = $_POST['store_updatename'];
    $storeupdateemail = $_POST['store_updateemail'];
    $storeupdateadd = $_POST['store_updateadd'];
    $storeupdatepassword = $_POST['store_updatepassword'];
    $updatestoreimg = $_FILES["store_img"]['name'];


    $query = "UPDATE food_company SET Company_name='$storeupdatename', company_email='$storeupdateemail', company_address='$storeupdateadd', company_pass='$storeupdatepassword', company_img= '$updatestoreimg' WHERE fcompany_id='$storeupdateid'";
    $query_run = mysqli_query($connection, $query);


    if($query_run)
    {
      move_uploaded_file($_FILES["store_img"]["tmp_name"], "store_img/".$_FILES["store_img"]["name"] );
      $_SESSION['success'] = "Hotel picture is updated";
    header('Location: store.php');
    }
     else 
    {


    $_SESSION['status'] = "Your Data is NOT updated";
        header('Location: store.php'); 
            }
 }


        // /:: Delete the store Data..
        if(isset($_POST['deletestore_btn']))
    {
        $storedeleteid = $_POST['deletestore_id'];

        $query = "DELETE FROM food_company WHERE fcompany_id='$storedeleteid' ";
        $query_run = mysqli_query($connection, $query);

        if($query_run)
        {
            $_SESSION['success'] = "Your Data is Deleted";
            // $_SESSION['status_code'] = "success";
            header('Location: store.php'); 
        }
        else
        {
            $_SESSION['status'] = "Your Data is NOT DELETED";
            // $_SESSION['status_code'] = "error";
            header('Location: store.php'); 
        }
    }



 //:: Update the store data:

 if(isset($_POST['updatestorebtn']))
{
    $storeupdateid = $_POST['store_updateid'];
    $storeupdatename = $_POST['store_updatename'];
    $storeupdateemail = $_POST['store_updateemail'];
    $storeupdateadd = $_POST['store_updateadd'];
    $storeupdatepassword = $_POST['store_updatepassword'];

    $query = "UPDATE food_company SET Company_name='$storeupdatename', company_email='$storeupdateemail', company_address='$storeupdateadd', company_pass='$storeupdatepassword' WHERE fcompany_id='$storeupdateid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "Your Data is Updated";
        // $_SESSION['status_code'] = "success";
        header('Location: store.php'); 
    }
    else
    {
        $_SESSION['status'] = "Your Data is NOT Updated";
        // $_SESSION['status_code'] = "error";
        header('Location: store.php'); 
    }
}

// /:: Delete the store Data..
if(isset($_POST['deletestore_btn']))
{
    $storedeleteid = $_POST['deletestore_id'];

    $query = "DELETE FROM food_company WHERE fcompany_id='$storedeleteid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "Your Data is Deleted";
        // $_SESSION['status_code'] = "success";
        header('Location: store.php'); 
    }
    else
    {
        $_SESSION['status'] = "Your Data is NOT DELETED";       
        // $_SESSION['status_code'] = "error";
        header('Location: store.php'); 
    }    
}


// Add new product item

if(isset($_POST['addproductitem'])){
   
    $item_title = $_POST['product_title'];
    $item_cat = $_POST['product_cat'];
    $item_company = $_POST['product_company'];
    $item_price = $_POST['product_price'];
    $item_desc = mysqli_real_escape_string($connection,$_POST['product_desc']);
    $status = "on";
    $item_youtubelink = $_POST['product_youtubelink'];
    $item_discounted_percentage = $_POST['product_discounted_percentage'];
    $item_quantity = $_POST['product_quantity'];
    $item_type = 'product';
    // //: Image Name
    $item_img = $_FILES['product_img']['name'];
    
    $Add_Product_Img = false;

    if($item_img == '')
    {
        $item_img = 'Default.png';
        $_SESSION['status'] = "Product Is Uploaded With Default Image. $store";
        $Add_Product_Img = false;
    }

    if($Add_Product_Img){    //file_exists("fooditem_images/" .$_FILES["product_img"]["name"])
        $image_already_exist = $_FILES["product_img"]["name"];
        $_SESSION['status'] = "Image Already exists. $store";
        header('Location: products.php'); 
    

    }else{

        $insert_product ="insert into food_items (fcat_id,fcat_sub_id,fcompany_id,date,item_title,item_img,item_price,item_desc,youtube_link,status,item_discounted_price,item_quantity,item_type,top_product,trending) values 
         ('$item_cat','0','$item_company',NOW(),'$item_title','$item_img',
         '$item_price','$item_desc','$item_youtubelink','$status','$item_discounted_percentage','$item_quantity', '$item_type','','')";

            if(mysqli_query($connection, $insert_product)){
            move_uploaded_file($_FILES["product_img"]["tmp_name"], "fooditem_images/".$_FILES["product_img"]["name"] );
            $_SESSION['success'] = "Item is Added";
            header('Location: products.php'); 
        }else{ 

            $_SESSION['status'] = "Item is not Added";       
            header('Location: products.php');
            //  echo "Error insertin data: " . mysqli_error($connection); 
        }

    }
    
  
}


//: Add product Category..

if(isset($_POST['addproductcategorybtn'])){

    $fcat_title = $_POST['productcategory_name'];
    $item_img = $_FILES['productcat_img']['name'];
    $fcat_type = 'product';

    if(file_exists("homepage_images/" .$_FILES["productcat_img"]["name"])){

        $image_already_exist = $_FILES["productcat_img"]["name"];
        $_SESSION['status'] = "Image Already exists. $store";
        header('Location: product_category.php'); 

    }else{

        $insert_foodcategory = "insert into food_category (food_cat,cat_image,cat_type) values ('$fcat_title','$item_img','$fcat_type')";


            if(mysqli_query($connection, $insert_foodcategory)){
            move_uploaded_file($_FILES["productcat_img"]["tmp_name"], "homepage_images/".$_FILES["productcat_img"]["name"] );
            $_SESSION['success'] = "Item is Added";
            header('Location: product_category.php'); 
        }else{ 

            $_SESSION['status'] = "Item is not Added";       
            header('Location: product_category.php'); 
            // echo "Error insertin data: " . mysqli_error($connection);

        }

    }

}

//:: Update Product Category
if(isset($_POST['updateproductcatbtn'])){

    $fcatupdateid = $_POST['productcat_updateid'];
    $fcatupdatename = $_POST['productcat_updatename'];
    $updateitem_img = $_FILES['productcat_updateimg']['name'];


     //:: UPDATING THE IMAGE::
     $update_img = "SELECT * FROM food_category where fcat_id= '$fcatupdateid'";
     $run_update_img = mysqli_query($connection, $update_img);
 
     foreach($run_update_img as $fa_row){
         if($updateitem_img == NULL){
             $image_data = $fa_row['cat_image'];
         }else{
             //: upload with new image and delete with old image
             if($img_path = "homepage_images/".$fa_row['cat_image']){
                 unlink($img_path);
                 $image_data = $updateitem_img;
             }
             
         }
     }

     $query = "UPDATE food_category SET food_cat='$fcatupdatename',cat_image='$image_data' WHERE fcat_id='$fcatupdateid' ";
     $query_run = mysqli_query($connection, $query);
 
     if($query_run)
     {
         if($updateitem_img == NULL){
             $_SESSION['success'] = "Item is Updated with existing image";
             header('Location: product_category.php'); 
         }else{
             //: upload with new image and delete with old image
             move_uploaded_file($_FILES["productcat_updateimg"]["tmp_name"], "homepage_images/".$_FILES["productcat_updateimg"]["name"] );
             $_SESSION['success'] = "Item is Updated";
             header('Location: product_category.php'); 
         }
       
     }
     else
     {
         $_SESSION['status'] = "item is NOT Updated";
         // $_SESSION['status_code'] = "error";
         header('Location: product_category.php'); 
     }

}


//:: Delete the Product Category Data..

if(isset($_POST['deleteproductcat_btn']))
{
    $fcatdeleteid = $_POST['deleteproductcat_id'];

    $query = "DELETE FROM food_category WHERE fcat_id='$fcatdeleteid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "Product Category is Deleted";
        // $_SESSION['status_code'] = "success";
        header('Location: product_category.php'); 
    }
    else
    {
        $_SESSION['status'] = "product Category Data is NOT DELETED";       
        // $_SESSION['status_code'] = "error";
        header('Location: product_category.php'); 
    }    
}


//: Update Product item

if(isset($_POST['updateproductitem'])){

    $updateitem_id = $_POST['product_updateid'];
    $updateitem_title = $_POST['product_updatetitle'];
    $updateitem_cat = $_POST['product_updatecat'];
    $updateitem_price = $_POST['product_updateprice'];
    $updateitem_desc = mysqli_real_escape_string($connection,$_POST['product_updatedesc']);
    $status = "on";
    $updateitem_youtubelink = $_POST['product_updateyoutubelink'];
    // $item_discounted_percentage = $_POST['item_discounted_percentage'];
    $updateitem_quantity = $_POST['product_updatequantity'];
    $item_company = $_POST['product_updatecompany'];
    $item_discounted_percentage = 1;
    $updateitem_img = $_FILES['product_updateimg']['name'];
    

    //:: UPDATING THE IMAGE::
    $update_img = "SELECT * FROM food_items where item_id= '$updateitem_id'";
    $run_update_img = mysqli_query($connection, $update_img);

    foreach($run_update_img as $fa_row){
        if($updateitem_img == NULL){
            $image_data = $fa_row['item_img'];
        }else{
            //: upload with new image and delete with old image
            if($img_path = "fooditem_images/".$fa_row['item_img']){
                unlink($img_path);
                $image_data = $updateitem_img;
            }
            
        }
    }

    $query = "UPDATE food_items SET fcat_id='$updateitem_cat', fcompany_id='$item_company', item_title='$updateitem_title', item_img='$image_data', item_price='$updateitem_price', 
    item_desc='$updateitem_desc', youtube_link='$updateitem_youtubelink', status='$status', item_discounted_price='$item_discounted_percentage', item_quantity='$updateitem_quantity'    
    WHERE item_id='$updateitem_id' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        if($updateitem_img == NULL){
            $_SESSION['success'] = "Item is Updated with existing image";
            header('Location: products.php'); 
        }else{
            //: upload with new image and delete with old image
            move_uploaded_file($_FILES["product_updateimg"]["tmp_name"], "fooditem_images/".$_FILES["product_updateimg"]["name"] );
            $_SESSION['success'] = "Item is Updated";
            header('Location: products.php'); 
        }
      
    }
    else
    {
        $_SESSION['status'] = "item is NOT Updated";
        // $_SESSION['status_code'] = "error";
        header('Location: products.php'); 
    }

}


//: Delete Product item
if(isset($_POST['deleteproduct_btn']))
{
    $prodictitemdeleteid = $_POST['deleteproduct_id'];

    $query = "DELETE FROM food_items WHERE item_id='$prodictitemdeleteid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "item is Deleted";
        // $_SESSION['status_code'] = "success";
        header('Location: products.php'); 
    }
    else
    {
        $_SESSION['status'] = "Item is NOT DELETED";       
        // $_SESSION['status_code'] = "error";
        header('Location: products.php'); 
    }    

    
}

//:: GOGO EMPIRE MALL ADD PRODUCT
if(isset($_POST['addgogoproductitem'])){
   
    $item_title = $_POST['product_title'];
    $item_cat = $_POST['product_cat'];
    $item_company = $_POST['product_company'];
    $item_price = $_POST['product_price'];
    $item_desc = mysqli_real_escape_string($connection,$_POST['product_desc']);
    $status = "on";
    $item_youtubelink = $_POST['product_youtubelink'];
    $item_discounted_percentage = $_POST['product_discounted_percentage'];
    $item_quantity = $_POST['item_quantity'];
    $item_type = 'gogo';
    // //: Image Name
    $item_img = $_FILES['product_img']['name'];
    
    
    if(file_exists("fooditem_images/" .$_FILES["product_img"]["name"])){

        $image_already_exist = $_FILES["product_img"]["name"];
        $_SESSION['status'] = "Image Already exists. $store";
        header('Location: userwebsite_homepage_gogomall.php'); 

    }else{

        $insert_product ="insert into food_items (fcat_id,fcat_sub_id,fcompany_id,date,item_title,item_img,item_price,item_desc,youtube_link,status,item_discounted_price,item_quantity,item_type,top_product,trending) values 
         ('$item_cat','0','$item_company',NOW(),'$item_title','$item_img',
         '$item_price','$item_desc','$item_youtubelink','$status','$item_discounted_percentage','$item_quantity', '$item_type','','')";

            if(mysqli_query($connection, $insert_product)){
            move_uploaded_file($_FILES["product_img"]["tmp_name"], "fooditem_images/".$_FILES["product_img"]["name"] );
            $_SESSION['success'] = "Item is Added";
            header('Location: userwebsite_homepage_gogomall.php'); 
        }else{ 

            $_SESSION['status'] = "Item is not Added";       
            header('Location: userwebsite_homepage_gogomall.php'); 
        }

    }
    
  
}

//: Update GOGO MALL Product item

if(isset($_POST['updategogoproductitem'])){

    $updateitem_id = $_POST['product_updateid'];
    $updateitem_title = $_POST['product_updatetitle'];
    $updateitem_cat = $_POST['product_updatecat'];
    $updateitem_price = $_POST['product_updateprice'];
    $updateitem_desc = mysqli_real_escape_string($connection,$_POST['product_updatedesc']);
    $status = "on";
    $updateitem_youtubelink = $_POST['product_updateyoutubelink'];
    // $item_discounted_percentage = $_POST['item_discounted_percentage'];
    $updateitem_quantity = $_POST['product_updatequantity'];
    $item_company = $_POST['product_updatecompany'];
    $item_discounted_percentage = 1;
    $updateitem_img = $_FILES['product_updateimg']['name'];
    

    //:: UPDATING THE IMAGE::
    $update_img = "SELECT * FROM food_items where item_id= '$updateitem_id'";
    $run_update_img = mysqli_query($connection, $update_img);

    foreach($run_update_img as $fa_row){
        if($updateitem_img == NULL){
            $image_data = $fa_row['item_img'];
        }else{
            //: upload with new image and delete with old image
            if($img_path = "fooditem_images/".$fa_row['item_img']){
                unlink($img_path);
                $image_data = $updateitem_img;
            }
            
        }
    }

    $query = "UPDATE food_items SET fcat_id='$updateitem_cat', fcompany_id='$item_company', item_title='$updateitem_title', item_img='$image_data', item_price='$updateitem_price', 
    item_desc='$updateitem_desc', youtube_link='$updateitem_youtubelink', status='$status', item_discounted_price='$item_discounted_percentage', item_quantity='$updateitem_quantity'   
    WHERE item_id='$updateitem_id' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        if($updateitem_img == NULL){
            $_SESSION['success'] = "Item is Updated with existing image";
            header('Location: userwebsite_homepage_gogomall.php'); 
        }else{
            //: upload with new image and delete with old image
            move_uploaded_file($_FILES["product_updateimg"]["tmp_name"], "fooditem_images/".$_FILES["product_updateimg"]["name"] );
            $_SESSION['success'] = "Item is Updated";
            header('Location: userwebsite_homepage_gogomall.php'); 
        }
      
    }
    else
    {
        $_SESSION['status'] = "item is NOT Updated";
        // $_SESSION['status_code'] = "error";
        header('Location: userwebsite_homepage_gogomall.php'); 
    }

}

//: Delete Gogo Empire mall item
if(isset($_POST['deletegogoproduct_btn']))
{
    $productitemdeleteid = $_POST['deleteproduct_id'];

    $query = "DELETE FROM food_items WHERE item_id='$productitemdeleteid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "item is Deleted";
        // $_SESSION['status_code'] = "success";
        header('Location:  userwebsite_homepage_gogomall.php'); 
    }
    else
    {
        $_SESSION['status'] = "Item is NOT DELETED";       
        // $_SESSION['status_code'] = "error";
        header('Location:  userwebsite_homepage_gogomall.php'); 
    }    

    
}

//:: INFLUENCER SECTION

//: Accept Influencer Request: 
if(isset($_POST['accept_influencer_request_btn'])){

    $influencer_acceptrequest_id = $_POST['accept_influencer_request_id'];

    $sel_acceptinfluencer = "SELECT * from influencer where i_id = '$influencer_acceptrequest_id'";
    $run_acceptinfluencer = mysqli_query($connection, $sel_acceptinfluencer);
    
    //: Fetching data
    $fetch_acceptinfluencer = mysqli_fetch_array($run_acceptinfluencer);

    $influencer_accept_name = $fetch_acceptinfluencer['i_name'];
    $influencer_accept_email = $fetch_acceptinfluencer['i_email'];
    $influencer_accept_pass = $fetch_acceptinfluencer['i_pass'];
    $influencer_accept_add = $fetch_acceptinfluencer['i_address'];
    $influencer_accept_gender = $fetch_acceptinfluencer['i_gender'];
    
    
    $message_body = "
             <html>

                 <h5> Dear $influencer_accept_name </h5>

                 <p> Your request is accepted, Click this link below and login with your email and password.</p>
                 
                 <a href = 'ebox.asia/gogostore/admin_area'> ebox.asia/gogostore/admin_area </a>

                 <p> Your Email: $influencer_accept_email  </p>
                 <p> Your Password: $influencer_accept_pass </p>
                 
                 <h5> Thankyou for using our website </h5>
             </html>
        ";
            
            $mail = new PHPMailer\PHPMailer\PHPMailer();
        
            // $mail ->AddReplyTo('ebox.asia@live.com', 'GOGO Empire');
            $mail->SetFrom('ebox.asia@live.com');
            $mail->AddAddress($influencer_accept_email);
            $mail->Subject="Your request is appoved by Ebox.asia";
            $mail->MsgHTML($message_body);
            $mail->send();


        //: insert influencer..    
        $insert_influencer = "INSERT INTO food_company (Company_name,company_address,company_openinghours,company_email,company_pass,company_type,company_banner_img) values ('$influencer_accept_name', '$influencer_accept_add','','$influencer_accept_email','$influencer_accept_pass','influencer','')";
        $run_insert_influencer = mysqli_query($connection, $insert_influencer);
        
        if($run_insert_influencer){
            
           $update_influencer = "Update influencer SET i_accountstatus = 'verifiedwithlogin' where i_id = '$influencer_acceptrequest_id'";
            $run_update_influencer = mysqli_query($connection, $update_influencer);
    

                 if($run_update_influencer){
            
            
                    $_SESSION['success'] = "Influencer's Request is Accepted";
                    header('Location:  request_influencers.php'); 
            
                    
            
                }else{
            
                    $_SESSION['status'] = "Influencer's Request is NOT Accepted";       
                    // $_SESSION['status_code'] = "error";
                    header('Location: request_influencers.php');
            
                }
            
        }else{
            $_SESSION['status'] = "";       
                    $_SESSION['status_code'] = "Error";
                    header('Location: request_influencers.php');
            
        }
        
        
    //     $update_influencer = "Update influencer SET i_accountstatus = 'verifiedwithlogin' where i_id = '$influencer_acceptrequest_id'";
    //     $run_update_influencer = mysqli_query($connection, $update_influencer);
    

    //  if($run_update_influencer){


    //     $_SESSION['success'] = "Influencer's Request is Accepted";
    //     header('Location:  request_influencers.php'); 

        

    // }else{

    //     $_SESSION['status'] = "Influencer's Request is NOT Accepted";       
    //     // $_SESSION['status_code'] = "error";
    //     header('Location: request_influencers.php');

    // }
    
    
    
    
    

}

//: Reject Influencer Request: 
if(isset($_POST['reject_influencer_request_btn'])){

    $influencer_rejectrequest_id = $_POST['reject_influencer_request_id'];
    $influencer_rejectrequest_email = $_POST['reject_influencer_request_email'];
    
    $sel_influencer = "SELECT * from influencer where i_id = '$influencer_rejectrequest_id'";
    $run_influencer = mysqli_query($connection, $sel_influencer);

    //: Fetching data
    $fetch_influencer = mysqli_fetch_array($run_influencer);

    $influencer_name = $fetch_influencer['i_name'];
    
    $message_body = "
             <html>

                 <h2> Dear $influencer_name </h2>

                 <p> Sorry, Your request is not approved</p>

                 <p> Your are not capable. </p>

                 <h3> Thankyou for using our website </h3>
             </html>
        ";
    $mail = new PHPMailer\PHPMailer\PHPMailer();

    // $mail ->AddReplyTo('ebox.asia@live.com', 'GOGO Empire');
    $mail->SetFrom('ebox.asia@live.com');
    $mail->AddAddress($influencer_rejectrequest_email);
    $mail->Subject="Your request is not appoved by Ebox.asia";
    $mail->MsgHTML($message_body);
    $mail->send();
    
    //: Delete data from influencer
    
    $delete_influencer = "Delete from influencer where i_id = '$influencer_rejectrequest_id'";
    $run_delete_influencer = mysqli_query($connection, $delete_influencer);
    

     if($run_delete_influencer){


        $_SESSION['success'] = "Influencer's Request is Deleted";
        header('Location:  request_influencers.php'); 

        

    }else{

        $_SESSION['status'] = "Influencer's Request is NOT DELETED";       
        // $_SESSION['status_code'] = "error";
        header('Location: request_influencers.php');

    }

    

    

 }




// USER WEBSITE CODE

//:: ADD OR UPDATE NEW BANNER

if(isset($_POST['addbanner'])){

    $item_company = $_POST['banner_company'];
    $updateitem_img = $_FILES['banner_img']['name'];

    //:: UPDATING THE IMAGE::
    $update_img = "SELECT * FROM food_company where fcompany_id= '$item_company'";
    $run_update_img = mysqli_query($connection, $update_img);

    foreach($run_update_img as $fa_row){
        if($updateitem_img == NULL){
            $image_data = $fa_row['company_banner_img'];
        }else{
            //: upload with new image and delete with old image
            if($img_path = "homepage_images/".$fa_row['company_banner_img']){
                unlink($img_path);
                $image_data = $updateitem_img;
            }
            
        }
    }


    $query = "UPDATE food_company SET company_banner_img='$image_data'    
    WHERE fcompany_id='$item_company' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        if($updateitem_img == NULL){
            $_SESSION['success'] = "Item is Added with existing image";
            header('Location: userwebsite_homepage_banner.php'); 
        }else{
            //: upload with new image and delete with old image
            move_uploaded_file($_FILES["banner_img"]["tmp_name"], "homepage_images/".$_FILES["banner_img"]["name"] );
            $_SESSION['success'] = "Item is Added";
            header('Location: userwebsite_homepage_banner.php'); 
        }
      
    }
    else
    {
        $_SESSION['status'] = "item is NOT Added";
        // $_SESSION['status_code'] = "error";
        header('Location: userwebsite_homepage_banner.php'); 
    } 

}





//: Delete Banner
if(isset($_POST['deletebanner_btn']))
{
    
    $prodictitemdeleteid = $_POST['deletebanner_id'];

    $query = "UPDATE food_company SET company_banner_img='' WHERE fcompany_id='$prodictitemdeleteid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "item is Deleted";
        // $_SESSION['status_code'] = "success";
        header('Location: userwebsite_homepage_banner.php'); 
    }
    else
    {
        $_SESSION['status'] = "Item is NOT DELETED";       
        // $_SESSION['status_code'] = "error";
        header('Location: userwebsite_homepage_banner.php'); 
    }    

    
}


//: top product yes button

if(isset($_POST['top_product_yes_btn'])){

    $topProduct_yes_id = $_POST['top_product_yes_id'];


    $update_topProduct = "UPDATE food_items SET top_product = 'yes' WHERE item_id='$topProduct_yes_id' ";
    $run_topProduct = mysqli_query($connection,$update_topProduct);

    if($run_topProduct){
        // $_SESSION['success'] = "Product is Added in Top Product";
        // $_SESSION['status_code'] = "success";
        header('Location: userwebsite_homepage_topproduct.php'); 
    }
    else
    {
        // $_SESSION['status'] = "Product is not Added in Top Product";       
        // $_SESSION['status_code'] = "error";
        header('Location:userwebsite_homepage_topproduct.php'); 
    }    
}

//: top product no button

if(isset($_POST['top_product_no_btn'])){

    $topProduct_no_id = $_POST['top_product_no_id'];
   

    $update_topProduct = "UPDATE food_items SET top_product='' WHERE item_id='$topProduct_no_id' ";
    $run_topProduct = mysqli_query($connection,$update_topProduct);

    if($run_topProduct){
        // $_SESSION['success'] = "Product is remove from Top Product";
        // $_SESSION['status_code'] = "success";
        header('Location: userwebsite_homepage_topproduct.php'); 
    }
    else
    {
        // $_SESSION['status'] = "Product is remove from Top Product";       
        // $_SESSION['status_code'] = "error";
        header('Location:userwebsite_homepage_topproduct.php'); 
    }    
}


//: New Arrival button

if(isset($_POST['new_arrival_btn'])){

    $new_arrival_id = $_POST['new_arrival_id'];


    //: Count New Arrival items
    $query = "Select * from food_items where trending='New Arrival'";
    $run_query = mysqli_query($connection, $query);
    $count =  mysqli_num_rows($run_query);


    if($count>2){
        $_SESSION['success'] = "Maximum no of adding the item in new arrival is 3";
        header('Location: userwebsite_homepage_topproduct.php'); 
    }else {

         $update_Product = "UPDATE food_items SET trending = 'New Arrival' WHERE item_id='$new_arrival_id' ";
         $run_Product = mysqli_query($connection,$update_Product);

            if($run_Product){
                // $_SESSION['success'] = "Product is Added in Top Product";
                // // $_SESSION['status_code'] = "success";
                header('Location: userwebsite_homepage_topproduct.php'); 
            }
            else
            {
                // $_SESSION['status'] = "Product is not Added in Top Product";       
                // // $_SESSION['status_code'] = "error";
                header('Location:userwebsite_homepage_topproduct.php'); 
            }    

    }


 
}

//: Daily Discover button

if(isset($_POST['daily_discover_btn'])){

    $daily_discover_id = $_POST['daily_discover_id'];

      //: Count New Arrival items
      $query = "Select * from food_items where trending='Daily Discover'";
      $run_query = mysqli_query($connection, $query);
      $count =  mysqli_num_rows($run_query);
  
  
      if($count>2){

        $_SESSION['success'] = "Maximum no of adding the item in Daily Discover is 3";
        header('Location: userwebsite_homepage_topproduct.php'); 

      }else{

            $update_Product = "UPDATE food_items SET trending = 'Daily Discover' WHERE item_id='$daily_discover_id' ";
            $run_Product = mysqli_query($connection,$update_Product);

            if($run_Product){
                // $_SESSION['success'] = "Product is Added in Top Product";
                // // $_SESSION['status_code'] = "success";
                header('Location: userwebsite_homepage_topproduct.php'); 
            }
            else
            {
                // $_SESSION['status'] = "Product is not Added in Top Product";       
                // // $_SESSION['status_code'] = "error";
                header('Location:userwebsite_homepage_topproduct.php'); 
            }  
    
    }
}

//: Trending Search Button 
if(isset($_POST['trending_search_btn'])){

    $trending_search_id = $_POST['trending_search_id'];

     //: Count New Arrival items
     $query = "Select * from food_items where trending='Trending Search'";
     $run_query = mysqli_query($connection, $query);
     $count =  mysqli_num_rows($run_query);


     if($count>2){

        $_SESSION['success'] = "Maximum no of adding the item in Trending Search is 3";
        header('Location: userwebsite_homepage_topproduct.php'); 

      }else{



            $update_Product = "UPDATE food_items SET trending = 'Trending Search' WHERE item_id='$trending_search_id' ";
            $run_Product = mysqli_query($connection,$update_Product);

            if($run_Product){
                // $_SESSION['success'] = "Product is Added in Trending Search";
                // // $_SESSION['status_code'] = "success";
                header('Location: userwebsite_homepage_topproduct.php'); 
            }
            else
            {
                // $_SESSION['status'] = "Product is not Added in Trending Search";       
                // // $_SESSION['status_code'] = "error";
                header('Location:userwebsite_homepage_topproduct.php'); 
            }   
    
    }
}

//: Null Button 
if(isset($_POST['null_btn'])){

    $null_id = $_POST['null_id'];

    $update_Product = "UPDATE food_items SET trending = '' WHERE item_id='$null_id' ";
    $run_Product = mysqli_query($connection,$update_Product);

    if($run_Product){
        // $_SESSION['success'] = "Product is Added in Top Product";
        // // $_SESSION['status_code'] = "success";
        header('Location: userwebsite_homepage_topproduct.php'); 
    }
    else
    {
        // $_SESSION['status'] = "Product is not Added in Top Product";       
        // // $_SESSION['status_code'] = "error";
        header('Location:userwebsite_homepage_topproduct.php'); 
    }    
}


//:: Contact Us Page: 
if(isset($_POST['updatecontactusbtn'])){

   $update_maplink = $_POST['update_maplink'];
   $update_address = mysqli_real_escape_string($connection,$_POST['update_address']);
   $update_phone = $_POST['update_phone'];
   $update_description = mysqli_real_escape_string($connection,$_POST['update_description']);

   $update_contactusdata = "UPDATE contact_page SET contact_link = '$update_maplink', contact_address = '$update_address', contact_phone = '$update_phone', contact_description = '$update_description' where contactid = '1' ";
   $run_update_contactusdata = mysqli_query($connection, $update_contactusdata);

   if($run_update_contactusdata)
    {
        $_SESSION['success'] = "Data is updated";
        // $_SESSION['status_code'] = "success";
        header('Location:  userwebsite_contactuspage_data.php'); 
    }
    else
    {
        $_SESSION['status'] = "Data is not updated";       
        // $_SESSION['status_code'] = "error";
        header('Location: userwebsite_contactuspage_data.php'); 
    }    
}


//:: About Us page: 

//:: Add new question
if(isset($_POST['addaboutusquestionbtn'])){

    $aboutus_question = $_POST['aboutus_question'];
    $aboutus_answer = $_POST['aboutus_answer'];

    $insert_aboutus = "INSERT INTO about_questions(about_question, about_answer) values('$aboutus_question','$aboutus_answer')";
    $run_aboutus = mysqli_query($connection,$insert_aboutus);

    if($run_aboutus)
    {
        $_SESSION['success'] = "Data is added";
        // $_SESSION['status_code'] = "success";
        header('Location:  userwebsite_aboutuspage_question.php'); 
    }
    else
    {
        $_SESSION['status'] = "Data is not added";       
        // $_SESSION['status_code'] = "error";
        header('Location: userwebsite_aboutuspage_question.php'); 
    }    


    

}

//: Update About us question
if(isset($_POST['updateaboutusbtn']))
{
    $aboutus_update_id = $_POST['aboutus_updateid'];
    $aboutus_update_question = $_POST['aboutus_updatequestion'];
    $aboutus_update_answer = $_POST['aboutus_updateanswer'];
  

    $update_aboutus = "UPDATE about_questions SET about_question='$aboutus_update_question', about_answer='$aboutus_update_answer' WHERE about_id='$aboutus_update_id' ";
    $run_update_aboutus = mysqli_query($connection, $update_aboutus);

    if($run_update_aboutus)
    {
        $_SESSION['success'] = "Data is Updated";
        // $_SESSION['status_code'] = "success";
        header('Location:  userwebsite_aboutuspage_question.php'); 
    }
    else
    {
        $_SESSION['status'] = "Data is not Updated";       
        // $_SESSION['status_code'] = "error";
        header('Location: userwebsite_aboutuspage_question.php'); 
    }   
}

//: Update About us question
if(isset($_POST['aboutus_delete_btn']))
{
    $aboutus_delete_id = $_POST['aboutus_delete_id'];

    $delete_aboutus = "DELETE FROM about_questions WHERE about_id ='$aboutus_delete_id' ";
    $run_delete_aboutus = mysqli_query($connection, $delete_aboutus);

    if($run_delete_aboutus)
    {
        $_SESSION['success'] = "Data is Delete";
        // $_SESSION['status_code'] = "success";
        header('Location:  userwebsite_aboutuspage_question.php'); 
    }
    else
    {
        $_SESSION['status'] = "Data is not Deleted";       
        // $_SESSION['status_code'] = "error";
        header('Location: userwebsite_aboutuspage_question.php'); 
    }   
    
}


//: Add Food Sub-Category
if(isset($_POST['addfoodsubcategorybtn'])){

    $subcat_food_title = $_POST['food_subcategory_name'];
    $maincategory_id = $_POST['main_cat_id'];

    $insert_subfoodcategory = "insert into subcategory_stageone (sc_main_id,sc_name) values ('$maincategory_id','$subcat_food_title')";

    if(mysqli_query($connection, $insert_subfoodcategory)){
        $_SESSION['success'] = "Food Sub-Category is Added";
        header('Location: food_category.php'); 
    }else{ 

        $_SESSION['status'] = "Food Sub-Category is not Added";       
        header('Location: food_category.php'); 
    }
}

//:: Update Sub-Food Category
if(isset($_POST['updatefood_subcat_btn'])){

    $sub_fcatupdateid = $_POST['subfoodcat_updateid'];
    $sub_fcatupdatename = $_POST['subfoodcat_updatename'];

    $query = "UPDATE subcategory_stageone SET sc_name='$sub_fcatupdatename' WHERE sc_id='$sub_fcatupdateid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "Your Sub-food Category is Updated";
        // $_SESSION['status_code'] = "success";
        header('Location: food_category.php'); 
    }
    else
    {
        $_SESSION['status'] = "Your Data is NOT Updated";
        // $_SESSION['status_code'] = "error";
        header('Location: food_category.php'); 
    }
}

//:: Delete the SUB-Food Category Data..

if(isset($_POST['delete_subfoodcat_btn']))
{
    $sub_fcatdeleteid = $_POST['delete_subfoodcat_id'];

    $query = "DELETE FROM subcategory_stageone WHERE sc_id='$sub_fcatdeleteid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "Food Sub-Category is Deleted";
        // $_SESSION['status_code'] = "success";
        header('Location: food_category.php'); 
    }
    else
    {
        $_SESSION['status'] = "Food Category Data is NOT DELETED";       
        // $_SESSION['status_code'] = "error";
        header('Location: food_category.php'); 
    }    
}


//:: Add subcategory at food-item table: 
if(isset($_POST['add_subcategoryforfood_btn'])){

    $item_id = $_POST['add_subcategoryforfood_itemid'];
    $subcategory_id = $_POST['add_subcategoryforfood_id'];

    $add_subcategory = "UPDATE food_items SET fcat_sub_id = '$subcategory_id' where item_id = '$item_id' ";
    $run_add_subcategory = mysqli_query($connection, $add_subcategory);
 
    if($run_add_subcategory)
     {
         $_SESSION['success'] = "DONE";
         // $_SESSION['status_code'] = "success";
         header('Location:  food_item.php'); 
     }
     else
     {
         $_SESSION['status'] = "Data is not added";       
         // $_SESSION['status_code'] = "error";
         header('Location: food_item.php'); 
        // echo "Error insertin data: " . mysqli_error($connection);
     }    
 }




//: Add Product Sub-Category
if(isset($_POST['addproductsubcategorybtn'])){

    $subcat_producttitle = $_POST['product_subcategory_name'];
    $maincategory_id = $_POST['main_cat_id'];

    $insert_subproductcategory = "insert into subcategory_stageone (sc_main_id,sc_name) values ('$maincategory_id','$subcat_producttitle')";

    if(mysqli_query($connection, $insert_subproductcategory)){
        $_SESSION['success'] = "Product Sub-Category is Added";
        header('Location: product_category.php'); 
    }else{ 

        $_SESSION['status'] = "Product Sub-Category is not Added";       
        header('Location: product_category.php'); 
    }
}

//:: Update Sub-product Category
if(isset($_POST['updateproduct_subcat_btn'])){

    $sub_fcatupdateid = $_POST['subproductcat_updateid'];
    $sub_fcatupdatename = $_POST['subproductcat_updatename'];

    $query = "UPDATE subcategory_stageone SET sc_name='$sub_fcatupdatename' WHERE sc_id='$sub_fcatupdateid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "Your Product Sub-Category is Updated";
        // $_SESSION['status_code'] = "success";
        header('Location: product_category.php'); 
    }
    else
    {
        $_SESSION['status'] = "Your Data is NOT Updated";
        // $_SESSION['status_code'] = "error";
        header('Location: product_category.php'); 
    }
}


//:: Delete the SUB-Product Category Data..

if(isset($_POST['delete_subproductcat_btn']))
{
    $sub_fcatdeleteid = $_POST['delete_subproductcat_id'];

    $query = "DELETE FROM subcategory_stageone WHERE sc_id='$sub_fcatdeleteid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "Product Sub-Category is Deleted";
        // $_SESSION['status_code'] = "success";
        header('Location: product_category.php'); 
    }
    else
    {
        $_SESSION['status'] = "Product Sub-Category Data is NOT DELETED";       
        // $_SESSION['status_code'] = "error";
        header('Location: product_category.php'); 
    }    
}

//:: Add subcategory at food-item table: 
if(isset($_POST['add_subcategoryforproduct_btn'])){

    $item_id = $_POST['add_subcategoryforproduct_itemid'];
    $subcategory_id = $_POST['add_subcategoryforproduct_id'];

    $add_subcategory = "UPDATE food_items SET fcat_sub_id = '$subcategory_id' where item_id = '$item_id' ";
    $run_add_subcategory = mysqli_query($connection, $add_subcategory);
 
    if($run_add_subcategory)
     {
         $_SESSION['success'] = "DONE";
         // $_SESSION['status_code'] = "success";
         header('Location:  product_subcategory.php'); 
     }
     else
     {
         $_SESSION['status'] = "Data is not added";       
         // $_SESSION['status_code'] = "error";
         header('Location:  product_subcategory.php'); 
        // echo "Error insertin data: " . mysqli_error($connection);
     }    
 }


 //: Add More images for food

 if(isset($_POST['addmoreimagesforfood'])){
   
    $item_id = $_POST['item_id'];
    $item_company_id = $_POST['item_company_id'];
    // //: Image Name
    $item_img = $_FILES['item_moreimg']['name'];


    // //: GET LAST ROW OF ITEM_IMG_NO

    // $query = "Select * from item_images WHERE it_item_id = '$item_id' ORDER BY it_id DESC LIMIT 1";
    // $run_query = mysqli_query($connection, $query);
    // $fetch_query = mysqli_fetch_assoc($run_query);

    // $fetch_id = $fetch_query['it_img_no'];

    // $item_img_no = ($fetch_id + 1);

    
    if(file_exists("fooditem_images/" .$_FILES["item_moreimg"]["name"])){

        $image_already_exist = $_FILES["item_moreimg"]["name"];
        $_SESSION['status'] = "Image Already exists. $store";
        header('Location: food_item.php'); 

    }else{

        $insert_moreimages ="insert into item_images (it_item_id, it_company_id,it_item_img) values 
         ('$item_id','$item_company_id','$item_img')";

            if(mysqli_query($connection, $insert_moreimages)){
            move_uploaded_file($_FILES["item_moreimg"]["tmp_name"], "fooditem_images/".$_FILES["item_moreimg"]["name"] );
            $_SESSION['success'] = "Item-Image is Added";
            header('Location: food_item.php'); 
        }else{ 

            $_SESSION['status'] = "Item-Image is not Added";       
            header('Location: food_item.php');
            //  echo "Error insertin data: " . mysqli_error($connection); 
        }

    }
    
  
}

//: DELETE MOREIMAGES for food

if(isset($_POST['delete_fooditem_images_btn']))
{
    $delete_moreimage_id = $_POST['delete_fooditem_images_id'];

    $query = "DELETE FROM item_images WHERE it_id='$delete_moreimage_id' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "Image is deleted";
        // $_SESSION['status_code'] = "success";
        header('Location: food_item.php'); 
    }
    else
    {
        $_SESSION['status'] = "Image is not deleted";       
        // $_SESSION['status_code'] = "error";
        header('Location: food_item.php'); 
    }    
}


//: Add More images for Product

if(isset($_POST['addmoreimagesforproduct'])){
   
    $item_id = $_POST['item_id'];
    $item_company_id = $_POST['item_company_id'];
    // //: Image Name
    $item_img = $_FILES['item_moreimg']['name'];

    $query = "Select * from item_images WHERE it_item_id = '$item_id' ORDER BY it_id DESC LIMIT 1";
    $run_query = mysqli_query($connection, $query);
    $fetch_query = mysqli_fetch_assoc($run_query);

    $fetch_id = $fetch_query['it_img_no'];

    $item_img_no = ($fetch_id + 1);

    
    if(file_exists("fooditem_images/" .$_FILES["item_moreimg"]["name"])){

        $image_already_exist = $_FILES["item_moreimg"]["name"];
        $_SESSION['status'] = "Image Already exists. $store";
        header('Location: food_item.php'); 

    }else{

        $insert_moreimages ="insert into item_images (it_item_id, it_company_id,it_item_img) values 
         ('$item_id','$item_company_id','$item_img')";

            if(mysqli_query($connection, $insert_moreimages)){
            move_uploaded_file($_FILES["item_moreimg"]["tmp_name"], "fooditem_images/".$_FILES["item_moreimg"]["name"] );
            $_SESSION['success'] = "Item-Image is Added";
            header('Location: products.php'); 
        }else{ 

            $_SESSION['status'] = "Item-Image is not Added";       
            header('Location: products.php');
            //  echo "Error insertin data: " . mysqli_error($connection); 
        }

    }
    
  
}

//: DELETE MOREIMAGES for Product

if(isset($_POST['delete_productitem_images_btn']))
{
    $delete_moreimage_id = $_POST['delete_productitem_images_id'];

    $query = "DELETE FROM item_images WHERE it_id='$delete_moreimage_id' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "Image is deleted";
        // $_SESSION['status_code'] = "success";
        header('Location: products.php'); 
    }
    else
    {
        $_SESSION['status'] = "Image is not deleted";       
        // $_SESSION['status_code'] = "error";
        header('Location: products.php'); 
    }    
}

//:Profiles 

//: Owner Info
if(isset($_POST['modifystore_1'])){
    
    
    $update_influencer_email = $_POST['influencer_email'];
    $update_owner_name = $_POST['store_updatename'];


    $query = "UPDATE food_company SET owner_name='$update_owner_name'
    WHERE company_email='$update_influencer_email' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
          header('Location: mainstore_setting.php'); 
        }
      
    
    else
    {
       header('Location: mainstore_setting.php'); 
    }
   
}

if(isset($_POST['modifystore_2'])){
    
    
    $update_influencer_email = $_POST['influencer_email'];
    $update_owner_ic = $_POST['store_updateic'];


    $query = "UPDATE food_company SET owner_ic='$update_owner_ic'
    WHERE company_email='$update_influencer_email' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
          header('Location: mainstore_setting.php'); 
        }
      
    
    else
    {
       header('Location: mainstore_setting.php'); 
    }
   
}

if(isset($_POST['modifystore_3'])){
    
    
    $update_influencer_email = $_POST['influencer_email'];
    $update_contactnum1 = $_POST['store_updatecontact1'];


    $query = "UPDATE food_company SET   contact_number1='$update_contactnum1'
    WHERE company_email='$update_influencer_email' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
          header('Location: mainstore_setting.php'); 
        }
      
    
    else
    {
       header('Location: mainstore_setting.php'); 
    }
   
}

if(isset($_POST['modifystore_4'])){
    
    
    $update_influencer_email = $_POST['influencer_email'];
    $update_contactnum2 = $_POST['store_updatecontact2'];


    $query = "UPDATE food_company SET contact_number2='$update_contactnum2'
    WHERE company_email='$update_influencer_email' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
          header('Location: mainstore_setting.php'); 
        }
      
    
    else
    {
       header('Location: mainstore_setting.php'); 
    }
   
}

if(isset($_POST['modifystore_5'])){
    
    
    $update_influencer_email = $_POST['influencer_email'];
    $update_owner_img = $_FILES['store_updateownerimg']['name'];
    //$item_img = $_FILES['product_img']['name'];
    

    // $Add_Food_Item = false; 
    
    if($update_owner_img == '') //If the image is empty, insert default image for it
    {
        $update_owner_img = 'Default.png';
        $_SESSION['status'] = "Image Is Uploaded With Default. $store";
        
    }
$Add_Owner_Img = false;
	if($Add_Owner_Img){  //file_exists("fooditem_images/" .$_FILES["item_img"]["name"])
        $image_already_exist = $_FILES["store_updateownerimg"]["name"];
        $_SESSION['status'] = "Image Already exists. $store";
        header('Location: mainstore_setting.php'); 
    
    }else{


    $query = "UPDATE food_company SET owner_img='$update_owner_img'
    WHERE company_email='$update_influencer_email' ";  
    $query_run = mysqli_query($connection, $query);

 if($query_run)
            {
               move_uploaded_file($_FILES["store_updateownerimg"]["tmp_name"], "owner_images/".$_FILES["store_updateownerimg"]["name"] );
                header('Location: mainstore_setting.php'); 
            }
            else 
            {

                header('Location: mainstore_setting.php'); 
            }
    }
   
}


//: Business Info
if(isset($_POST['modifystore_6'])){
    
    
    $update_influencer_email = $_POST['influencer_email'];
    $update_shop_name = $_POST['store_updatestorename'];


    $query = "UPDATE food_company SET Company_name='$update_shop_name'
    WHERE company_email='$update_influencer_email' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
          header('Location: mainstore_setting.php'); 
        }
      
    
    else
    {
       header('Location: mainstore_setting.php'); 
    }
   
}

if(isset($_POST['modifystore_7'])){
    
    
    $update_influencer_email = $_POST['influencer_email'];
    $update_shop_address = $_POST['store_updateaddress'];


    $query = "UPDATE food_company SET company_address='$update_shop_address'
    WHERE company_email='$update_influencer_email' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
          header('Location: mainstore_setting.php'); 
        }
      
    
    else
    {
       header('Location: mainstore_setting.php'); 
    }
   
}

if(isset($_POST['modifystore_8'])){
    
    
    $update_influencer_email = $_POST['influencer_email'];
    $update_ssn = $_POST['store_updatessn'];


    $query = "UPDATE food_company SET ssn_number='$update_ssn'
    WHERE company_email='$update_influencer_email' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
          header('Location: mainstore_setting.php'); 
        }
      
    
    else
    {
       header('Location: mainstore_setting.php'); 
    }
   
}

if(isset($_POST['modifystore_9'])){
    
    
    $update_influencer_email = $_POST['influencer_email'];
    $update_insurance = $_POST['store_updateinsurance'];


    $query = "UPDATE food_company SET insurance='$update_insurance'
    WHERE company_email='$update_influencer_email' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
          header('Location: mainstore_setting.php'); 
        }
      
    
    else
    {
       header('Location: mainstore_setting.php'); 
    }
   
}

if(isset($_POST['modifystore_10'])){
    
    
    $update_influencer_email = $_POST['influencer_email'];
    $update_shop_contactnum = $_POST['store_updatestorenum'];


    $query = "UPDATE food_company SET bcontact_number='$update_shop_contactnum'
    WHERE company_email='$update_influencer_email' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
          header('Location: mainstore_setting.php'); 
        }
      
    
    else
    {
       header('Location: mainstore_setting.php'); 
    }
   
}

//: Bank Info
if(isset($_POST['modifystore_11'])){
    
    
    $update_influencer_email = $_POST['influencer_email'];
    $update_bank_name = $_POST['store_updatebankname'];


    $query = "UPDATE food_company SET bank_name='$update_bank_name'
    WHERE company_email='$update_influencer_email' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
          header('Location: mainstore_setting.php'); 
        }
      
    
    else
    {
       header('Location: mainstore_setting.php'); 
    }
   
}

if(isset($_POST['modifystore_12'])){
    
    
    $update_influencer_email = $_POST['influencer_email'];
    $update_acc_name = $_POST['store_updateaccname'];


    $query = "UPDATE food_company SET acc_name='$update_acc_name'
    WHERE company_email='$update_influencer_email' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
          header('Location: mainstore_setting.php'); 
        }
      
    
    else
    {
       header('Location: mainstore_setting.php'); 
    }
   
}

if(isset($_POST['modifystore_13'])){
    
    
    $update_influencer_email = $_POST['influencer_email'];
    $update_acc_num = $_POST['store_updateaccnum'];


    $query = "UPDATE food_company SET acc_num='$update_acc_num'
    WHERE company_email='$update_influencer_email' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
          header('Location: mainstore_setting.php'); 
        }
      
    
    else
    {
       header('Location: mainstore_setting.php'); 
    }
   
}

?>
 
<?php

 // if(mysqli_query($connection, $insert_foodcategory)){
    //     echo "data insert successfully";
    // }else{

    //     echo "Error insertin data: " . mysqli_error($connection);
    // }
?>


