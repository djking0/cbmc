<?php
include('productstore_security.php');
include('includes/header.php'); 
include('includes/navbar_product.php'); 
?>


<div class="container-fluid">

    <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">

            <div class="card-header py-3">

                <?php
                
                    if(isset($_POST['productstore_influencer_btn'])){
                        echo "<h6 class='m-0 font-weight-bold text-primary'>Influencer's Detail
                        </h6> ";
                    }else if(isset($_POST['productstore_viewitem_btn'])){

                        echo "<h6 class='m-0 font-weight-bold text-primary'>Item's Detail
                        </h6>";

                    }else {

                    }

                ?>        
        
            </div>

            <div class="card-body">
                <?php

                    if(isset($_POST['productstore_influencer_btn'])){
                        $email = $_POST['productstore_influencer_email'];
                        require 'dbconfig.php'; 

                        // echo "$email";

                        $query = "SELECT * FROM influencer WHERE i_email='$email'";
                        $query_run = mysqli_query($connection, $query);


                        foreach($query_run as $row)
                        {
                            ?>
                                <div class="modal-body">

                                    <div class="form-row">
                                        <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;" src="influencer_images/<?php echo $row['i_image']; ?>" alt="">
                                    </div>

                                    <div class="form-row">
                                        <h4> <strong><?php  echo "&nbsp;&nbsp;&nbsp;";  echo $row['i_name']; ?> </strong></h4>
                                        
                                    </div>

                                    <br>

                                    <div class="form-row">
                                        
                                            <div class="form-group col-md-6">
                                            <strong><label><?php echo "&nbsp;&nbsp;&nbsp;" ?>Email:</label></strong>
                                                <br>
                                                <label> <?php echo "&nbsp;&nbsp;&nbsp;"; echo $row['i_email']; ?></label>
                                            </div>
                                            <div class="form-group col-md-6">
                                            <strong><label>Phone:</label></strong>
                                            <br>
                                                <label> <?php  echo $row['i_contact']; ?> </label>
                                            </div>
                                    </div>

                                    <div class="form-row">
                                        
                                            <div class="form-group col-md-6">
                                            <strong> <label> <?php echo "&nbsp;&nbsp;&nbsp;" ?>Gender:</label></strong>
                                                <br>
                                                <label> <?php echo "&nbsp;&nbsp;&nbsp;";  echo $row['i_gender']; ?></label>
                                            </div>
                                            <div class="form-group col-md-6">
                                            <strong> <label>Full Address</label></strong>
                                            <br>
                                                <label> <?php  echo $row['i_address']; ?>  </label>
                                            </div>
                                    </div>

                                    <div class="form-row">
                                        
                                            <div class="form-group col-md-6">
                                                <label><strong><?php echo "&nbsp;&nbsp;&nbsp;" ?>Page:</strong></label>
                                                <br>
                                                <label> <?php echo "&nbsp;&nbsp;&nbsp;" ?> (<?php  echo $row['i_pagename']; ?>) -<?php  echo $row['i_pagetype']; ?></label><br>
                                                <label><?php echo "&nbsp;&nbsp;&nbsp;" ?><a href="<?php  echo $row['i_pagelink']; ?>"><?php  echo $row['i_pagelink']; ?></a></label><br>
                                                <label> <?php echo "&nbsp;&nbsp;&nbsp;" ?><?php  echo $row['i_followers']; ?></label>
                                                
                                            </div>
                                    </div>

                                </div>


                            <?php
                        }

                    } //: End if

                    if(isset($_POST['productstore_viewitem_btn'])){
                        $id = $_POST['productstore_viewitem_id'];

                        require 'dbconfig.php'; 
                        
                        $query = "SELECT * FROM food_items WHERE item_id='$id'";
                        $query_run = mysqli_query($connection, $query);


                        foreach($query_run as $row)
                        {
                            ?>
                                <div class="modal-body">
                            
                            <div class="form-row">
                                <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;" src="fooditem_images/<?php echo $row['item_img']; ?>" alt="">
                            </div>

                            <div class="form-row">
                                <h4> <strong><?php  echo "&nbsp;&nbsp;&nbsp;";  echo $row['item_title']; ?> </strong></h4> 
                            </div>

                            <br>
                            <div class="form-row">
                                
                                    <div class="form-group col-md-6">
                                    <strong><label><?php echo "&nbsp;&nbsp;&nbsp;" ?>Store:</label></strong>
                                        <br>
                                        <label> 

                                            <?php   
                                                echo "&nbsp;&nbsp;&nbsp;"; 
                                                $hotel_id =  $row['fcompany_id']; 

                                                $get_hotel_data = "Select * from food_company where fcompany_id = '$hotel_id'";
                                                $run_hotel_data = mysqli_query($connection,$get_hotel_data);
                                                $fetch_hotel_data = mysqli_fetch_array($run_hotel_data);

                                                $hotel_name = $fetch_hotel_data['Company_name'];

                                                echo "$hotel_name";
                                            ?>
                                        </label>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <strong><label>Category:</label></strong>
                                        <br>
                                        <label> 
                                            <?php  

                                                $category_id =  $row['fcat_id']; 

                                                $get_cat_data = "Select * from food_category where fcat_id = '$category_id'";
                                                $run_cat_data = mysqli_query($connection,$get_cat_data);
                                                $fetch_cat_data = mysqli_fetch_array($run_cat_data);

                                                $category_name = $fetch_cat_data['food_cat'];

                                                echo $category_name;

                                            ?> 
                                        </label>
                                    </div>
                            </div>

                            <div class="form-row">
                                
                                    <div class="form-group col-md-6">
                                    <strong> <label> <?php echo "&nbsp;&nbsp;&nbsp;" ?>Price:</label></strong>
                                        <br>
                                        <label> <?php echo "&nbsp;&nbsp;&nbsp;";  echo'RM'; echo $row['item_price'];  ?></label>
                                    </div>
                                    <div class="form-group col-md-6">
                                    <strong> <label>Item Description</label></strong>
                                    <br>
                                        <label> <?php  echo $row['item_desc']; ?>  </label>
                                    </div>
                            </div>

                            <div class="form-row">
                                
                                    <div class="form-group col-md-6">

                                        <?php 
                                            $youtube = $row['youtube_link']; 

                                            if($youtube == ''){
                                                echo "";
                                            }else {
                                                ?>

                                                <label><strong><?php echo "&nbsp;&nbsp;&nbsp;" ?>Youtube link</strong></label>
                                                <br>
                                                <label> <?php echo "&nbsp;&nbsp;&nbsp;" ?> <a href = "<?php  echo  $row['youtube_link']; ?>"> <?php  echo  $row['youtube_link']; ?></a></label><br>


                                                <?php
                                            }
                                        ?>
                                        
                                    
                                        
                                    </div>
                            </div>

                        </div>


                            <?php
                        }

                    } //: End if
                   
                ?>
                 
            </div>
               
            
        </div>
       
       
    </div>

</div>
<!-- /.container-fluid -->


















<?php
include('includes/scripts.php');
include('includes/footer.php');
?>