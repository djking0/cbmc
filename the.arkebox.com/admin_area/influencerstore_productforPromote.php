<?php
include('influencerstore_security.php');
include('includes/header.php'); 
include('includes/navbar_influencer.php'); 
?>




<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">

    <h6 class="m-0 font-weight-bold text-primary">Available Item for Promote
           
    </h6>
  </div>

  <div class="card-body">

    <?php
      if(isset($_SESSION['success']) && $_SESSION['success']!=''){
        echo '<h2> '.$_SESSION['success'].' </h2>.';
        unset($_SESSION['success']);
      } 
      if(isset($_SESSION['status']) && $_SESSION['status']!=''){
        echo '<h2> '.$_SESSION['status'].' </h2>.';
        unset($_SESSION['status']);
      } 
      
    ?>

    <div class="table-responsive">

     

      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>

            <?php

      
            require 'dbconfig.php';

            $email = $_SESSION['influencerusername'];

            $query = "SELECT * FROM store_promote_product where ssp_status = 'yes'";
            $query_run = mysqli_query($connection, $query);

            ?>
            <tr>
                <th> ID </th>
                <th>Item  </th>
                <th>Store</th>
                <th>Your Commission</th>
                <th>Action</th>
                <!-- <th>EDIT </th> -->
            </tr>
            </thead>
            <tbody>

            <?php 
                if(mysqli_num_rows($query_run) > 0)        
                {
                    while($row = mysqli_fetch_assoc($query_run))
                    {
                        

                    ?>
    
                    <tr>
                        <td> <?php  echo $row['spp_id']; ?></td>
                        <td> 
                          <?php  
                      
                              $item_id =  $row['ssp_itemid']; 

                              $get_item_details = "Select * from food_items where item_id = '$item_id'";
                              $run_item_details =  mysqli_query($connection, $get_item_details);
                              $fetch_item_details = mysqli_fetch_assoc($run_item_details);

                              //: Item name 
                              $item_name = $fetch_item_details['item_title'];
                              

                            

                              ?>


                              <form action="influencerstore_viewitem.php" method="post">
                                  <input type="hidden" name="influencerstore_viewitem_id" value="<?php  echo "$item_id" ?>">
                                  <input type="submit"  name="influencerstore_viewitem_btn" value = "<?php echo "$item_name" ?>" style = "background: none;
                                                            border: none;
                                                            color: blue;
                                                            text-decoration: underline;
                                                            cursor: pointer;
                                                            text-decoration: none; " />
                                  
                              </form>
                              
                             
                            <?php
                        
                          ?>
                        </td>
                        <td> 
                          <?php 
        
                            $hotel_id = $row['ssp_hotelid']; 

                            $get_company_details = "Select * from food_company where fcompany_id = '$hotel_id'";
                            $run_company_details = mysqli_query($connection, $get_company_details);
                            $fetch_company_details = mysqli_fetch_array($run_company_details);

                            $store_name = $fetch_company_details['Company_name'];

                            ?>


                              <form action="influencerstore_viewitem.php" method="post">
                                  <input type="hidden" name="influencerstore_viewitem_storeid" value="<?php  echo "$hotel_id" ?>">
                                  <input type="submit"  name="influencerstore_viewitem_storebtn" value = "<?php echo "$store_name" ?>" style = "background: none;
                                                            border: none;
                                                            color: blue;
                                                            text-decoration: underline;
                                                            cursor: pointer;
                                                            text-decoration: none; " />
                                  
                              </form>
                              
                             
                            <?php


                          ?> 
                        </td>
                        <td> <?php  echo $row['ssp_influencer_commission']; ?>% </td>
                        
                        <td>

                        <?php 

                        //: Getting influencer id
                        $email = $_SESSION['influencerusername'];

                        $get_influencer_details = "Select * from food_company where company_email='$email'";
                        $run_influencer_details = mysqli_query($connection, $get_influencer_details);
                        $fetch_influencer_details = mysqli_fetch_array($run_influencer_details);
                                                                                                                        
                        $influencer_id = $fetch_influencer_details['fcompany_id']; //:: Customer id 

                        $item_id = $row['ssp_itemid'];

                        // echo "$influencer_id";
                        // echo "$item_id";

                        //: Getting data from product_promote_request

                        $query = "Select * from store_promote_request where spr_influencer_id = '$influencer_id' AND spr_item_id = '$item_id' AND spr_status = 'Request Pending'";
                        $run_query = mysqli_query($connection, $query);
                        $count_rows = mysqli_num_rows($run_query);

                        //: Getting data from product_promote_request  (Request deleted)

                        $query_delete_request = "Select * from store_promote_request where spr_influencer_id = '$influencer_id' AND spr_item_id = '$item_id' AND spr_status = 'Request Deleted'";
                        $run_query_delete_request = mysqli_query($connection, $query_delete_request);
                        $count_rows_delete_request = mysqli_num_rows($run_query_delete_request);

                        //: Getting data from influencer_product

                        $query_influencer_product = "Select * from influencer_product where ip_influencer_id = '$influencer_id' AND ip_item_id = '$item_id'";
                        $run_query_influencer_product = mysqli_query($connection, $query_influencer_product);
                        $count_rows_influencer_product = mysqli_num_rows($run_query_influencer_product); 


                        if($count_rows_influencer_product > 0 ){
                          echo "Your Request is Accepted";

                        } else if($count_rows_delete_request > 0 ){
                         
                          echo "<p style = 'color: RED;'> Your Request is not approved</p>";
                        }else {
                          
                        
                       
                        if($count_rows == 0){

                          ?>
                              <form action="influencerstore_code.php" method="post">
                                  <input type="hidden" name="storepromoterequestsent_id" value="<?php  echo $row['spp_id']; ?>">
                                  <button  type="submit" name="storepromoterequestsent_btn" class="btn btn-success"> Send Request
                                  </button>
                              </form>
                          <?php

                      }else{
                           
                            ?>
                                <form action="influencerstore_code.php" method="post">
                                    <input type="hidden" name="cancelpromoterequestsent_id" value="<?php  echo $row['spp_id']; ?>">
                                    <button  type="submit" name="cancelpromoterequestsent_btn" class="btn btn-danger"> Cancel Request
                                    </button>
                                </form>
                            <?php
                        }
                      }
                        ?>
                            

                        
                    </td>
                    </tr>
            <?php
                    }
                }else {
                    echo "No Record Found";
                }
            ?>
        
            
            
            </tbody>
      </table>

    

    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>