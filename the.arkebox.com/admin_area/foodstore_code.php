<?php

include('foodstore_security.php');
include('dbconfig.php');
// // $connection = mysqli_connect("localhost","root","","gogoshop");
// $connection = mysqli_connect("localhost","gogoempi_gotravelweb","OfKTL6PbD6IP","gogoempi_gotravelweb");



//: Getting current foodstore id: 

$store_email = $_SESSION['foodusername'];
                                    
$get_foodstore_details = "Select * from food_company where company_email='$store_email'";
$run_foodstore_details = mysqli_query($connection, $get_foodstore_details);
$fetch_foodstore_details = mysqli_fetch_array($run_foodstore_details);
                                                                                                
$foodstore_id = $fetch_foodstore_details['fcompany_id']; //:: Customer id 
$foodstore_name = $fetch_foodstore_details['Company_name']; //:: customer name


//: Add New Food Item..

if(isset($_POST['addfoodstoreitem'])){
   
    $item_title = $_POST['food_title'];
    $item_cat = $_POST['food_cat'];
    $item_price = $_POST['food_price'];
    $item_desc = mysqli_real_escape_string($connection,$_POST['food_desc']);
    $status = "on";
    $item_youtubelink = $_POST['food_youtubelink'];
    // $item_discounted_percentage = $_POST['item_discounted_percentage'];
    $item_quantity = $_POST['food_quantity'];
    $item_company = "$foodstore_id";
    $item_discounted_percentage = '1';
    $item_type = 'food';
    $food_img = $_FILES['food_img']['name'];

    if(file_exists("fooditem_images/" .$_FILES["food_img"]["name"])){

        $image_already_exist = $_FILES["food_img"]["name"];
        $_SESSION['status'] = "Image Already exists. $store";
        header('Location: foodstore_fooditems.php'); 

    }else{

        $insert_fooditem = "insert into food_items (fcat_id,fcat_sub_id,fcompany_id,date,item_title,item_img,item_price,item_desc,youtube_link,status,item_discounted_price,item_quantity,item_type,top_product,trending) values 
        ('$item_cat','0', '$item_company',NOW(),'$item_title','$food_img',
        '$item_price','$item_desc','$item_youtubelink','$status','$item_discounted_percentage','$item_quantity', '$item_type','','')";

        if(mysqli_query($connection, $insert_fooditem)){
            move_uploaded_file($_FILES["food_img"]["tmp_name"], "fooditem_images/".$_FILES["food_img"]["name"] );
            $_SESSION['success'] = "Food Item is Added";
            header('Location: foodstore_fooditems.php'); 
        }else{ 

            $_SESSION['status'] = "Food Item is not Added";       
            header('Location: foodstore_fooditems.php'); 
        }

    //      if(mysqli_query($connection, $insert_fooditem)){
    //         move_uploaded_file($_FILES["food_img"]["tmp_name"], "fooditem_images/".$_FILES["food_img"]["name"] );
    //             $_SESSION['success'] = "Food Item is Added";
    //             header('Location: foodstore_fooditems.php'); 
    // }else{

    //     echo "Error insertin data: " . mysqli_error($connection);
    // }
    }
}

//: Update Food item

if(isset($_POST['updatefoodstoreitem'])){

    $updateitem_id = $_POST['food_updateid'];
    $updateitem_title = $_POST['food_updatetitle'];
    $updateitem_cat = $_POST['food_updatecat'];
    $updateitem_price = $_POST['food_updateprice'];
    $updateitem_desc = mysqli_real_escape_string($connection,$_POST['food_updatedesc']);
    $status = "on";
    $updateitem_youtubelink = $_POST['food_updateyoutubelink'];
    // $item_discounted_percentage = $_POST['item_discounted_percentage'];
    $updateitem_quantity = $_POST['food_updatequantity'];
    $item_company = "$foodstore_id";
    $item_discounted_percentage = 1;
    $updateitem_img = $_FILES['food_updateimg']['name'];
    

    //:: UPDATING THE IMAGE::
    $update_img = "SELECT * FROM food_items where item_id= '$updateitem_id'";
    $run_update_img = mysqli_query($connection, $update_img);

    foreach($run_update_img as $fa_row){
        if($updateitem_img == NULL){
            $image_data = $fa_row['item_img'];
        }else{
            //: upload with new image and delete with old image
            if($img_path = "fooditem_images/".$fa_row['item_img']){
                unlink($img_path);
                $image_data = $updateitem_img;
            }
            
        }
    }

    $query = "UPDATE food_items SET fcat_id='$updateitem_cat', fcompany_id='$item_company', item_title='$updateitem_title', item_img='$image_data', item_price='$updateitem_price', 
    item_desc='$updateitem_desc', youtube_link='$updateitem_youtubelink', status='$status', item_discounted_price='$item_discounted_percentage', item_quantity='$updateitem_quantity'   
    WHERE item_id='$updateitem_id' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        if($updateitem_img == NULL){
            $_SESSION['success'] = "Item is Updated with existing image";
            header('Location: foodstore_fooditems.php'); 
        }else{
            //: upload with new image and delete with old image
            move_uploaded_file($_FILES["food_updateimg"]["tmp_name"], "fooditem_images/".$_FILES["food_updateimg"]["name"] );
            $_SESSION['success'] = "Item is Updated";
            header('Location: foodstore_fooditems.php'); 
        }
      
    }
    else
    {
        $_SESSION['status'] = "item is NOT Updated";
        // $_SESSION['status_code'] = "error";
        header('Location: foodstore_fooditems.php'); 
    }

}

//: Delete Food item
if(isset($_POST['deletefoodstore_btn']))
{
    $fooditemdeleteid = $_POST['deletefoodstore_id'];

    $query = "DELETE FROM food_items WHERE item_id='$fooditemdeleteid' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "item is Deleted";
        // $_SESSION['status_code'] = "success";
        header('Location: foodstore_fooditems.php'); 
    }
    else
    {
        $_SESSION['status'] = "Item is NOT DELETED";       
        // $_SESSION['status_code'] = "error";
        header('Location: foodstore_fooditems.php'); 
    }    
}


//: Completed Pickup order button
if(isset($_POST['confirm_pending_order_btn']))
{
    $confirmpickuporderid = $_POST['confirm_pending_order_id'];

    $query = "Select * from customer_pickuporder where porder_id = '$confirmpickuporderid' ";
    $run_query = mysqli_query($connection, $query);
    $fetch_query = mysqli_fetch_array($run_query);

    $getcustomer_id = $fetch_query['customer_id'];
    $getcustomer_order_no = $fetch_query['customer_order_no'];
    $gethotel_id = $fetch_query['hotel_id'];
    $getorder_date = $fetch_query['order_date'];
    $getinvoiceno = $fetch_query['invoiceno'];
    $getorder_desc = $fetch_query['order_description'];
    $getdue_amount = $fetch_query['due_amount'];
    $getpickup_order_date = $fetch_query['pickup_order_date'];
    $getpickup_order_time = $fetch_query['pickup_order_time'];
    $getorder_status = 'Completed with Payment';
    $ordertype = 'FOOD-PICKUP';


    $insert_completed_order = "Insert into completed_orders(customer_id,customer_order_no,hotel_id,order_date,invoice_no,order_description,due_amount,
    pickup_order_date,pickup_order_time,order_status,order_type)values('$getcustomer_id','$getcustomer_order_no','$gethotel_id',
    '$getorder_date','$getinvoiceno','$getorder_desc','$getdue_amount','$getpickup_order_date','$getpickup_order_time','$getorder_status','$ordertype')";

    $run_completed_order = mysqli_query($connection, $insert_completed_order);

    $delete_pickup_order = "DELETE FROM customer_pickuporder where porder_id='$confirmpickuporderid'";
    $run_delete_query = mysqli_query($connection, $delete_pickup_order);

    if($run_delete_query){
        $_SESSION['success'] = "DONE";
        // $_SESSION['status_code'] = "success";
        header('Location: foodstore_pickuporders.php'); 
        
    }else{ 

        $_SESSION['status'] = "NOT DONE";       
        // $_SESSION['status_code'] = "error";
        header('Location: foodstore_pickuporders.php'); 
    
    }
}

//: Cancelled Pickup order button
if(isset($_POST['cancel_pending_order_btn']))
{
    $cancelledickuporderid = $_POST['cancel_pending_order_id'];

    $query = "Select * from customer_pickuporder where porder_id = '$cancelledickuporderid' ";
    $run_query = mysqli_query($connection, $query);
    $fetch_query = mysqli_fetch_array($run_query);

    $getcustomer_id = $fetch_query['customer_id'];
    $getcustomer_order_no = $fetch_query['customer_order_no'];
    $gethotel_id = $fetch_query['hotel_id'];
    $getorder_date = $fetch_query['order_date'];
    $getinvoiceno = $fetch_query['invoiceno'];
    $getorder_desc = $fetch_query['order_description'];
    $getdue_amount = $fetch_query['due_amount'];
    $getpickup_order_date = $fetch_query['pickup_order_date'];
    $getpickup_order_time = $fetch_query['pickup_order_time'];
    $getorder_status = 'Cancelled';
    $ordertype = 'FOOD-PICKUP';


    $insert_cancelled_order = "Insert into cancelled_orders(customer_id,customer_order_no,hotel_id,order_date,invoice_no,order_description,due_amount,
    pickup_order_date,pickup_order_time,order_status,order_type)values('$getcustomer_id','$getcustomer_order_no','$gethotel_id',
    '$getorder_date','$getinvoiceno','$getorder_desc','$getdue_amount','$getpickup_order_date','$getpickup_order_time','$getorder_status','$ordertype')";

    $run_completed_order = mysqli_query($connection, $insert_cancelled_order);

    $delete_pickup_order = "DELETE FROM customer_pickuporder where porder_id='$cancelledickuporderid'";
    $run_delete_query = mysqli_query($connection, $delete_pickup_order);

    if($run_delete_query){
        $_SESSION['success'] = "DONE";
        // $_SESSION['status_code'] = "success";
        header('Location: foodstore_pickuporders.php'); 
        
    }else{ 

        $_SESSION['status'] = "NOT DONE";       
        // $_SESSION['status_code'] = "error";
        header('Location: foodstore_pickuporders.php'); 
    
    }
}

//: Preparing Pickup Order..
if(isset($_POST['confirm_preparing_pending_order_btn']))
{
    $id = $_POST['confirm_preparing_pending_order_id'];

    $order_status = 'Preparing Order';

    $query = "UPDATE customer_pickuporder SET order_status = '$order_status' where porder_id = '$id' ";
    $run_query = mysqli_query($connection, $query);
  
    if($run_query){

        $_SESSION['success'] = "DONE";
        // $_SESSION['status_code'] = "success";
        header('Location: foodstore_pickuporders.php'); 

    }else {

        $_SESSION['status'] = "NOT DONE";       
        // $_SESSION['status_code'] = "error";
        header('Location: foodstore_pickuporders.php'); 

    }
}

//: Ready for pickup... food pickup
if(isset($_POST['confirm_readyforpickup_pending_order_btn']))
{
    $id = $_POST['confirm_readyforpickup_pending_order_id'];

    $order_status = 'Ready for Pickup';

    $query = "UPDATE customer_pickuporder SET order_status = '$order_status' where porder_id = '$id' ";
    $run_query = mysqli_query($connection, $query);
  
    if($run_query){

        $_SESSION['success'] = "DONE";
        // $_SESSION['status_code'] = "success";
         header('Location: foodstore_pickuporders.php'); 

    }else {

        $_SESSION['status'] = "NOT DONE";       
        // $_SESSION['status_code'] = "error";
        header('Location: foodstore_pickuporders.php'); 
    }
}

// : Preparing for Delivery (Pickup-Order)
if(isset($_POST['confirm_preparingdelivery_pending_order_btn']))
{
    $id = $_POST['confirm_preparingdelivery_pending_order_id'];

    $order_status = 'Preparing For Delivery';

    $query = "UPDATE customer_pickuporder SET order_status = '$order_status' where porder_id = '$id' ";
    $run_query = mysqli_query($connection, $query);
  
    if($run_query){

        $_SESSION['success'] = "DONE";
        // $_SESSION['status_code'] = "success";
         header('Location: foodstore_pickuporders.php'); 

    }else {

        $_SESSION['status'] = "NOT DONE";       
        // $_SESSION['status_code'] = "error";
         header('Location: foodstore_pickuporders.php'); 
    }
}



//: Completed Dine-In order button
if(isset($_POST['confirm_pending_orderDiningIn_btn']))
{
    $confirmDiningInid = $_POST['confirm_pending_orderDiningIn_id'];

    $query = "Select * from customer_dyingin_order where dorder_id = '$confirmDiningInid' ";
    $run_query = mysqli_query($connection, $query);
    $fetch_query = mysqli_fetch_array($run_query);

    $getcustomer_id = $fetch_query['dcustomer_id'];
    $getcustomer_order_no = $fetch_query['dcustomer_order_no'];
    $gethotel_id = $fetch_query['dhotel_id'];
    $getorder_date = $fetch_query['dorder_date'];
    $getinvoiceno = $fetch_query['dinvoiceno'];
    $getorder_desc = $fetch_query['dorder_description'];
    $getdue_amount = $fetch_query['ddue_amount'];
    $gettable_no = $fetch_query['dorder_tableno'];
    // $getpickup_order_date = $fetch_query['pickup_order_date'];
    // $getpickup_order_time = $fetch_query['pickup_order_time'];
    $getorder_status = 'Completed with Payment';
    $ordertype = 'Dine-In'; 
    $table_description = '<br>table/Room No:';

    $fordertype = $ordertype.$table_description.$gettable_no;


    $insert_completed_order = "Insert into completed_orders(customer_id,customer_order_no,hotel_id,order_date,invoice_no,order_description,due_amount,
    pickup_order_date,pickup_order_time,order_status,order_type)values('$getcustomer_id','$getcustomer_order_no','$gethotel_id',
    '$getorder_date','$getinvoiceno','$getorder_desc','$getdue_amount','0','0','$getorder_status','$fordertype')";

    $run_completed_order = mysqli_query($connection, $insert_completed_order);

    if($run_completed_order){

        $delete_dine_in_order = "DELETE FROM customer_dyingin_order where dorder_id='$confirmDiningInid'";
        $run_delete_query = mysqli_query($connection, $delete_dine_in_order);

        if($run_delete_query){
            $_SESSION['success'] = "DONE";
            // $_SESSION['status_code'] = "success";
            header('Location: foodstore_diningInorders.php'); 
            
        }else{ 

            $_SESSION['status'] = "NOT DONEA";       
            // $_SESSION['status_code'] = "error";
            header('Location: foodstore_diningInorders.php');
        
        }
        
    }else {
        $_SESSION['status'] = "NOT DONE";       
        // $_SESSION['status_code'] = "error";
        header('Location: foodstore_diningInorders.php');
        // echo "Error insertin data: " . mysqli_error($connection);
    }

    
}

//: Cancelled dinning-in order button
if(isset($_POST['cancel_pending_orderDiningIn_btn']))
{
    $cancelleddiningid = $_POST['cancel_pending_orderDiningIn_id'];

    $query = "Select * from customer_dyingin_order where dorder_id = '$cancelleddiningid' ";
    $run_query = mysqli_query($connection, $query);
    $fetch_query = mysqli_fetch_array($run_query);

    $getcustomer_id = $fetch_query['dcustomer_id'];
    $getcustomer_order_no = $fetch_query['dcustomer_order_no'];
    $gethotel_id = $fetch_query['dhotel_id'];
    $getorder_date = $fetch_query['dorder_date'];
    $getinvoiceno = $fetch_query['dinvoiceno'];
    $getorder_desc = $fetch_query['dorder_description'];
    $getdue_amount = $fetch_query['ddue_amount'];
    $gettable_no = $fetch_query['dorder_tableno'];
    
    $getorder_status = 'Cancelled';
    $ordertype = 'Dine-In'; 
    $table_description = '<br>table/Room No:';

    $fordertype = $ordertype.$table_description.$gettable_no;


    $insert_cancelled_order = "Insert into cancelled_orders(customer_id,customer_order_no,hotel_id,order_date,invoice_no,order_description,due_amount,
    pickup_order_date,pickup_order_time,order_status,order_type)values('$getcustomer_id','$getcustomer_order_no','$gethotel_id',
    '$getorder_date','$getinvoiceno','$getorder_desc','$getdue_amount','0','0','$getorder_status','$fordertype')";

    $run_completed_order = mysqli_query($connection, $insert_cancelled_order);

    $delete_pickup_order = "DELETE FROM customer_dyingin_order where dorder_id = '$cancelleddiningid'";
    $run_delete_query = mysqli_query($connection, $delete_pickup_order);

    if($run_delete_query){
        $_SESSION['success'] = "DONE";
        // $_SESSION['status_code'] = "success";
        header('Location: foodstore_diningInorders.php');

    }else{ 

        $_SESSION['status'] = "NOT DONE";       
        // $_SESSION['status_code'] = "error";
        header('Location: foodstore_diningInorders.php');
    
    }
}


//: Preparing Order dine in..
if(isset($_POST['confirm_preparing_orderDiningIn_btn']))
{
    $id = $_POST['confirm_preparing_orderDiningIn_id'];

    $order_status = 'Preparing Order';

    $query = "UPDATE customer_dyingin_order SET dorder_status = '$order_status' where dorder_id = '$id' ";
    $run_query = mysqli_query($connection, $query);
  
    if($run_query){

        $_SESSION['success'] = "DONE";
        // $_SESSION['status_code'] = "success";
        header('Location: foodstore_diningInorders.php');

    }else {

        $_SESSION['status'] = "NOT DONE";       
        // $_SESSION['status_code'] = "error";
        header('Location: foodstore_diningInorders.php');
    

    }
}

//: Ready for pickup Order dine in..
if(isset($_POST['confirm_readyforpickup_orderDiningIn_btn']))
{
    $id = $_POST['confirm_readyforpickup_orderDiningIn_id'];

    $order_status = 'Ready for Pickup';

    $query = "UPDATE customer_dyingin_order SET dorder_status = '$order_status' where dorder_id = '$id' ";
    $run_query = mysqli_query($connection, $query);
  
    if($run_query){

        $_SESSION['success'] = "DONE";
        // $_SESSION['status_code'] = "success";
        header('Location: foodstore_diningInorders.php');

    }else {

        $_SESSION['status'] = "NOT DONE";       
        // $_SESSION['status_code'] = "error";
        header('Location: foodstore_diningInorders.php');
    

    }
}

//: Preparing for Delivery Order dine in..
if(isset($_POST['confirm_preparingdelivery_orderDiningIn_btn']))
{
    $id = $_POST['confirm_preparingdelivery_orderDiningIn_id'];

    $order_status = 'Preparing For Delivery';

    $query = "UPDATE customer_dyingin_order SET dorder_status = '$order_status' where dorder_id = '$id' ";
    $run_query = mysqli_query($connection, $query);
  
    if($run_query){

        $_SESSION['success'] = "DONE";
        // $_SESSION['status_code'] = "success";
        header('Location: foodstore_diningInorders.php');

    }else {

        $_SESSION['status'] = "NOT DONE";       
        // $_SESSION['status_code'] = "error";
        header('Location: foodstore_diningInorders.php');
    

    }
}

//:: Add subcategory at food-item table (For foodstore)
if(isset($_POST['add_subcategoryforfoodstore_btn'])){

    $item_id = $_POST['add_subcategoryforfood_itemid'];
    $subcategory_id = $_POST['add_subcategoryforfood_id'];

    $add_subcategory = "UPDATE food_items SET fcat_sub_id = '$subcategory_id' where item_id = '$item_id' ";
    $run_add_subcategory = mysqli_query($connection, $add_subcategory);
 
    if($run_add_subcategory)
     {
         $_SESSION['success'] = "DONE";
         // $_SESSION['status_code'] = "success";
         header('Location:  foodstore_subcategory.php'); 
     }
     else
     {
         $_SESSION['status'] = "Data is not added";       
         // $_SESSION['status_code'] = "error";
         header('Location: foodstore_subcategory.php'); 
        // echo "Error insertin data: " . mysqli_error($connection);
     }    
 }

//:Profiles 

//: Owner Info
if(isset($_POST['modifystore_1'])){
    
    
    $update_influencer_email = $_POST['influencer_email'];
    $update_owner_name = $_POST['store_updatename'];


    $query = "UPDATE food_company SET owner_name='$update_owner_name'
    WHERE company_email='$update_influencer_email' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
          header('Location: foodstore_setting.php'); 
        }
      
    
    else
    {
       header('Location: foodstore_setting.php'); 
    }
   
}

if(isset($_POST['modifystore_2'])){
    
    
    $update_influencer_email = $_POST['influencer_email'];
    $update_owner_ic = $_POST['store_updateic'];


    $query = "UPDATE food_company SET owner_ic='$update_owner_ic'
    WHERE company_email='$update_influencer_email' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
          header('Location: foodstore_setting.php'); 
        }
      
    
    else
    {
       header('Location: foodstore_setting.php'); 
    }
   
}

if(isset($_POST['modifystore_3'])){
    
    
    $update_influencer_email = $_POST['influencer_email'];
    $update_contactnum1 = $_POST['store_updatecontact1'];


    $query = "UPDATE food_company SET   contact_number1='$update_contactnum1'
    WHERE company_email='$update_influencer_email' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
          header('Location: foodstore_setting.php'); 
        }
      
    
    else
    {
       header('Location: foodstore_setting.php'); 
    }
   
}

if(isset($_POST['modifystore_4'])){
    
    
    $update_influencer_email = $_POST['influencer_email'];
    $update_contactnum2 = $_POST['store_updatecontact2'];


    $query = "UPDATE food_company SET contact_number2='$update_contactnum2'
    WHERE company_email='$update_influencer_email' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
          header('Location: foodstore_setting.php'); 
        }
      
    
    else
    {
       header('Location: foodstore_setting.php'); 
    }
   
}

if(isset($_POST['modifystore_5'])){
    
    
    $update_influencer_email = $_POST['influencer_email'];
    $update_owner_img = $_FILES['store_updateownerimg']['name'];
    //$item_img = $_FILES['product_img']['name'];
    

    // $Add_Food_Item = false; 
    
    if($update_owner_img == '') //If the image is empty, insert default image for it
    {
        $update_owner_img = 'Default.png';
        $_SESSION['status'] = "Image Is Uploaded With Default. $store";
        
    }
$Add_Owner_Img = false;
	if($Add_Owner_Img){  //file_exists("fooditem_images/" .$_FILES["item_img"]["name"])
        $image_already_exist = $_FILES["store_updateownerimg"]["name"];
        $_SESSION['status'] = "Image Already exists. $store";
        header('Location: foodstore_setting.php'); 
    
    }else{


    $query = "UPDATE food_company SET owner_img='$update_owner_img'
    WHERE company_email='$update_influencer_email' ";  
    $query_run = mysqli_query($connection, $query);

 if($query_run)
            {
               move_uploaded_file($_FILES["store_updateownerimg"]["tmp_name"], "owner_images/".$_FILES["store_updateownerimg"]["name"] );
                header('Location: foodstore_setting.php'); 
            }
            else 
            {

                header('Location: foodstore_setting.php'); 
            }
    }
   
}


//: Business Info
if(isset($_POST['modifystore_6'])){
    
    
    $update_influencer_email = $_POST['influencer_email'];
    $update_shop_name = $_POST['store_updatestorename'];


    $query = "UPDATE food_company SET Company_name='$update_shop_name'
    WHERE company_email='$update_influencer_email' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
          header('Location: foodstore_setting.php'); 
        }
      
    
    else
    {
       header('Location: foodstore_setting.php'); 
    }
   
}

if(isset($_POST['modifystore_7'])){
    
    
    $update_influencer_email = $_POST['influencer_email'];
    $update_shop_address = $_POST['store_updateaddress'];


    $query = "UPDATE food_company SET company_address='$update_shop_address'
    WHERE company_email='$update_influencer_email' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
          header('Location: foodstore_setting.php'); 
        }
      
    
    else
    {
       header('Location: foodstore_setting.php'); 
    }
   
}

if(isset($_POST['modifystore_8'])){
    
    
    $update_influencer_email = $_POST['influencer_email'];
    $update_ssn = $_POST['store_updatessn'];


    $query = "UPDATE food_company SET ssn_number='$update_ssn'
    WHERE company_email='$update_influencer_email' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
          header('Location: foodstore_setting.php'); 
        }
      
    
    else
    {
       header('Location: foodstore_setting.php'); 
    }
   
}

if(isset($_POST['modifystore_9'])){
    
    
    $update_influencer_email = $_POST['influencer_email'];
    $update_insurance = $_POST['store_updateinsurance'];


    $query = "UPDATE food_company SET insurance='$update_insurance'
    WHERE company_email='$update_influencer_email' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
          header('Location: foodstore_setting.php'); 
        }
      
    
    else
    {
       header('Location: foodstore_setting.php'); 
    }
   
}

if(isset($_POST['modifystore_10'])){
    
    
    $update_influencer_email = $_POST['influencer_email'];
    $update_shop_contactnum = $_POST['store_updatestorenum'];


    $query = "UPDATE food_company SET bcontact_number='$update_shop_contactnum'
    WHERE company_email='$update_influencer_email' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
          header('Location: foodstore_setting.php'); 
        }
      
    
    else
    {
       header('Location: foodstore_setting.php'); 
    }
   
}

//: Bank Info
if(isset($_POST['modifystore_11'])){
    
    
    $update_influencer_email = $_POST['influencer_email'];
    $update_bank_name = $_POST['store_updatebankname'];


    $query = "UPDATE food_company SET bank_name='$update_bank_name'
    WHERE company_email='$update_influencer_email' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
          header('Location: foodstore_setting.php'); 
        }
      
    
    else
    {
       header('Location: foodstore_setting.php'); 
    }
   
}

if(isset($_POST['modifystore_12'])){
    
    
    $update_influencer_email = $_POST['influencer_email'];
    $update_acc_name = $_POST['store_updateaccname'];


    $query = "UPDATE food_company SET acc_name='$update_acc_name'
    WHERE company_email='$update_influencer_email' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
          header('Location: foodstore_setting.php'); 
        }
      
    
    else
    {
       header('Location: foodstore_setting.php'); 
    }
   
}

if(isset($_POST['modifystore_13'])){
    
    
    $update_influencer_email = $_POST['influencer_email'];
    $update_acc_num = $_POST['store_updateaccnum'];


    $query = "UPDATE food_company SET acc_num='$update_acc_num'
    WHERE company_email='$update_influencer_email' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
          header('Location: foodstore_setting.php'); 
        }
      
    
    else
    {
       header('Location: foodstore_setting.php'); 
    }
   
}


//: Add More images for Product

if(isset($_POST['addmoreimagesforfoodstore'])){
   
    $item_id = $_POST['item_id'];
    $item_company_id = $_POST['item_company_id'];
    // //: Image Name
    $item_img = $_FILES['item_moreimg']['name'];

    $query = "Select * from item_images WHERE it_item_id = '$item_id' ORDER BY it_id DESC LIMIT 1";
    $run_query = mysqli_query($connection, $query);
    $fetch_query = mysqli_fetch_assoc($run_query);

    $fetch_id = $fetch_query['it_img_no'];

    $item_img_no = ($fetch_id + 1);


        $insert_moreimages ="insert into item_images (it_item_id, it_company_id,it_item_img) values 
         ('$item_id','$item_company_id','$item_img')";

            if(mysqli_query($connection, $insert_moreimages)){
            move_uploaded_file($_FILES["item_moreimg"]["tmp_name"], "fooditem_images/".$_FILES["item_moreimg"]["name"] );
            $_SESSION['success'] = "Item-Image is Added";
            header('Location: foodstore_fooditems.php'); 
        }else{ 

            $_SESSION['status'] = "Item-Image is not Added";       
            header('Location: foodstore_fooditems.php');
            //  echo "Error insertin data: " . mysqli_error($connection); 
        }


    
  
}

//: DELETE MOREIMAGES for Product

if(isset($_POST['delete_foodstore_images_btn']))
{
    $delete_moreimage_id = $_POST['delete_foodstore_images_id'];

    $query = "DELETE FROM item_images WHERE it_id='$delete_moreimage_id' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        $_SESSION['success'] = "Image is deleted";
        // $_SESSION['status_code'] = "success";
        header('Location: foodstore_fooditems.php'); 
    }
    else
    {
        $_SESSION['status'] = "Image is not deleted";       
        // $_SESSION['status_code'] = "error";
        header('Location: foodstore_fooditems.php'); 
    }    
}

?>


