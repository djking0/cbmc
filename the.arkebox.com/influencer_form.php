<?php
session_start();
include("includes/db.php");
include("functions/functions.php");
include "mail_function.php";

?>

<?php

date_default_timezone_set('Asia/Kolkata');

$success = "";
$error_message = "";

if(isset($_POST['register_influencer'])){

    //: Personal Details: 
    $influencer_fname = $_POST['influencer_fname'];
    $influencer_lname = $_POST['influencer_lname'];
    $influencer_name = "$influencer_fname $influencer_lname";

    $influencer_email = $_POST['influencer_email'];
    $influencer_contact = $_POST['influencer_number'];
    $influencer_gender = $_POST['influencer_gender'];

    // echo "$influencer_fname", "$influencer_lname", "$influencer_name", "$influencer_email", "$influencer_contact", "$influencer_gender";

    //:: Image
    $influencer_img = $_FILES['influencer_img']['name'];


    //: Influencer Page: 
    $influencer_pagetype = $_POST['influencer_pagetype'];
    $influencer_pagename = $_POST['influencer_pagename'];
    $influencer_pagelink = $_POST['influencer_pagelink'];
    $influencer_followers = $_POST['influencer_followers'];

    // echo "$influencer_pagetype", "$influencer_pagename", "$influencer_pagelink", "$influencer_followers";
    
    
   //: Influencer Address: 
   $influencer_address = $_POST['influencer_address'];
   $influencer_city = $_POST['influencer_city'];
   $influencer_country = $_POST['influencer_country'];
   $influencer_fulladdress = "$influencer_address - $influencer_city - $influencer_country";

   // echo "$influencer_fulladdress";

   //: Influencer Password: 
   $influencer_pass = $_POST['influencer_pass'];
   $influencer_confirmpass = $_POST['influencer_confirmpass'];
    // echo "$influencer_pass, $influencer_confirmpass";

    $checkemail_query = "SELECT * FROM influencer WHERE i_email = '$influencer_email'";
    $checkemail_query_run = mysqli_query($con, $checkemail_query);
    if(mysqli_num_rows($checkemail_query_run) > 0)
    {
        echo "<script>alert('This account is already exist.. Try with different email address!')</script>";
        echo "<script>window.open('influencer_form.php','_self')</script>";
    }else{

        if($influencer_pass === $influencer_confirmpass){

            
                if(file_exists("admin_area/influencer_images/" .$_FILES["influencer_img"]["name"])){

                    $image_already_exist = $_FILES["influencer_img"]["name"];
                    echo "<script>alert('This image is alraedy added in our database, Please try another one.')</script>";
                    echo "<script>window.open('influencer_form.php','_self')</script>";


                }else{

           
                    $otp =  rand(100000, 999999);
            
                    //To save Email Address at DB 
                    // $insert = mysqli_query($con, "INSERT INTO influencer (i_name, i_email, i_pass, i_contact, i_gender, i_pagetype, i_pagename, i_pagelink,i_followers,i_address,i_accountstatus )
                    // VALUES ('$influencer_name','$influencer_email','$influencer_pass','$influencer_contact','$influencer_gender','$influencer_pagetype','$influencer_pagename','$influencer_pagelink','$influencer_followers','$influencer_fulladdress', 'notverified') ");

                    $insert_product ="insert into influencer (i_name, i_email, i_pass, i_contact, i_gender, i_pagetype, i_pagename, i_pagelink,i_followers,i_address,i_image,i_accountstatus ) values 
                    ('$influencer_name','$influencer_email','$influencer_pass','$influencer_contact','$influencer_gender','$influencer_pagetype','$influencer_pagename','$influencer_pagelink','$influencer_followers','$influencer_fulladdress','$influencer_img', 'notverified')";
                    
                    
                    if(mysqli_query($con, $insert_product)){
                        move_uploaded_file($_FILES["influencer_img"]["tmp_name"], "admin_area/influencer_images/".$_FILES["influencer_img"]["name"] );
                        // $_SESSION['success'] = "Item is Added";
                        // header('Location: food_item.php'); 
                    }else{ 
                        echo "wrong";
                        // $_SESSION['status'] = "Item is not Added";       
                        // header('Location: food_item.php'); 
                    }

                    //To save OTP,expired, create date at DB.
                    $result = mysqli_query($con, "INSERT INTO otp_expiry(otp_email,otp, is_expired, created_at) VALUES ('$influencer_email','" . $otp . "', 0, '" . date("Y-m-d H:i:s") . "')");
                    
                    //send OTP
                    if($result){
                        
                    sendOTP($influencer_email, $otp);
                
                    
                    }else{
                        echo $mysqli->error;
                    }
                    $current_id = mysqli_insert_id($con);
            
                        if(!empty($current_id)){
                            $success = 1;
                        } else {
                        $error_message = "Email Does not Exists!!";
                    }
                }

           
        } else{
            echo "<script>alert('Your password and confirm password is not match.')</script>";
            echo "<script>window.open('influencer_form.php','_self')</script>";
        }
    }
}

if(!empty($_POST["submit_otp"])){
    //Checking of OTP is expired
    $result = mysqli_query($con, "SELECT * FROM otp_expiry WHERE otp = '" . $_POST["otp"] ."' AND is_expired != 1 AND NOW() <= DATE_ADD(created_at, INTERVAL 24 HOUR)");
    $count = mysqli_num_rows($result);

        if(!empty($count)){
            
            $result = mysqli_query($con, "UPDATE otp_expiry SET is_expired = 1 WHERE otp
                 = '" . $_POST["otp"] ."'");
                 
            $get_otpdata = "Select * from otp_expiry WHERE otp = '" . $_POST["otp"]."'  ";
            $run_otpdata = mysqli_query($con, $get_otpdata);
            $fetch_otpdata = mysqli_fetch_array($run_otpdata);
            
            $get_email = $fetch_otpdata['otp_email'];
                 
            $udate_influencer = mysqli_query($con, "UPDATE influencer SET i_accountstatus  = 'verified' WHERE i_email
                 = '$get_email' ");
            $success = 2;
        } else {
            $success = 1;
            $error_message = "Invalid OTP!";
        }
}




?>

<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Basic page needs
    ============================================ -->
    <title>Gogo Empire</title>
    <meta charset="utf-8">
    <meta name="keywords"
        content="Gogo Empire Sdn Bhd" />
    <meta name="description"
        content="Gogo Empire Sdn Bhd" />
    <meta name="author" content="Gogo Empire">
    <meta name="robots" content="index, follow" />

    <!-- Mobile specific metas
    ============================================ -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Favicon
    ============================================ -->

    <link rel="shortcut icon" type="image/png" href="ico/favicon-16x16.png" />


    <!-- Libs CSS
    ============================================ -->
    <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="js/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="js/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="css/themecss/lib.css" rel="stylesheet">
    <link href="js/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <link href="js/minicolors/miniColors.css" rel="stylesheet">

    <!-- Theme CSS
    ============================================ -->
    <link href="css/themecss/so_searchpro.css" rel="stylesheet">
    <link href="css/themecss/so_megamenu.css" rel="stylesheet">
    <link href="css/themecss/so-categories.css" rel="stylesheet">
    <link href="css/themecss/so-listing-tabs.css" rel="stylesheet">
    <link href="css/themecss/so-newletter-popup.css" rel="stylesheet">

    <link href="css/footer/footer1.css" rel="stylesheet">
    <link href="css/header/header1.css" rel="stylesheet">
    <link id="color_scheme" href="css/theme.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!-- Google web fonts
    ============================================ -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700' rel='stylesheet'
        type='text/css'>
    <style type="text/css">
        body {
            font-family: 'Roboto', sans-serif
        }
    

        
    </style>

<style>
	body{
		font-family: calibri;
	}
	.tblLogin{
		border: #95bee6 1px solid;
		background: #d1e8ff;
		border-radius: 4px;
		max-width: 300px;
		padding: 20px 30px 30px;
		text-align: center;

	}
	.tableheader{font-size: 20px;}
	.tablerow{padding:20px;}
	.error_message{
		color: #b12d2d;
		background: #ffb5b5;
		border: #c76969 1px solid;

	}
	.message{
		width: 100%;
		max-width: 300px;
		padding: 10px 30px;
		border-radius: 4px;
		margin-bottom: 5px;


	}
	 </style>

</head>

<body class="res layout-1 layout-subpage">

    <div id="wrapper" class="wrapper-fluid banners-effect-5">


        <!-- Header Container  -->
        <header id="header" class=" typeheader-1">
            <!-- Header Top -->
            <div class="header-top hidden-compact">
                <div class="container">
                    <div class="row">
                        <div class="header-top-left col-lg-7 col-md-8 col-sm-6 col-xs-4">
                            
                                
                            <ul class="top-link list-inline hidden-lg hidden-md">
                                <li class="account" id="my_account">
                                    
                                    
                                </li>
                            </ul>
                        </div>
                        <div class="header-top-right collapsed-block col-lg-5 col-md-4 col-sm-6 col-xs-8">
                            <ul class="top-link list-inline lang-curr">
                                <li class="language">
                                    <div class="btn-group languages-block ">
                                        <form action="http://demo.smartaddons.com/templates/html/emarket/index.html"
                                            method="post" enctype="multipart/form-data" id="bt-language">
                                            <a class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                                <span class="">English</span>
                                                <span class="fa fa-angle-down"></span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li><a href="index.html"> English </a></li>
                                                <li> <a href="index.html">  Bhasa Malayu </a> </li>
                                            </ul>
                                        </form>
                                    </div>

                                </li>
                            </ul>



                        </div>
                    </div>
                </div>
            </div>
            <!-- //Header Top -->

            <!-- Header center -->
            <div class="header-middle">
                <div class="container">
                    <div class="row">
                        <!-- Logo -->
                        <div class="navbar-logo col-lg-2 col-md-2 col-sm-12 col-xs-12">
                            <div class="logo"><a href="index.html"><img src="image/catalog/ark_logo.png" height="90" width="90" title="Your Store"></a></div>
                        </div>
                        <!-- //end Logo -->

                        <!-- Main menu -->
                        <div class="main-menu col-lg-6 col-md-7 ">
                            <div class="responsive so-megamenu megamenu-style-dev">
                                <nav class="navbar-default">
                                    <div class=" container-megamenu  horizontal open ">
                                        <div class="navbar-header">
                                            <button type="button" id="show-megamenu" data-toggle="collapse"
                                                class="navbar-toggle">
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                        </div>

                                        <div class="megamenu-wrapper">
                                            <span id="remove-megamenu" class="fa fa-times"></span>
                                            <div class="megamenu-pattern">
                                                <div class="container-mega">
                                                <ul class="megamenu" 
                                                        data-animationtime="250">
                                                        <li class="home hover">
                                                            <a href="index.php">HOME </a>
                                                        </li>
                                                        <li class="home hover">
                                                        <a href="hotels.php">STORES </a>
                                                        </li>
                                                        <li class="home hover">
                                                            <a href="displaycategory.php">CATEGORIES </a>
                                                        </li>
                                                        <li class="home hover">
                                                            <a href="aboutus.php">ABOUT US </a>
                                                        </li>
                                                        <li class="home hover">
                                                            <a href="contact.php">CONTACT US </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </nav>
                            </div>
                        </div>
                        <!-- //end Main menu -->

                        <div class="middle-right col-lg-4 col-md-3 col-sm-6 col-xs-8">
                            <div class="signin-w hidden-sm hidden-xs">
                                <ul class="signin-link blank">
                                
                                </ul>
                            </div>
                            <div class="signin-w hidden-sm hidden-xs">
                                <ul class="signin-link blank">
                               
                                </ul>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
            <!-- //Header center -->

            <!-- Header Bottom -->
            <div class="header-bottom hidden-compact">
                <div class="container">
                    <div class="row">

                    </div>
                </div>

            </div>
        </header>
        <!-- //Header Container  -->


        <!-- Main Container  -->
        <div class="main-container container">
            <ul class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-home"></i></a></li>
                <li><a href="">INFLUENCER</a></li>

            </ul>

            <div class="row">
                <div id="content" class="col-sm-12">
                    <h2 class="title">Influencer Form:</h2>
                    <p>Fill the all below fields</p>
                    
                        <?php
                        	if(!empty($error_message)){
                        		?>
                        		<div class="message error_message"><?php echo $error_message; ?> </div>
                        	<?php
                        	} 
                        	?>
                    
                        <form action="influencer_form.php" method="POST" enctype="multipart/form-data"
                        class="form-horizontal account-register clearfix">

                        <?php 
					if(!empty($success == 1)){
                        ?>
                        
                        <div class= "tableheader">Enter OTP</div>
                        <p style="color:#31ab00;">Check your email for the OTP</p>
                        <div class="tablerow">
                            <input type="text" name="otp" placeholder="one time password" class="form-control" required="">
                        </div>
                        <br>
                        <div class="">
                            <input type="submit" name="submit_otp" value="submit" class="btn btn-primary mb-5">
                        </div>

                    	<?php 
				    } else if ($success == 2){
                        ?>
                        
                        <p style="color:#31ab00;">Thankyou for your intrest! <br> Your request is under process. We will send you a confirmation email at your given email address</p>			
			<?php 
			}else {
                ?>

                        <fieldset id="account">
                            <legend>Your Personal Details</legend>
                            <div class="form-group required" style="display: none;">
                                <label class="col-sm-2 control-label">Customer Group</label>
                                <div class="col-sm-10">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="customer_group_id" value="1" checked="checked">
                                            Default
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label">First Name</label>
                                <div class="col-sm-10">
                                    <input type="text" name="influencer_fname" value="" placeholder="First Name"
                                         class="form-control" required="">
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-lastname">Last Name</label>
                                <div class="col-sm-10">
                                    <input type="text" name="influencer_lname" value="" placeholder="Last Name"
                                         class="form-control" required="">
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-email">E-Mail</label>
                                <div class="col-sm-10">
                                    <input type="email" name="influencer_email" value="" placeholder="E-Mail" 
                                        class="form-control" required="">
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label">Telephone</label>
                                <div class="col-sm-10">
                                    <input type="tel" name="influencer_number" value="" placeholder="Telephone"
                                         class="form-control" required="">
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" >Gender</label>
                                <div class=" col-md-10">
                                    <select name="influencer_gender" class="form-control" required="required" >
                                                <option>Male</option>
                                                <option>FEMALE</option>
                                            </select>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-telephone">Your Picture</label>
                                <div class="col-sm-10">
                                    <input type="file" class="form-control" placeholder="" name="influencer_img" required> 
                                </div>
                            </div>
                        </fieldset>
                        <fieldset id="address">
                            <legend>Your Page</legend>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-country">Your Page Type</label>
                                <div class="col-sm-10">
                                    <select name="influencer_pagetype" class="form-control" required="">
                                    <option selected="">Youtube</option>
                                    <option>Tiktok</option>
                                    <option>Facebook</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-city">Your Page Name</label>
                                <div class="col-sm-10">
                                    <input type="text" name="influencer_pagename" value="" placeholder="Example: BM TECH" 
                                        class="form-control" required="">
                                </div>
                            </div>
                            
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-city">Your Page Link</label>
                                <div class="col-sm-10">
                                    <input type="text" id="origin-input" name="influencer_pagelink" value="" placeholder="Example: https://www.youtube.com/channel/UCkNADfI4jFhIRqB6S3" id="input-city"
                                        class="form-control" required="">
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-city">Your Page Followers</label>
                                <div class="col-sm-10">
                                    <input type="text" name="influencer_followers" value="" placeholder="Example: 1 Million in Youtube" id="input-city"
                                        class="form-control" required="">
                                </div>
                            </div>
                        </fieldset>
                        <fieldset id="address">
                            <legend>Your Address</legend>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-address-1">Address</label>
                                <div class="col-sm-10">
                                    <input type="text" name="influencer_address" value="" placeholder="Example: SO-7-1, Menara 1, No 3, Jalan Bangsar, KL Eco City"
                                        id="input-address-1" class="form-control" required="">
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-city">City</label>
                                <div class="col-sm-10">
                                    <input type="text" name="influencer_city" value="" placeholder="Example: Kuala Lumpur " id="input-city"
                                        class="form-control" required="">
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-country">Country</label>
                                <div class="col-sm-10">
                                    <select name="influencer_country" class="form-control" required="">
                                    <option> Choose...</option>
					                <option>Singapore</option>
					                <option selected="">Malaysia</option>
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Your Password</legend>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-password">Password</label>
                                <div class="col-sm-10">
                                    <input type="password" name="influencer_pass" value="" placeholder="Password"
                                        id="input-password" class="form-control" required="">
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-confirm">Password Confirm</label>
                                <div class="col-sm-10">
                                    <input type="password" name="influencer_confirmpass" value="" placeholder="Password Confirm"
                                        id="input-confirm" class="form-control" required="">
                                </div>
                            </div>
                        </fieldset>
                        <div class="buttons">
                            
                            <div class="pull-right">
                                <button type="submit" name="register_influencer" class="btn btn-primary">Register</button>
                            </div>
                        </div>
                <?php 
				}	
			?>

                    </form>
                </div>
            </div>
        </div>
        <!-- //Main Container -->


    <!-- Footer Container -->
      <footer class="footer-container typefooter-1">

        <hr>

        <div class="footer-middle ">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-style">
                        <div class="infos-footer">
                            <h3 class="modtitle">Contact Us</h3>
                            <ul class="menu">
                                <li class="adres">
                                     Unit 25-2-15, Level 2, Plaza Prima, Bt 4 ½ , Jalan Klang Lama, 58200 Kuala Lumpur 
                                </li>
                                <li class="phone">
                                    03- 8210 8677 / 013-2199 373
                                </li>
                                <li class="mail">
                                    <a href="">cbmc.malaysia60@gmail.com</a>
                                </li>
                                <li class="time">
                                    Open time: 7:00AM - 5:30PM
                                </li>
                            </ul>
                        </div>


                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                        <div class="box-information box-footer">
                            <div class="module clearfix">
                                <h3 class="modtitle">Information</h3>
                                <div class="modcontent">
                                    <ul class="menu">
                                        <li><a href="aboutus.php">ABOUT US </a></li>
                                        <li><a href="#">FAQ</a></li>
                                        <li><a href="#">Warranty And Services</a></li>
                                        <li><a href="#">Support 24/7 page</a></li>
                                        <li><a href="#">Product Registration</a></li>
                                        <li><a href="#">Product Support</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                        <div class="box-account box-footer">
                            <div class="module clearfix">
                                <h3 class="modtitle">Extras</h3>
                                <div class="modcontent">
                                    <ul class="menu">
                                        <li> <a href="aboutus.php"> Gogo Empire </a></li>
                                        <li><a href="displaycategory.php">CATEGORIES</a></li>
                                        <li><a href="contact.php">Contact Us</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                        <div class="box-service box-footer">
                            <div class="module clearfix">
                                <h3 class="modtitle">Services</h3>
                                <div class="modcontent">
                                    <ul class="menu">
                                        <li><a href="#">Kuala Lumpur</a></li>
                                        <li><a href="#">Selangor</a></li>
                                        <li><a href="#">KL Central</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-style">


                    </div>

                    <div class="col-lg-12 col-xs-12 text-center">
                        <img src="image/catalog/demo/payment/payment.png" alt="imgpayment">
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Bottom Container -->
        <div class="footer-bottom ">
            <div class="container">
                <div class="copyright">
                    CBMC © 2020. All Rights Reserved. 
                </div>
            </div>
        </div>
        <!-- /Footer Bottom Container -->


        <!--Back To Top-->
        <div class="back-to-top"><i class="fa fa-angle-up"></i></div>
        </footer>
    <!-- //end Footer Container -->

    </div>

    <!-- Include Libs & Plugins
	============================================ -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="js/jquery-2.2.4.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/owl-carousel/owl.carousel.js"></script>
    <script type="text/javascript" src="js/themejs/libs.js"></script>
    <script type="text/javascript" src="js/unveil/jquery.unveil.js"></script>
    <script type="text/javascript" src="js/countdown/jquery.countdown.min.js"></script>
    <script type="text/javascript" src="js/dcjqaccordion/jquery.dcjqaccordion.2.8.min.js"></script>
    <script type="text/javascript" src="js/datetimepicker/moment.js"></script>
    <script type="text/javascript" src="js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>


    <!-- Theme files
	============================================ -->


    <script type="text/javascript" src="js/themejs/so_megamenu.js"></script>
    <script type="text/javascript" src="js/themejs/addtocart.js"></script>
    <script type="text/javascript" src="js/themejs/application.js"></script>

    



</body>


</html>

