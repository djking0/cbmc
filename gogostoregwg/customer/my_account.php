<?php
include("includes/db.php");
include("functions/functions.php");
session_start();

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Gogo Empire Store</title>

    <!-- Bootstrap CDN -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
      crossorigin="anonymous"
    />

    <!--  Font Awesome CDN -->
    <script src="https://kit.fontawesome.com/23412c6a8d.js"></script>

    <!-- Slick Slider -->
    <link
      rel="stylesheet"
      type="text/css"
      href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"
    />

    
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="./css/style.css" />
    <link rel="stylesheet" href="./css/viewproduct.css" />
    <link type="text/css" rel="stylesheet" href="./css/main.css" />


  </head>

  <body>
    <!-- header -->

    <header>
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-12 col-12">
            <div class="btn-group">
              <button
                class="btn border dropdown-toggle my-md-4 my-2 text-white"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                ENG
              </button>
              <div class="dropdown-menu">
                <a href="#" class="dropdown-item">Bhasa - Malaysia</a>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-12 text-center">
            <h2 class="my-md-3 site-title text-white">Gogo Empire </h2>
          </div>
          <div class="col-md-4 col-12 text-right">
          <p class="my-md-4 header-links">
              <?php
                if(!isset($_SESSION['customer_email'])){
                echo"";
                }else{
                  
                  echo"<a href='' class='px-1'> $_SESSION[customer_email] </a>";
                  echo"<a class='px-2' style='color:white;'>|</a>";
                }
              ?>
              <?php
                if(!isset($_SESSION['customer_email'])){
                echo"<a href='checkout.php' class='px-2'>login</a>";
                echo"<a class='px-2' style='color:white;'>|</a>";
                echo"<a href='register.php' class='px-2'>Create an Account</a>";
                }else{
                  echo"<a href='logout.php' class='px-2'>logout</a>";
                }
              ?>
            </p>
          </div>
        </div>
      </div>

      <div class="container-fluid p-0">
        <nav class="navbar navbar-expand-lg navbar-light bg-white">
          <button
            class="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item active">
                <a href="index.php" class="nav-link" >HOME <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a href="index.php" class="nav-link" > FEATURES </a>
              </li>
              <li class="nav-item">
                <a href="index.php" class="nav-link" > COLLECTION</a>
              </li>
              <li class="nav-item">
                <a href="index.php" class="nav-link" > SHOP</a>
              </li>
              <li class="nav-item">
                <a href="index.php" class="nav-link" > ABOUT US</a>
              </li>
            </ul>
          </div>
          <div class="navbar-nav">
            <form class="form-inline my-2 my-lg-0" method="get" action="results.php" enctype="multipart/form-data">
                  <input class="form-control mr-sm-2" type="search" placeholder="Search a Product" aria-label="Search" name="user_query"/>
                  <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>
                  </form>
          </div>
        </nav>
      </div>

    </header>

    <!-- /header -->
    
    <!-- Main Section   -->

    <div class="content-area">

<div class="account-page">
    <div class="container">

      <div class="row">
        <div class="col-sm-3">
            <h2>My Account</h2>
            <ul>
              <?php
              if(isset($_SESSION['customer_email'])){

                $customer_session = $_SESSION['customer_email'];

                $get_customer_name = "select * from customers where customer_email = '$customer_session'";

                $run_customer = mysqli_query($con, $get_customer_name);

                $row_customer = mysqli_fetch_array($run_customer);

                $customer_name = $row_customer['customer_name'];
                $customer_email = $row_customer['customer_email'];
                $customer_contact = $row_customer['customer_contact'];
                $customer_address = $row_customer['customer_address'];
              }
              ?>
                <!-- <li  class="active"><a href="my_account.php">Account Control Panel</a></li> -->
                <li><a href="my_account.php">Account Control Panel</a></li>
                <li><a href="my_account.php?personal_information">Modify Your Personal Data</a></li>
                <li><a href="my_account.php?my_orders">My All Orders</a></li>
                <li><a href="my_account.php?account_wishlist">My Wishlist</a></li>
                <li><a href="my_account.php?account_reviews">My Reviews and Ratings</a></li>
                <li><a href="my_account.php?delete_account">Delete Account</a></li>
            </ul>
        </div>

        <?php 

              if(isset($_GET['my_orders'])){
                include("my_orders.php");
              }elseif(isset($_GET['personal_information'])){
                include("my_personalinfo.php");
              }elseif(isset($_GET['account_wishlist'])){
                include("my_accountwishlist.php");
              }elseif(isset($_GET['account_reviews'])){
                include("my_accountreviews.php");
              }elseif(isset($_GET['delete_account'])){
                include("my_deleteaccount.php");
              }else{
        ?>
        <div class="col-sm-9">
            <h2>Account Control Panel</h2>
            <strong>Hello <?php echo "$customer_name"?></strong><br />
            <p>From your account control panel, you can access all of your recent activites, orders, save products and you can edit your personal information and other details.</p>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="well">
                        <h3>Contact Information</h3>
                        <p>Name : <?php echo "$customer_name"?></p>
                        <p>Email : <?php echo "$customer_email"?></p>
                        <p><a href="account_change_email.html">Change Email</a> | <a href="account_change_password.html">Change Password</a></p>
                        <p class="pull-right"><a href="#"><i class="fa fa-edit"></i> Edit</a></p>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6">
                    <div class="well">
                        <h3>News Letters</h3>
                        <p>Do you want to get the latest product news and promotion offers then make it on otherwise off it.</p>
                        <p class="pull-right"><a href="#"><i class="fa fa-edit"></i> Edit</a></p>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6">
                    <div class="well">
                        <h3>Default Billing Address</h3>
                        <address class="address">
                            <strong>Name:</strong> <?php echo "$customer_name" ?> <br />
                            <strong>Email:</strong> <?php echo "$customer_email" ?> <br />
                            <strong>Contact No:</strong> <?php echo "$customer_contact" ?><br />
                        </address>
                        <p class="pull-right"><a href="#"><i class="fa fa-edit"></i> Edit</a></p>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6">
                    <div class="well">
                    <h3>Default Delivery Address Address</h3>
                        <address class="address">
                            <strong>Name:</strong> <?php echo "$customer_name" ?> <br />
                            <strong>Address:</strong>  <br />
                            <?php echo "$customer_address" ?><br />
                           
                        </address>
                        <p class="pull-right"><a href="#"><i class="fa fa-edit"></i> Edit</a></p>
                        <div class="clearfix"></div>
                    </div>
                </div>


            </div>
        </div>

        <?php
              } //:: End of isset myorder
        ?>
        
    </div> <!--End Row-->

    

</div>
</div> <!--End Account page div-->

</div> <!-- End content Area class -->

    <!-- /Main Section   -->


    <canvas class="my-4 w-100" id="myChart" width="900" height="180"></canvas>
   
    <hr> </hr>
    <!-- Footer -->

    <footer>
        <div class="container-fluid px-5">
          <div class="row">
            <div class="col-md-4 col-sm-12">
              <h4>Contact Us</h4>
              <div class="row pl-md-1 text-secondary">
                <div class="col-12">
                  <i class="fa fa-home px-md-2"></i>
                  <small>S0-7-01, Menara 1, No 3, Jalan Bangsar, KL Eco City</small>
                </div>
              </div>
              <div class="row pl-md-1 text-secondary py-4">
                  <div class="col-12">
                      <i class="fa fa-paper-plane px-md-2"></i>
                      <small>www.gogoempire.com</small>
                    </div>
              </div>
               <div class="row pl-md-1 text-secondary">
                  <div class="col-12">
                      <i class="fa fa-phone-volume px-md-2"></i>
                      <small>(+60) 123817908</small>
                    </div>
               </div>

              <div class="row social text-secondary">
                <div class="col-12 py-3">
                  <i class="fab fa-twitter"></i>
                  <i class="fab fa-facebook-f"></i>
                  <i class="fab fa-google-plus-g"></i>
                  <i class="fab fa-skype"></i>
                  <i class="fab fa-pinterest-p"></i>
                  <i class="fab fa-youtube"></i>
                  <i class="fab fa-linkedin-in"></i>
                </div>
              </div>
            </div>
            <div class="col-md-2 col-sm-12">
              <h4>Our Services</h4>
              <div class="d-flex flex-column pl-md-3">
                <small class="pt-0">Kuala Lumpur</small>
                <small>Selangor</small>
                <small>KL Centeral</small>
              </div>
            </div>
            <div class="col-md-2 col-sm-12">
              <h4>Extras</h4>
              <div class="d-flex flex-column pl-md-3">
                  <small class="pt-0">About GogoEmpire</small>
                  <small>Collection</small>
                  <small>Contact Us</small>
                </div>
            </div>
            <div class="col-md-4 follow-us col-sm-12">
              <h4>Follow Instagram</h4>
              <div class="d-flex flex-row">
                <img src="./assets/256_n.jpg" alt="Instagram 1" class="img-fluid">
                <img src="./assets/792_n.jpg" alt="Instagram 2" class="img-fluid">
                <img src="./assets/392_n.jpg" alt="Instagram 3" class="img-fluid">
              </div>
              <div class="d-flex flex-row">
                  <img src="./assets/664_n.jpg" alt="Instagram 1" class="img-fluid">
                  <img src="./assets/088_n.jpg" alt="Instagram 2" class="img-fluid">
                  <img src="./assets/896_n.jpg" alt="Instagram 3" class="img-fluid">
                </div>
            </div>
          </div>
        </div>

        <div class="container-fluid news pt-5">
          <div class="row">
            <div class="col-md-6 col-12 pl-5">
              <h4 class="primary-color font-roboto m-0 p-0">
                Need Help? Call Our Award-Warning
              </h4>
              <p class="m-0 p-0 text-secondary">
                Support Team 24/7 At (+60) 176844365
              </p>
            </div>
            <div class="col-md-4 col-12 my-md-0 my-3 pl-md-0 pl-5">
              <input type="text" class="form-control border-0 bg-light" placeholder="Enter Your email Address">
            </div>
            <div class="col-md-2 col-12 my-md-0 my-3 pl-md-0 pl-5">
              <button class="btn bg-primary-color text-white">Subscribe</button>
            </div>
          </div>
        </div>

        <div class="container text-center">
          <p class="pt-5">
            <img src="./assets/payment.png" alt="payment image" class="img-fluid">
          </p>
          <small class="text-secondary py-4">GogoEmpire © 2020 All Rights Reserved.</small>
        </div>

    </footer>

    <!-- /Footer -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>
    <script
      src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
      integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
      integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
      integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
      crossorigin="anonymous"
    ></script>
    <script
      type="text/javascript"
      src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"
    ></script>
    <script src="./js/main.js"></script>

   

    <script>
    $(document).ready(function () {
        $('.updateitembtn').on('click',function(){
            $('#updateitemmodal').modal('show');


            var data = $.parseJSON($(this).attr('data-button'));

            console.log(data);

            // console.log('Modal Opened');

            // var button = $(event.relatedTarget);
            // var title = button.data('mytitle');
            // var titlea = '1';


            // var modal = $(this);
  
            // modal.find('.modal-body #product_title').value(title );



              //  $tr = $(this).closet('tr');

              // var data = tr.children("td").map(function(){
              //     return $this.text();
              // }).get();

              // console.log(data);

            // $(#updateproduct_id).value(data[0]);
            // $(#product_title).value(data[1]);
            // $(#fixquantity).value(data[2]);
            // $(#productsingle_price).value(data[3]);

        });
    });
    </script>

<!-- <script>
    $('#edit').on('show.bs.modal',function(event){

      console.log('Modal Opened');

      var button = $(event.relatedTarget);
      var recipient = button.data('whatever');

      var modal = $(this);
      modal.find('.modal-title').text('New message to' + recipient);
      modal.find('.modal-body input').value(recipient);


    })
    </script> -->


<script>
$('#my_modal').on('show.bs.modal', function(e) {
    var bookId = $(e.relatedTarget).data('book-id');
    $(e.currentTarget).find('input[value="bookId"]').val(bookId);
});
</script>



    
</body>
  
</html>


<?php

if(isset($_GET['removeitem'])){

    $remove_id = $_GET['removeitem'];

    $delete_product = "delete from cart where p_id='$remove_id'";

    $run_delete = mysqli_query($con, $delete_product);

    if($run_delete){
        echo "<script>window.open('cart.php','_self')</script>";
    }else {

    }

}

// function updatecart(){
//     global $con;
//     $ip = getIp();
//     if(isset($_POST['update'])){
//         foreach($_POST['remove'] as $remove_id){
//             $delete_product = "delete from cart where p_id='$remove_id'";
//             $run_delete = mysqli_query($con, $delete_product);
//             if($run_delete){
//                 echo "<script>window.open('cart.php','_self')</script>";
//             }
//         }
//     }
//     if(isset($_POST['continue'])){
//         echo "<script>window.open('viewproduct.php?cat=1','_self')</script>";
//     }
// }
// echo @$up_cart = updatecart();



            // if(isset($_GET['updateitem'])){
                
            //     $updateitemid = $_GET['updateitem'];

            //     $displaydatainmodal = "SELECT * FROM cart WHERE p_id = '$updateitemid'";

            //     // $data = mysqli_query($con, $displaydatainmodal);

            //     // $result = mysqli_fetch_assoc($data);

            //     // $quantity = $result['qty'];

            //     // echo $quantity;

            //     $result = mysqli_query($con, $displaydatainmodal);

            //         if (mysqli_num_rows($result) > 0) {
            //         // output data of each row
            //         while($row = mysqli_fetch_assoc($result)) {
                        
            //             $quantity = $row["qty"];

            //         }
            //         } else {
            //             echo "0 results";
            //         }


                
            // }


        
                
            ?>



