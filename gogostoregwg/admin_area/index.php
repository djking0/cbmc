<?php
include('security.php');
include('includes/header.php'); 
include('includes/navbar.php'); 
?>


<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
    <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
        class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
  </div>

  <!-- Content Row -->

  <!-- FOOD -->
  <div class="row">

     <!-- Registered Food Stores -->
     <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Registered Food Store</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">

                <?php 

                  require 'dbconfig.php';
                  $query = "SELECT * from food_company where company_type = 'food'";
                  $query_run = mysqli_query($connection, $query);

                  $row = mysqli_num_rows($query_run);

                  echo "<div class='h5 mb-0 font-weight-bold text-gray-800'> $row </div>"

                ?>

              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-calendar fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

     <!-- Total Food Items -->
     <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Food items</div>
              
              <?php 

                require 'dbconfig.php';
                $query = "SELECT * from food_items where item_type = 'food'";
                $query_run = mysqli_query($connection, $query);

                $row = mysqli_num_rows($query_run);

                echo "<div class='h5 mb-0 font-weight-bold text-gray-800'> $row </div>";

                ?>
              <!-- <div class="h5 mb-0 font-weight-bold text-gray-800">$215,000</div> -->
            </div>
            <div class="col-auto">
              <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Completed Food-Pickup Orders -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Completed Food PICKUP Orders</div>

              <?php 

                require 'dbconfig.php';
                $query = "SELECT * from completed_orders where order_type = 'FOOD-PICKUP'";
                $query_run = mysqli_query($connection, $query);

                $row = mysqli_num_rows($query_run);

                echo "<div class='h5 mb-0 font-weight-bold text-gray-800'> $row </div>";
                
                ?>
            </div>
            <div class="col-auto">
              <i class="fas fa-calendar fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Completed Food-Dine-in Orders -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Completed Food Dine-in Orders</div>

              <?php 

                require 'dbconfig.php';

                $query = "SELECT * from completed_orders where order_type like '%Dine-In%'";
                $query_run = mysqli_query($connection, $query);

                $row = mysqli_num_rows($query_run);

                echo "<div class='h5 mb-0 font-weight-bold text-gray-800'> $row </div>";
                
                ?>
            </div>
            <div class="col-auto">
              <i class="fas fa-calendar fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <!-- PRODUCT -->
  <div class="row">

    <!-- Registered PRODUCT Stores -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Total Registered Product Store</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">

                <?php 

                  require 'dbconfig.php';
                  $query = "SELECT * from food_company where company_type = 'product'";
                  $query_run = mysqli_query($connection, $query);

                  $row = mysqli_num_rows($query_run);

                  echo "<div class='h5 mb-0 font-weight-bold text-gray-800'> $row </div>"

                ?>

              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-calendar fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

     <!-- Total Products -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Total PRODUCTS</div>
              <div class="row no-gutters align-items-center">
                <div class="col-auto">
                <?php 

                  require 'dbconfig.php';
                  $query = "SELECT * from food_items where item_type = 'product'";
                  $query_run = mysqli_query($connection, $query);

                  $row = mysqli_num_rows($query_run);

                  echo "<div class='h5 mb-0 font-weight-bold text-gray-800'> $row </div>";

                  ?>
                  
                </div>
              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

     <!-- Completed Product-Pickup Orders -->
     <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Completed Product PICKUP Orders</div>

              <?php 

                require 'dbconfig.php';
                $query = "SELECT * from completed_orders where order_type = 'PRODUCT-PICKUP'";
                $query_run = mysqli_query($connection, $query);

                $row = mysqli_num_rows($query_run);

                echo "<div class='h5 mb-0 font-weight-bold text-gray-800'> $row </div>";
                
                ?>
            </div>
            <div class="col-auto">
              <i class="fas fa-calendar fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

     <!-- Completed Product-Dine-in Orders -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Completed PRODICT Dine-in Orders</div>

              <?php 

                require 'dbconfig.php';

                $query = "SELECT * from completed_orders where order_type like '%ProductDine-In%'";
                $query_run = mysqli_query($connection, $query);

                $row = mysqli_num_rows($query_run);

                echo "<div class='h5 mb-0 font-weight-bold text-gray-800'> $row </div>";
                
                ?>
            </div>
            <div class="col-auto">
              <i class="fas fa-calendar fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

   

  </div>


    

    

  <!-- CUSTOMERS -->
  <div class="row">

    <!-- Total Customers -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-warning shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Total Registered Customers</div>
              <?php 

                  require 'dbconfig.php';
                  $query = "SELECT customer_id from customers ORDER BY customer_id";
                  $query_run = mysqli_query($connection, $query);

                  $row = mysqli_num_rows($query_run);

                  echo "<div class='h5 mb-0 font-weight-bold text-gray-800'> $row </div>";

                  ?>
              
            </div>
            <div class="col-auto">
              <i class="fas fa-comments fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>


  <!-- INFLUENCERS -->
  <div class="row">

    <!-- Total REGISTERED INFLUENCERS -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-info shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Total REGISTERED INFLUENCERS</div>
              <?php 

                  require 'dbconfig.php';
                  $query = "SELECT i_id from influencer where i_accountstatus = 'verifiedwithlogin' ";
                  $query_run = mysqli_query($connection, $query);

                  $row = mysqli_num_rows($query_run);

                  echo "<div class='h5 mb-0 font-weight-bold text-gray-800'> $row </div>";

                  ?>
              
            </div>
            <div class="col-auto">
              <i class="fas fa-comments fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

</div>








  <?php
include('includes/scripts.php');
include('includes/footer.php');
?>