<?php
include ("includes/db.php");


$c = $_SESSION ['customer_email'];

$get_c = "select * from customers where customer_email = '$c'";

$run_c = mysqli_query($con , $get_c);

$row_c = mysqli_fetch_array($run_c);

$customer_id = $row_c['customer_id'];
?>

 <!--Middle Part Start-->
 <div id="content" class="col-sm-9">
                    <h2 class="title">Order History</h2>
                    <div class="table-responsive">
                    
                    <?php 

                    $query = "SELECT * FROM completed_orders where customer_id = '$customer_id' ORDER BY order_date desc";
                    $query_run = mysqli_query($con, $query);


                    ?>
                        <table class="table table-bordered table-hover">
                            <thead>

                                <tr>
                                    <td class="text-center">Invoice No</td>
                                    <td class="text-center">Hotel/Store</td>
                                    <td class="text-center">Order Date</td>
                                    <td class="text-center">Order Description</td>
                                    <td class="text-center">Amount </td>
                                    <td class="text-center">Pickup Order Date and Time</td>
                                    <td class="text-center">Order Status</td>
                                    <td class="text-center">Order Type</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>

                            <?php 

                                if(mysqli_num_rows($query_run) > 0)        
                                {
                                    while($row = mysqli_fetch_assoc($query_run))
                                    {

                            ?>
                                <tr>
                                    <td> <?php  echo $row['invoice_no']; ?></td>
                                    <td> 
                                        <?php  
                                            $hotel_id  = $row['hotel_id']; 

                                            $get_hotel_data = "Select * from food_company where fcompany_id = '$hotel_id'";
                                            $run_hotel_data = mysqli_query($con,$get_hotel_data);
                                            $fetch_hotel_data = mysqli_fetch_array($run_hotel_data);

                                            $hotel_name = $fetch_hotel_data['Company_name'];

                                            echo "$hotel_name";
                                        
                                        
                                        
                                        ?>
                                    </td>
                                    <td> <?php  echo $row['order_date']; ?></td>
                                    <td> <?php  echo $row['order_description']; ?></td>
                                    <td> <?php  echo $row['due_amount']; ?></td>
                                    <td> <?php  echo $row['pickup_order_date']; ?>
                                    <?php  echo "<br>" ?>
                                    <?php  echo $row['pickup_order_time']; ?></td>
                                    <td> <?php  echo $row['order_status']; ?></td>
                                <td> <?php  echo $row['order_type']; ?></td>
                                    <td class="text-center"><a class="btn btn-info" title="" data-toggle="tooltip"
                                            href="customer_myaccount.php?order_orderhistory_information=<?php echo $row['completed_order_id']?>" data-original-title="View"><i
                                                class="fa fa-eye"></i></a>
                                    </td>
                                </tr>

                            <?php
                                }
                                }else {
                                echo "No Record Found";
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>

                </div>
                <!--Middle Part End-->
