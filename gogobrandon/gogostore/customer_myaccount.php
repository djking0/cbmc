<?php
session_start();
include("includes/db.php");
include("functions/functions.php");

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Basic page needs
    ============================================ -->
    <title>GOGO EMPIRE STORE</title>
    <meta charset="utf-8">
    <meta name="keywords"
        content="Gogo Empire Sdn Bhd" />
    <meta name="description"
        content="Gogo Empire Sdn Bhd" />
    <meta name="author" content="Gogo Empire">
    <meta name="robots" content="index, follow" />

    <!-- Mobile specific metas
    ============================================ -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Favicon
    ============================================ -->

    <link rel="shortcut icon" type="image/png" href="ico/favicon-16x16.png" />


      <!--  jQuery -->
      <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

        <!-- Bootstrap Date-Picker Plugin -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">





    <!-- Libs CSS
    ============================================ -->
    <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="js/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="js/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="css/themecss/lib.css" rel="stylesheet">
    <link href="js/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <link href="js/minicolors/miniColors.css" rel="stylesheet">

    <!-- Theme CSS
    ============================================ -->
    <link href="css/themecss/so_searchpro.css" rel="stylesheet">
    <link href="css/themecss/so_megamenu.css" rel="stylesheet">
    <link href="css/themecss/so-categories.css" rel="stylesheet">
    <link href="css/themecss/so-listing-tabs.css" rel="stylesheet">
    <link href="css/themecss/so-newletter-popup.css" rel="stylesheet">

    <link href="css/footer/footer1.css" rel="stylesheet">
    <link href="css/header/header1.css" rel="stylesheet">
    <link id="color_scheme" href="css/theme.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!-- Google web fonts
    ============================================ -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700' rel='stylesheet'
        type='text/css'>
    <style type="text/css">
        body {
            font-family: 'Roboto', sans-serif
        }
    </style>

</head>

<body class="res layout-1">

    <div id="wrapper" class="wrapper-fluid banners-effect-5">


        <!-- Header Container  -->
        <header id="header" class=" typeheader-1">
                <!-- Header Top -->
                <div class="header-top hidden-compact">
                    <div class="container">
                        <div class="row">
                            <div class="header-top-left col-lg-7 col-md-8 col-sm-6 col-xs-4">
                                <div class="hidden-sm hidden-xs welcome-msg"><b>Welcome to GogoFood Store !</b> ! Wrap new offers /
                                    gift every single day on Weekends </div>
                                <ul class="top-link list-inline hidden-lg hidden-md">
                                    <li class="account" id="my_account">
                                        <?php
                                        if(!isset($_SESSION['customer_email'])){
                                        ?>
                                            <ul class="dropdown-menu ">
                                                    <li><a href="register.html"><i class="fa fa-user"></i> Register</a></li>
                                                    <li><a href="login.html"><i class="fa fa-pencil-square-o"></i> Login</a></li>
                                                </ul>
                                        <?php
                                        }else{
                                        ?>
                                                <?php
                                                
                                                $customer_email = $_SESSION['customer_email'];
                                        
                                                $get_customer_details = "Select * from customers where customer_email='$customer_email'";
                                                $run_customer_details = mysqli_query($con, $get_customer_details);
                                                $fetch_customer_details = mysqli_fetch_array($run_customer_details);
                                                                                                    
                                                $customer_id = $fetch_customer_details['customer_id']; //:: Customer id 
                                                $customer_name = $fetch_customer_details['customer_name']; //:: customer name

                                                ?>
                                                <?php echo" 
                                                $customer_name"?>
                                                <a href="#" title="My Account " class="btn-xs dropdown-toggle"
                                                data-toggle="dropdown"> <span class="hidden-xs">My Account </span> <span
                                                class="fa fa-caret-down"></span>
                                                </a>
                                                <ul class="dropdown-menu ">
                                                    <li><a href="food.php"><i class="fa fa-user"></i> <?php echo" 
                                                    $_SESSION[customer_email]"?></a></li>
                                                    <li><a href="logout.php"><i class="fa fa-pencil-square-o"></i> Logout</a></li>
                                                    
                                                </ul>

                                        <?php
                                        }

                                        ?>
                                        
                                        <!-- <ul class="dropdown-menu ">
                                            <li><a href="register.html"><i class="fa fa-user"></i> Register</a></li>
                                            <li><a href="login.html"><i class="fa fa-pencil-square-o"></i> Login</a></li>
                                        </ul> -->
                                    </li>
                                </ul>
                            </div>
                            <div class="header-top-right collapsed-block col-lg-5 col-md-4 col-sm-6 col-xs-8">
                                <ul class="top-link list-inline lang-curr">
                                    <li class="language">
                                        <div class="btn-group languages-block ">
                                            <form action="http://demo.smartaddons.com/templates/html/emarket/index.html"
                                                method="post" enctype="multipart/form-data" id="bt-language">
                                                <a class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                                    <span class="">English</span>
                                                    <span class="fa fa-angle-down"></span>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="index.html"> English </a></li>
                                                    <li> <a href="index.html">  Bhasa Malayu </a> </li>
                                                </ul>
                                            </form>
                                        </div>

                                    </li>
                                </ul>



                            </div>
                        </div>
                    </div>
                </div>
                <!-- //Header Top -->

                <!-- Header center -->
                <div class="header-middle">
                    <div class="container">
                        <div class="row">
                            <!-- Logo -->
                            <div class="navbar-logo col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <div class="logo"><a href="index.html"><h1 style="color: white;"> GOGO EMPIRE</h1></a></div>
                            </div>
                            <!-- //end Logo -->

                            <!-- Main menu -->
                            <div class="main-menu col-lg-6 col-md-7 ">
                                <div class="responsive so-megamenu megamenu-style-dev">
                                    <nav class="navbar-default">
                                        <div class=" container-megamenu  horizontal open ">
                                            <div class="navbar-header">
                                                <button type="button" id="show-megamenu" data-toggle="collapse"
                                                    class="navbar-toggle">
                                                    <span class="icon-bar"></span>
                                                    <span class="icon-bar"></span>
                                                    <span class="icon-bar"></span>
                                                </button>
                                            </div>

                                            <div class="megamenu-wrapper">
                                                <span id="remove-megamenu" class="fa fa-times"></span>
                                                <div class="megamenu-pattern">
                                                    <div class="container-mega">
                                                    <ul class="megamenu" 
                                                            data-animationtime="250">
                                                            <li class="home hover">
                                                                <a href="index.php">HOME </a>
                                                            </li>
                                                            <li class="home hover">
                                                            <a href="hotels.php">STORES </a>
                                                            </li>
                                                            <li class="home hover">
                                                                <a href="">COLLECTION </a>
                                                            </li>
                                                            <li class="home hover">
                                                                <a href="aboutus.php">ABOUT US </a>
                                                            </li>
                                                            <li class="home hover">
                                                                <a href="contact.php">CONTACT US </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </nav>
                                </div>
                            </div>
                            <!-- //end Main menu -->

                            <div class="middle-right col-lg-4 col-md-3 col-sm-6 col-xs-8">
                                <div class="signin-w hidden-sm hidden-xs">
                                    <ul class="signin-link blank">
                                    <?php 
                                    if(!isset($_SESSION['customer_email'])){
                                        echo"";
                                    }else{
                                        echo" <li class='log login'> 
                                        <a class='link-lg'>$_SESSION[customer_email]</a>
                                    </li>";
                                    }
                                    ?>
                                    </ul>
                                </div>



                            

                                <div class="signin-w hidden-sm hidden-xs">
                                    <ul class="signin-link blank">
                                    <?php
                                if(!isset($_SESSION['customer_email'])){
                                    echo" <li class='log login'></i> 
                                    <a class='link-lg' href='checkout.php'>Login</a>
                                </li>";
                                echo" <li class='log login'></i> 
                                <a class='link-lg' href='checkout.php'>|</a>
                            </li>";
                                echo" <li class='log login'></i> 
                                <a class='link-lg' href='login.html'>Create an Account</a>
                            </li>";
                                }else{
                                    echo"<li class='log login'></i> 
                                    <a class='link-lg' href='logout.php'>Logout</a>
                                </li>";
                                }
                                ?>
                                    </ul>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>
                <!-- //Header center -->

                <!-- Header Bottom -->
                <div class="header-bottom hidden-compact">
                    <div class="container">
                        <div class="row">

                            <!-- Categories -->
                        <?php
                        include("features/allcategories.php");
                        ?>
                        <!-- //all Categories -->

                        <!-- Search -->
                        <?php
                        include("features/search.php");
                        ?>
                        <!-- //end Search -->

                            <!-- Secondary menu -->
                            <div class="bottom3 col-lg-3 col-md-3 col-sm-3">


                            

                        </div>
                    </div>

                </div>
            </header>
        <!-- //Header Container  -->


     
        <?php

        

        ?>

    <!-- Main Container  -->
    <div class="main-container container">

        <?php
                            if(isset($_SESSION['customer_email'])){

                                $customer_session = $_SESSION['customer_email'];

                                $get_customer_name = "select * from customers where customer_email = '$customer_session'";

                                $run_customer = mysqli_query($con, $get_customer_name);

                                $row_customer = mysqli_fetch_array($run_customer);

                                $customer_name = $row_customer['customer_name'];
                                $customer_email = $row_customer['customer_email'];
                                $customer_contact = $row_customer['customer_contact'];
                                $customer_address = $row_customer['customer_address'];
                                $customer_city = $row_customer['customer_city'];
                                $customer_country = $row_customer['customer_country'];
        ?>

                <ul class="breadcrumb">
                    <li><a href="index.php"><i class="fa fa-home"></i></a></li>
                    <li><a href="">Your Account</a></li>
                </ul>


                <div class="row">
                    <!--Right Part Start -->
                    <aside class="col-sm-3 hidden-xs" id="column-right">
                        <h2 class="subtitle">Account</h2>
                        <div class="list-group">
                            <ul class="list-item">
                                <li><a href="customer_myaccount.php">Account Control Panel</a>
                                </li>
                                <li><a href="customer_myaccount.php?my_orders">Pickup Orders</a></a>
                                </li>
                                <li><a href="customer_myaccount.php?dinein_orders">Dine-In Orders</a>
                                </li>
                                <li><a href="customer_myaccount.php?order_history">Order History</a>
                                </li>
                            </ul>
                        </div>
                    </aside>
                    <!--Right Part End -->
                    <!--Middle Part Start-->
                    <div id="content" class="col-sm-9">
                    <?php 

                        if(isset($_GET['my_orders'])){
                            //   echo "bunny";
                            include("customer_pickuporders.php");
                        }
                        elseif(isset($_GET['dinein_orders'])){
                            include("customer_dineinorders.php");
                        }
                        elseif(isset($_GET['order_history'])){
                            include("customer_orderhistory.php");
                        }
                        elseif(isset($_GET['order_dinein_information'])){
                            include("customer_dinein_orderinformation.php");
                        }
                        elseif(isset($_GET['order_pickup_information'])){
                            include("customer_pickup_orderinformation.php");
                        }
                        elseif(isset($_GET['order_orderhistory_information'])){
                            include("customer_orderhistory_orderinformation.php");
                        }
                        elseif(isset($_GET['customer_edit_contactInformation'])){
                            include("customer_edit_contactInformation.php");
                        }
                        elseif(isset($_GET['customer_edit_addressInformation'])){
                            include("customer_edit_addressInformation.php");
                        }
                        elseif(isset($_GET['customer_change_password'])){
                            include("customer_change_password.php");
                        }
                        else{
                        ?>
        
                                <h2>Account Control Panel</h2>
                                <strong>Hello <?php echo "$customer_name"?></strong><br />
                                <p>From your account control panel, you can access all of your recent activites, orders, save products and you can edit your personal information and other details.</p>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="well">
                                            <h3>Contact Information</h3>
                                            <p>Name : <?php echo "$customer_name"?></p>
                                            <p>Email : <?php echo "$customer_email"?></p>
                                            <p>Contact No: <?php echo "$customer_contact"?></p>
                                            <p><a href="customer_myaccount.php?customer_change_password">Change Password</a></p>
                                            <p class="pull-right"><a href="customer_myaccount.php?customer_edit_contactInformation"><i class="fa fa-edit"></i> Edit</a></p>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-6">
                                        <div class="well">
                                            <h3>Delivery Address</h3>
                                            <address class="address">
                                                <strong>Your Address:</strong>  <br />
                                                <?php echo "$customer_address" ?> <br/>
                                                <?php echo"$customer_city"?> <?php echo "$customer_country" ?><br />
                                            
                                            </address>
                                            <p class="pull-right"><a href="customer_myaccount.php?customer_edit_addressInformation"><i class="fa fa-edit"></i> Edit</a></p>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>



                                </div>

                        <?php
                                } //:: End of isset myorder
                            ?>

                        <!-- <h2 class="title">Order History</h2>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <td class="text-center">Image</td>
                                        <td class="text-left">Product Name</td>
                                        <td class="text-center">Order ID</td>
                                        <td class="text-center">Qty</td>
                                        <td class="text-center">Status</td>
                                        <td class="text-center">Date Added</td>
                                        <td class="text-right">Total</td>
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center">
                                            <a href="product.html"><img width="85" class="img-thumbnail"
                                                    title="Aspire Ultrabook Laptop" alt="Aspire Ultrabook Laptop"
                                                    src="image/catalog/demo/product/funiture/1.jpg">
                                            </a>
                                        </td>
                                        <td class="text-left"><a href="product.html">Aspire Ultrabook Laptop</a>
                                        </td>
                                        <td class="text-center">#214521</td>
                                        <td class="text-center">1</td>
                                        <td class="text-center">Shipped</td>
                                        <td class="text-center">21/06/2016</td>
                                        <td class="text-right">$228.00</td>
                                        <td class="text-center"><a class="btn btn-info" title="" data-toggle="tooltip"
                                                href="order-information.html" data-original-title="View"><i
                                                    class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            <a href="product.html"><img width="85" class="img-thumbnail"
                                                    title="Xitefun Causal Wear Fancy Shoes"
                                                    alt="Xitefun Causal Wear Fancy Shoes"
                                                    src="image/catalog/demo/product/funiture/4.jpg">
                                            </a>
                                        </td>
                                        <td class="text-left"><a href="product.html">Xitefun Causal Wear Fancy Shoes</a>
                                        </td>
                                        <td class="text-center">#1565245</td>
                                        <td class="text-center">1</td>
                                        <td class="text-center">Shipped</td>
                                        <td class="text-center">20/06/2016</td>
                                        <td class="text-right">$133.20</td>
                                        <td class="text-center"><a class="btn btn-info" title="" data-toggle="tooltip"
                                                href="order-information.html" data-original-title="View"><i
                                                    class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div> -->

                    </div>
                    <!--Middle Part End-->
                
                </div>

        <?php
        }else {
            ?>
                <h1 class='breadcrumb'> Page Not Found...
                    </h1>

                <?php
        }
        ?>
    </div>
        <!-- //Main Container -->


           
           
       

    <!-- Footer Container -->
        <footer class="footer-container typefooter-1">

            <hr>

            <div class="footer-middle ">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-style">
                            <div class="infos-footer">
                                <h3 class="modtitle">Contact Us</h3>
                                <ul class="menu">
                                    <li class="adres">
                                        S0-7-01, Menara 1, No 3, Jalan Bangsar, KL Eco City, Kuala lumpur Malaysia
                                    </li>
                                    <li class="phone">
                                        (+60) 123817908
                                    </li>
                                    <li class="mail">
                                        <a href="">www.gogoempire.com</a>
                                    </li>
                                    <li class="time">
                                        Open time: 19:00AM - 5:30PM
                                    </li>
                                </ul>
                            </div>


                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                            <div class="box-information box-footer">
                                <div class="module clearfix">
                                    <h3 class="modtitle">Information</h3>
                                    <div class="modcontent">
                                        <ul class="menu">
                                            <li><a href="aboutus.php">ABOUT US </a></li>
                                            <li><a href="#">FAQ</a></li>
                                            <li><a href="#">Warranty And Services</a></li>
                                            <li><a href="#">Support 24/7 page</a></li>
                                            <li><a href="#">Product Registration</a></li>
                                            <li><a href="#">Product Support</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                            <div class="box-account box-footer">
                                <div class="module clearfix">
                                    <h3 class="modtitle">Extras</h3>
                                    <div class="modcontent">
                                        <ul class="menu">
                                            <li> <a href="aboutus.php"> Gogo Empire </a></li>
                                            <li><a href="#">Collection</a></li>
                                            <li><a href="contact.php">Contact Us</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-style">
                            <div class="box-service box-footer">
                                <div class="module clearfix">
                                    <h3 class="modtitle">Services</h3>
                                    <div class="modcontent">
                                        <ul class="menu">
                                            <li><a href="#">Kuala Lumpur</a></li>
                                            <li><a href="#">Selangor</a></li>
                                            <li><a href="#">KL Central</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-style">


                        </div>

                        <div class="col-lg-12 col-xs-12 text-center">
                            <img src="image/catalog/demo/payment/payment.png" alt="imgpayment">
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer Bottom Container -->
            <div class="footer-bottom ">
                <div class="container">
                    <div class="copyright">
                        gogoempire © 2020. All Rights Reserved. 
                    </div>
                </div>
            </div>
            <!-- /Footer Bottom Container -->


            <!--Back To Top-->
            <div class="back-to-top"><i class="fa fa-angle-up"></i></div>
        </footer>
    <!-- //end Footer Container -->

    



    <!-- Include Libs & Plugins
	============================================ -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/owl-carousel/owl.carousel.js"></script>
    <script type="text/javascript" src="js/themejs/libs.js"></script>
    <script type="text/javascript" src="js/unveil/jquery.unveil.js"></script>
    <script type="text/javascript" src="js/countdown/jquery.countdown.min.js"></script>
    <script type="text/javascript" src="js/dcjqaccordion/jquery.dcjqaccordion.2.8.min.js"></script>
    <script type="text/javascript" src="js/datetimepicker/moment.js"></script>
    <script type="text/javascript" src="js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>


    <!-- Datetimepicker Script -->
    <script>
                $(document).ready(function(){
                var date_input=$('input[name="date"]'); //our date input has the name "date"
                var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                var options={
                    format: 'dd-mm-yyyy',
                    timepicker: true,
                    
                };
                date_input.datepicker(options);
                })
                </script>
    
    <!-- Getting date id Script -->


    <!-- Theme files
	============================================ -->


    <script type="text/javascript" src="js/themejs/so_megamenu.js"></script>
    <script type="text/javascript" src="js/themejs/addtocart.js"></script>
    <script type="text/javascript" src="js/themejs/application.js"></script>
    <script type="text/javascript">
     
        // Check if Cookie exists
        if ($.cookie('display')) {
            view = $.cookie('display');
        } else {
            view = 'grid';
        }
        if (view) display(view);
        
    </script>
</body>

<!--  demo.smartaddons.com/templates/html/emarket/category.html by AkrAm, Sat, 20 Apr 2019 20:00:15 GMT -->

</html>




