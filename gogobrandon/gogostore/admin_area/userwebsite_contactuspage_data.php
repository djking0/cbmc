<?php
include('security.php');
include('includes/header.php'); 
include('includes/navbar.php'); 
?>

<div class="modal fade" id="addadminprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Contact Us Page: </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="code.php" method="POST">

        <div class="modal-body">

        <?php

      
            require 'dbconfig.php';

            $query = "SELECT * FROM contact_page";
            $query_run = mysqli_query($connection, $query);
            $fetch_run = mysqli_fetch_assoc($query_run);

            $map_link = $fetch_run['contact_link']; 
            $address= $fetch_run['contact_address']; 
            $phone = $fetch_run['contact_phone']; 
            $description = $fetch_run['contact_description'];        



            ?>

        <div class="form-group">
            <label> Map Location Link</label>
            <input type="text" name="update_maplink" class="form-control" value="<?php echo "$map_link" ?>" required="required">
        </div>

        <div class="form-group">
            <label>Address:</label>
            <input type="text" name="update_address" class="form-control" value="<?php echo "$address" ?>" placeholder="Enter Hotel Email" required="required">
        </div>

        <div class="form-group">
            <label>Contact Number:</label>
            <input type="text" name="update_phone" class="form-control" value="<?php echo "$phone" ?>" placeholder="Enter Hotel Address" required="required">
        </div>
        <div class="form-group">
            <label>Description:</label>
            
             <textarea rows="10" id="input-enquiry"
              class="form-control" name= "update_description" required="required"><?php echo "$description" ?></textarea>
              
        </div>

            
        
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="updatecontactusbtn" class="btn btn-primary">Update</button>
        </div>
      </form>

    </div>
  </div>
</div>


<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
  <h6 class="m-0 font-weight-bold text-primary">CONTACT US PAGE

        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addadminprofile">
              Update Data
            </button>
            
    </h6>
  </div>

  <div class="card-body">

  <?php
      if(isset($_SESSION['success']) && $_SESSION['success']!=''){
        echo '<h2> '.$_SESSION['success'].' </h2>.';
        unset($_SESSION['success']);
      } 
      if(isset($_SESSION['status']) && $_SESSION['status']!=''){
        echo '<h2> '.$_SESSION['status'].' </h2>.';
        unset($_SESSION['status']);
      } 
      
    ?>

    <div class="table-responsive">
      <?php

      
      require 'dbconfig.php';

      $query = "SELECT * FROM contact_page";
      $query_run = mysqli_query($connection, $query);
      $fetch_run = mysqli_fetch_assoc($query_run);

     $map_link = $fetch_run['contact_link']; 
     $address= $fetch_run['contact_address']; 
     $phone = $fetch_run['contact_phone']; 
     $description = $fetch_run['contact_description'];        

    

      ?>

        <div class="modal-body">

        <div class="form-group">
            <label> <strong> Map Location Link: </strong> </label><br>
            <label> <?php echo "$map_link" ?></label>
            <!-- <input type="text" name="hotel_name" class="form-control" value="<?php echo "$map_link" ?>" required="required"> -->
        </div>

        <div class="form-group">
            <label> <strong>Address: </strong> </label> 
            <br>
            <label><?php echo "$address" ?></label>
            <!-- <input type="email" name="hotel_email" class="form-control" placeholder="Enter Hotel Email" required="required"> -->
        </div>

        <div class="form-group">
            <label> <strong> Contact Number: </strong> </label> 
            <br>
            <label><?php echo "$phone" ?></label>
            <!-- <input type="text" name="hotel_add" class="form-control" placeholder="Enter Hotel Address" required="required"> -->
        </div>
        <div class="form-group">
            <label> <strong> Description: </strong> </label> 
            <br>
            <label><?php echo "$description" ?></label>
        </div>
        </div>

      

      <!-- <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th> Map Location link </th>
            <th> Address </th>
            <th> Phone </th>
            <th>Description</th>
            <th>Edit</th>
          </tr>
        </thead>
        <tbody>

          <?php 
            if(mysqli_num_rows($query_run) > 0)        
            {
                while($row = mysqli_fetch_assoc($query_run))
                {

                  ?>

                <tr>
                   
                    <td> <?php  echo $row['contact_link']; ?></td>
                    <td> <?php  echo $row['contact_address']; ?></td>
                    <td> <?php  echo $row['contact_phone']; ?> </td>
                    <td> <?php  echo $row['contact_description']; ?></td>
                    <td>
                        <form action="userwebsite_contactuspage_data_edit.php" method="post">
                            <input type="hidden" name="edithotel_id" value="<?php  echo $row['fcompany_id']; ?>">
                            <button  type="submit" name="edithotel_btn" class="btn btn-success"> EDIT</button>
                        </form>
                  </td>
                </tr>
          <?php
                }
              }else {
                echo "No Record Found";
              }
          ?>
     
          
        
        </tbody>
      </table> -->

    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>