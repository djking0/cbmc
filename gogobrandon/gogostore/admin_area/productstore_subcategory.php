<?php
include('productstore_security.php');
include('includes/header.php'); 
include('includes/navbar_product.php'); 
?>



<!-- MODAL -->


<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Product's Sub-Category
    </h6>
  </div>

  <div class="card-body">

    <?php
      if(isset($_SESSION['success']) && $_SESSION['success']!=''){
        echo '<h2> '.$_SESSION['success'].' </h2>.';
        unset($_SESSION['success']);
      } 
      if(isset($_SESSION['status']) && $_SESSION['status']!=''){
        echo '<h2> '.$_SESSION['status'].' </h2>.';
        unset($_SESSION['status']);
      } 
      
    ?>

    <div class="table-responsive">

      <?php



        // : Getting FoodStore ID
        require 'dbconfig.php';

        $store_email = $_SESSION['productusername'];
                                    
        $get_foodstore_details = "Select * from food_company where company_email='$store_email'";
        $run_foodstore_details = mysqli_query($connection, $get_foodstore_details);
        $fetch_foodstore_details = mysqli_fetch_array($run_foodstore_details);
                                                                                                
        $foodstore_id = $fetch_foodstore_details['fcompany_id']; //:: Customer id 
        $foodstore_name = $fetch_foodstore_details['Company_name']; //:: customer name

        //:: Getting the food_items..
        $query = "SELECT * FROM food_items where fcompany_id = '$foodstore_id' ";
        $query_run = mysqli_query($connection, $query);


      ?>

      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th> Image </th>
            <th> Title </th>
            <th> Category </th>
            <th> Sub-Category</th>
          </tr>
        </thead>
        <tbody>

<?php 
  if(mysqli_num_rows($query_run) > 0)        
  {
      while($row = mysqli_fetch_assoc($query_run))
      {

        ?>

        




      <tr>
          <td> <?php echo '<img src="fooditem_images/'.$row['item_img'].'" width="100px;" height="100px;" alt="image" >' ?> </td>
          <td> <?php  echo $row['item_title']; ?></td>
          

          
          
          <!-- Fetch Category -->
          <?php 

        
          ?>

          <td> 
            <?php
           
              $category_id = $row['fcat_id']; 

              $get_cat_data = "Select * from food_category where fcat_id = '$category_id'";
              $run_cat_data = mysqli_query($connection,$get_cat_data);
              $fetch_cat_data = mysqli_fetch_array($run_cat_data);

              $category_name = $fetch_cat_data['food_cat'];

              echo $category_name;
            
            
            ?>
          </td>

           <td>

                     <div class="btn-group" role="group">
                        <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php

                            $subcategory_itemid = $row['item_id'];
                            
                            //:: Get sub-foodcategory id from fooditem table. 
                            $subfoodcategory_id = "Select * from food_items where item_id = '$subcategory_itemid'";
                            $runfoodcategory_id = mysqli_query($connection, $subfoodcategory_id);
                            $fetchfoodcategory_id = mysqli_fetch_array($runfoodcategory_id);

                            $subfoodcategoryid = $fetchfoodcategory_id['fcat_sub_id'];


                            //: Get sub-foodcateory name from subcategory_stageone table

                            if($subfoodcategoryid == '0'){
                              
                              echo "Not Set";

                            }else {
                            $subfoodcategory_name = "Select * from subcategory_stageone where sc_id = '$subfoodcategoryid'";
                            $runfoodcategory_name = mysqli_query($connection, $subfoodcategory_name);
                            $fetchfoodcategory_name = mysqli_fetch_array($runfoodcategory_name);

                            $subfoodcategoryname = $fetchfoodcategory_name['sc_name'];

                            echo "$subfoodcategoryname";
                            
                            }

                            ?>
                            
                          
                        </button>
                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">


                            <?php 

                            $subcategory_query = "Select * from subcategory_stageone where sc_main_id = '$category_id'";
                            $subcategory_runquery = mysqli_query($connection, $subcategory_query);
                            $countcategory = mysqli_num_rows($subcategory_runquery);

                            if($countcategory == 0){
                              echo "Sub-Category is not set";
                            }else{

                            while($subcategory_fetchquery = mysqli_fetch_array($subcategory_runquery)){

                              $subcategory_id = $subcategory_fetchquery['sc_id'];
                              $subcategory_name = $subcategory_fetchquery['sc_name'];
                              
                              ?>

                              <form action='productstore_code.php' method='post'>
                              <input type='hidden' name='add_subcategoryforproduct_itemid' value='<?php  echo $row['item_id']; ?>'>
                              <input type='hidden' name='add_subcategoryforproduct_id' value='<?php  echo "$subcategory_id" ?>'>
                              <button  type='submit' name='add_subcategoryforproduct_btn'  class='dropdown-item'><?php  echo "$subcategory_name"?></button>
                              </form>

                              <?php


                              }
                            }
                            ?>                      
                          
                        </div>
                      </div>
                    
                  </td>
      </tr>
<?php
      }
    }else {
      echo "No Record Found";
    }
?>



</tbody>
      </table>

    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>