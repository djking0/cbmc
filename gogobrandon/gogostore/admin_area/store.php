<?php
include('security.php');
include('includes/header.php'); 
include('includes/navbar.php'); 
?>


<div class="modal fade" id="addadminprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Store</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="code.php" method="POST">

        <div class="modal-body">

            <div class="form-group">
                <label> Store Name </label>
                <input type="text" name="store_name" class="form-control" placeholder="Enter Hotel Name" required="required">
            </div>
            <div class="form-group">
                <label>Store Email</label>
                <input type="email" name="store_email" class="form-control" placeholder="Enter Hotel Email" required="required">
            </div>
            <div class="form-group">
                <label>Store Address</label>
                <input type="text" name="store_add" class="form-control" placeholder="Enter Hotel Address" required="required">
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" name="store_password" class="form-control" placeholder="Enter Password" required="required">
            </div>
            <div class="form-group">
                <label>Confirm Password</label>
                <input type="password" name="store_confirmpassword" class="form-control" placeholder="Confirm Password" required="required">
            </div>
        
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="addstore_btn" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>


<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Store's Data
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addadminprofile">
              Add New Store
            </button>
    </h6>
  </div>

  <div class="card-body">

    <?php
      if(isset($_SESSION['success']) && $_SESSION['success']!=''){
        echo '<h2> '.$_SESSION['success'].' </h2>.';
        unset($_SESSION['success']);
      } 
      if(isset($_SESSION['status']) && $_SESSION['status']!=''){
        echo '<h2> '.$_SESSION['status'].' </h2>.';
        unset($_SESSION['status']);
      } 
      
    ?>

    <div class="table-responsive">

      <?php

      
      require 'dbconfig.php';

      $query = "SELECT * FROM food_company where company_type = 'product'";
      $query_run = mysqli_query($connection, $query);

      ?>

      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th> ID </th>
            <th> Name </th>
            <th>Email </th>
            <th>Address</th>
            <th>EDIT </th>
            <th>DELETE </th>
          </tr>
        </thead>
        <tbody>

          <?php 
            if(mysqli_num_rows($query_run) > 0)        
            {
                while($row = mysqli_fetch_assoc($query_run))
                {

                  ?>

                <tr>
                    <td> <?php  echo $row['fcompany_id']; ?></td>
                    <td> <?php  echo $row['Company_name']; ?></td>
                    <td> <?php  echo $row['company_email']; ?></td>
                    <td> <?php  echo $row['company_address']; ?> </td>
                    <td>
                        <form action="store_edit.php" method="post">
                            <input type="hidden" name="editstore_id" value="<?php  echo $row['fcompany_id']; ?>">
                            <button  type="submit" name="editstore_btn" class="btn btn-success"> EDIT</button>
                        </form>
                  </td>
                  <td>
                      <form action="code.php" method="post">
                        <input type="hidden" name="deletestore_id" value="<?php  echo $row['fcompany_id']; ?>">
                        <button type="submit" name="deletestore_btn" class="btn btn-danger"> DELETE</button>
                      </form>
                  </td>
                </tr>
          <?php
                }
              }else {
                echo "No Record Found";
              }
          ?>
     
          
        
        </tbody>
      </table>

    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>