<?php
include('productstore_security.php');
include('includes/header.php'); 
include('includes/navbar_product.php'); 
?>


<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
    <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
        class="fas fa-download fa-sm text-white-50"></i> Generate Hotel Report</a>
  </div>

  <!-- Content Row -->
  <div class="row">

    <!-- Registered FoodItems -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Registered Products</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">

            
               
               <?php 

                // : Getting FoodStore ID
                require 'dbconfig.php';

                $store_email = $_SESSION['productusername'];
                                              
                $get_foodstore_details = "Select * from food_company where company_email='$store_email'";
                $run_foodstore_details = mysqli_query($connection, $get_foodstore_details);
                $fetch_foodstore_details = mysqli_fetch_array($run_foodstore_details);
                                                                                                          
                $foodstore_id = $fetch_foodstore_details['fcompany_id']; //:: Customer id 
                $foodstore_name = $fetch_foodstore_details['Company_name']; //:: customer name
                 
                $query = "SELECT * from food_items where fcompany_id='$foodstore_id' AND item_type = 'product' ";
                $query_run = mysqli_query($connection, $query);

                $row = mysqli_num_rows($query_run);

                  echo "<div class='h5 mb-0 font-weight-bold text-gray-800'> $row </div>"

                ?>

              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-calendar fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Food Items -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Total Products Pickup Orders</div>
              
              <?php 
                
                // : Getting FoodStore ID
                require 'dbconfig.php';

                $store_email = $_SESSION['productusername'];
                                              
                $get_foodstore_details = "Select * from food_company where company_email='$store_email'";
                $run_foodstore_details = mysqli_query($connection, $get_foodstore_details);
                $fetch_foodstore_details = mysqli_fetch_array($run_foodstore_details);
                                                                                                          
                $foodstore_id = $fetch_foodstore_details['fcompany_id']; //:: Customer id 
               

                //:: 
                $query = "SELECT * from customer_pickuporder where hotel_id = '$foodstore_id'";
                $query_run = mysqli_query($connection, $query);

                $row = mysqli_num_rows($query_run);

                echo "<div class='h5 mb-0 font-weight-bold text-gray-800'> $row </div>";

                ?>
              <!-- <div class="h5 mb-0 font-weight-bold text-gray-800">$215,000</div> -->
            </div>
            <div class="col-auto">
              <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Total Products -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-info shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Total Product Delivery Orders</div>
              <div class="row no-gutters align-items-center">
                <div class="col-auto">
                <?php 

                  // require 'dbconfig.php';
                  // $query = "SELECT ";
                  // $query_run = mysqli_query($connection, $query);

                  // $row = mysqli_num_rows($query_run);

                  // echo "<div class='h5 mb-0 font-weight-bold text-gray-800'> $row </div>";

                  ?>
                  
                </div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800">0</div>
              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Total Customers -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-warning shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Total Completed Orders</div>
              <?php 

                // : Getting FoodStore ID
                require 'dbconfig.php';

                $store_email = $_SESSION['productusername'];
                                              
                $get_foodstore_details = "Select * from food_company where company_email='$store_email'";
                $run_foodstore_details = mysqli_query($connection, $get_foodstore_details);
                $fetch_foodstore_details = mysqli_fetch_array($run_foodstore_details);
                                                                                                          
                $foodstore_id = $fetch_foodstore_details['fcompany_id']; //:: Customer id 

                //::
                $query = "SELECT * from completed_orders where hotel_id='$foodstore_id'";
                  $query_run = mysqli_query($connection, $query);

                  $row = mysqli_num_rows($query_run);

                  echo "<div class='h5 mb-0 font-weight-bold text-gray-800'> $row </div>";

                  ?>
              
            </div>
            <div class="col-auto">
              <i class="fas fa-comments fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <div class="row">

    <!-- Completed Food Orders -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Earning So for..</div>
              <?php 

                // : Getting FoodStore ID
                require 'dbconfig.php';

                $store_email = $_SESSION['productusername'];
                                              
                $get_foodstore_details = "Select * from food_company where company_email='$store_email'";
                $run_foodstore_details = mysqli_query($connection, $get_foodstore_details);
                $fetch_foodstore_details = mysqli_fetch_array($run_foodstore_details);
                                                                                                          
                $foodstore_id = $fetch_foodstore_details['fcompany_id']; //:: Customer id 

                //::
                  $query = "select SUM(due_amount) as 'sum_amount' from completed_orders where hotel_id=' $foodstore_id'";
                  $res = mysqli_query($connection, $query);
                  $data = mysqli_fetch_array($res);
                  $total = $data['sum_amount'];
                  echo "<div class='h5 mb-0 font-weight-bold text-gray-800'> $total RM </div>";

                  ?>
            </div>
            <div class="col-auto">
              <i class="fas fa-calendar fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>



  </div>

  

  <!-- Content Row -->








  <?php
include('includes/scripts.php');
include('includes/footer.php');
?>