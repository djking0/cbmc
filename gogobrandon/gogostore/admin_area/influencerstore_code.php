<?php

include('influencerstore_security.php');
include('dbconfig.php');


//: Getting current influencer email 
$influencer_email = $_SESSION['influencerusername'];

$get_influencer_details = "Select * from food_company where company_email='$influencer_email'";
$run_influencer_details = mysqli_query($connection, $get_influencer_details);
$fetch_influencer_details = mysqli_fetch_array($run_influencer_details);
                                                                                                
$influencer_id = $fetch_influencer_details['fcompany_id']; //:: Customer id 
$influencer_name = $fetch_influencer_details['Company_name']; //:: customer name
                               

//: Add new page
if(isset($_POST['search_btn'])){

    $influencer_pageemail = $_SESSION['influencerusername'];
    $influencer_pagename = $_POST['influencer_pagename'];
    $influencer_pagetype = $_POST['influencer_pagetype'];
    $influencer_pagelink = $_POST['influencer_pagelink'];
    $influencer_pagefollowers = $_POST['influencer_pagefollowers'];


    // echo "$influencer_pagename", "$influencer_pagefollowers", "$influencer_pagelink", "$influencer_pagetype";

    $insert_new_page = "INSERT INTO influencer_digital_page (ip_email,ip_pagename,ip_pagelink,ip_followers,ip_pagetype) values ('$influencer_pageemail', '$influencer_pagename', '$influencer_pagelink','$influencer_pagefollowers','$influencer_pagetype')";
    $run_insert_newpage = mysqli_query($connection, $insert_new_page);

    if($run_insert_newpage){
        $_SESSION['success'] = "New page is Added";
        // $_SESSION['status_code'] = "success";
        header('Location: influencerstore_addnewpage.php'); 
    }else{
        $_SESSION['success'] = "New page is not Added";
        // $_SESSION['status_code'] = "success";
        header('Location: influencerstore_addnewpage.php'); 
    }

}

//: Edit Page
if(isset($_POST['updateinfluencer_editpage_btn'])){

    $influencer_updatepageid = $_POST['influencer_updatepageid'];
    // $influencer_updatepageemail = $_SESSION['influencerusername'];
    $influencer_updatepagename = $_POST['influencer_updatepagename'];
    $influencer_updatepagetype = $_POST['influencer_updatepagetype'];
    $influencer_updatepagelink = $_POST['influencer_updatepagelink'];
    $influencer_updatepagefollowers = $_POST['influencer_updatepagefollowers'];

    $update_influencerpage = "UPDATE influencer_digital_page SET ip_pagename='$influencer_updatepagename', ip_pagelink='$influencer_updatepagelink', ip_followers='$influencer_updatepagefollowers', ip_pagetype='$influencer_updatepagetype' WHERE id='$influencer_updatepageid' ";
    $run_update_influencerpage = mysqli_query($connection, $update_influencerpage);

    if($run_update_influencerpage){

        $_SESSION['success'] = "Your data is Updated";
        // $_SESSION['status_code'] = "success";
        header('Location: influencerstore_addnewpage.php');

    }else {
        $_SESSION['success'] = "Your data is not Updated";
        // $_SESSION['status_code'] = "success";
        header('Location: influencerstore_addnewpage.php');

    }
}

//: Delete Page.
if(isset($_POST['deleteinfluencermainpage_btn'])){

    $influencer_deletepageid = $_POST['deleteinfluencermainpage_id'];

    $delete_influencerpage = "DELETE FROM influencer_digital_page WHERE id='$influencer_deletepageid' ";
    $run_delete_influencerpage = mysqli_query($connection, $delete_influencerpage);

    if($run_delete_influencerpage){

        $_SESSION['success'] = "Your data is Deleted";
        // $_SESSION['status_code'] = "success";
        header('Location: influencerstore_addnewpage.php');

    }else {
        $_SESSION['success'] = "Your data is not Deleted";
        // $_SESSION['status_code'] = "success";
        header('Location: influencerstore_addnewpage.php');

    }

}

//:: Sent request to store admin
if(isset($_POST['storepromoterequestsent_btn'])){

    $requestsent_id = $_POST['storepromoterequestsent_id'];

    //:: Fetch details from store_promote_product
    $getproductdetails = "Select * from store_promote_product where spp_id = '$requestsent_id'";
    $runproductdetails = mysqli_query($connection, $getproductdetails);
    $fetchproductdetails = mysqli_fetch_array($runproductdetails);

    $requestsent_hotelid = $fetchproductdetails['ssp_hotelid'];
    $requestsent_itemid = $fetchproductdetails['ssp_itemid'];
    $requestsent_itemcommission = $fetchproductdetails['ssp_influencer_commission'];

    //: Influencer id
    $requestsent_influnecerid = $influencer_id;

    //: Status 
    $requestsent_status = 'Request Pending';

    $insert_promoteRequest = "insert into store_promote_request(spr_influencer_id, spr_hotel_id, spr_item_id, spr_commission, spr_date, spr_status) values 
    ('$requestsent_influnecerid', '$requestsent_hotelid', '$requestsent_itemid', '$requestsent_itemcommission', NOW(), '$requestsent_status')";

    $run_promoteRequest = mysqli_query($connection, $insert_promoteRequest);

    if($run_promoteRequest){

        $_SESSION['success'] = "Your Request is Sent";
        // $_SESSION['status_code'] = "success";
        header('Location: influencerstore_productforPromote.php');

    }else {
        $_SESSION['success'] = "Your Request is not Sent";
        // $_SESSION['status_code'] = "success";
        header('Location: influencerstore_productforPromote.php');
       

    }


   
}

//:: Cancel request
if(isset($_POST['cancelpromoterequestsent_btn'])){

    $cancelrequest_id = $_POST['cancelpromoterequestsent_id'];

    //:: Fetch details from store_promote_product
    $getproductdetails = "Select * from store_promote_product where spp_id = '$cancelrequest_id'";
    $runproductdetails = mysqli_query($connection, $getproductdetails);
    $fetchproductdetails = mysqli_fetch_array($runproductdetails);

    $cancelrequest_hotelid = $fetchproductdetails['ssp_hotelid'];
    $cancelrequest_itemid = $fetchproductdetails['ssp_itemid'];

    //: Influencer id
    $cancelrequest_influnecerid = $influencer_id;
  
    //: Status 
    // $requestsent_status = 'Cancel Request';

    $cancel_cancelRequest = "DELETE from store_promote_request where spr_influencer_id ='$cancelrequest_influnecerid' AND spr_hotel_id ='$cancelrequest_hotelid' AND spr_item_id = '$cancelrequest_itemid'";
    $run_cancelRequest = mysqli_query($connection, $cancel_cancelRequest);

    
    if($run_cancelRequest){

        $_SESSION['success'] = "Your Request is deleted";
        // $_SESSION['status_code'] = "success";
        header('Location: influencerstore_productforPromote.php');

    }else {
        // $_SESSION['success'] = "Your Request is not Sent";
        // // $_SESSION['status_code'] = "success";
        // header('Location: influencerstore_productforPromote.php');
        echo "Error insertin data: " . mysqli_error($connection);

    }


   
}

//: Modify Influencer Name
if(isset($_POST['modifyinfluencer_name'])){

    $influencer_updatefname = $_POST['influencer_updatefname'];
    $influencer_updatelname = $_POST['influencer_updatelname'];

    $influencer_updatename = "$influencer_updatefname $influencer_updatelname";

    $update_influencername = "UPDATE food_company SET Company_name='$influencer_updatename' WHERE company_email='$influencer_email' ";
    $run_update_influencername = mysqli_query($connection, $update_influencername);

    if($run_update_influencername){

        $update_influencernametwo = "UPDATE influencer SET i_name='$influencer_updatename' WHERE i_email='$influencer_email' ";
        $run_update_influencernametwo = mysqli_query($connection, $update_influencernametwo);

        if($run_update_influencernametwo){
            $_SESSION['success'] = "Your data is Updated";
            // $_SESSION['status_code'] = "success";
            header('Location: influencerstore_setting.php');

        }else{

            $_SESSION['success'] = "Your data is not Updated";
            // $_SESSION['status_code'] = "success";
            header('Location: influencerstore_setting.php');

        }
    }else {
        $_SESSION['success'] = "Your data is not Updated";
        // $_SESSION['status_code'] = "success";
        header('Location: influencerstore_setting.php');
    }
}

//: Modify Influencer Address
if(isset($_POST['modifyinfluencer_address'])){

    $influencer_updateaddress = $_POST['influencer_updateaddress'];


    $update_influenceraddress = "UPDATE food_company SET company_address='$influencer_updateaddress' WHERE company_email='$influencer_email' ";
    $run_update_influenceraddress = mysqli_query($connection, $update_influenceraddress);

    if($run_update_influenceraddress){

        $update_influenceraddresstwo = "UPDATE influencer SET i_address='$influencer_updateaddress' WHERE i_email='$influencer_email' ";
        $run_update_influenceraddresstwo = mysqli_query($connection, $update_influenceraddresstwo);

        if($run_update_influenceraddresstwo){
            $_SESSION['success'] = "Your data is Updated";
            // $_SESSION['status_code'] = "success";
            header('Location: influencerstore_setting.php');

        }else{

            $_SESSION['success'] = "Your data is not Updated";
            // $_SESSION['status_code'] = "success";
            header('Location: influencerstore_setting.php');

        }
    }else {
        $_SESSION['success'] = "Your data is not Updated";
        // $_SESSION['status_code'] = "success";
        header('Location: influencerstore_setting.php');
    }
}


if(isset($_POST['mofifyinfluencer_image'])){

    $updateitem_img = $_FILES['influencer_updateimage']['name'];
    
    //:: UPDATING THE IMAGE::
    $update_img = "SELECT * FROM influencer where i_email='$influencer_email'";
    $run_update_img = mysqli_query($connection, $update_img);

    foreach($run_update_img as $fa_row){
        if($updateitem_img == NULL){
            $image_data = $fa_row['i_image'];
        }else{
            //: upload with new image and delete with old image
            if($img_path = "influencer_images/".$fa_row['i_image']){
                unlink($img_path);
                $image_data = $updateitem_img;
            }
            
        }
    }

    $query = "UPDATE influencer SET i_image='$image_data' WHERE i_email='$influencer_email' ";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
        if($updateitem_img == NULL){
            $_SESSION['success'] = "Item is Updated with existing image";
            header('Location: influencerstore_setting.php'); 
            //  echo "Error insertin data: " . mysqli_error($connection);
        }else{
            //: upload with new image and delete with old image
            move_uploaded_file($_FILES["influencer_updateimage"]["tmp_name"], "influencer_images/".$_FILES["influencer_updateimage"]["name"] );
            $_SESSION['success'] = "Item is Updated";
            header('Location: influencerstore_setting.php');
             
        }
      
    }
    else
    {
        $_SESSION['status'] = "item is NOT Updated";
        // $_SESSION['status_code'] = "error";
        header('Location: influencerstore_setting.php'); 
        //  echo "Error insertin data: " . mysqli_error($connection);
    }

}

if(isset($_POST['modifyinfluencer_password'])){

    
    $current_pass = $_POST['current_pass'];
    $new_pass = $_POST['new_pass'];
    $confirm_pass = $_POST['confirm_pass'];

     //: fetching user password

    $query = "Select * from food_company where company_email = '$influencer_email'";
    $run_query = mysqli_query($connection, $query);
    $fetch_query = mysqli_fetch_array($run_query);

    $fetch_password = $fetch_query['company_pass'];

    if($current_pass != '' && $new_pass != '' && $confirm_pass != ''){

        if($current_pass == $fetch_password){

            if($new_pass == $confirm_pass){

                $query = "UPDATE food_company SET company_pass = '$new_pass' where company_email = '$influencer_email'";
                $run_query = mysqli_query($connection, $query);

                if($run_query){
                    $_SESSION['success'] = "Passowrd Changed Successfully";
                    header('Location: influencerstore_setting.php');

                }else {
                    $_SESSION['success'] = "Error, There must be something wrong";
                    header('Location: influencerstore_setting.php');
                }
            }else {
                $_SESSION['success'] = "Your new password and confirm password is not match";
                header('Location: influencerstore_setting.php');
            }
        }else {
            $_SESSION['success'] = "Your current password is wrong";
            header('Location: influencerstore_setting.php');
            
        }
    }else {
        $_SESSION['success'] = "Fields are empty";
            header('Location: influencerstore_setting.php');
    }



  

}







?>