<?php
include('foodstore_security.php');
include('includes/header.php'); 
include('includes/navbar_food.php'); 
?>




<div class="container">

    <div class="row justify-content-center">
        <div class="col-xl-10">
            <div class= "row">
                <div class="col-xl-9">
                    <div id="general" class="mb-5">

                        <h4 class="text-gray-900"> 
                            <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-900"></i>
                            General
                        </h4 class="text-gray-900">
                        <p class="text-gray-900 text-s">  View and update your general account information and settings. </p>
                        <div class = "card">
                                <div class="list-group list-group-flush">


                                    <!-- PHP CODE -->

                                    <?php

                                    // : Getting FoodStore ID
                                    require 'dbconfig.php';

                                    $influencer_email = $_SESSION['foodusername'];

                                                                
                                    $get_influencer_details = "Select * from food_company where company_email='$influencer_email'";
                                    $run_influencer_details = mysqli_query($connection, $get_influencer_details);
                                    $fetch_influencer_details = mysqli_fetch_array($run_influencer_details);
                                                                                                                            
                                    $influencer_id = $fetch_influencer_details['fcompany_id']; //:: Customer id 
                                    $influencer_name = $fetch_influencer_details['Company_name']; //:: customer name
                                    $influencer_address = $fetch_influencer_details['company_address'];
                                    $influencer_email = $fetch_influencer_details['company_email'];
                                    $influencer_type = $fetch_influencer_details['company_type'];
                                    
                                    
                                   


                                    ?>

                                    
                                                
                                                  

                                                    <?php
                                                        if(isset($_SESSION['success']) && $_SESSION['success']!=''){
                                                            ?>

                                                                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                                <?php 
                                                                echo ''.$_SESSION['success'].'';
                                                                ?>
                                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                                </div>

                                                            <?php
                                                            unset($_SESSION['success']);
                                                        } 
                                                        if(isset($_SESSION['status']) && $_SESSION['status']!=''){
                                                            ?>

                                                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                            <?php 
                                                            echo ''.$_SESSION['success'].'';
                                                            ?>
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                            </div>

                                                        <?php
                                                        unset($_SESSION['status']);
                                                        } 
                                                        
                                                    ?>

                                    


                                    
                                    <div class="list-group-item d-flex align-items-center">        
                                        <div class="media text-muted pt-3">
                                            <!-- <img src="influencer_images/<?php echo "$influencer_image" ?>" width="100px;" height="100px;" alt="image"></img> -->
                                            <div class="flex-fill">
                                                <h4 class="text-gray-900">&nbsp; &nbsp; <b><?php echo "$influencer_name" ?></b></h4>
                                                <div class="text-gray-600">
                                                    &nbsp; &nbsp;  &nbsp; <?php echo "$influencer_email" ?>
                                                </div>
                                              
                                                
                                            </div>   
                                        </div>
                                    </div>
                                    
                                    <!-- NAME -->
                                    <div class="list-group-item d-flex align-items-center">
                                        <div class="flex-fill">
                                            <div class="text-gray-800">Name</div>
                                            <div class="text-gray-600"><?php echo "$influencer_name" ?></div>
                                        </div>
                                        <div class="width-100">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editname">Edit</button>
                                        </div>
                                    </div> 

                                    <!-- EMAIL -->
                                    <!-- <div class="list-group-item d-flex align-items-center">
                                        <div class="flex-fill">
                                        <div class="text-gray-800">Email</div>
                                            <div class="text-gray-600">Mathankumar67@outlook.com</div>
                                        </div>
                                        <div class="width-100">
                                        <button type="button" class="btn width-100 btn-primary">Edit</button>
                                        </div>
                                    </div>  -->

                                    <!-- ADDRESS -->
                                    <div class="list-group-item d-flex align-items-center">
                                        <div class="flex-fill">
                                            <div class="text-gray-800">Address</div>
                                            <div class="text-gray-600"><?php echo "$influencer_address" ?></div>
                                        </div>
                                        <div class="width-100">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editaddress">Edit</button>
                                        </div>
                                    </div>
                                    
                                    <!-- PROFILE PICTURE -->
                                    <!-- <div class="list-group-item d-flex align-items-center">
                                        <div class="flex-fill">
                                            <div class="text-gray-800">Profile Picture</div>
                                        </div>
                                        <div class="width-100">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editprofilepicture">Edit</button>
                                        </div>
                                    </div>  -->

                                    <!-- PASSWORD -->
                                    <div class="list-group-item d-flex align-items-center">
                                        <div class="flex-fill">
                                            <div class="text-gray-800">Password</div>
                                        </div>
                                        <div class="width-100">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editpassword">Edit</button>
                                        </div>
                                    </div> 

                                </div>
                        </div>
                    </div>
                    <div id="notification" class="mb-5">
                        <h4 class="text-gray-900"> 
                            <i class="fas fa-bell fa-sm fa-fw mr-2 text-gray-900" aria-hidden="true"></i>
                            Notifications
                        </h4 class="text-gray-900">
                        <p class="text-gray-900 text-s">  Enable or  disable what notifications you want to see. </p>
                        <div class = "card">
                            <div class="list-group list-group-flush">
                                <div class="list-group-item d-flex align-items-center">
                                    <div class="flex-fill">
                                        <div class="text-gray-800">Product Reviews</div>
                                        <div class="text-gray-600">(Enabled)</div>
                                        <!-- <a href="" class="btn btn-success btn-icon-split">
                                        <span class="icon text-white-50">
                                            <i class="fas fa-check"></i>
                                        </span>
                                        <span class="text">Enabled</span>
                                        </a> -->
                                    </div>
                                    <div class="width-100">
                                    
                                    <label class="switch">
                                    <input type="checkbox" checked>
                                    <span class="slider round"></span>
                                    </label>
                                    <!-- <button type="button" class="btn width-100 btn-primary">Edit</button> -->
                                    </div>
                                </div> 
                                <div class="list-group-item d-flex align-items-center">
                                    <div class="flex-fill">
                                    <div class="text-gray-800">New Orders</div>
                                        <div class="text-gray-600">(Enabled)</div>
                                    </div>
                                    <div class="width-100">
                                    <label class="switch">
                                    <input type="checkbox" checked>
                                    <span class="slider round"></span>
                                    </label>
                                    <!-- <button type="button" class="btn width-100 btn-primary">Edit</button> -->
                                    </div>
                                </div> 
                                <div class="list-group-item d-flex align-items-center">
                                    <div class="flex-fill">
                                        <div class="text-gray-800">Sale Report</div>
                                        <div class="text-gray-600">
                                        <div class = "btn btn-danger btn-circle-xs">
                                            </div>
                                        (Disabled)</div>
                                    </div>
                                    <div class="width-100">
                                    <label class="switch">
                                    <input type="checkbox" checked>
                                    <span class="slider round"></span>
                                    </label>
                                    <!-- <button type="button" class="btn width-100 btn-primary">Edit</button> -->
                                    </div>
                                </div> 
                                <div class="list-group-item d-flex align-items-center">
                                    <div class="flex-fill">
                                        <div class="text-gray-800">Influencers Request</div>
                                        <div class="text-gray-600">
                                            <div class = "btn btn-success btn-circle-xs">
                                            </div>
                                            (Enabled)
                                        </div>
                                    </div>
                                    <div class="width-100">
                                    <label class="switch">
                                    <input type="checkbox" checked>
                                    <span class="slider round"></span>
                                    </label>
                                    <!-- <button type="button" class="btn width-100 btn-primary">Edit</button> -->
                                    </div>
                                </div> 
                                <!-- <div class="list-group-item d-flex align-items-center">
                              
                                    <img src="influencer_images/vishal.jpg" width="100px;" height="100px;" alt="image">

                                    </img>
                            
                               
                                    
                                </div>  -->
                                
                               

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3">
                    <div class="navbar navbar-sticky d-none d-xl-block">
                        <ul class="nav">
                            <li class="nav-item active">
                                <a href="#general" target="_self" class="nav-link active">General</a>
                            </li>
                            <li class="nav item active">
                                <a href="#notification" target="_self" class="nav-link active">Notifications</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- MODALS -->

<!-- NAME MODAL -->
<div class="modal fade" id="editname" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Name</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="foodstore_code.php" method="POST">

        <div class="modal-body">

            <div class="form-group">
                <label> Name: </label>
                <div class="row row-space-10">
                    <div class = "col-6">
                        <input type="text" name="store_updatefname" class="form-control"  required="required" placeholder="First Name">
                    </div>
                    <div class = "col-6">
                        <input type="text" name="store_updatelname" class="form-control"  required="required" placeholder="Last Name">
                    </div>
                </div>
                <br>
               
                <div class="alert alert-dark" role="alert">
                    <b>Please note: </b>
                    Don't add any unusual capatalization, punctuation,  characters or random words.
                </div>
            </div>
           
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="modifystore_name" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>

<!-- ADDRESS MODAL -->
<div class="modal fade" id="editaddress" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Address</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="foodstore_code.php" method="POST">

        <div class="modal-body">

            <div class="form-group">
                <label> Full Address: </label>
                <div class="row row-space-10">
                    <div class = "col-12">
                        <input type="text" name="store_updateaddress" class="form-control"  required="required" placeholder="First Name">
                    </div>
                </div>
                <br>
               
                <div class="alert alert-dark" role="alert">
                    <b>Example: </b>
                   E-10-02, One South, 43300 Seri Kembangan, Selangor, Malaysia.
                </div>
            </div>
           
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="modifystore_address" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>

<!-- PROFILE PICTURE -->
<div class="modal fade" id="editprofilepicture" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Profile Picture</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="influencerstore_code.php" method="POST" enctype="multipart/form-data">

        <div class="modal-body">

            <div class="form-group">
                <label> New Image: </label>
                <div class="row row-space-10">
                    <div class = "col-12">
                        <input type="file" name="influencer_updateimage" class="form-control"  required="required">
                    </div>
                </div>
                <br>
                <!-- <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="inputZip">Food Image</label>
                        <input type="file" class="form-control" name="food_updateimg">
                    </div>
                </div>
                -->
                <div class="alert alert-dark" role="alert">
                    <b>Your Current Picture: </b>
                    <br>
                    <img src="influencer_images/<?php echo "$influencer_image" ?>" width="100px;" height="100px;" alt="image"></img>
                </div>
            </div>
           
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="mofifyinfluencer_image" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>

<!-- EDIT PASSWORD -->
<div class="modal fade" id="editpassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="foodstore_code.php" method="POST" enctype="multipart/form-data">

        <div class="modal-body">

            <div class="form-group">
                <label> Current Password: </label>
                <div class="row row-space-10">
                    <div class = "col-12">
                        <input type="text" name="current_pass" class="form-control"  required="required" placeholder="Last Name">
                    </div>
                </div>
                <br>
                <label> New Password: </label>
                <div class="row row-space-10">
                    <div class = "col-6">
                        <input type="text" name="new_pass" class="form-control"  required="required" placeholder="New Password">
                    </div>
                    <div class = "col-6">
                        <input type="text" name="confirm_pass" class="form-control"  required="required" placeholder="Confirm Password">
                    </div>
                </div>
                <br>
            </div>
           
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="modifystore_password" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>




<?php
include('includes/scripts.php');
include('includes/footer.php');
?>